//`指定分层用于接受入款的账号列表`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

export let options = opts;

const amount = Math.floor(Math.random() * 50) + 1;
const RETRY_COUNT = 3;

export function setup() {
  let data = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
  }
  return data;
}

export default function (data) {
  let error_code = -1;
  let user = (`${__VU}` - 1) % __ENV.USERS + 1;
  let loginId = vars["{{user_prefix}}"] + user;
  let ret = null;
  let retry_count = 0;

  if (DEBUG) console.log('loginId = ' + loginId);

  //測試正常的流程
  console.log("測試正常的流程");
  do {
    ret = api.layer_deposit_to_account_qrcode_get({
      "sessionId": data[loginId].sessionId,
      "toAccountId": 17006
    });
    if(ret.error === null)
      break;
    retry_count++;
    if (DEBUG) console.warn('retry_count = ' + retry_count);
    sleep(60);
  }while (retry_count < RETRY_COUNT);
  check(ret, {
    "指定分层用于接受入款的账号列表": (ret) => ret.error === null
  }) || errorCount.add(1);
  check(ret.result.length, {
    "檢查QR code的長度": (ret.result.length > 0)
  }) || errorCount.add(1);
  if(ret.result.length > 0) {
    check(ret, {
      "檢查QR code": (ret) => ret.result[0].qrcodeBase64Data != null
    }) || errorCount.add(1);
  }
  //測試錯誤的会话ID
  console.log("測試錯誤的会话ID");
  ret = api.layer_deposit_to_account_qrcode_get({
    "sessionId": '123456789',
    "toAccountId": 17006
  });
  check(ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (ret.error)
  }) || errorCount.add(1);
  if(ret.error) {
    error_code = ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
  //測試錯誤的帐号ID
  console.log("測試錯誤的帐号ID");
  ret = api.layer_deposit_to_account_qrcode_get({
    "sessionId": data[loginId].sessionId,
    "toAccountId": -1
  });
  check(ret, {
    "測試錯誤的帐号ID": (ret) => ret.result.length === 0
  }) || errorCount.add(1);
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}