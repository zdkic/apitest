//`获取厅主支付类型与出入款开关信息`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

export let options = opts;

const params = JSON.parse(open("./params/sn_settings_get.json"))

export default function (data) {
  let args = {}

  Object.keys(params).forEach(idx => {
    for (const [k, v] of Object.entries(params[idx])) {
      args[k] = v
    }
    let ret = api.sn_settings_get(args)
    if (ret.error) {
      console.error('[sn_settings_get]' + 'error code = ' + ret.error.code);
    }
    check(ret, {
      "檢查响应参数": (ret.hasOwnProperty('result'))
    }) || errorCount.add(1);
  })
}