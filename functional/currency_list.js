//`币种`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

export let options = opts;

const amount = Math.floor(Math.random() * 50) + 1;

export function setup() {
  let data = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
  }
  return data;
}

export default function (data) {
  let user = (`${__VU}` - 1) % __ENV.USERS + 1;
  let loginId = vars["{{user_prefix}}"] + user;
  if (DEBUG) console.log('loginId = ' + loginId);
  //測試正常的流程
  console.log("測試正常的流程");
  let currency_list_ret = api.currency_list();
  check(currency_list_ret, {
    "查询币种列表": (currency_list_ret) => currency_list_ret.error === null
  }) || errorCount.add(1);
  for (let idx = 0; idx < currency_list_ret.result.length; idx++) {
    let currency = currency_list_ret.result[idx];
    check(currency, {
      "檢查厅主ID": (currency) => currency.hasOwnProperty('sn')
    }) || errorCount.add(1);
    check(currency, {
      "檢查币种ID": (currency) => currency.hasOwnProperty('currencyId')
    }) || errorCount.add(1);
    check(currency, {
      "檢查币种代码": (currency) => currency.hasOwnProperty('currencyCode')
    }) || errorCount.add(1);
    check(currency, {
      "符号": (currency) => currency.hasOwnProperty('currencySymbol')
    }) || errorCount.add(1);
    check(currency, {
      "币种名称": (currency) => currency.hasOwnProperty('currencyName')
    }) || errorCount.add(1);
    check(currency, {
      "币种与RMB的汇率": (currency) => currency.hasOwnProperty('rate')
    }) || errorCount.add(1);
    check(currency, {
      "币种记录状态": (currency) => currency.hasOwnProperty('status')
    }) || errorCount.add(1);
    console.log('currency.sn = ' + currency.sn);
  }
  //測試获取单个币种
  console.log("測試获取单个币种");
  let currency_get_ret = api.currency_get({
    "currencyId": 1
  });
  check(currency_get_ret, {
    "查询获取单个币种": (currency_get_ret) => currency_get_ret.error === null
  }) || errorCount.add(1);
  check(currency_get_ret, {
    "檢察币种ID": (currency_get_ret) => currency_get_ret.result.hasOwnProperty('currencyId')
  }) || errorCount.add(1);
  check(currency_get_ret, {
    "檢察币种代码": (currency_get_ret) => currency_get_ret.result.hasOwnProperty('currencyCode')
  }) || errorCount.add(1);
  check(currency_get_ret, {
    "檢察币种符号": (currency_get_ret) => currency_get_ret.result.hasOwnProperty('currencySymbol')
  }) || errorCount.add(1);
  check(currency_get_ret, {
    "檢察币种名称": (currency_get_ret) => currency_get_ret.result.hasOwnProperty('currencyName')
  }) || errorCount.add(1);
  check(currency_get_ret, {
    "檢察币种与RMB的汇率": (currency_get_ret) => currency_get_ret.result.hasOwnProperty('rate')
  }) || errorCount.add(1);
  check(currency_get_ret, {
    "檢察币种记录状态": (currency_get_ret) => currency_get_ret.result.hasOwnProperty('status')
  }) || errorCount.add(1);
  //測試錯誤的币种ID
  console.log("測試錯誤的币种ID");
  currency_get_ret = api.currency_get({
    "currencyId": -1
  });
  check(currency_get_ret, {
    "檢查币种ID是否為空": (currency_get_ret) => !currency_get_ret.result.hasOwnProperty('currencyId')
  }) || errorCount.add(1);
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}