//`二维码`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

import http from 'k6/http';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

export let options = opts;

function check_image(url) {
  let res = http.get(url);
  for (let p in res.headers) {
    if (res.headers.hasOwnProperty(p)) {
      console.log(p + " : " + res.headers[p]);
    }
  }
  console.log("res.body => ");
  if(res.body.length > 0) {
    for(let idx=0;idx<4&&idx<res.body.length;idx++) {
      console.log(res.body[idx]);
    }
  }
  check(res, {
    "status is 200": (res) => res.status === 200,
    "Content-Disposition": (res) => res.headers.hasOwnProperty('Content-Disposition') && res.headers['Content-Disposition'].indexOf("attachment;filename=") !== -1,
    "Content-Type": (res) => res.headers.hasOwnProperty('Content-Type') && res.headers['Content-Type'].indexOf("application/octet-stream") !== -1,
    "Response body length > 0": (res) => res.body.length > 0
  }) || errorCount.add(1);

}

export default function (data) {
  console.log("測試qrcode.mobilesite");
  check_image(vars["{{host}}"] + "/cloud/api.do?pa=qrcode.mobilesite&sn=" + vars["{{sn}}"]);

  console.log("測試qrcode.mobileorder");
  check_image(vars["{{host}}"] + "/cloud/api.do?pa=qrcode.mobileorder&sn=" + vars["{{sn}}"] + "&token=sdfasdfawefmon");

  console.log("測試qrcode.generator");
  check_image(vars["{{host}}"] + "/cloud/api.do?pa=qrcode.generator&data={'hello'}");
}