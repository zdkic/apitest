#### 功能測試 ####
- 安裝node.js套件

```bash
  npm install -g typescript
  npm install -g ts-node
  npm install -g @types/node
  npm install -g schema-payload-generator
```

- 根據json schema產生測試參數

```bash
  api_name=user_charge_option_get
  API=${api_name} ts-node -O '{"lib":["es2015","dom"]}' api.ts > params/${api_name}.json
  k6 run -e DEBUG=1 -e FUNCTIONAL_TEST=1 -e COLOR=1 ${api_name}.js
```

- 執行單一測試
   - k6 run -e DEBUG=1 -e FUNCTIONAL_TEST=1 <script>
     - ex: k6 run -e DEBUG=1 -e FUNCTIONAL_TEST=1 -e COLOR=1 user_audit_about_current_apply.js
- 產生測試報告
   - cd tool/;bash ./gen_functional_report.sh