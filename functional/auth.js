//`鉴权`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

import http from 'k6/http';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

export let options = opts;

export default function (data) {
  let ret = null;
  let sessionId = null;
  // 測試auth.mobile.login
  console.log("[auth.mobile.login]測試正常流程");
  let loginId = vars["{{user_prefix}}"] + 1;
  ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": vars["{{password}}"]
  });
  check(ret, {
    "[auth.mobile.login]測試沒有error": (ret) => ret.error === null
  }) || errorCount.add(1);
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    return;
  }
  check(ret, {
    "[auth.mobile.login]測試有loginId": (ret) => ret.result.loginId === loginId
  }) || errorCount.add(1);
  check(ret, {
    "[auth.mobile.login]測試有sessionId": (ret) => ret.result.sessionId.length > 0
  }) || errorCount.add(1);
  if (ret.result.sessionId.length <= 0) {
    console.error('[auth_mobile_login]沒有sessionId');
    return;
  }
  sessionId = ret.result.sessionId;
  console.log("[auth.mobile.login]測試錯誤帳密");
  ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": "12345678"
  });
  check(ret, {
    "[[auth.mobile.login]]測試錯誤密碼應該有error": (ret) => ret.error != null
  }) || errorCount.add(1);
  // 測試auth.session.validate
  console.log("[auth.session.validate]測試正常流程");
  ret = api.auth_session_validate({
    "sessionId": sessionId
  });
  check(ret, {
    "[auth.session.validate]測試沒有error": (ret) => ret.error === null
  }) || errorCount.add(1);
  if (ret.error) {
    console.error('[auth_session_validate]' + 'error code = ' + ret.error.code);
    return;
  }
  check(ret, {
    "[auth.session.validate]測試flag為1": (ret) => ret.result.flag === "1"
  }) || errorCount.add(1);
  check(ret, {
    "[auth.session.validate]測試userId": (ret) => ret.result.userId > 0
  }) || errorCount.add(1);
  console.log("[auth.session.validate]測試錯誤sessionId");
  ret = api.auth_session_validate({
    "sessionId": "12345678"
  });
  check(ret, {
    "[auth.session.validate]測試錯誤sessionId應該有error": (ret) => ret.error != null
  }) || errorCount.add(1);
  // 測試auth.online.logout
  console.log("[auth.online.logout]測試正常流程");
  ret = api.auth_online_logout({
    "sessionId": sessionId
  });
  check(ret, {
    "[auth.online.logout]測試沒有error": (ret) => ret.error === null
  }) || errorCount.add(1);
  if (ret.error) {
    console.error('[auth_online_logout]' + 'error code = ' + ret.error.code);
    return;
  }
  check(ret, {
    "[auth.online.logout]測試result": (ret) => ret.result === "1"
  }) || errorCount.add(1);
  console.log("[auth.online.logout]測試錯誤sessionId");
  ret = api.auth_online_logout({
    "sessionId": "12345678"
  });
  check(ret, {
    "[auth.online.logout]測試錯誤sessionId應該有error": (ret) => ret.error != null
  }) || errorCount.add(1);
}