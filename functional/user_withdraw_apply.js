//`我要提款`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

const idx = 5;

export let options = opts;

const amount = 400 + Math.floor(Math.random() * 50) + 1;
const RETRY_COUNT = 3;

export function setup() {
  let data = {};
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
  }
  data[loginId] = ret.result;
  return data;
}

export default function (data) {
  let error_code = -1;
  let loginId = vars["{{user_prefix}}"] + idx;
  let withdraw_apply_ret = null;
  let retry_count = 0;

  if (DEBUG) console.log('loginId = ' + loginId);

  //測試正常的流程
  console.log("測試正常的流程");
  let balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let original_balance = balance_ret.result.balance;
  withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  audit_about_current_ret = api.user_audit_about_current_get({
    "sessionId": data[loginId].sessionId
  });
  do {
    withdraw_apply_ret = api.user_withdraw_apply({
      "sessionId": data[loginId].sessionId,
      "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
      "withdrawNo": withdraw_no_ret.result,
      "amount": amount,
      "statsCacheKey": audit_about_current_ret.result.statsCacheKey
    });
    if(withdraw_apply_ret.error === null)
      break;
    retry_count++;
    if (DEBUG) console.warn('retry_count = ' + retry_count);
    sleep(60);
  }while (retry_count < RETRY_COUNT);
  check(withdraw_apply_ret, {
    "用户取款申请": (withdraw_apply_ret) => withdraw_apply_ret.error === null
  }) || errorCount.add(1);
  //測試提款後的帳戶餘額
  console.log("測試提款後的帳戶餘額");
  balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let diff = original_balance - balance_ret.result.balance;
  if (DEBUG) console.log('amount = ' + amount);
  if (DEBUG) console.log('diff = ' + diff);
  check(diff, {
    "測試提款後的帳戶餘額": (diff) => diff === amount
  }) || errorCount.add(1);
  sleep(60*2);
  let withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  let audit_about_current_ret = api.user_audit_about_current_get({
    "sessionId": data[loginId].sessionId
  });
  check(audit_about_current_ret, {
    "即时稽核查询": (audit_about_current_ret) => audit_about_current_ret.error === null
  }) || errorCount.add(1);
  //測試過低的申请出款金额
  console.log("測試過低的申请出款金额");
  withdraw_apply_ret = api.user_withdraw_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result,
    "amount": 1,
    "statsCacheKey": audit_about_current_ret.result.statsCacheKey
  });
  check(withdraw_apply_ret.error, {
    "測試過低的申请出款金额:回傳值有error物件": (withdraw_apply_ret.error)
  }) || errorCount.add(1);
  if(withdraw_apply_ret.error) {
    error_code = withdraw_apply_ret.error.code;
    check(error_code, {
      "測試過低的申请出款金额": (error_code) => error_code === '2420' || error_code === '2438' || error_code === '2442'
    }) || errorCount.add(1);
  }
  //測試過高的申请出款金额
  console.log("測試過高的申请出款金额");
  withdraw_apply_ret = api.user_withdraw_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result,
    "amount": 30000000,
    "statsCacheKey": audit_about_current_ret.result.statsCacheKey
  });
  check(withdraw_apply_ret.error, {
    "測試過高的申请出款金额:回傳值有error物件": (withdraw_apply_ret.error)
  }) || errorCount.add(1);
  if(withdraw_apply_ret.error){
    error_code = withdraw_apply_ret.error.code;
    check(error_code, {
      "測試過高的申请出款金额": (error_code) => error_code === '2420'
    }) || errorCount.add(1);
  }
  //測試錯誤的会话ID
  console.log("測試錯誤的会话ID");
  withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  withdraw_apply_ret = api.user_withdraw_apply({
    "sessionId": '123456789',
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result,
    "amount": amount,
    "statsCacheKey": audit_about_current_ret.result.statsCacheKey
  });
  check(withdraw_apply_ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (withdraw_apply_ret.error)
  }) || errorCount.add(1);
  if(withdraw_apply_ret.error) {
    error_code = withdraw_apply_ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
  sleep(5);
  //測試錯誤的取款密码
  console.log("測試錯誤的取款密码");
  withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  withdraw_apply_ret = api.user_withdraw_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": '123456789',
    "withdrawNo": withdraw_no_ret.result,
    "amount": amount,
    "statsCacheKey": audit_about_current_ret.result.statsCacheKey
  });
  check(withdraw_apply_ret.error, {
    "測試錯誤的取款密码:回傳值有error物件": (withdraw_apply_ret.error)
  }) || errorCount.add(1);
  if(withdraw_apply_ret.error) {
    error_code = withdraw_apply_ret.error.code;
    check(error_code, {
      "測試錯誤的取款密码": (error_code) => error_code === '2273'
    }) || errorCount.add(1);
  }
}

export function teardown(data) {
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  api.auth_online_logout({
    "sessionId": data[loginId].sessionId
  });
}