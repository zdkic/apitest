//`获取厅主支付类型与出入款开关信息`
import {
    check,
    sleep
  } from 'k6';
  
  import {
    DEBUG,
    vars,
    errorCount,
    get_paypwd_hash
  } from '../lib/utils.js';
  import * as api from '../lib/api.js';
  
  __ENV.USERS = 1;
  
  let opts = {
    setupTimeout: "60s",
    teardownTimeout: "60s",
    thresholds: {
      "errors": [ "count<1",
                  { threshold: "count<=100", abortOnFail: true }
                ]
    }
  };
  
  if (__ENV.CONFIG == 'once') {
    opts.vus = __ENV.USERS;
    opts.iterations = __ENV.USERS;
  }
  
  const idx = 5;

  export let options = opts;
  
  const params = JSON.parse(open("./params/user_charge_option_get.json"))

  export function setup() {
    let data = {};
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
    return data;
  }
  
  export default function (data) {
    let args = {}
    let loginId = vars["{{user_prefix}}"] + idx
    args["sessionId"] = data[loginId].sessionId
    Object.keys(params).forEach(i => {
    for (const [k, v] of Object.entries(params[i])) {
        args[k] = v
    }
    let ret = api.user_charge_option_get(args)
    if (ret.error) {
        console.error('[user_charge_option_get]' + 'error code = ' + ret.error.code);
    }
    check(ret, {
        "檢查响应参数": (ret.hasOwnProperty('result'))
    }) || errorCount.add(1)
    check(ret, {
        "檢查取款金额": (ret.result.hasOwnProperty('amount'))
    }) || errorCount.add(1)
    check(ret, {
        "檢查存款优惠金额": (ret.result.hasOwnProperty('chargeCoupon'))
    }) || errorCount.add(1)
    check(ret, {
        "檢查综合稽核打码量(优惠稽核打码量)": (ret.result.hasOwnProperty('couponAuditBetAmount'))
    }) || errorCount.add(1)
    check(ret, {
        "檢查是否进行常态性稽核": (ret.result.hasOwnProperty('isAudit'))
    }) || errorCount.add(1)
    })
  }

  export function teardown(data) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }