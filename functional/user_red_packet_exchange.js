//`BG红包充值兑换`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

const idx = 5;

export let options = opts;

const amount = Math.floor(Math.random() * 50) + 1;

export function setup() {
  let data = {};
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
  }
  data[loginId] = ret.result;
  return data;
}

export default function (data) {
  let error_code = -1;
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  //測試正常的流程
  console.log("測試正常的流程");
  let balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let original_balance = balance_ret.result.balance;
  let ret = api.user_red_packet_exchange({
    "sessionId": data[loginId].sessionId,
    "amount": amount
  });
  check(ret, {
    "BG红包充值兑换(error==null)": (ret) => ret.error === null
  }) || errorCount.add(1);
  check(ret, {
    "BG红包充值兑换(success==1)": (ret) => ret.result.success === "1"
  }) || errorCount.add(1);
  //測試提款後的帳戶餘額
  console.log("測試提款後的帳戶餘額");
  balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let diff = balance_ret.result.balance - original_balance;
  if (DEBUG) console.log('amount = ' + amount);
  if (DEBUG) console.log('diff = ' + diff);
  check(diff, {
    "測試提款後的帳戶餘額": (diff) => diff === amount
  }) || errorCount.add(1);
  //測試錯誤的会话ID
  console.log("測試錯誤的会话ID");
  ret = api.user_red_packet_exchange({
    "sessionId": '123456789',
    "amount": amount
  });
  check(ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (ret.error)
  }) || errorCount.add(1);
  if(ret.error) {
    error_code = ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
  //測試錯誤的资金存款申请金额
  console.log("測試錯誤的资金存款申请金额");
  ret = api.user_red_packet_exchange({
    "sessionId": data[loginId].sessionId,
    "amount": -100
  });
  check(ret.error, {
    "測試錯誤的资金存款申请金额:回傳值有error物件": (ret.error)
  }) || errorCount.add(1);
  if(ret.error) {
    error_code = ret.error.code;
    check(error_code, {
      "測試錯誤的资金存款申请金额": (error_code) => error_code === '2417'
    }) || errorCount.add(1);
  }
}

export function teardown(data) {
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  api.auth_online_logout({
    "sessionId": data[loginId].sessionId
  });
}