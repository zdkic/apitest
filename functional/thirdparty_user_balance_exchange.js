//`额度转换`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  data_generator,
  errorCount
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

const idx = 5;

export let options = opts;

const amount = data_generator["amount"]();

export function setup() {
  let data = {};
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
  }
  data[loginId] = ret.result;
  return data;
}

export default function (data) {
  let error_code = -1;
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let original_balance = balance_ret.result.balance;
  //測試正常的流程
  console.log("測試正常的流程");
  let balance_exchange_ret = api.thirdparty_user_balance_exchange({
    "sessionId": data[loginId].sessionId,
    "from": 0,
    "to": 4,
    "amount": amount,
    "terminal":1
  });
  check(balance_exchange_ret, {
    "測試正常流程": (balance_exchange_ret) => balance_exchange_ret.error === null
  }) || errorCount.add(1);
  check(balance_exchange_ret, {
    "檢查响应参数結果": (balance_exchange_ret) => balance_exchange_ret.result === "1"
  }) || errorCount.add(1);
  //測試額度轉換後的帳戶餘額
  console.log("測試額度轉換後的帳戶餘額");
  balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let diff = original_balance - balance_ret.result.balance;
  if (DEBUG) console.log('amount = ' + amount);
  if (DEBUG) console.log('diff = ' + diff);
  check(diff, {
    "測試額度轉換後的帳戶餘額": (diff) => diff === amount
  }) || errorCount.add(1);
  //測試刷新额度
  console.log("測試刷新额度接口");
  let refresh_ret = api.thirdparty_user_balance_refresh({
    "sessionId": data[loginId].sessionId,
    "from": 0,
    "to": 4,
  });
  check(refresh_ret, {
    "檢查刷新额度接口响应": (refresh_ret) => refresh_ret.result.flag === "1"
  }) || errorCount.add(1);
  //測試錯誤的会话ID
  console.log("測試錯誤的会话ID");
  balance_exchange_ret = api.thirdparty_user_balance_exchange({
    "sessionId": '123456789',
    "from": 0,
    "to": 5,
    "amount": amount
  });
  check(balance_exchange_ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (balance_exchange_ret.error)
  }) || errorCount.add(1);
  if(balance_exchange_ret.error) {
    error_code = balance_exchange_ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
  //測試超過的額度轉換
  console.log("測試超過的額度轉換");
  balance_exchange_ret = api.thirdparty_user_balance_exchange({
    "sessionId": data[loginId].sessionId,
    "from": 0,
    "to": 5,
    "amount": 240000
  });
  check(balance_exchange_ret.error, {
    "測試超過的額度轉換:回傳值有error物件": (balance_exchange_ret.error)
  }) || errorCount.add(1);
  if(balance_exchange_ret.error) {
    error_code = balance_exchange_ret.error.code;
    check(error_code, {
      "測試超過的額度轉換": (error_code) => error_code === '2821'
    }) || errorCount.add(1);
  }
  //測試錯誤的来源平台ID
  console.log("測試錯誤的来源平台ID");
  balance_exchange_ret = api.thirdparty_user_balance_exchange({
    "sessionId": data[loginId].sessionId,
    "from": -1,
    "to": 5,
    "amount": amount
  });
  check(balance_exchange_ret.error, {
    "測試錯誤的来源平台ID:回傳值有error物件": (balance_exchange_ret.error)
  }) || errorCount.add(1);
  if(balance_exchange_ret.error) {
    error_code = balance_exchange_ret.error.code;
    check(error_code, {
      "測試錯誤的来源平台ID": (error_code) => error_code === '2435'
    }) || errorCount.add(1);
  }
  //測試錯誤的目标平台ID
  console.log("測試錯誤的目标平台ID");
  balance_exchange_ret = api.thirdparty_user_balance_exchange({
    "sessionId": data[loginId].sessionId,
    "from": 0,
    "to": -5,
    "amount": amount
  });
  check(balance_exchange_ret.error, {
    "測試錯誤的目标平台ID:回傳值有error物件": (balance_exchange_ret.error)
  }) || errorCount.add(1);
  if(balance_exchange_ret.error) {
    error_code = balance_exchange_ret.error.code;
    check(error_code, {
      "測試錯誤的目标平台ID": (error_code) => error_code === '2436'
    }) || errorCount.add(1);
  }
}

export function teardown(data) {
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  api.auth_online_logout({
    "sessionId": data[loginId].sessionId
  });
}