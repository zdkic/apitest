//`稽核预申请`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

const idx = 5;

export let options = opts;

const amount = Math.floor(Math.random() * 50) + 1;

export function setup() {
  let data = {};
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
  }
  data[loginId] = ret.result;
  return data;
}

export default function (data) {
  let error_code = -1;
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  //測試正常的流程
  console.log("測試正常的流程");
  let withdraw_option_get_ret = api.user_withdraw_option_get({
    "sessionId": data[loginId].sessionId
  });
  check(withdraw_option_get_ret, {
    "用户出款金额可选项(有效范围)查询": (withdraw_option_get_ret) => withdraw_option_get_ret.error === null
  }) || errorCount.add(1);
  let withdraw_account_default_get_ret = api.user_withdraw_account_default_get({
    "sessionId": data[loginId].sessionId
  });
  check(withdraw_account_default_get_ret, {
    "查询用户默认出款银行记录": (withdraw_account_default_get_ret) => withdraw_account_default_get_ret.error === null
  }) || errorCount.add(1);
  let audit_about_current_cancel_ret = api.user_audit_about_current_cancel({
    "sessionId": data[loginId].sessionId
  });
  check(audit_about_current_cancel_ret, {
    "稽核申请取消": (audit_about_current_cancel_ret) => audit_about_current_cancel_ret.error === null
  }) || errorCount.add(1);
  let withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  check(withdraw_no_ret, {
    "获取充值流水号": (withdraw_no_ret) => withdraw_no_ret.error === null
  }) || errorCount.add(1);
  let audit_about_current_apply_ret = api.user_audit_about_current_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result
  });
  check(audit_about_current_apply_ret, {
    "稽核预申请": (audit_about_current_apply_ret) => audit_about_current_apply_ret.error === null
  }) || errorCount.add(1);
  let audit_about_current_trace_ret = api.user_audit_about_current_trace({
    "sessionId": data[loginId].sessionId
  });
  check(audit_about_current_trace_ret, {
    "即时稽核状态查询": (audit_about_current_trace_ret) => audit_about_current_trace_ret.error === null
  }) || errorCount.add(1);
  //測試錯誤的会话ID
  console.log("測試錯誤的会话ID");
  withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  audit_about_current_apply_ret = api.user_audit_about_current_apply({
    "sessionId": '123456789',
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result
  });
  check(audit_about_current_apply_ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (audit_about_current_apply_ret.error)
  }) || errorCount.add(1);
  if(audit_about_current_apply_ret.error) {
    error_code = audit_about_current_apply_ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
  //測試錯誤的取款密码
  console.log("測試錯誤的取款密码");
  withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  audit_about_current_apply_ret = api.user_audit_about_current_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": '123456789',
    "withdrawNo": withdraw_no_ret.result
  });
  check(audit_about_current_apply_ret.error, {
    "測試錯誤的取款密码:回傳值有error物件": (audit_about_current_apply_ret.error)
  }) || errorCount.add(1);
  if(audit_about_current_apply_ret.error) {
    error_code = audit_about_current_apply_ret.error.code;
    check(error_code, {
      "測試錯誤的取款密码": (error_code) => error_code === '2273'
    }) || errorCount.add(1);
  }
  //測試錯誤的充值流水号
  console.log("測試錯誤的充值流水号");
  withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  audit_about_current_apply_ret = api.user_audit_about_current_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": "123456789",
  });
  check(audit_about_current_apply_ret.error, {
    "測試錯誤的充值流水号:回傳值有error物件": (audit_about_current_apply_ret.error)
  }) || errorCount.add(1);
  //測試超過的取款金额
  console.log("測試超過的取款金额");
  withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  audit_about_current_apply_ret = api.user_audit_about_current_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result,
    "amount": 240000
  });
  check(audit_about_current_apply_ret.error, {
    "測試超過的取款金额:回傳值有error物件": (audit_about_current_apply_ret.error)
  }) || errorCount.add(1);
  if(audit_about_current_apply_ret.error) {
    error_code = audit_about_current_apply_ret.error.code;
    check(error_code, {
      "稽核预申请(測試超過的取款金额)": (error_code) => error_code === '2434' || error_code === '2902'
    }) || errorCount.add(1);
  }
}

export function teardown(data) {
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  api.auth_online_logout({
    "sessionId": data[loginId].sessionId
  });
}