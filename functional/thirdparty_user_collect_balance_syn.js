//`一键回收第三方平台余额异步`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

const idx = 5;

export let options = opts;

export function setup() {
  let data = {};
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
  }
  data[loginId] = ret.result;
  return data;
}

export default function (data) {
  let error_code = -1;
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let collect_balance_syn_ret = null;
  let collect_balance_info_ret = null;

  //測試正常的流程
  console.log("測試正常流程");
  collect_balance_syn_ret = api.thirdparty_user_collect_balance_syn({
  "sessionId": data[loginId].sessionId
  });
  check(collect_balance_syn_ret, {
    "測試正常流程": (collect_balance_syn_ret) => collect_balance_syn_ret.error === null
  }) || errorCount.add(1);
  let count = 0;
  top:
  for (;;) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    let processing = 0;
    collect_balance_info_ret = api.thirdparty_user_collect_balance_info({
      "sessionId": data[loginId].sessionId
    });
    if (!collect_balance_info_ret.error) {
      for (const [k, v] of Object.entries(collect_balance_info_ret.result)) {
        if (DEBUG) console.log('gameId = ' + k + ' , result = ' + v + ' (1 成功 0 执行中 -1 失败)');
        if (v == 0)
          processing++;
      }
    } else {
      errorCount.add(1);
    }
    if (0 == processing)
      count++;
    if (count >= __ENV.USERS)
      break top;
    sleep(5);
  }
  check(count, {
    "測試一鍵回收": (count) => count > 0
  }) || errorCount.add(1);
  //測試錯誤的会话ID(thirdparty_user_collect_balance_syn)
  console.log("測試錯誤的会话ID(thirdparty_user_collect_balance_syn)");
  collect_balance_syn_ret = api.thirdparty_user_collect_balance_syn({
    "sessionId": "123456789"
  });
  check(collect_balance_syn_ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (collect_balance_syn_ret.error)
  }) || errorCount.add(1);
  if(collect_balance_syn_ret.error) {
    error_code = collect_balance_syn_ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
  //測試錯誤的会话ID(thirdparty_user_collect_balance_info)
  console.log("測試錯誤的会话ID(thirdparty_user_collect_balance_info)");
  collect_balance_info_ret = api.thirdparty_user_collect_balance_info({
    "sessionId": "123456789"
  });
  check(collect_balance_info_ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (collect_balance_info_ret.error)
  }) || errorCount.add(1);
  if(collect_balance_info_ret.error) {
    error_code = collect_balance_info_ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
}

export function teardown(data) {
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  api.auth_online_logout({
    "sessionId": data[loginId].sessionId
  });
}