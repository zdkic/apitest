//`我要充值`
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount
} from '../lib/utils.js';
import * as api from '../lib/api.js';

__ENV.USERS = 1;

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

const idx = 5;

export let options = opts;

export function setup() {
  let data = {};
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  let ret = api.auth_mobile_login({
    "loginId": loginId,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
  }
  data[loginId] = ret.result;
  return data;
}

export default function (data) {
  let error_code = -1;
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  //測試正常的流程
  console.log("測試正常的流程");
  let charge_bank_list_ret = api.user_charge_bank_list({
    "sessionId": data[loginId].sessionId
  });
  check(charge_bank_list_ret, {
    "用户充值时支持的银行列表": (charge_bank_list_ret) => charge_bank_list_ret.error === null
  }) || errorCount.add(1);
  let charge_no_ret = api.user_charge_no_get({
    "sessionId": data[loginId].sessionId
  });
  check(charge_no_ret, {
    "获取充值流水号": (charge_no_ret) => charge_no_ret.error === null
  }) || errorCount.add(1);
  let charge_transfer_apply_ret = api.user_charge_transfer_apply({
    "sessionId": data[loginId].sessionId,
    "chargeNo": charge_no_ret.result,
    "fromBankId": charge_bank_list_ret.result[0].bankId
  });
  check(charge_transfer_apply_ret, {
    "用户账户转账充值申请": (charge_transfer_apply_ret) => charge_transfer_apply_ret.error === null
  }) || errorCount.add(1);
  //測試存款申请单流水号已使用
  console.log("測試存款申请单流水号已使用");
  charge_transfer_apply_ret = api.user_charge_transfer_apply({
    "sessionId": data[loginId].sessionId,
    "chargeNo": charge_no_ret.result,
    "fromBankId": charge_bank_list_ret.result[0].bankId
  });
  check(charge_transfer_apply_ret.error, {
    "測試存款申请单流水号已使用:回傳值有error物件": (charge_transfer_apply_ret.error)
  }) || errorCount.add(1);
  if(charge_transfer_apply_ret.error) {
    error_code = charge_transfer_apply_ret.error.code;
    check(error_code, {
      "測試存款申请单流水号已使用": (error_code) => error_code === '2434' || error_code === '2902'
    }) || errorCount.add(1);
  }
  //測試錯誤的支付方式来源
  console.log("測試錯誤的支付方式来源");
  charge_no_ret = api.user_charge_no_get({
    "sessionId": data[loginId].sessionId
  });
  charge_transfer_apply_ret = api.user_charge_transfer_apply({
    "sessionId": data[loginId].sessionId,
    "chargeNo": charge_no_ret.result,
    "fromBankId": charge_bank_list_ret.result[0].bankId,
    "fromChannel": -1
  });
  check(charge_transfer_apply_ret.error, {
    "測試錯誤的支付方式来源:回傳值有error物件": (charge_transfer_apply_ret.error)
  }) || errorCount.add(1);
  if(charge_transfer_apply_ret.error) {
    error_code = charge_transfer_apply_ret.error.code;
    check(error_code, {
      "測試錯誤的支付方式来源": (error_code) => error_code === '2418'
    }) || errorCount.add(1);
  }
  //測試錯誤的会话ID
  console.log("測試錯誤的会话ID");
  charge_no_ret = api.user_charge_no_get({
    "sessionId": data[loginId].sessionId
  });
  charge_transfer_apply_ret = api.user_charge_transfer_apply({
    "sessionId": '123456789',
    "chargeNo": charge_no_ret.result,
    "fromBankId": charge_bank_list_ret.result[0].bankId
  });
  check(charge_transfer_apply_ret.error, {
    "測試錯誤的会话ID:回傳值有error物件": (charge_transfer_apply_ret.error)
  }) || errorCount.add(1);
  if(charge_transfer_apply_ret.error) {
    error_code = charge_transfer_apply_ret.error.code;
    check(error_code, {
      "測試錯誤的会话ID": (error_code) => error_code === '2202'
    }) || errorCount.add(1);
  }
  //測試錯誤的接收账户ID
  console.log("測試錯誤的接收账户ID");
  charge_no_ret = api.user_charge_no_get({
    "sessionId": data[loginId].sessionId
  });
  charge_transfer_apply_ret = api.user_charge_transfer_apply({
    "sessionId": data[loginId].sessionId,
    "chargeNo": charge_no_ret.result,
    "fromBankId": charge_bank_list_ret.result[0].bankId,
    "toAccountId": -1
  });
  check(charge_transfer_apply_ret.error, {
    "測試錯誤的接收账户ID:回傳值有error物件": (charge_transfer_apply_ret.error)
  }) || errorCount.add(1);
  if(charge_transfer_apply_ret.error) {
    error_code = charge_transfer_apply_ret.error.code;
    check(error_code, {
      "測試錯誤的接收账户ID": (error_code) => error_code === '2418'
    }) || errorCount.add(1);
  }
}

export function teardown(data) {
  let loginId = vars["{{user_prefix}}"] + idx;
  if (DEBUG) console.log('loginId = ' + loginId);
  api.auth_online_logout({
    "sessionId": data[loginId].sessionId
  });
}