import {
  get_hash_password
} from '../lib/utils.js';
import * as api from '../lib/api.js';

export let options = {
  setupTimeout: "60s"
};

export default function (data) {
  for (let idx = 1; idx <= 10; idx++) {
    let loginId = "bgqauser_" + idx;
    api.auth_reg_users({
      "loginId": loginId,
      "password": get_hash_password("123456"),
      "payPassword": get_hash_password("1234")
    });
  }
}