import {
  vars,
  get_paypwd_hash,
  get_hash_password,
  get_salted_password
} from '../lib/utils.js';
import * as api from '../lib/api.js';

export let options = {
  setupTimeout: "60s"
};

function reverse_string(str) {
  let token_array = str.split("");
  let reverse_token_array = token_array.reverse();
  let reverse_str = reverse_token_array.join("");
  return reverse_str;
}

function check_user_account(loginId) {
  let ret = api.auth_mobile_login({
    "loginId":loginId,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.log('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    let reverse_password = reverse_string(vars["{{password}}"]);
    let salted_password_ret = get_salted_password(reverse_password);
    let new_password = get_hash_password(vars["{{password}}"]);
    ret = api.auth_mobile_login({
      "loginId":loginId,
      "password": reverse_password
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
      return;
    }
    api.user_password_update({
      "sessionId": ret.result.sessionId,
      "saltedPassword": salted_password_ret["saltedPassword"],
      "salt": salted_password_ret["salt"],
      "newPassword": new_password
    });
  }
  let withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": ret.result.sessionId
  });
  if (withdraw_no_ret.error) {
    console.error('[user_withdraw_no_get]' + 'error code = ' + withdraw_no_ret.error.code);
    return;
  }
  let audit_about_current_apply_ret = api.user_audit_about_current_apply({
    "sessionId": ret.result.sessionId,
    "payPasword": get_paypwd_hash(ret.result.userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result
  });
  if (audit_about_current_apply_ret.error) {
    console.log('[user_audit_about_current_apply]' + 'error code = ' + audit_about_current_apply_ret.error.code);
    let reverse_paypwd = reverse_string(vars["{{paypwd}}"]);
    api.user_paypwd_update({
      "sessionId": ret.result.sessionId,
      "oldPayPwd": get_paypwd_hash(ret.result.userId, reverse_paypwd),
      "newPayPwd": get_paypwd_hash(ret.result.userId, vars["{{paypwd}}"])
    });
  }
  api.auth_online_logout({
    "sessionId": ret.result.sessionId
  });
}

export default function (data) {
  // 檢查bgqajohnload{1-10}帳號密碼/提款密碼
  for (let idx = 1; idx <= 10; idx++) {
    check_user_account("bgqajohnload" + idx);
  }
  // 檢查bgqauser_{1-10}帳號密碼/提款密碼
  for (let idx = 1; idx <= 10; idx++) {
    check_user_account("bgqauser_" + idx);
  }
}
