// js 0_sha1.js

function SHA1() {
    function b(a, b) {
        a[b >> 5] |= 128 << 24 - b % 32;
        a[(b + 64 >> 9 << 4) + 15] = b;
        b = Array(80);
        for (var c = 1732584193, e = -271733879, g = -1732584194, k = 271733878, p = -1009589776, t = 0; t < a.length; t += 16) {
            for (var r = c, W = e, N = g, Ca = k, na = p, A = 0; 80 > A; A++) {
                if (16 > A) b[A] = a[t + A];
                else {
                    var G = b[A - 3] ^ b[A - 8] ^ b[A - 14] ^ b[A - 16];
                    b[A] = G << 1 | G >>> 31
                }
                var G = c << 5 | c >>> 27;
                var Q = 20 > A ? e & g | ~e & k : 40 > A ? e ^ g ^ k : 60 > A ? e & g | e & k | g & k : e ^ g ^ k;
                G = d(d(G, Q), d(d(p, b[A]), 20 > A ? 1518500249 : 40 > A ? 1859775393 : 60 > A ? -1894007588 : -899497514));
                p = k;
                k = g;
                g = e << 30 | e >>> 2;
                e = c;
                c = G
            }
            c = d(c, r);
            e = d(e, W);
            g = d(g, N);
            k = d(k, Ca);
            p = d(p, na)
        }
        return [c, e, g, k, p]
    }

    function d(a, b) {
        var c = (a & 65535) + (b & 65535);
        return (a >> 16) + (b >> 16) + (c >> 16) << 16 | c & 65535
    }

    function c(a) {
        for (var b = [], c = (1 << r) - 1, d = 0; d < a.length * r; d += r) b[d >> 5] |= (a.charCodeAt(d / r) & c) << 24 - d % 32;
        return b
    }
    this.hash = function (d) {
        d = b(c(d), d.length * r);
        for (var e = a ? "0123456789ABCDEF" : "0123456789abcdef", m = "", h = 0; h < 4 * d.length; h++) m += e.charAt(d[h >> 2] >> 8 * (3 - h % 4) + 4 & 15) + e.charAt(d[h >> 2] >> 8 * (3 - h % 4) & 15);
        return m
    };
    this.hashToBase64 = function (a) {
        a = b(c(a), a.length * r);
        for (var d =
                "", m = 0; m < 4 * a.length; m += 3)
            for (var h = (a[m >> 2] >> 8 * (3 - m % 4) & 255) << 16 | (a[m + 1 >> 2] >> 8 * (3 - (m + 1) % 4) & 255) << 8 | a[m + 2 >> 2] >> 8 * (3 - (m + 2) % 4) & 255, l = 0; 4 > l; l++) d = 8 * m + 6 * l > 32 * a.length ? d + e : d + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(h >> 6 * (3 - l) & 63);
        return d + "="
    };
    var a = 0,
        e = "",
        r = 8
};

var sha1 = new SHA1();
var r1 = sha1.hashToBase64('654321')
console.log(r1);
var r2 = r1 + '1535082304471';
console.log(r2);
var r3 = sha1.hashToBase64(r2);
console.log(r3);

console.log('salt = ' + Date.now());
console.log('123456 => ' + sha1.hashToBase64('123456'));

console.log('0000 => ' + sha1.hashToBase64('0000'));

function get_paypwd_hash(userId, paypwd) {
    let sha1 = new SHA1();
    let r1 = sha1.hashToBase64(paypwd)
    let r2 = userId + r1;
    let hash = sha1.hashToBase64(r2);
    return hash;
}

console.log('1234 => ' + get_paypwd_hash('16848582', '1234'));
console.log('4321 => ' + get_paypwd_hash('16848582', '4321'));
console.log('9999 => ' + get_paypwd_hash('16848582', '9999'));
console.log('0000 => ' + get_paypwd_hash('16848582', '0000'));