randomCharString = function () {
    for (var b = "", a = 0; 32 > a; a++) b += "0123456789abcdefghijklmnopqrstuvwxyz".charAt(Math.floor(36 * Math.random()));
    return b
};

function MD5() {
    function b(a, b, c, d, k, q) {
        a = e(e(b, a), e(d, q));
        return e(a << k | a >>> 32 - k, c)
    }

    function d(a, c, d, e, k, q, u) {
        return b(c & d | ~c & e, a, c, k, q, u)
    }

    function c(a, c, d, e, k, q, u) {
        return b(c & e | d & ~e, a, c, k, q, u)
    }

    function a(a, c, d, e, k, q, u) {
        return b(d ^ (c | ~e), a, c, k, q, u)
    }

    function e(a, b) {
        var c = (a & 65535) + (b & 65535);
        return (a >> 16) + (b >> 16) + (c >> 16) << 16 | c & 65535
    }
    this.hash = function (g) {
        for (var m = [], h = (1 << u) - 1, l = 0; l < g.length * u; l += u) m[l >> 5] |= (g.charCodeAt(l / u) & h) << l % 32;
        g = g.length * u;
        m[g >> 5] |= 128 << g % 32;
        m[(g + 64 >>> 9 << 4) + 14] = g;
        g = 1732584193;
        for (var h = -271733879, l = -1732584194, k = 271733878, q = 0; q < m.length; q += 16) {
            var V = g,
                ia = h,
                W = l,
                N = k;
            g = d(g, h, l, k, m[q + 0], 7, -680876936);
            k = d(k, g, h, l, m[q + 1], 12, -389564586);
            l = d(l, k, g, h, m[q + 2], 17, 606105819);
            h = d(h, l, k, g, m[q + 3], 22, -1044525330);
            g = d(g, h, l, k, m[q + 4], 7, -176418897);
            k = d(k, g, h, l, m[q + 5], 12, 1200080426);
            l = d(l, k, g, h, m[q + 6], 17, -1473231341);
            h = d(h, l, k, g, m[q + 7], 22, -45705983);
            g = d(g, h, l, k, m[q + 8], 7, 1770035416);
            k = d(k, g, h, l, m[q + 9], 12, -1958414417);
            l = d(l, k, g, h, m[q + 10], 17, -42063);
            h = d(h, l, k, g, m[q + 11], 22, -1990404162);
            g = d(g,
                h, l, k, m[q + 12], 7, 1804603682);
            k = d(k, g, h, l, m[q + 13], 12, -40341101);
            l = d(l, k, g, h, m[q + 14], 17, -1502002290);
            h = d(h, l, k, g, m[q + 15], 22, 1236535329);
            g = c(g, h, l, k, m[q + 1], 5, -165796510);
            k = c(k, g, h, l, m[q + 6], 9, -1069501632);
            l = c(l, k, g, h, m[q + 11], 14, 643717713);
            h = c(h, l, k, g, m[q + 0], 20, -373897302);
            g = c(g, h, l, k, m[q + 5], 5, -701558691);
            k = c(k, g, h, l, m[q + 10], 9, 38016083);
            l = c(l, k, g, h, m[q + 15], 14, -660478335);
            h = c(h, l, k, g, m[q + 4], 20, -405537848);
            g = c(g, h, l, k, m[q + 9], 5, 568446438);
            k = c(k, g, h, l, m[q + 14], 9, -1019803690);
            l = c(l, k, g, h, m[q + 3], 14, -187363961);
            h = c(h, l, k, g, m[q + 8], 20, 1163531501);
            g = c(g, h, l, k, m[q + 13], 5, -1444681467);
            k = c(k, g, h, l, m[q + 2], 9, -51403784);
            l = c(l, k, g, h, m[q + 7], 14, 1735328473);
            h = c(h, l, k, g, m[q + 12], 20, -1926607734);
            g = b(h ^ l ^ k, g, h, m[q + 5], 4, -378558);
            k = b(g ^ h ^ l, k, g, m[q + 8], 11, -2022574463);
            l = b(k ^ g ^ h, l, k, m[q + 11], 16, 1839030562);
            h = b(l ^ k ^ g, h, l, m[q + 14], 23, -35309556);
            g = b(h ^ l ^ k, g, h, m[q + 1], 4, -1530992060);
            k = b(g ^ h ^ l, k, g, m[q + 4], 11, 1272893353);
            l = b(k ^ g ^ h, l, k, m[q + 7], 16, -155497632);
            h = b(l ^ k ^ g, h, l, m[q + 10], 23, -1094730640);
            g = b(h ^ l ^ k, g, h, m[q + 13], 4, 681279174);
            k = b(g ^ h ^
                l, k, g, m[q + 0], 11, -358537222);
            l = b(k ^ g ^ h, l, k, m[q + 3], 16, -722521979);
            h = b(l ^ k ^ g, h, l, m[q + 6], 23, 76029189);
            g = b(h ^ l ^ k, g, h, m[q + 9], 4, -640364487);
            k = b(g ^ h ^ l, k, g, m[q + 12], 11, -421815835);
            l = b(k ^ g ^ h, l, k, m[q + 15], 16, 530742520);
            h = b(l ^ k ^ g, h, l, m[q + 2], 23, -995338651);
            g = a(g, h, l, k, m[q + 0], 6, -198630844);
            k = a(k, g, h, l, m[q + 7], 10, 1126891415);
            l = a(l, k, g, h, m[q + 14], 15, -1416354905);
            h = a(h, l, k, g, m[q + 5], 21, -57434055);
            g = a(g, h, l, k, m[q + 12], 6, 1700485571);
            k = a(k, g, h, l, m[q + 3], 10, -1894986606);
            l = a(l, k, g, h, m[q + 10], 15, -1051523);
            h = a(h, l, k, g, m[q + 1], 21,
                -2054922799);
            g = a(g, h, l, k, m[q + 8], 6, 1873313359);
            k = a(k, g, h, l, m[q + 15], 10, -30611744);
            l = a(l, k, g, h, m[q + 6], 15, -1560198380);
            h = a(h, l, k, g, m[q + 13], 21, 1309151649);
            g = a(g, h, l, k, m[q + 4], 6, -145523070);
            k = a(k, g, h, l, m[q + 11], 10, -1120210379);
            l = a(l, k, g, h, m[q + 2], 15, 718787259);
            h = a(h, l, k, g, m[q + 9], 21, -343485551);
            g = e(g, V);
            h = e(h, ia);
            l = e(l, W);
            k = e(k, N)
        }
        m = [g, h, l, k];
        g = r ? "0123456789ABCDEF" : "0123456789abcdef";
        h = "";
        for (l = 0; l < 4 * m.length; l++) h += g.charAt(m[l >> 2] >> l % 4 * 8 + 4 & 15) + g.charAt(m[l >> 2] >> l % 4 * 8 & 15);
        return h
    };
    var r = 0,
        u = 8
}

random = randomCharString()

a = {
    sn: "ae00"
};

md5 = new MD5;
a.random = randomCharString();
a.orderId = 591699050;
a.sign = md5.hash(a.random + a.sn + "HG1RCYUIKU2T9NP0GBCMCBMRP5X38HRE");


let body = JSON.stringify(a, null, 2);
console.log(body)

