import {
  vars,
  get_paypwd_hash
} from '../lib/utils.js';
import * as api from '../lib/api.js';

export let options = {
  setupTimeout: "60s"
};

export default function (data) {
  for (let idx = 1; idx <= 10; idx++) {
    let ret = api.auth_mobile_login({
      "loginId":"bgqajohnload" + idx,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    api.user_paypwd_update({
      "sessionId": ret.result.sessionId,
      "oldPayPwd": get_paypwd_hash(ret.result.userId, "0000"),
      "newPayPwd": get_paypwd_hash(ret.result.userId, vars["{{paypwd}}"])
    });
    api.auth_online_logout({
      "sessionId": ret.result.sessionId
    });
  }
}