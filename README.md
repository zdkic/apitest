#### 測試工具 #####
- nosetests
  - https://nose.readthedocs.io/en/latest/man.html
- k6
  - https://docs.k6.io/docs/welcome
- Grafana
  - https://grafana.com/dashboards/2587

#### smoke測試 ####
- 執行smoke測試
  - nosetests -v -s smoke.py
  

#### 功能測試 ####
- 執行單一測試
   - k6 run -e DEBUG=1 -e FUNCTIONAL_TEST=1 script
     - ex: k6 run -e DEBUG=1 -e FUNCTIONAL_TEST=1 -e COLOR=1 user_audit_about_current_apply.js
- 產生測試報告
   - cd tool/;bash ./gen_functional_report.sh

#### 負載測試 ####
- 執行負載測試
   - k6 run --out influxdb=${INFLUXDB} -e USERS=${USERS} -e CONFIG=${CONFIG} script
     - ex: k6 run --out influxdb=http://localhost:8086/myk6db -e USERS=1 -e CONFIG=load 3_transaction_record.js
   - CONFIG(測試組態)
     - once(只執行一次)
     - baseline(基線)
     - load(負載)
-  產生測試報告
   - cd tool/;bash ./gen_load_report.sh [CONFIG] [USERS] [MAX_VU]
     - ex: cd tool/;bash ./gen_load_report.sh load 10 50
   - CONFIG: 測試組態(預設值為once)
   - USERS: 真實使用者數(預設值為1)
   - MAX_VU: 最大虛擬使用者數(預設值為10)
- Grafana(顯示k6測試結果)
  - URL=http://[grafana server]:3000
  - username=admin
  - password=admin123

#### 分析錯誤log ####
- cd tool/;bash ./parse_errlog.sh script
    - ex: cd tool/;bash ./parse_errlog.sh user_audit_about_current_apply.js
