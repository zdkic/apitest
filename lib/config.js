export let OPT_DEBUG = false;
export let OPT_FUNCTIONAL_TEST = false;
export let OPT_COLOR = false;
export const cvars = {
  "{{host}}": "http://ae.bg1207.com",
  "{{sn}}": "ae00",
  "{{domain}}": "ae.bg1207.com",
  "{{user_prefix}}": "bgqajohnload",
  "{{startTime}}": "2018-08-01",
  "{{beginTime}}": "2018-08-01",
  "{{beginTime2}}": "2018-08-01 00:00:00",
  "{{password}}": "123456",
  "{{paypwd}}": "1234"
};

if (__ENV.hasOwnProperty('DEBUG')&&__ENV.DEBUG==1) {
    OPT_DEBUG = true;
}

if (__ENV.hasOwnProperty('FUNCTIONAL_TEST')&&__ENV.FUNCTIONAL_TEST==1) {
    OPT_FUNCTIONAL_TEST = true;
}

if (__ENV.hasOwnProperty('COLOR')&&__ENV.COLOR==1) {
    OPT_COLOR = true;
}
