import {
  check
} from 'k6';
import {
  DEBUG,
  data_generator,
  sendRequest,
  SHA1
} from './utils.js';

export function auth_reg_users(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "auth.reg.login",
    "params": {
      "agentCode": "15053",
      "loginId": "{{loginId}}",
      "password": "{{password}}",
      "name": "",
      "birthday": data_generator["birthday"](),
      "mobile": "",
      "wechat": "",
      "qq": "",
      "email": data_generator["email"](),
      "currency": "1",
      "payPassword": "{{payPassword}}",
      "whihAuth": "1",
      "regChannel": "ae.bg1207.com",
      "pwd2": null,
      "sn": "{{sn}}",
      "regFrom": 8,
      "movieKey": "",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function user_bank_update(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.bank.update",
    "params": {
      "sessionId": "{{sessionId}}",
      "bankId": 1,
      "name": "user",
      "bankBranch": "1111",
      "bankAccount": "123456789012",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function auth_mobile_login(args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.mobile.login",
    "params": {
      "sn": "{{sn}}",
      "loginId": "{{loginId}}",
      "withAuth": "1",
      "withProfile": "1",
      "saltedPassword": "{{saltedPassword}}",
      "salt": "{{salt}}",
      "domain": "{{domain}}",
      "captchaKey": "3094255924",
      "captchaCode": "6711"
    },
    "jsonrpc": "2.0"
  };
  json.params.salt = data_generator["randomcharstring"]();
  let sha1 = new SHA1();
  let r1 = sha1.hashToBase64(args.password);
  let r2 = r1 + json.params.salt;
  json.params.saltedPassword = sha1.hashToBase64(r2);
  return sendRequest(json, args);
}

export function auth_online_logout(args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.online.logout",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function auth_mobile_trial_login(args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.mobile.trial.login",
    "params": {
      "sn": "{{sn}}"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function user_balance_get(args) {
  let json = {
    "id": "{{id}}",
    "method": "user.balance.get",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function thirdparty_user_balance_list(args) {
  let json = {
    "id": "{{id}}",
    "method": "thirdparty.user.balance.list",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function account_item_list() {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "account.item.list",
    "params": {
      "sn": "{{sn}}",
      "clazz": "ltuserview"
    }
  };
  return sendRequest(json);
}

export function user_cashflow_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.cashflow.query",
    "params": {
      "sessionId": "{{sessionId}}",
      "accountItem": null,
      "startTime": "{{startTime}}",
      "endTime": "{{endTime}}",
      "pageIndex": 1,
      "pageSize": 100
    }
  };
  return sendRequest(json, args);
}

export function sn_order_switch_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.order.switch.list",
    "params": {
      "sessionId": "{{sessionId}}",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function user_order_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.order.query",
    "params": {
      "sessionId": "{{sessionId}}",
      "startTime": "{{startTime}}",
      "endTime": "{{endTime}}",
      "moduleId": "2",
      "pageIndex": 1,
      "pageSize": 100,
      "withItemDesc": 1,
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function user_charge_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.charge.list",
    "params": {
      "sessionId": "{{sessionId}}",
      "startTime": "{{startTime}}",
      "endTime": "{{endTime}}",
      "popupFlag": "N",
      "pageIndex": 1,
      "pageSize": 100,
      "scope": 0
    }
  };
  return sendRequest(json, args);
}

export function user_withdraw_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.withdraw.list",
    "params": {
      "sessionId": "{{sessionId}}",
      "startTime": "{{startTime}}",
      "endTime": "{{endTime}}",
      "popupFlag": "N",
      "pageIndex": 1,
      "pageSize": 100,
      "scope": 0
    }
  };
  return sendRequest(json, args);
}

export function report_day_order_groupby_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "report.day.order.groupby.query",
    "params": {
      "beginTime": "{{beginTime}}",
      "endTime": "{{endTime}}",
      "moduleIds": [

      ],
      "sessionId": "{{sessionId}}",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function thirdparty_user_balance_exchange(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "sessionId": "{{sessionId}}",
      "from": -1,
      "to": -1,
      "amount": -1
    }
  };
  return sendRequest(json, args);
}

export function thirdparty_user_balance_refresh(args) {
  let json = {  
    "id":"{{id}}",
    "jsonrpc":"2.0",
    "method":"thirdparty.user.balance.refresh",
    "params":{  
       "sessionId":"{{sessionId}}",
       "thirdpartyId":"4",
       "terminal":1
    }
 };
  return sendRequest(json, args);
}

export function thirdparty_user_collect_balance_info(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "thirdparty.user.collect.balance.info",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function thirdparty_user_collect_balance_syn(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "thirdparty.user.collect.balance.syn",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_charge_option_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.charge.option.get",
    "params": {
      "sessionId":"{{sessionId}}",
      "chargeType": 1,
      "amount":data_generator["amount"]()
    }
  };
  return sendRequest(json, args);
}

export function user_charge_bank_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.charge.bank.list",
    "params": {
      "sn": "{{sn}}",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function layer_deposit_to_account_noqrcode_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "layer.deposit.to.account.noqrcode.list",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function layer_deposit_to_account_qrcode_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "layer.deposit.to.account.qrcode.get",
    "params": {
      "sessionId": "{{sessionId}}",
      "toAccountId": -1
    }
  };
  return sendRequest(json, args);
}

export function user_charge_transfer_apply(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.charge.transfer.apply",
    "params": {
      "sessionId": "{{sessionId}}",
      "chargeNo": "{{chargeNo}}",
      "chargeFrom": 1,
      "amount": data_generator["amount"](100, 500),
      "fromAccountOwner": "test",
      "memo": null,
      "toAccountId": 12001,
      "fromBankId": 1,
      "fromChannel": "8",
      "chargeFromChannel": "8",
      "couponMode": 1,
      "terminal": 1
    }
  };
  return sendRequest(json, args);
}

export function user_withdraw_option_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.withdraw.option.get",
    "params": {
      "sessionId": "{{sessionId}}",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function user_withdraw_account_default_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.withdraw.account.default.get",
    "params": {
      "sessionId": "{{sessionId}}",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function user_withdraw_no_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.withdraw.no.get",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_audit_about_current_cancel(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.audit.about.current.cancel",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_audit_about_current_trace(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.audit.about.current.trace",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_audit_about_current_apply(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.audit.about.current.apply",
    "params": {
      "sessionId": "{{sessionId}}",
      "payPasword": "{{payPasword}}",
      "withdrawNo": "{{withdrawNo}}",
      "amount": data_generator["amount"](100,500)
    }
  };
  return sendRequest(json, args);
}

export function user_audit_about_current_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.audit.about.current.get",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function layer_deposit_to_account_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "layer.deposit.to.account.list",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_center_deposit_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.center.deposit.get",
    "params": {
      "sessionId": "{{sessionId}}",
      "startTime":"{{startTime}}",
      "endTime":"{{endTime}}"
    }
  };
  return sendRequest(json, args);
}

export function user_center_withdraw_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.center.withdraw.get",
    "params": {
      "sessionId": "{{sessionId}}",
      "startTime":"{{startTime}}",
      "endTime":"{{endTime}}"
    }
  };
  return sendRequest(json, args);
}

export function user_withdraw_apply(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.withdraw.apply",
    "params": {
      "sessionId": "{{sessionId}}",
      "withdrawNo": "{{withdrawNo}}",
      "payPasword": "{{payPasword}}",
      "toBankId": 1,
      "amount": "{{amount}}",
      "statsCacheKey": "{{statsCacheKey}}"
    }
  };
  return sendRequest(json, args);
}

export function user_withdraw_account_list(args) {
  let json = {
    "id":"{{id}}",
    "jsonrpc":"2.0",
    "method":"user.withdraw.account.list",
    "params":{
       "sessionId":"{{sessionId}}"
    }
 };
  return sendRequest(json, args);
}

export function auth_session_validate(args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.session.validate",
    "params": {
      "sessionId": "{{sessionId}}",
      "withAmount": "0",
      "withUser": "0"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function auth_loginId_exists(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "auth.loginId.exists",
    "params": {
      "sn": "{{sn}}",
      "loginId": "{{loginId}}",
    }
  };
  return sendRequest(json, args);
}

export function user_profile_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.profile.get",
    "params": {
      "sessionId": "{{sessionId}}",
      "withVipData": 1
    }
  };
  return sendRequest(json, args);
}

export function user_profile_update(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.profile.update",
    "params": {
      "sessionId": "{{sessionId}}",
      "email": data_generator["email"](),
      "mobile": "",
      "wechat": "",
      "qq": "",
      "birthday": data_generator["birthday"]()
    }
  };
  return sendRequest(json, args);
}

export function user_password_update(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.password.update",
    "params": {
      "sessionId": "{{sessionId}}",
      "saltedPassword": "{{saltedPassword}}",
      "salt": "{{salt}}",
      "newPassword": "{{newPassword}}"
    }
  };
  return sendRequest(json, args);
}

export function user_paypwd_update(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.paypwd.update",
    "params": {
      "sessionId": "{{sessionId}}",
      "oldPayPwd": "{{oldPayPwd}}",
      "newPayPwd": "{{newPayPwd}}"
    }
  };
  return sendRequest(json, args);
}

export function user_charge_no_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.charge.no.get",
    "params": {
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function sn_payment_biglist(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.payment.biglist",
    "params": {
      "sessionId": "{{sessionId}}",
      "changeNo": "{{changeNo}}",
      "isFromMobile": 1,
      "iframeFlag": 0
    }
  };
  return sendRequest(json, args);
}

export function sn_payment_batch_limit_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.payment.batch.limit.query",
    "params": {
      "sessionId": "{{sessionId}}",
      "payIds": []
    }
  };
  return sendRequest(json, args);
}

export function sn_payment_charge_bank_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.payment.charge.bank.list",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function sn_website_proitem_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.website.proitem.query",
    "params": {
      "sn": "{{sn}}",
      "tempId": "t8889",
      "ismobile": 2
    }
  };
  return sendRequest(json, args);
}

export function sn_website_slice_image_url(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.website.slice.image.url",
    "params": {
      "sn": "{{sn}}",
      "tempId": "t8889",
      "ismobile": 2
    }
  };
  return sendRequest(json, args);
}

export function sn_website_logo_image_url(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.website.logo.image.url",
    "params": {
      "sn": "{{sn}}",
      "tempId": "t8889",
      "ismobile": 2
    }
  };
  return sendRequest(json, args);
}

export function sn_website_doc_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.website.doc.query",
    "params": {
      "sn": "{{sn}}",
      "tempId": "t8889"
    }
  };
  return sendRequest(json, args);
}

export function sn_lottery_order_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.lottery.order.query",
    "params": {
      "sessionId": "{{sessionId}}",
      "startTime": "{{startTime}}",
      "endTime": "{{endTime}}",
      "moduleId": 106,
      "pageIndex": 1,
      "pageSize": 100,
      "withItemDesc": 1,
      "beginTime": "{{beginTime2}}",
      "pageNo": 1
    }
  };
  return sendRequest(json, args);
}

export function lottery_issue_current_all(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "lottery.issue.current.all",
    "params": {
      "sn": "{{sn}}"
    }
  };
  return sendRequest(json, args);
}

export function lottery_sn_list(args) {
  let json = {
    "id": "{{id}}",
    "method": "lottery.sn.list",
    "params": {
      "sn": "{{sn}}"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function lottery_data_current(args) {
  let json = {
    "id": "{{id}}",
    "method": "lottery.data.current",
    "params": {
      "lotteryId": "18",
      "isMobile": 1,
      "token": "ae0000E0512E125D2C5CB63B26A1a6ea"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function sn_notice_new_layer_query(args) {
  let json = {
    "id": "{{id}}",
    "method": "sn.notice.new.layer.query",
    "params": {
      "sn": "{{sn}}",
      "pageIndex": 1,
      "pageSize": 10
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function notice_system_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "notice.system.query",
    "params": {
      "current": 0,
      "popupFlag": "N",
      "noticeFrom": 1
    }
  };
  return sendRequest(json, args);
}

export function notice_system_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "notice.system.get",
    "params": {
      "noticePk":16068
    }
  };
  return sendRequest(json, args);
}

export function user_notice_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.notice.list",
    "params": {
      "sessionId": "{{sessionId}}",
      "popupFlag": "N"
    }
  };
  return sendRequest(json, args);
}

export function sn_notice_new_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.notice.new.query",
    "params": {
      "sessionId": "{{sessionId}}",
      "popupFlag": "N"
    }
  };
  return sendRequest(json, args);
}

export function sn_notice_history_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.notice.history.query",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_notice_unread_touch(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.notice.unread.touch",
    "params": {
      "sessionId": "{{sessionId}}",
      "terminal":8
    }
  };
  return sendRequest(json, args);
}

export function thirdparty_ky_game_url(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "thirdparty.ky.game.url",
    "params": {
      "sessionId": "{{sessionId}}",
      "lineCode": 1,
      "gameId": "830"
    }
  };
  return sendRequest(json, args);
}

export function thirdparty_ky_order_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "thirdparty.ky.order.query",
    "params": {
      "sessionId": "{{sessionId}}",
      "startTime": "{{startTime}}",
      "endTime": "{{endTime}}",
      "moduleId": "209",
      "pageIndex": 1,
      "pageSize": 100,
      "withItemDesc": 1
    }
  };
  return sendRequest(json, args);
}

export function sn_thirdparty_pwd_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.thirdparty.pwd.get",
    "params": {
      "sessionId": "{{sessionId}}",
      "terminal": 8
    }
  };
  return sendRequest(json, args);
}

export function user_red_packet_exchange(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.red.packet.exchange",
    "params": {
      "sessionId": "{{sessionId}}",
      "amount": -1
    }
  };
  return sendRequest(json, args);
}

export function currency_list(args) {
  let json = {
    "id": "{{id}}",
    "method": "currency.list",
    "params": {
      "sn": "{{sn}}"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function currency_get(args) {
  let json = {
    "id": "{{id}}",
    "method": "currency.get",
    "params": {
      "sn": "{{sn}}",
      "currencyId": -1
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function user_order_cost(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.order.cost",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_order_cost_sum(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.order.cost.sum",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function host_info(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "host.info",
    "params": {
      "domain":"{{domain}}"
    }
  };
  return sendRequest(json, args);
}

export function module_maintain_info(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "module.maintain.info",
    "params": {
      "moduleId": "226"
    }
  };
  return sendRequest(json, args);
}

export function sn_info(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.info",
    "params": {
      "domain":"{{domain}}"
    }
  };
  return sendRequest(json, args);
}

export function sn_settings_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.settings.get",
    "params": {
      "sn":"{{sn}}",
      "type":"USER"
    }
  };
  return sendRequest(json, args);
}

export function sn_switch_item_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.switch.item.query",
    "params": {
      "sn":"{{sn}}",
      "key":"wm.game.bg.livechatroom.url",
      "userId":"{{userId}}"
    }
  };
  return sendRequest(json, args);
}

export function sn_user_reg_setting(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.user.reg.setting",
    "params": {
      "sn":"{{sn}}"
    }
  };
  return sendRequest(json, args);
}

export function sn_agent_reg_setting(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.agent.reg.setting",
    "params": {
      "sn":"{{sn}}"
    }
  };
  return sendRequest(json, args);
}

export function thirdparty_list(args) {
  let json = {  
    "id":"{{id}}",
    "jsonrpc":"2.0",
    "method":"thirdparty.list",
    "params":{  
       "sessionId":"{{sessionId}}",
       "terminal":8
    }
 };
  return sendRequest(json, args);
}

export function tool_select_options_get(args) {
  let json = {  
    "id":"{{id}}",
    "jsonrpc":"2.0",
    "method":"tool.select.options.get",
    "params":{  
       "clazz":"{{clazz}}"
    }
 };
  return sendRequest(json, args);
}

export function agent_code(args) {
  let json = {  
    "id":"{{id}}",
    "jsonrpc":"2.0",
    "method":"agent.code",
    "params":{  
       "domain":"{{domain}}"
    }
 };
  return sendRequest(json, args);
}

export function user_deposit_draw_setting_get(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.deposit.draw.setting.get",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  };
  return sendRequest(json, args);
}

export function user_reg_recommend_check(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.reg.recommend.check",
    "params": {
      "sn": "{{sn}}",
      "recommendCode": "15053"
    }
  };
  return sendRequest(json, args);
}

export function user_name_check_duplicate(args) {
  let json = {
    "id": "{{id}}",
    "method": "user.name.check.duplicate",
    "params": {
      "sn": "{{sn}}",
      "name": "1399999999"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(json, args);
}

export function user_mobile_check_duplicate(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.mobile.check.duplicate",
    "params": {
      "sn": "{{sn}}",
      "mobile": "1234567"
    }
  };
  return sendRequest(json, args);
}

export function report_user_validamount(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "report.user.validamount",
    "params": {
      "sessionId": "{{sessionId}}",
      "beginTime": "{{beginTime}}",
      "endTime": "{{endTime}}"
    }
  };
  return sendRequest(json, args);
}

export function sn_website_float_image_url(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.website.float.image.url",
    "params": {
      "host": "ko8161.bgbet3.com",
      "tempId": 10
    }
  };
  return sendRequest(json, args);
}

export function sn_website_pronotice_query(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.website.pronotice.query",
    "params": {
      "host": "ko8161.bgbet3.com",
      "tempId": 10
    }
  };
  return sendRequest(json, args);
}

export function user_charge_payment_apply(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.charge.payment.apply",
    "params": {
      "sessionId": "{{sessionId}}",
      "chargeNo": "{{withdrawNo}}",
      "chargeFrom": 8,
      "toPayId": 28063,
      "amount": data_generator["amount"](100, 500),
      "digest": "11111",
      "memo": null,
      "toAccountId": 12212,
      "fromBankId": 23,
      "fromChannel": 10,
      "couponMode": 1
    }
  };
  return sendRequest(json, args);
}

export function sn_user_bg_video_order_list(args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.user.bg.video.order.list",
    "params": {
      "opsId":"{{sessionId}}",
      "terminal":7,
      "host":"av31193.sn.bgbet5.com",
      "bySettle":"1",
      "timeZone":2,
      "startTime":"2019-11-06 00:00:00",
      "endTime":"2019-11-06 23:59:59",
      "page":1,
      "pageSize":10
    }
  };
  return sendRequest(json, args);
}