import {
  check
} from 'k6';
import http from 'k6/http';
import {
  Counter
} from 'k6/metrics';
import {
  cvars,
  OPT_DEBUG,
  OPT_FUNCTIONAL_TEST,
  OPT_COLOR
} from './config.js';

export const DEBUG = OPT_DEBUG;
export const FUNCTIONAL_TEST = OPT_FUNCTIONAL_TEST;
export const COLOR = OPT_COLOR;
export const vars = cvars;
export let errorCount = new Counter("errors");

const color_red = COLOR?'\x1b[31m':'';
const color_green = COLOR?'\x1b[32m':'';
const color_yellow = COLOR?'\x1b[33m':'';
const color_reset = COLOR?'\x1b[0m':'';

export let data_generator = {
  "id": () => {
    let id = Date.now();
    id += '.';
    let possible = "0123456789";
    for (let idx = 0; idx < 16; idx++) {
      id += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return id;
  },
  "end_time": () => {
    return new Date().toISOString().slice(0, 10) + " 23:59:59";
  },
  "amount": (base = 1, range = 50) => {
    return base + Math.floor(Math.random() * range);
  },
  "email": () => {
    let possible = "abcdefghijklmnopqrstuvwxyz";
    let name = '';
    for (let idx = 0; idx < 10; idx++) {
      name += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return name + "@gmail.com";
  },
  "birthday": () => {
    let now = new Date();
    let birthday = new Date();
    let age = 10 + Math.floor(Math.random() * 60);
    birthday.setFullYear(now.getFullYear() - age);
    return birthday.toISOString().slice(0, 10);
  },
  "url": (method) => {
    if(method == "lottery.issue.current.all" || method == "lottery.sn.list" ||　method ==　"lottery.data.current")
      return vars["{{host}}"] + "/lottery-api/api/" + method;
    return vars["{{host}}"] + "/cloud/api/" + method;
  },
  "randomcharstring": () => {
    let str = "";
    const charset = "0123456789abcdefghijklmnopqrstuvwxyz";
    for (let a = 0; 32 > a; a++)
      str += charset.charAt(Math.floor(36 * Math.random()));
    return str;
  }
};

export function sendRequest(json, args = {}) {
  for (const [k, v] of Object.entries(args)) {
    if (json.params.hasOwnProperty(k))
      json.params[k] = v;
  }
  for (const [k, v] of Object.entries(json.params)) {
    if (vars.hasOwnProperty(v))
      json.params[k] = vars[v];
  }
  json.id = data_generator["id"]();
  if (json.params.hasOwnProperty("endTime")) {
    json.params["endTime"] = data_generator["end_time"]();
  }
  let body = JSON.stringify(json);
  if (DEBUG) {
    try {
      let json_payload = JSON.parse(body);
      console.log('[' + json.method + '] -> ');
      console.log(color_green + JSON.stringify(json_payload, null, 2) + color_reset);
    } catch(e) {
      console.log('[' + json.method + '] -> ');
      console.log(color_green + body + color_reset);
      errorCount.add(1);
      throw e;
    }
  }
  let payload = "json=" + encodeURIComponent(body);
  let params = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    "timeout": 240000
  };
  let url = data_generator["url"](json.method);
  let r = http.post(url, payload, params);
  check(r, {
    "status is 200": (r) => r.status === 200
  }) || errorCount.add(1);
  try {
    let obj = JSON.parse(r.body);
    if (obj.error) {
      if(DEBUG) {
      } else {
        let json_payload = JSON.parse(body);
        console.log('[' + json.method + '] -> ');
        console.log(color_green + JSON.stringify(json_payload, null, 2) + color_reset);
      }
      console.log('[' + json.method + '] <- ');
      console.log(color_red + JSON.stringify(obj, null, 2) + color_reset);
    } else {
      if (DEBUG) {
        console.log('[' + json.method + '] <- ');
        console.log(color_yellow + JSON.stringify(obj, null, 2) + color_reset);
      }
    }
    if (!FUNCTIONAL_TEST) {
      check(obj, {
        "check no error": (obj) => obj.error === null
      }) || errorCount.add(1);
      if (obj.error) {
        console.error('[' + json.method + ']' + 'error code = ' + obj.error.code);
      }
    }
    return obj;
  } catch(e) {
    let json_payload = JSON.parse(body);
    console.log('[' + json.method + '] -> ');
    console.log(color_green + JSON.stringify(json_payload, null, 2) + color_reset);
    console.log('[' + json.method + '] <- ');
    console.log(color_green + r.body + color_reset);
    errorCount.add(1);
    throw e;
  }
  return null;
}

export function SHA1() {
  function b(a, b) {
    a[b >> 5] |= 128 << 24 - b % 32;
    a[(b + 64 >> 9 << 4) + 15] = b;
    b = Array(80);
    for (var c = 1732584193, e = -271733879, g = -1732584194, k = 271733878, p = -1009589776, t = 0; t < a.length; t += 16) {
      for (var r = c, W = e, N = g, Ca = k, na = p, A = 0; 80 > A; A++) {
        if (16 > A) b[A] = a[t + A];
        else {
          var G = b[A - 3] ^ b[A - 8] ^ b[A - 14] ^ b[A - 16];
          b[A] = G << 1 | G >>> 31
        }
        var G = c << 5 | c >>> 27;
        var Q = 20 > A ? e & g | ~e & k : 40 > A ? e ^ g ^ k : 60 > A ? e & g | e & k | g & k : e ^ g ^ k;
        G = d(d(G, Q), d(d(p, b[A]), 20 > A ? 1518500249 : 40 > A ? 1859775393 : 60 > A ? -1894007588 : -899497514));
        p = k;
        k = g;
        g = e << 30 | e >>> 2;
        e = c;
        c = G
      }
      c = d(c, r);
      e = d(e, W);
      g = d(g, N);
      k = d(k, Ca);
      p = d(p, na)
    }
    return [c, e, g, k, p]
  }

  function d(a, b) {
    var c = (a & 65535) + (b & 65535);
    return (a >> 16) + (b >> 16) + (c >> 16) << 16 | c & 65535
  }

  function c(a) {
    for (var b = [], c = (1 << r) - 1, d = 0; d < a.length * r; d += r) b[d >> 5] |= (a.charCodeAt(d / r) & c) << 24 - d % 32;
    return b
  }
  this.hash = function (d) {
    d = b(c(d), d.length * r);
    for (var e = a ? "0123456789ABCDEF" : "0123456789abcdef", m = "", h = 0; h < 4 * d.length; h++) m += e.charAt(d[h >> 2] >> 8 * (3 - h % 4) + 4 & 15) + e.charAt(d[h >> 2] >> 8 * (3 - h % 4) & 15);
    return m
  };
  this.hashToBase64 = function (a) {
    a = b(c(a), a.length * r);
    for (var d =
      "", m = 0; m < 4 * a.length; m += 3)
      for (var h = (a[m >> 2] >> 8 * (3 - m % 4) & 255) << 16 | (a[m + 1 >> 2] >> 8 * (3 - (m + 1) % 4) & 255) << 8 | a[m + 2 >> 2] >> 8 * (3 - (m + 2) % 4) & 255, l = 0; 4 > l; l++) d = 8 * m + 6 * l > 32 * a.length ? d + e : d + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(h >> 6 * (3 - l) & 63);
    return d + "="
  };
  var a = 0,
    e = "",
    r = 8
}

export function get_salted_password(password) {
  let sha1 = new SHA1();
  let salt = data_generator["randomcharstring"]();;
  let r1 = sha1.hashToBase64(password);
  let r2 = r1 + salt;
  let saltedPassword = sha1.hashToBase64(r2);
  return {
    "saltedPassword": saltedPassword,
    "salt": salt
  };
}

export function get_hash_password(password) {
  let sha1 = new SHA1();
  return sha1.hashToBase64(password);
}

export function get_paypwd_hash(userId, paypwd) {
  let sha1 = new SHA1();
  let r1 = sha1.hashToBase64(paypwd)
  let r2 = userId + r1;
  let hash = sha1.hashToBase64(r2);
  return hash;
}

export function check_game_url(url) {
  let r = http.get(url);
  check(r, {
    "status is 200": (r) => r.status === 200
  }) || errorCount.add(1);
}