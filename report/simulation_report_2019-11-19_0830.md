
### Environment ###

Host | sn 
--- | --- 
http://ae.bg1207.com | ae00 
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | --- 
10.37.1.129 | Ubuntu 18.04.1 LTS | 2019/11/19 08:30:02
### Parameter ###

Config | Users | MAX_VU 
--- | --- | --- 
load | 10 | 500
### Grafana ###

URL | Username | Password
--- | --- | ---
http://10.37.1.129:3000 | admin | admin123
### Testing Result ###
- Connection: http://ae.bg1207.com
    - Connected
- Testing Result

     <img src="../report/images/img_xNWZEAa7_2019-11-19_0830.png"  width="400" height="300">
- simulation.js
  - 混合情境
  - k6 result

     ```bash
    checks.....................: 85.81% ✓ 768   ✗ 127  
    collect_balance_sync.......: 2      0.226946/s
    data_received..............: 895 kB 102 kB/s
    data_sent..................: 68 kB  7.7 kB/s
  ✗ errors.....................: 127    14.411051/s
    host.......................: 3      0.340419/s
    http_req_blocked...........: avg=19.94ms  min=1.42µs  med=6.07µs   max=418.65ms p(90)=24.05ms  p(95)=60.42ms 
    http_req_connecting........: avg=19.83ms  min=0s      med=0s       max=418.5ms  p(90)=23.8ms   p(95)=59.09ms 
    http_req_duration..........: avg=387.69ms min=73.71ms med=103.9ms  max=2.69s    p(90)=1.08s    p(95)=1.6s    
    http_req_receiving.........: avg=18.69ms  min=24.09µs med=109.81µs max=703.09ms p(90)=8.31ms   p(95)=72.9ms  
    http_req_sending...........: avg=88.96µs  min=11.74µs med=48.83µs  max=875.3µs  p(90)=220.35µs p(95)=287.17µs
    http_req_tls_handshaking...: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=368.9ms  min=73.29ms med=98.02ms  max=2.69s    p(90)=1.02s    p(95)=1.6s    
    http_reqs..................: 448    50.835834/s
    iteration_duration.........: avg=5.29s    min=1.13s   med=5.11s    max=8.49s    p(90)=7.1s     p(95)=7.19s   
    iterations.................: 45     5.106278/s
    lottery....................: 2      0.226946/s
    notice.....................: 12     1.361674/s
    order_query................: 25     2.836821/s
    payment....................: 14     1.58862/s
    report_query...............: 14     1.58862/s
    thirdparty.................: 4      0.453891/s
    transaction_record.........: 23     2.609875/s
    vus........................: 147    min=1   max=147
    vus_max....................: 500    min=500 max=500
    web........................: 15     1.702093/s
    withdraw...................: 1      0.113473/s

     ```
  - Virtual Users

     ![](../report/images/img_Xcln9euS_2019-11-19_0830.png)
  - Errors Per Second

     ![](../report/images/img_z1Ibm2lS_2019-11-19_0830.png)

  - Error log [here](../report/log/simulation_error_log_2019-11-19_0830.md)
  - Requests per Second

     ![](../report/images/img_qzioUlqn_2019-11-19_0830.png)
  - http_req_duration

     ![](../report/images/img_0YhuR8Zi_2019-11-19_0830.png)
  - grafana url
     - <http://10.37.1.129:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1574123404000&to=1574123456000>
