
### Environment ###

Host | sn
--- | ---
http://ae.bg1207.com | ae00
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | ---
10.37.3.28 | Ubuntu 18.04.3 LTS | 2020/03/13 08:50:01
### Testing Result ###
- <span style=color:red>2019-01-15 無法連線http://ae.bg1207.com</span>
- 额度转换 (1_amount_transfer.js)

     ![](../report/trend/images/1_amount_transfer.png)
- 一键回收第三方平台余额异步 (2_collect_balance_syn.js)

     ![](../report/trend/images/2_collect_balance_syn.png)
- 交易記錄 (3_transaction_record.js)

     ![](../report/trend/images/3_transaction_record.png)
- 注單查詢 (4_order_query.js)

     ![](../report/trend/images/4_order_query.png)
- 充值提款記錄 (5_charge_withdraw_record.js)

     ![](../report/trend/images/5_charge_withdraw_record.png)
- 報表查詢 (6_report_query.js)

     ![](../report/trend/images/6_report_query.png)
- 我要充值 (7_charge.js)

     ![](../report/trend/images/7_charge.png)
- 我要提款 (8_withdraw.js)

     ![](../report/trend/images/8_withdraw.png)
- 鉴权服务接口 (9_auth.js)

     ![](../report/trend/images/9_auth.png)
- 第三方平台接口 (10_thirdparty.js)

     ![](../report/trend/images/10_thirdparty.png)
- 第三方支付接口 (11_payment.js)

     ![](../report/trend/images/11_payment.png)
- 网站管理服务接口 (12_web.js)

     ![](../report/trend/images/12_web.png)
- 彩票注单 (13_lottery.js)

     ![](../report/trend/images/13_lottery.png)
- 公告服务接口 (14_notice.js)

     ![](../report/trend/images/14_notice.png)
- 厅主接口 (15_host.js)

     ![](../report/trend/images/15_host.png)
- 用户服务接口 (16_user.js)

     ![](../report/trend/images/16_user.png)
