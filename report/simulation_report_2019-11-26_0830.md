
### Environment ###

Host | sn 
--- | --- 
http://ae.bg1207.com | ae00 
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | --- 
10.37.1.129 | Ubuntu 18.04.3 LTS | 2019/11/26 08:30:01
### Parameter ###

Config | Users | MAX_VU 
--- | --- | --- 
load | 10 | 500
### Grafana ###

URL | Username | Password
--- | --- | ---
http://10.37.1.129:3000 | admin | admin123
### Testing Result ###
- Connection: http://ae.bg1207.com
    - Connected
- Testing Result

     <img src="../report/images/img_beUBN8Gq_2019-11-26_0830.png"  width="400" height="300">
- simulation.js
  - 混合情境
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_OznXAcli_2019-11-26_0830.png)
  - Errors Per Second

     ![](../report/images/img_mzgdunrs_2019-11-26_0830.png)

  - Error log [here](../report/log/simulation_error_log_2019-11-26_0830.md)
  - Requests per Second

     ![](../report/images/img_Hx8Yxwl2_2019-11-26_0830.png)
  - http_req_duration

     ![](../report/images/img_UFtuXzYQ_2019-11-26_0830.png)
  - grafana url
     - <http://10.37.1.129:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1574728203000&to=1574728239000>
