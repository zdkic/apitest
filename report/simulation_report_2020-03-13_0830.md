
### Environment ###

Host | sn 
--- | --- 
http://ae.bg1207.com | ae00 
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | --- 
10.37.3.28 | Ubuntu 18.04.3 LTS | 2020/03/13 08:30:01
### Parameter ###

Config | Users | MAX_VU 
--- | --- | --- 
load | 10 | 500
### Grafana ###

URL | Username | Password
--- | --- | ---
http://10.37.3.28:3000 | admin | admin123
### Testing Result ###
- Connection: http://ae.bg1207.com
    - Connected
- Testing Result

     <img src="../report/images/img_CFiI0sSn_2020-03-13_0830.png"  width="400" height="300">
- simulation.js
  - 混合情境
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_XW5WyJIU_2020-03-13_0831.png)
  - Errors Per Second

     ![](../report/images/img_ChQzbW0k_2020-03-13_0831.png)

  - Error log [here](../report/log/simulation_error_log_2020-03-13_0830.md)
  - Requests per Second

     ![](../report/images/img_nRYgo60k_2020-03-13_0831.png)
  - http_req_duration

     ![](../report/images/img_51x8cdfF_2020-03-13_0831.png)
  - grafana url
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1584059403000&to=1584059474000>
