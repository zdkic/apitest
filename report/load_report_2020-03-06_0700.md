
### Environment ###

Host | sn 
--- | --- 
http://ae.bg1207.com | ae00 
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | --- 
10.37.3.28 | Ubuntu 18.04.3 LTS | 2020/03/06 07:00:02
### Parameter ###

Config | Users | MAX_VU 
--- | --- | --- 
load | 10 | 500
### Grafana ###

URL | Username | Password
--- | --- | ---
http://10.37.3.28:3000 | admin | admin123
### Network ###
- Connected(http://ae.bg1207.com)

### Testing Result ###
1. [额度转换](#1_amount_transfer.js)
2. [一键回收第三方平台余额异步](#2_collect_balance_syn.js)
3. [交易記錄](#3_transaction_record.js)
4. [注單查詢](#4_order_query.js)
5. [充值提款記錄](#5_charge_withdraw_record.js)
6. [報表查詢](#6_report_query.js)
7. [我要充值](#7_charge.js)
8. [我要提款](#8_withdraw.js)
9. [鉴权服务接口](#9_auth.js)
10. [第三方平台接口](#10_thirdparty.js)
11. [第三方支付接口](#11_payment.js)
12. [网站管理服务接口](#12_web.js)
13. [彩票注单](#13_lottery.js)
14. [公告服务接口](#14_notice.js)
15. [厅主接口](#15_host.js)
16. [用户服务接口](#16_user.js)

- 1_amount_transfer.js <a name=1_amount_transfer.js></a>
  - 额度转换
  - k6 result

     ```bash
    checks.....................: 93.33% ✓ 140  ✗ 10  
    data_received..............: 50 kB  6.1 kB/s
    data_sent..................: 31 kB  3.7 kB/s
  ✗ errors.....................: 10     1.207088/s
    http_req_blocked...........: avg=3.98ms   min=2.13µs  med=6.32µs   max=24.63ms  p(90)=22.96ms  p(95)=23.44ms 
    http_req_connecting........: avg=3.94ms   min=0s      med=0s       max=24.53ms  p(90)=22.87ms  p(95)=23.19ms 
    http_req_duration..........: avg=86.28ms  min=72.79ms med=82.53ms  max=252.58ms p(90)=93.45ms  p(95)=98.49ms 
    http_req_receiving.........: avg=178.21µs min=47.33µs med=117.79µs max=2.12ms   p(90)=297.21µs p(95)=438.35µs
    http_req_sending...........: avg=101.98µs min=15.66µs med=42.73µs  max=2.9ms    p(90)=137.22µs p(95)=167.66µs
    http_req_tls_handshaking...: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=86ms     min=72.71ms med=82.43ms  max=252.03ms p(90)=93.04ms  p(95)=97.19ms 
    http_reqs..................: 70     8.449614/s
    iteration_duration.........: avg=5.8s     min=1.82s   med=6.26s    max=8.27s    p(90)=8.27s    p(95)=8.27s   
    iterations.................: 10     1.207088/s
    vus........................: 10     min=10 max=10
    vus_max....................: 10     min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_sp70O7R5_2020-03-06_0700.png)
  - Errors Per Second

     ![](../report/images/img_SK6TasTr_2020-03-06_0700.png)

  - Error log [here](../report/log/1_amount_transfer.js_error_log_2020-03-06_0700.md)
  - Requests per Second

     ![](../report/images/img_y1XTG6jU_2020-03-06_0700.png)
  - http_req_duration

     ![](../report/images/img_LDTS3yTP_2020-03-06_0700.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449204000&to=1583449245000>
- 2_collect_balance_syn.js <a name=2_collect_balance_syn.js></a>
  - 一键回收第三方平台余额异步
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 340  ✗ 0   
    data_received..............: 83 kB   99 kB/s
    data_sent..................: 73 kB   86 kB/s
    http_req_blocked...........: avg=1.64ms   min=2.03µs   med=3.94µs   max=24.38ms  p(90)=76.61µs  p(95)=22.7ms  
    http_req_connecting........: avg=1.62ms   min=0s       med=0s       max=23.95ms  p(90)=0s       p(95)=22.62ms 
    http_req_duration..........: avg=79.5ms   min=72.1ms   med=79.28ms  max=112.75ms p(90)=84.88ms  p(95)=87.88ms 
    http_req_receiving.........: avg=147.32µs min=28.68µs  med=92.74µs  max=3.85ms   p(90)=216.5µs  p(95)=353.26µs
    http_req_sending...........: avg=69.31µs  min=15.28µs  med=30.42µs  max=3.03ms   p(90)=125.16µs p(95)=158.78µs
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=79.28ms  min=71.96ms  med=79.06ms  max=112.53ms p(90)=84.65ms  p(95)=87.41ms 
    http_reqs..................: 170     202.102619/s
    iteration_duration.........: avg=1.18s    min=797.43ms med=822.47ms max=3.55s    p(90)=2.32s    p(95)=2.97s   
    iterations.................: 10      11.888389/s
    vus........................: 10      min=10 max=10
    vus_max....................: 10      min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_Yyu7DIhw_2020-03-06_0701.png)
  - Requests per Second

     ![](../report/images/img_6uuEKgFc_2020-03-06_0701.png)
  - http_req_duration

     ![](../report/images/img_SFYQAWQ1_2020-03-06_0701.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449245000&to=1583449281000>
- 3_transaction_record.js <a name=3_transaction_record.js></a>
  - 交易記錄
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 24578 ✗ 0    
    data_received..............: 415 MB  2.3 MB/s
    data_sent..................: 6.7 MB  37 kB/s
    http_req_blocked...........: avg=1.48ms   min=2.72µs   med=5.34µs   max=246.21ms p(90)=8.07µs   p(95)=45.6µs  
    http_req_connecting........: avg=1.46ms   min=0s       med=0s       max=245.58ms p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=281.87ms min=73.73ms  med=145.63ms max=4.39s    p(90)=362.38ms p(95)=980.68ms
    http_req_receiving.........: avg=41.64ms  min=63.62µs  med=27.04ms  max=1.18s    p(90)=52.32ms  p(95)=77.11ms 
    http_req_sending...........: avg=197.92µs min=13.15µs  med=30.3µs   max=110.23ms p(90)=80.73µs  p(95)=125.56µs
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=240.03ms min=73.48ms  med=115.25ms max=4.39s    p(90)=323.36ms p(95)=873.71ms
    http_reqs..................: 12290   68.276994/s
    iteration_duration.........: avg=6.32s    min=844.21ms med=6.19s    max=12.61s   p(90)=8.18s    p(95)=8.32s   
    iterations.................: 11780   65.443694/s
    vus........................: 1       min=1   max=500
    vus_max....................: 500     min=500 max=500

     ```
  - Virtual Users

     ![](../report/images/img_qVwF94r0_2020-03-06_0705.png)
  - Requests per Second

     ![](../report/images/img_6c2FcPS1_2020-03-06_0705.png)
  - http_req_duration

     ![](../report/images/img_cagc2Ubt_2020-03-06_0705.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449279000&to=1583449517000>
- 4_order_query.js <a name=4_order_query.js></a>
  - 注單查詢
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_5k9MQA2L_2020-03-06_0706.png)
  - Errors Per Second

     ![](../report/images/img_t1rr4Bs8_2020-03-06_0706.png)

  - Error log [here](../report/log/4_order_query.js_error_log_2020-03-06_0705.md)
  - Requests per Second

     ![](../report/images/img_nL5r0Zw1_2020-03-06_0706.png)
  - http_req_duration

     ![](../report/images/img_yOwHi2P2_2020-03-06_0706.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449517000&to=1583449578000>
- 5_charge_withdraw_record.js <a name=5_charge_withdraw_record.js></a>
  - 充值提款記錄
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_44dqbwqh_2020-03-06_0707.png)
  - Errors Per Second

     ![](../report/images/img_wMMXptSs_2020-03-06_0707.png)

  - Error log [here](../report/log/5_charge_withdraw_record.js_error_log_2020-03-06_0706.md)
  - Requests per Second

     ![](../report/images/img_X4fIPMzJ_2020-03-06_0707.png)
  - http_req_duration

     ![](../report/images/img_eNqrd0BE_2020-03-06_0707.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449579000&to=1583449638000>
- 6_report_query.js <a name=6_report_query.js></a>
  - 報表查詢
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_wUSex10S_2020-03-06_0708.png)
  - Errors Per Second

     ![](../report/images/img_lrsWUesn_2020-03-06_0708.png)

  - Error log [here](../report/log/6_report_query.js_error_log_2020-03-06_0707.md)
  - Requests per Second

     ![](../report/images/img_Y8SVLyyR_2020-03-06_0708.png)
  - http_req_duration

     ![](../report/images/img_VoFShKPE_2020-03-06_0708.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449638000&to=1583449699000>
- 7_charge.js <a name=7_charge.js></a>
  - 我要充值
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_iMpTOroc_2020-03-06_0709.png)
  - Errors Per Second

     ![](../report/images/img_ls02DF4v_2020-03-06_0709.png)

  - Error log [here](../report/log/7_charge.js_error_log_2020-03-06_0708.md)
  - Requests per Second

     ![](../report/images/img_5yFCGWiR_2020-03-06_0709.png)
  - http_req_duration

     ![](../report/images/img_wTx4xspd_2020-03-06_0709.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449700000&to=1583449796000>
- 8_withdraw.js <a name=8_withdraw.js></a>
  - 我要提款
  - k6 result

     ```bash
    checks.....................: 80.76% ✓ 210  ✗ 50  
    data_received..............: 131 kB 126 kB/s
    data_sent..................: 59 kB  57 kB/s
  ✗ errors.....................: 50     48.167353/s
    http_req_blocked...........: avg=2.79ms   min=2.01µs   med=3.51µs   max=45.26ms  p(90)=57.62µs  p(95)=28.56ms 
    http_req_connecting........: avg=2.58ms   min=0s       med=0s       max=35.31ms  p(90)=0s       p(95)=25.53ms 
    http_req_duration..........: avg=83.94ms  min=72.21ms  med=81.51ms  max=121.16ms p(90)=95.25ms  p(95)=101.76ms
    http_req_receiving.........: avg=135.84µs min=38.15µs  med=83.2µs   max=3.76ms   p(90)=193.89µs p(95)=289.69µs
    http_req_sending...........: avg=60.81µs  min=14.6µs   med=25.58µs  max=1.34ms   p(90)=103.33µs p(95)=188.76µs
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=83.74ms  min=72.11ms  med=81.3ms   max=121.04ms p(90)=95.1ms   p(95)=101.66ms
    http_reqs..................: 130    125.235119/s
    iteration_duration.........: avg=977.17ms min=898.98ms med=966.52ms max=1.09s    p(90)=1.02s    p(95)=1.05s   
    iterations.................: 10     9.633471/s
    vus........................: 10     min=10 max=10
    vus_max....................: 10     min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_dz1olS9p_2020-03-06_0710.png)
  - Errors Per Second

     ![](../report/images/img_HycLdUnA_2020-03-06_0710.png)

  - Error log [here](../report/log/8_withdraw.js_error_log_2020-03-06_0709.md)
  - Requests per Second

     ![](../report/images/img_gJ4HBfEy_2020-03-06_0710.png)
  - http_req_duration

     ![](../report/images/img_7Ik0cH8j_2020-03-06_0710.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449797000&to=1583449829000>
- 9_auth.js <a name=9_auth.js></a>
  - 鉴权服务接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 50518 ✗ 0    
    data_received..............: 13 MB   70 kB/s
    data_sent..................: 11 MB   59 kB/s
    http_req_blocked...........: avg=487.43µs min=1.46µs   med=4.06µs  max=373.87ms p(90)=6.23µs  p(95)=9.62µs  
    http_req_connecting........: avg=478.44µs min=0s       med=0s      max=373.79ms p(90)=0s      p(95)=0s      
    http_req_duration..........: avg=79.89ms  min=71.37ms  med=78.93ms max=1.08s    p(90)=84.43ms p(95)=91.34ms 
    http_req_receiving.........: avg=114.33µs min=21.63µs  med=78.58µs max=20.57ms  p(90)=142.3µs p(95)=174.37µs
    http_req_sending...........: avg=44.66µs  min=10.68µs  med=25.35µs max=16.27ms  p(90)=56.21µs p(95)=88.92µs 
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s      
    http_req_waiting...........: avg=79.73ms  min=71.3ms   med=78.8ms  max=1.08s    p(90)=84.17ms p(95)=91.12ms 
    http_reqs..................: 25259   140.325551/s
    iteration_duration.........: avg=6.15s    min=838.22ms med=6.15s   max=8.64s    p(90)=8.15s   p(95)=8.16s   
    iterations.................: 12130   67.38782/s
    vus........................: 1       min=1   max=500
    vus_max....................: 500     min=500 max=500

     ```
  - Virtual Users

     ![](../report/images/img_L6K3bxGI_2020-03-06_0714.png)
  - Requests per Second

     ![](../report/images/img_nCIhycpY_2020-03-06_0714.png)
  - http_req_duration

     ![](../report/images/img_XfS36wUg_2020-03-06_0714.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583449829000&to=1583450067000>
- 10_thirdparty.js <a name=10_thirdparty.js></a>
  - 第三方平台接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 49  ✗ 0  
    data_received..............: 42 kB   5.7 kB/s
    data_sent..................: 13 kB   1.8 kB/s
    http_req_blocked...........: avg=13.64ms  min=2.92µs   med=6.96µs   max=270.03ms p(90)=23.4ms   p(95)=24.05ms 
    http_req_connecting........: avg=3.09ms   min=0s       med=0s       max=23.48ms  p(90)=16.83ms  p(95)=23.31ms 
    http_req_duration..........: avg=142.97ms min=74.67ms  med=80.69ms  max=796.28ms p(90)=188.3ms  p(95)=638.79ms
    http_req_receiving.........: avg=736.51µs min=77.6µs   med=144.15µs max=14.3ms   p(90)=392.18µs p(95)=409.42µs
    http_req_sending...........: avg=64.7µs   min=23.46µs  med=47.3µs   max=202.11µs p(90)=107.39µs p(95)=152.56µs
    http_req_tls_handshaking...: avg=6.08ms   min=0s       med=0s       max=152.23ms p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=142.17ms min=74.43ms  med=80.5ms   max=795.99ms p(90)=179.62ms p(95)=635.78ms
    http_reqs..................: 25      3.441727/s
    iteration_duration.........: avg=3.03s    min=829.16ms med=1s       max=7.26s    p(90)=6.01s    p(95)=6.63s   
    iterations.................: 1       0.137669/s
    vus........................: 1       min=1 max=1
    vus_max....................: 1       min=1 max=1

     ```
  - Virtual Users

     ![](../report/images/img_yuiAwAUF_2020-03-06_0714.png)
  - Requests per Second

     ![](../report/images/img_bG8hzqXB_2020-03-06_0714.png)
  - http_req_duration

     ![](../report/images/img_yLFzfzOA_2020-03-06_0714.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583450068000&to=1583450104000>
- 11_payment.js <a name=11_payment.js></a>
  - 第三方支付接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 64741 ✗ 0    
    data_received..............: 893 MB  5.0 MB/s
    data_sent..................: 14 MB   75 kB/s
    http_req_blocked...........: avg=2.36ms   min=1.48µs   med=4.35µs   max=1.06s   p(90)=6.41µs   p(95)=17.8µs  
    http_req_connecting........: avg=2.31ms   min=0s       med=0s       max=1.06s   p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=546.04ms min=71.72ms  med=428.35ms max=4.46s   p(90)=959.83ms p(95)=1.41s   
    http_req_receiving.........: avg=23.58ms  min=20.85µs  med=84.44µs  max=1.27s   p(90)=117.94ms p(95)=186.86ms
    http_req_sending...........: avg=1.3ms    min=9.91µs   med=27.86µs  max=934.9ms p(90)=58.57µs  p(95)=98.62µs 
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s      p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=521.14ms min=71.59ms  med=401.02ms max=4.44s   p(90)=940.87ms p(95)=1.38s   
    http_reqs..................: 32395   179.971293/s
    iteration_duration.........: avg=9.39s    min=853.81ms med=9.31s    max=21.04s  p(90)=12.17s   p(95)=13.2s   
    iterations.................: 7660    42.555336/s
    vus........................: 1       min=1   max=500
    vus_max....................: 500     min=500 max=500

     ```
  - Virtual Users

     ![](../report/images/img_q1IF68ks_2020-03-06_0718.png)
  - Requests per Second

     ![](../report/images/img_tgirsRyH_2020-03-06_0718.png)
  - http_req_duration

     ![](../report/images/img_Qy5CZVDU_2020-03-06_0718.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583450102000&to=1583450341000>
- 12_web.js <a name=12_web.js></a>
  - 网站管理服务接口
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_SEexHpfi_2020-03-06_0719.png)
  - Errors Per Second

     ![](../report/images/img_tec5Tr9u_2020-03-06_0719.png)

  - Error log [here](../report/log/12_web.js_error_log_2020-03-06_0719.md)
  - Requests per Second

     ![](../report/images/img_DrC18T6B_2020-03-06_0720.png)
  - http_req_duration

     ![](../report/images/img_2Emxoma9_2020-03-06_0720.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583450344000&to=1583450406000>
- 13_lottery.js <a name=13_lottery.js></a>
  - 彩票注单
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_20H5Ctn8_2020-03-06_0720.png)
  - Errors Per Second

     ![](../report/images/img_wQ2yqc8d_2020-03-06_0720.png)

  - Error log [here](../report/log/13_lottery.js_error_log_2020-03-06_0720.md)
  - Requests per Second

     ![](../report/images/img_BWSFeF8w_2020-03-06_0720.png)
  - http_req_duration

     ![](../report/images/img_IV8vBvcy_2020-03-06_0721.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583450407000&to=1583450464000>
- 14_notice.js <a name=14_notice.js></a>
  - 公告服务接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 139424 ✗ 0    
    data_received..............: 209 MB  1.2 MB/s
    data_sent..................: 28 MB   155 kB/s
    http_req_blocked...........: avg=370.08µs min=1.5µs    med=3.59µs   max=127.2ms  p(90)=5.13µs   p(95)=11.95µs 
    http_req_connecting........: avg=360.74µs min=0s       med=0s       max=127.06ms p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=255.22ms min=71.23ms  med=111.12ms max=6.61s    p(90)=655.15ms p(95)=898.35ms
    http_req_receiving.........: avg=3.81ms   min=20.21µs  med=68.83µs  max=559.63ms p(90)=22.94ms  p(95)=24.37ms 
    http_req_sending...........: avg=68.75µs  min=10.42µs  med=21.81µs  max=156.79ms p(90)=52.06µs  p(95)=80.1µs  
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=251.33ms min=71.14ms  med=103.19ms max=6.59s    p(90)=649.83ms p(95)=891.74ms
    http_reqs..................: 69712   387.284896/s
    iteration_duration.........: avg=7.82s    min=802.07ms med=7.77s    max=16.66s   p(90)=10.03s   p(95)=10.71s  
    iterations.................: 9492    52.73279/s
    vus........................: 1       min=1    max=500
    vus_max....................: 500     min=500  max=500

     ```
  - Virtual Users

     ![](../report/images/img_I0AML2Ep_2020-03-06_0724.png)
  - Requests per Second

     ![](../report/images/img_UxcvR2qT_2020-03-06_0724.png)
  - http_req_duration

     ![](../report/images/img_rzv51OcT_2020-03-06_0724.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583450465000&to=1583450702000>
- 15_host.js <a name=15_host.js></a>
  - 厅主接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 203052 ✗ 0    
    data_received..............: 157 MB  873 kB/s
    data_sent..................: 37 MB   205 kB/s
    http_req_blocked...........: avg=408.46µs min=1.28µs  med=3.52µs  max=205.97ms p(90)=4.97µs   p(95)=16.22µs 
    http_req_connecting........: avg=398.14µs min=0s      med=0s      max=205.37ms p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=97.81ms  min=69.57ms med=82.64ms max=1.9s     p(90)=124.55ms p(95)=155.21ms
    http_req_receiving.........: avg=225.64µs min=20.51µs med=61.47µs max=141.36ms p(90)=149.82µs p(95)=299.49µs
    http_req_sending...........: avg=82.51µs  min=10.29µs med=20.93µs max=110.94ms p(90)=50.28µs  p(95)=79.32µs 
    http_req_tls_handshaking...: avg=0s       min=0s      med=0s      max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=97.51ms  min=69.51ms med=82.47ms max=1.9s     p(90)=123.78ms p(95)=154.16ms
    http_reqs..................: 101526  564.033116/s
    iteration_duration.........: avg=6.85s    min=839.7ms med=6.78s   max=11s      p(90)=8.79s    p(95)=8.94s   
    iterations.................: 10830   60.166644/s
    vus........................: 1       min=1    max=500
    vus_max....................: 500     min=500  max=500

     ```
  - Virtual Users

     ![](../report/images/img_77U02wDp_2020-03-06_0728.png)
  - Requests per Second

     ![](../report/images/img_QyMI4NrW_2020-03-06_0728.png)
  - http_req_duration

     ![](../report/images/img_B4x2pOXZ_2020-03-06_0728.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583450707000&to=1583450944000>
- 16_user.js <a name=16_user.js></a>
  - 用户服务接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 260  ✗ 0   
    data_received..............: 73 kB   5.4 kB/s
    data_sent..................: 62 kB   4.6 kB/s
    http_req_blocked...........: avg=2.29ms   min=2.1µs    med=4.52µs  max=28.54ms  p(90)=14.37µs  p(95)=24.25ms 
    http_req_connecting........: avg=2.26ms   min=0s       med=0s      max=28.47ms  p(90)=0s       p(95)=23.58ms 
    http_req_duration..........: avg=128.38ms min=73.66ms  med=84.93ms max=319.33ms p(90)=296.27ms p(95)=302.5ms 
    http_req_receiving.........: avg=132.34µs min=32.71µs  med=92.34µs max=3.25ms   p(90)=175.85µs p(95)=214.26µs
    http_req_sending...........: avg=64.12µs  min=16.05µs  med=29.5µs  max=2.44ms   p(90)=90.33µs  p(95)=113.57µs
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s      max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=128.18ms min=72.11ms  med=84.81ms max=319.2ms  p(90)=295.94ms p(95)=302.32ms
    http_reqs..................: 130     9.521632/s
    iteration_duration.........: avg=9.5s     min=829.25ms med=10.61s  max=13.65s   p(90)=12.62s   p(95)=13.08s  
    iterations.................: 10      0.732433/s
    vus........................: 10      min=10 max=10
    vus_max....................: 10      min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_6yX1Hwyd_2020-03-06_0729.png)
  - Requests per Second

     ![](../report/images/img_k9NG7MCP_2020-03-06_0729.png)
  - http_req_duration

     ![](../report/images/img_ZsZoT6qM_2020-03-06_0729.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583450951000&to=1583450997000>
