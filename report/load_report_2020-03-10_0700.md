
### Environment ###

Host | sn 
--- | --- 
http://ae.bg1207.com | ae00 
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | --- 
10.37.3.28 | Ubuntu 18.04.3 LTS | 2020/03/10 07:00:01
### Parameter ###

Config | Users | MAX_VU 
--- | --- | --- 
load | 10 | 500
### Grafana ###

URL | Username | Password
--- | --- | ---
http://10.37.3.28:3000 | admin | admin123
### Network ###
- Connected(http://ae.bg1207.com)

### Testing Result ###
1. [额度转换](#1_amount_transfer.js)
2. [一键回收第三方平台余额异步](#2_collect_balance_syn.js)
3. [交易記錄](#3_transaction_record.js)
4. [注單查詢](#4_order_query.js)
5. [充值提款記錄](#5_charge_withdraw_record.js)
6. [報表查詢](#6_report_query.js)
7. [我要充值](#7_charge.js)
8. [我要提款](#8_withdraw.js)
9. [鉴权服务接口](#9_auth.js)
10. [第三方平台接口](#10_thirdparty.js)
11. [第三方支付接口](#11_payment.js)
12. [网站管理服务接口](#12_web.js)
13. [彩票注单](#13_lottery.js)
14. [公告服务接口](#14_notice.js)
15. [厅主接口](#15_host.js)
16. [用户服务接口](#16_user.js)

- 1_amount_transfer.js <a name=1_amount_transfer.js></a>
  - 额度转换
  - k6 result

     ```bash
    checks.....................: 93.33% ✓ 140  ✗ 10  
    data_received..............: 50 kB  6.0 kB/s
    data_sent..................: 31 kB  3.7 kB/s
  ✗ errors.....................: 10     1.199577/s
    http_req_blocked...........: avg=4.01ms   min=2.28µs  med=6.99µs   max=25.23ms  p(90)=23.02ms  p(95)=23.44ms 
    http_req_connecting........: avg=3.97ms   min=0s      med=0s       max=24.38ms  p(90)=22.89ms  p(95)=23.29ms 
    http_req_duration..........: avg=95.68ms  min=73.56ms med=90.28ms  max=165.2ms  p(90)=127.87ms p(95)=134.69ms
    http_req_receiving.........: avg=244.63µs min=37.42µs med=130.14µs max=5.39ms   p(90)=381.27µs p(95)=484.46µs
    http_req_sending...........: avg=71.75µs  min=15.73µs med=54.23µs  max=393.18µs p(90)=149.2µs  p(95)=200.33µs
    http_req_tls_handshaking...: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=95.36ms  min=72.99ms med=90.02ms  max=165.07ms p(90)=127.77ms p(95)=134.59ms
    http_reqs..................: 70     8.397037/s
    iteration_duration.........: avg=5.44s    min=1.91s   med=5.81s    max=8.32s    p(90)=7.34s    p(95)=7.78s   
    iterations.................: 10     1.199577/s
    vus........................: 10     min=10 max=10
    vus_max....................: 10     min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_msV9asyr_2020-03-10_0700.png)
  - Errors Per Second

     ![](../report/images/img_HG4531kz_2020-03-10_0700.png)

  - Error log [here](../report/log/1_amount_transfer.js_error_log_2020-03-10_0700.md)
  - Requests per Second

     ![](../report/images/img_WmSRV3Jw_2020-03-10_0700.png)
  - http_req_duration

     ![](../report/images/img_rIqeRssO_2020-03-10_0700.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583794803000&to=1583794843000>
- 2_collect_balance_syn.js <a name=2_collect_balance_syn.js></a>
  - 一键回收第三方平台余额异步
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 340  ✗ 0   
    data_received..............: 83 kB   56 kB/s
    data_sent..................: 73 kB   49 kB/s
    http_req_blocked...........: avg=1.7ms    min=1.56µs  med=5.51µs   max=26.02ms  p(90)=14.28µs  p(95)=23.26ms 
    http_req_connecting........: avg=1.67ms   min=0s      med=0s       max=25.86ms  p(90)=0s       p(95)=22.77ms 
    http_req_duration..........: avg=116.1ms  min=72.29ms med=76.84ms  max=713.25ms p(90)=94.58ms  p(95)=685.75ms
    http_req_receiving.........: avg=188.77µs min=36.21µs med=100.51µs max=10.54ms  p(90)=245.33µs p(95)=317.53µs
    http_req_sending...........: avg=56.85µs  min=10.87µs med=38.9µs   max=308.43µs p(90)=125.85µs p(95)=166.62µs
    http_req_tls_handshaking...: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=115.86ms min=72.21ms med=76.67ms  max=713.13ms p(90)=94.32ms  p(95)=685.65ms
    http_reqs..................: 170     115.560322/s
    iteration_duration.........: avg=1.71s    min=1.44s   med=1.45s    max=3.51s    p(90)=2.34s    p(95)=2.92s   
    iterations.................: 10      6.797666/s
    vus........................: 10      min=10 max=10
    vus_max....................: 10      min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_CT1bdJBK_2020-03-10_0701.png)
  - Requests per Second

     ![](../report/images/img_0bbtzHGy_2020-03-10_0701.png)
  - http_req_duration

     ![](../report/images/img_h2UJlmOd_2020-03-10_0701.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583794843000&to=1583794880000>
- 3_transaction_record.js <a name=3_transaction_record.js></a>
  - 交易記錄
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 24636 ✗ 0    
    data_received..............: 416 MB  2.3 MB/s
    data_sent..................: 6.7 MB  38 kB/s
    http_req_blocked...........: avg=1.15ms   min=2.52µs   med=5.22µs   max=132.13ms p(90)=9.17µs   p(95)=39.03µs 
    http_req_connecting........: avg=1.13ms   min=0s       med=0s       max=131.96ms p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=263.81ms min=75.45ms  med=140.54ms max=4.22s    p(90)=335.49ms p(95)=949.81ms
    http_req_receiving.........: avg=42.73ms  min=55.7µs   med=25.04ms  max=1.1s     p(90)=48.47ms  p(95)=76.91ms 
    http_req_sending...........: avg=115.96µs min=14.08µs  med=29.42µs  max=76.2ms   p(90)=79.98µs  p(95)=125.7µs 
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=220.96ms min=75.23ms  med=113.3ms  max=3.31s    p(90)=295.17ms p(95)=806.52ms
    http_reqs..................: 12318   68.433255/s
    iteration_duration.........: avg=6.29s    min=803.54ms med=6.18s    max=12.27s   p(90)=8.18s    p(95)=8.27s   
    iterations.................: 11817   65.649925/s
    vus........................: 1       min=1   max=500
    vus_max....................: 500     min=500 max=500

     ```
  - Virtual Users

     ![](../report/images/img_Sw5bzOuy_2020-03-10_0705.png)
  - Requests per Second

     ![](../report/images/img_SuliHs48_2020-03-10_0705.png)
  - http_req_duration

     ![](../report/images/img_FY8G7Mmg_2020-03-10_0705.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583794877000&to=1583795116000>
- 4_order_query.js <a name=4_order_query.js></a>
  - 注單查詢
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_PkozgvXJ_2020-03-10_0706.png)
  - Errors Per Second

     ![](../report/images/img_eCjQQIAC_2020-03-10_0706.png)

  - Error log [here](../report/log/4_order_query.js_error_log_2020-03-10_0705.md)
  - Requests per Second

     ![](../report/images/img_z0RPNgJQ_2020-03-10_0706.png)
  - http_req_duration

     ![](../report/images/img_4cENgj4w_2020-03-10_0706.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795117000&to=1583795179000>
- 5_charge_withdraw_record.js <a name=5_charge_withdraw_record.js></a>
  - 充值提款記錄
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_gO0UXPKj_2020-03-10_0707.png)
  - Errors Per Second

     ![](../report/images/img_crMjq73g_2020-03-10_0707.png)

  - Error log [here](../report/log/5_charge_withdraw_record.js_error_log_2020-03-10_0706.md)
  - Requests per Second

     ![](../report/images/img_8H1BSjGD_2020-03-10_0707.png)
  - http_req_duration

     ![](../report/images/img_WD4zvHb8_2020-03-10_0707.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795179000&to=1583795236000>
- 6_report_query.js <a name=6_report_query.js></a>
  - 報表查詢
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_QoXKYjjK_2020-03-10_0708.png)
  - Errors Per Second

     ![](../report/images/img_07JG58uj_2020-03-10_0708.png)

  - Error log [here](../report/log/6_report_query.js_error_log_2020-03-10_0707.md)
  - Requests per Second

     ![](../report/images/img_ilAvetUK_2020-03-10_0708.png)
  - http_req_duration

     ![](../report/images/img_bxSivF9R_2020-03-10_0708.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795237000&to=1583795298000>
- 7_charge.js <a name=7_charge.js></a>
  - 我要充值
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_XlP0E1v9_2020-03-10_0709.png)
  - Errors Per Second

     ![](../report/images/img_pOe5lHqh_2020-03-10_0709.png)

  - Error log [here](../report/log/7_charge.js_error_log_2020-03-10_0708.md)
  - Requests per Second

     ![](../report/images/img_9XqNQgiS_2020-03-10_0709.png)
  - http_req_duration

     ![](../report/images/img_mRxrrhor_2020-03-10_0709.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795298000&to=1583795401000>
- 8_withdraw.js <a name=8_withdraw.js></a>
  - 我要提款
  - k6 result

     ```bash
    checks.....................: 80.76% ✓ 210  ✗ 50  
    data_received..............: 131 kB 139 kB/s
    data_sent..................: 59 kB  63 kB/s
  ✗ errors.....................: 50     52.932077/s
    http_req_blocked...........: avg=2.36ms   min=1.85µs   med=3.46µs   max=46.82ms  p(90)=28.5µs   p(95)=23.12ms 
    http_req_connecting........: avg=2.13ms   min=0s       med=0s       max=24.2ms   p(90)=0s       p(95)=22.97ms 
    http_req_duration..........: avg=79ms     min=72.47ms  med=76.86ms  max=106.88ms p(90)=87.84ms  p(95)=90.66ms 
    http_req_receiving.........: avg=208.57µs min=32.54µs  med=83.01µs  max=6.55ms   p(90)=234.27µs p(95)=280.73µs
    http_req_sending...........: avg=43.23µs  min=11.67µs  med=26.98µs  max=255.88µs p(90)=85.4µs   p(95)=149.04µs
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=78.75ms  min=72.36ms  med=76.65ms  max=106.59ms p(90)=87.76ms  p(95)=90.44ms 
    http_reqs..................: 130    137.6234/s
    iteration_duration.........: avg=924.69ms min=819.68ms med=915.76ms max=1.11s    p(90)=931.14ms p(95)=1.01s   
    iterations.................: 10     10.586415/s
    vus........................: 10     min=10 max=10
    vus_max....................: 10     min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_SD0ZPOMj_2020-03-10_0710.png)
  - Errors Per Second

     ![](../report/images/img_zKLi05KT_2020-03-10_0710.png)

  - Error log [here](../report/log/8_withdraw.js_error_log_2020-03-10_0710.md)
  - Requests per Second

     ![](../report/images/img_A6gBD3S9_2020-03-10_0710.png)
  - http_req_duration

     ![](../report/images/img_kp8TE2YC_2020-03-10_0710.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795402000&to=1583795434000>
- 9_auth.js <a name=9_auth.js></a>
  - 鉴权服务接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 50480 ✗ 0    
    data_received..............: 13 MB   70 kB/s
    data_sent..................: 11 MB   59 kB/s
    http_req_blocked...........: avg=475.1µs  min=1.53µs   med=4µs     max=45.18ms  p(90)=6.47µs   p(95)=11.88µs 
    http_req_connecting........: avg=466.19µs min=0s       med=0s      max=45.07ms  p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=78.83ms  min=71.41ms  med=74.74ms max=654.05ms p(90)=82.07ms  p(95)=89.47ms 
    http_req_receiving.........: avg=117.92µs min=20.84µs  med=79.16µs max=83.94ms  p(90)=144.12µs p(95)=180.85µs
    http_req_sending...........: avg=43.72µs  min=10.73µs  med=25.05µs max=11.3ms   p(90)=57.89µs  p(95)=93.53µs 
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s      max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=78.67ms  min=71.35ms  med=74.6ms  max=653.97ms p(90)=81.88ms  p(95)=89.28ms 
    http_reqs..................: 25240   140.222172/s
    iteration_duration.........: avg=6.14s    min=809.27ms med=6.15s   max=8.71s    p(90)=8.15s    p(95)=8.16s   
    iterations.................: 12119   67.327754/s
    vus........................: 1       min=1   max=500
    vus_max....................: 500     min=500 max=500

     ```
  - Virtual Users

     ![](../report/images/img_QIwVl8jT_2020-03-10_0714.png)
  - Requests per Second

     ![](../report/images/img_lcCxvKVA_2020-03-10_0714.png)
  - http_req_duration

     ![](../report/images/img_3qlwyPoS_2020-03-10_0714.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795434000&to=1583795670000>
- 10_thirdparty.js <a name=10_thirdparty.js></a>
  - 第三方平台接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 49  ✗ 0  
    data_received..............: 41 kB   4.2 kB/s
    data_sent..................: 13 kB   1.3 kB/s
    http_req_blocked...........: avg=16.93ms  min=3µs      med=7.33µs   max=352.67ms p(90)=23.29ms  p(95)=24.04ms 
    http_req_connecting........: avg=2.96ms   min=0s       med=0s       max=24.05ms  p(90)=15.36ms  p(95)=22.87ms 
    http_req_duration..........: avg=120.88ms min=23.62ms  med=78.06ms  max=745.75ms p(90)=117.03ms p(95)=416.75ms
    http_req_receiving.........: avg=261.97µs min=72.86µs  med=170.58µs max=1.22ms   p(90)=435.32µs p(95)=671.94µs
    http_req_sending...........: avg=67.97µs  min=23.11µs  med=52.26µs  max=233.95µs p(90)=129.27µs p(95)=153.74µs
    http_req_tls_handshaking...: avg=9.07ms   min=0s       med=0s       max=226.86ms p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=120.55ms min=22.83ms  med=77.78ms  max=745.47ms p(90)=116.72ms p(95)=416.54ms
    http_reqs..................: 25      2.537521/s
    iteration_duration.........: avg=3.88s    min=798.92ms med=1.01s    max=9.85s    p(90)=8.08s    p(95)=8.96s   
    iterations.................: 1       0.101501/s
    vus........................: 1       min=1 max=1
    vus_max....................: 1       min=1 max=1

     ```
  - Virtual Users

     ![](../report/images/img_p8U7cMBB_2020-03-10_0714.png)
  - Requests per Second

     ![](../report/images/img_99FneSBu_2020-03-10_0715.png)
  - http_req_duration

     ![](../report/images/img_5rUQljUm_2020-03-10_0715.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795670000&to=1583795709000>
- 11_payment.js <a name=11_payment.js></a>
  - 第三方支付接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 66203 ✗ 0    
    data_received..............: 912 MB  5.1 MB/s
    data_sent..................: 14 MB   77 kB/s
    http_req_blocked...........: avg=2.15ms   min=1.59µs   med=4.28µs   max=781.93ms p(90)=6.3µs    p(95)=17.7µs 
    http_req_connecting........: avg=2.08ms   min=0s       med=0s       max=781.81ms p(90)=0s       p(95)=0s     
    http_req_duration..........: avg=491.43ms min=71.52ms  med=427.33ms max=5.37s    p(90)=817.12ms p(95)=1.06s  
    http_req_receiving.........: avg=21.36ms  min=16.75µs  med=83.67µs  max=5.15s    p(90)=103.32ms p(95)=175.5ms
    http_req_sending...........: avg=1.12ms   min=11.04µs  med=26.87µs  max=681.3ms  p(90)=57.6µs   p(95)=94.41µs
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s     
    http_req_waiting...........: avg=468.94ms min=71.45ms  med=404.12ms max=4.24s    p(90)=803.29ms p(95)=1.04s  
    http_reqs..................: 33112   183.95515/s
    iteration_duration.........: avg=9.13s    min=808.78ms med=9.13s    max=15.65s   p(90)=11.57s   p(95)=12.19s 
    iterations.................: 7887    43.81657/s
    vus........................: 1       min=1   max=500
    vus_max....................: 500     min=500 max=500

     ```
  - Virtual Users

     ![](../report/images/img_vBkfJkXd_2020-03-10_0718.png)
  - Requests per Second

     ![](../report/images/img_OZrGxxnk_2020-03-10_0718.png)
  - http_req_duration

     ![](../report/images/img_0ccV6dHN_2020-03-10_0718.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795707000&to=1583795944000>
- 12_web.js <a name=12_web.js></a>
  - 网站管理服务接口
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_veCiE60H_2020-03-10_0719.png)
  - Errors Per Second

     ![](../report/images/img_QZdadyMI_2020-03-10_0719.png)

  - Error log [here](../report/log/12_web.js_error_log_2020-03-10_0719.md)
  - Requests per Second

     ![](../report/images/img_rQ2WbOOS_2020-03-10_0720.png)
  - http_req_duration

     ![](../report/images/img_oUaTitrY_2020-03-10_0720.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583795946000&to=1583796007000>
- 13_lottery.js <a name=13_lottery.js></a>
  - 彩票注单
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_EktAElcZ_2020-03-10_0720.png)
  - Errors Per Second

     ![](../report/images/img_PmjuA5bN_2020-03-10_0720.png)

  - Error log [here](../report/log/13_lottery.js_error_log_2020-03-10_0720.md)
  - Requests per Second

     ![](../report/images/img_uFq2DRGT_2020-03-10_0721.png)
  - http_req_duration

     ![](../report/images/img_a4oeyenS_2020-03-10_0721.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583796007000&to=1583796065000>
- 14_notice.js <a name=14_notice.js></a>
  - 公告服务接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 149930 ✗ 0    
    data_received..............: 232 MB  1.3 MB/s
    data_sent..................: 30 MB   167 kB/s
    http_req_blocked...........: avg=357.11µs min=1.32µs   med=3.51µs  max=153.43ms p(90)=5µs      p(95)=11.94µs 
    http_req_connecting........: avg=346.89µs min=0s       med=0s      max=152.98ms p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=180.19ms min=71.66ms  med=103.3ms max=4.09s    p(90)=387.08ms p(95)=529.06ms
    http_req_receiving.........: avg=3.88ms   min=19.96µs  med=66.53µs max=1.06s    p(90)=23.76ms  p(95)=25ms    
    http_req_sending...........: avg=60.62µs  min=10.08µs  med=20.8µs  max=77.75ms  p(90)=49.77µs  p(95)=78.38µs 
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s      max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=176.25ms min=71.55ms  med=97.97ms max=4.04s    p(90)=380.98ms p(95)=523.05ms
    http_reqs..................: 74965   416.471438/s
    iteration_duration.........: avg=7.19s    min=800.89ms med=7.14s   max=14.16s   p(90)=9.14s    p(95)=9.46s   
    iterations.................: 10257   56.983226/s
    vus........................: 1       min=1    max=500
    vus_max....................: 500     min=500  max=500

     ```
  - Virtual Users

     ![](../report/images/img_hrB8ZVoN_2020-03-10_0724.png)
  - Requests per Second

     ![](../report/images/img_jC0IN521_2020-03-10_0724.png)
  - http_req_duration

     ![](../report/images/img_z7bcIqLW_2020-03-10_0724.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583796066000&to=1583796302000>
- 15_host.js <a name=15_host.js></a>
  - 厅主接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 205876 ✗ 0    
    data_received..............: 158 MB  876 kB/s
    data_sent..................: 37 MB   208 kB/s
    http_req_blocked...........: avg=371.17µs min=1.34µs   med=3.45µs  max=109.1ms  p(90)=4.83µs   p(95)=15.83µs 
    http_req_connecting........: avg=362.05µs min=0s       med=0s      max=108.98ms p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=88.11ms  min=69.47ms  med=79.32ms max=1.48s    p(90)=106.28ms p(95)=122.51ms
    http_req_receiving.........: avg=196.53µs min=21.92µs  med=59.25µs max=148.77ms p(90)=143µs    p(95)=283.65µs
    http_req_sending...........: avg=68.12µs  min=9.95µs   med=20.15µs max=124.21ms p(90)=47.81µs  p(95)=75.97µs 
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s      max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=87.84ms  min=69.39ms  med=79.13ms max=1.48s    p(90)=105.85ms p(95)=121.65ms
    http_reqs..................: 102938  571.877244/s
    iteration_duration.........: avg=6.79s    min=797.45ms med=6.76s   max=10.08s   p(90)=8.76s    p(95)=8.84s   
    iterations.................: 10969   60.938832/s
    vus........................: 1       min=1    max=500
    vus_max....................: 500     min=500  max=500

     ```
  - Virtual Users

     ![](../report/images/img_Wp36YrKN_2020-03-10_0728.png)
  - Requests per Second

     ![](../report/images/img_Ooj7AG4s_2020-03-10_0728.png)
  - http_req_duration

     ![](../report/images/img_W8z6Cfbx_2020-03-10_0728.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583796307000&to=1583796545000>
- 16_user.js <a name=16_user.js></a>
  - 用户服务接口
  - k6 result

     ```bash
    checks.....................: 100.00% ✓ 260  ✗ 0   
    data_received..............: 73 kB   5.5 kB/s
    data_sent..................: 62 kB   4.6 kB/s
    http_req_blocked...........: avg=2.22ms   min=2.19µs  med=4.02µs  max=25.3ms   p(90)=51.17µs  p(95)=23.84ms 
    http_req_connecting........: avg=2.18ms   min=0s      med=0s      max=24.54ms  p(90)=0s       p(95)=23.45ms 
    http_req_duration..........: avg=113.96ms min=73.41ms med=81.95ms max=359.38ms p(90)=241.65ms p(95)=263.13ms
    http_req_receiving.........: avg=158.3µs  min=35.01µs med=74.99µs max=4.05ms   p(90)=195.74µs p(95)=258.45µs
    http_req_sending...........: avg=57.92µs  min=15.45µs med=25.77µs max=1.21ms   p(90)=94µs     p(95)=142.87µs
    http_req_tls_handshaking...: avg=0s       min=0s      med=0s      max=0s       p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=113.74ms min=73.28ms med=81.88ms max=359.24ms p(90)=241.51ms p(95)=263.01ms
    http_reqs..................: 130     9.679625/s
    iteration_duration.........: avg=9.02s    min=785.7ms med=9.52s   max=13.39s   p(90)=12.48s   p(95)=12.9s   
    iterations.................: 10      0.744587/s
    vus........................: 10      min=10 max=10
    vus_max....................: 10      min=10 max=10

     ```
  - Virtual Users

     ![](../report/images/img_32cPpRXK_2020-03-10_0729.png)
  - Requests per Second

     ![](../report/images/img_grzGGTOx_2020-03-10_0729.png)
  - http_req_duration

     ![](../report/images/img_FAn1yPNb_2020-03-10_0729.png)
  - grafana url(admin/admin123)
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583796551000&to=1583796594000>
