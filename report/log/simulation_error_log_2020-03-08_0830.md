

```bash
[sn.lottery.order.query] -> 
{
  "id": "1583627455457.0555016302442270",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583627455457.0555016302442270",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 87,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.charge.list] -> 
{
  "id": "1583627455763.1262187820166020",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627455844.91209162599514642",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627455763.1262187820166020",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627455879.3687936099654443",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627455936.2811424985720178741",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627455879.3687936099654443",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627455884.9043452427971735",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627455968.229841945210405640",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627455884.9043452427971735",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583627456038.7420406463282095",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627456088.2968470719871272784",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456038.7420406463282095",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627456169.5335746354653451",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627456219.358036728781602820",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456169.5335746354653451",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627456409.2812795170198601",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627456469.7550264913125946806",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456409.2812795170198601",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 28,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627456542.5288712926258381",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627456622.8284070248328330176",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456542.5288712926258381",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627456648.7444050911408259",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627456699.1370783412150945654",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456648.7444050911408259",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627456724.6318773005974617",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627456797.6196855221600108440",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456724.6318773005974617",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627456832.9677266249422204",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627456882.2886603647221760068",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456832.9677266249422204",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627456843.3707796431771116",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627456917.2522302233618416150",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627456843.3707796431771116",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[sn.lottery.order.query] -> 
{
  "id": "1583627456910.5243077749240586",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583627456910.5243077749240586",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 32,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.order.query] -> 
{
  "id": "1583627457141.4448377940496601",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627457191.9115099575354370",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457141.4448377940496601",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583627457265.0638149740923981",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583627457265.0638149740923981",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 32,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583627457446.8751923574447723",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627457527.8070098403695901568",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457446.8751923574447723",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627457508.1423450418837051",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627457582.9213627819851972604",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457508.1423450418837051",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627457567.0116437325442748",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627457641.9220979429079824304",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457567.0116437325442748",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627457667.2563019784233364",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627457724.1297108302941405712",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457667.2563019784233364",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627457730.1805732874284093",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627457781.37299987440943116",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457730.1805732874284093",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627457790.3072346621325180",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627457840.36945957898813520",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457790.3072346621325180",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627457845.0743730988587142",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627457903.577646629014208516",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627457845.0743730988587142",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627458094.3922527315761931",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627458144.2595519346240930370",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627458094.3922527315761931",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627458220.6982907023171614",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627458271.9068960878004010684",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627458220.6982907023171614",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627458575.2565350282785167",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627458625.7835676189518577660",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627458575.2565350282785167",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627458771.0398971643226045",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627458850.108368450421802016",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627458771.0398971643226045",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627458832.8397941691458584",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627458905.2314859039460232448",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627458832.8397941691458584",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627458878.5379831974162698",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627458933.2401065664165969940",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627458878.5379831974162698",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627458932.1547980744706718",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627458983.2056596943240579818",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627458932.1547980744706718",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627459320.1718771522734058",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627459394.36028947428950304",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627459320.1718771522734058",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627459380.9337887311201579",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011327199C81874811A383174f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627459456.9079186134558160822",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627459380.9337887311201579",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011327199C81874811A383174f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583627459420.1076183574945459",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627459476.306529124045815888",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627459420.1076183574945459",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627459532.9732074136424222",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627459582.1143494265536772",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627459532.9732074136424222",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627459615.4275858648356077",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132110AECF159211A382ceec",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627459688.8935112918689627640",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627459615.4275858648356077",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627459714.1857429639062551",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132110AECF159211A382ceec",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627459769.7822752453420906144",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627459714.1857429639062551",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627459795.3226625473086563",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627459877.8896843432817523704",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627459795.3226625473086563",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847652
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583627460084.0429621448411598",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627460134.8835322496770095103",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460084.0429621448411598",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583627460036.0868242196622961",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583627460036.0868242196622961",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 32,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583627460157.9899255963953014",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627460241.777153004880022569",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460157.9899255963953014",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627460224.9741930210074898",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627460280.9004819628039339481",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460224.9741930210074898",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627460306.4219740896116612",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627460355.4856543012602880",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460306.4219740896116612",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627460317.5779523371501209",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132110AECF159211A382ceec",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627460367.8502750869914778752",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460317.5779523371501209",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627460497.6105522259877125",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627460552.8854006494102551227",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460497.6105522259877125",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627460699.3693900792299479",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627460780.81242919277183488",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460699.3693900792299479",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627460819.0932437564532635",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132110AECF159211A382ceec",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627460893.362012144008429696",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460819.0932437564532635",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627460919.3825815540745003",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132110AECF159211A382ceec",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627460972.8934837087392153599",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460919.3825815540745003",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627460997.4641090882357234",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461053.6917493428731248111",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627460997.4641090882357234",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627461066.8884870517510686",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461117.649380824135895044",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461066.8884870517510686",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627461089.4147391300542530",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461159.8051801577798483744",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461089.4147391300542530",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 23,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627461196.5830588322824696",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461253.806150395281932809",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461196.5830588322824696",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627461246.7248085084749328",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627461306.9169183647639797231",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461246.7248085084749328",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583627461275.6649496258291207",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461328.2910511183848079522",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461275.6649496258291207",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627461280.1945480785723938",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461336.7705465022981340368",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461280.1945480785723938",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627461350.9173870421542528",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461405.1310549076410975297",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461350.9173870421542528",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627461401.3231384358697585",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461458.2702767257815944724",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461401.3231384358697585",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627461422.7970085337011709",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132110AECF159211A382ceec",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627461496.76649156879401028",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461422.7970085337011709",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583627461588.1976431200015262",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461639.7772332788579024128",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461588.1976431200015262",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627461722.6135767886775121",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461797.1805961322606166208",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461722.6135767886775121",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627461823.5052918777183543",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461889.18911620483252882",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461823.5052918777183543",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627461903.8805089961274403",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627461998.6799202470017825248",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461903.8805089961274403",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627461963.0366109017890705",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462023.1734633655575249528",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627461963.0366109017890705",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627462034.4467922910664395",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462090.5619264483195549491",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462034.4467922910664395",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627462086.0117998690414646",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462159.8491514520608421856",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462086.0117998690414646",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627462185.9146883220087576",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462242.2310708348704784899",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462185.9146883220087576",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627462266.8076276466452650",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462345.8069020616211544896",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462266.8076276466452650",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627462371.0952506621777006",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462428.973503832896061456",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462371.0952506621777006",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627462377.9419441040907964",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462434.5691562016708099070",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462377.9419441040907964",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627462386.0823716880156314",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462470.7925204290218475260",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462386.0823716880156314",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627462426.1005485702465585",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462482.6840790674601606653",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462426.1005485702465585",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627462452.8653705831613741",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462508.3481302374039961633",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462452.8653705831613741",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 12,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627462449.5538151937381529",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132110AECF159211A382ceec",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627462520.2090798910951014501",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462449.5538151937381529",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 24,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627462466.7777140022001161",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627462535.8286600198779565037",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462466.7777140022001161",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 16,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583627462495.9761028250717682",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462552.3184044973194478024",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462495.9761028250717682",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 13,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627462448.0439615444758221",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627462551.7472562484389788906",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462448.0439615444758221",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847656
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 28,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627462612.6492478174259816",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462663.389738956410980362",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462612.6492478174259816",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627462682.9380236689792238",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462748.12456144610018356",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462682.9380236689792238",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627462692.7585700808263089",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462751.3028692740921639360",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462692.7585700808263089",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 12,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627462861.0584626442073074",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627462919.162767303329466124",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462861.0584626442073074",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627462995.1592961231543595",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627463047.9221676581060017078",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627462995.1592961231543595",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627463169.3993098000715013",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627463243.9151314305373568880",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463169.3993098000715013",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627463350.4856367269183729",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627463425.1162228332335466368",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463350.4856367269183729",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627463521.9818754208612421",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627463571.25653831605944842",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463521.9818754208612421",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627463587.2144641422635094",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627463643.6888180861049224056",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463587.2144641422635094",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627463597.5446103637983935",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627463653.5475692085103034360",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463597.5446103637983935",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627463689.2986316147981734",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627463739.9015995347912906",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463689.2986316147981734",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627463812.3302971739167563",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627463868.2017614162793202704",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463812.3302971739167563",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627463866.7421046002130162",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627463938.8925992756514193392",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463866.7421046002130162",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 23,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.audit.about.current.cancel] -> 
{
  "id": "1583627463857.1784610404268327",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f"
  }
}
[user.audit.about.current.cancel] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583627463949.8285954811223982080",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.cancel",
    "action": "null"
  },
  "id": "1583627463857.1784610404268327",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.cancel",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011327199C81874811A383174f",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 37,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.cancel]error code = 502
[user.charge.list] -> 
{
  "id": "1583627463891.6712991527949365",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627463978.8645556514326314746",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463891.6712991527949365",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627463975.7429099614917487",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464025.8068180589811793640",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627463975.7429099614917487",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627464016.0489162706321326",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464073.1234235364401217537",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464016.0489162706321326",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.audit.about.current.apply] -> 
{
  "id": "1583627464055.2473840704181340",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "payPasword": "n4zc4H6EiTJtfJ7V4mnWFd9hAAU=",
    "withdrawNo": "200307203104788389",
    "amount": 363
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583627464122.1578546889207333254",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583627464055.2473840704181340",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 363,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200307203104788389",
      "payPasword": "n4zc4H6EiTJtfJ7V4mnWFd9hAAU=",
      "sessionId": "ae0001011327199C81874811A383174f",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 15,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.apply]error code = 502
[user.audit.about.current.trace] -> 
{
  "id": "1583627464146.1809022391857510",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f"
  }
}
[user.audit.about.current.trace] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583627464201.9214355469649051648",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.trace",
    "action": "null"
  },
  "id": "1583627464146.1809022391857510",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.trace",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011327199C81874811A383174f",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.trace]error code = 502
[user.charge.list] -> 
{
  "id": "1583627464132.3497767794374035",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464211.295356109434193160",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464132.3497767794374035",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627464176.0625495188781910",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464226.9079168796764061502",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464176.0625495188781910",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627464179.2821183144791359",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464235.903266399278858628",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464179.2821183144791359",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583627464188.4738802390899135",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464244.236448944828271896",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464188.4738802390899135",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.audit.about.current.get] -> 
{
  "id": "1583627464227.0305534342350454",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583627464284.4040291816277885056",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583627464227.0305534342350454",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011327199C81874811A383174f",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.get]error code = 502
[user.withdraw.list] -> 
{
  "id": "1583627464237.3550374460762335",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464293.6321074589736565488",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464237.3550374460762335",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627464251.1709231180734429",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464307.2551518359778888209",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464251.1709231180734429",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583627463817.3579774273198683",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583627463817.3579774273198683",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 461,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.withdraw.apply] -> 
{
  "id": "1583627464318.3545461976546907",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "withdrawNo": "200307203104788389",
    "payPasword": "n4zc4H6EiTJtfJ7V4mnWFd9hAAU=",
    "toBankId": 1,
    "amount": 453
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583627464379.288608883911508480",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583627464318.3545461976546907",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 453,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200307203104788389",
      "payPasword": "n4zc4H6EiTJtfJ7V4mnWFd9hAAU=",
      "sessionId": "ae0001011327199C81874811A383174f",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply]error code = 0000
[report.day.order.groupby.query] -> 
{
  "id": "1583627464381.5065403400480854",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627464439.874149140467548320",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464381.5065403400480854",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627464374.7181270025117187",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627464450.8338398934661068702",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464374.7181270025117187",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627464434.1703566021067225",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132110AECF159211A382ceec",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627464508.8646767162625752159",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464434.1703566021067225",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627464614.3540981648160032",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464696.144120708472193044",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464614.3540981648160032",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627464642.7613840025123225",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627464714.6301345084884385744",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464642.7613840025123225",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 19,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583627464695.6686397407822289",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464751.6786924088487837662",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464695.6686397407822289",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627464722.1407463028230765",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464779.8575745248406255020",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464722.1407463028230765",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627464779.7798723232322246",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132110AECF159211A382ceec",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627464836.1170937005354910080",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464779.7798723232322246",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[sn.lottery.order.query] -> 
{
  "id": "1583627464735.6358037935417857",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583627464735.6358037935417857",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 43,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583627464795.9000581286781072",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011327199C81874811A383174f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627464872.726252925609837568",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464795.9000581286781072",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011327199C81874811A383174f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627464915.8781649690616648",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627464994.2961262858379215426",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464915.8781649690616648",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583627464962.5809049950481094",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465021.2415694666746954784",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627464962.5809049950481094",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "endTime": "2020-03-08 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583627465020.9635142827625052",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465071.8021473816767101952",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465020.9635142827625052",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627465050.6069772403627130",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11A382b473",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627465107.1647619282421434593",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465050.6069772403627130",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae00010113201724BAA31E11A382b473",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627465049.8852193635384756",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132110AECF159211A382ceec",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465124.2814750910201984",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465049.8852193635384756",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627465112.5451252332198566",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627465164.1177730476283151424",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465112.5451252332198566",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583627465152.4926911631304275",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132110AECF159211A382ceec",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465209.40673145946112256",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465152.4926911631304275",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132110AECF159211A382ceec",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627465221.0074621679206741",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627465279.8899099389818027504",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465221.0074621679206741",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847656
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627465397.0414279478939836",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465477.254524022677570304",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465397.0414279478939836",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627465443.5348119600835470",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113231223A719CE11A38270f3",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627465504.36031060479459972",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465443.5348119600835470",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae00010113231223A719CE11A38270f3",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627465458.0450314154968319",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011328127D1841FA11A383d9d6",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627465533.1199366319898887334",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465458.0450314154968319",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011328127D1841FA11A383d9d6",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847656
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583627465504.5454618692316155",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465555.9219765636431331000",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465504.5454618692316155",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627465577.5397150168728747",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465626.29700026075054210",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465577.5397150168728747",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627465644.4326478257580686",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132216EEF2183811A382a1d9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627465703.5142393136524279680",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465644.4326478257580686",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132216EEF2183811A382a1d9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583627465656.5659414508844106",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132411DCB8AE5711A38268d8",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627465713.9150975784502866934",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465656.5659414508844106",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5711A38268d8",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583627465881.5424373807083858",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132519EA461C7B11A3829225",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627465965.8860548589797504959",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465881.5424373807083858",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae000101132519EA461C7B11A3829225",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627465942.9643823584244489",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627466025.33850666867035176",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627465942.9643823584244489",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583627466000.5611170421367272",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011327199C81874811A383174f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583627466082.648538966479603217",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627466000.5611170421367272",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-08 23:59:59",
      "sessionId": "ae0001011327199C81874811A383174f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583627466129.3364129701037936",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F144214D4B311A38268f4",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627466204.9078956918319087612",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627466129.3364129701037936",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F144214D4B311A38268f4",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627466159.6025033002200479",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326150F760DAF11A3838bf9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627466209.6293358144109658112",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627466159.6025033002200479",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326150F760DAF11A3838bf9",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583627466161.2622430293456846",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011327199C81874811A383174f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-08 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583627466211.6336479598716387284",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583627466161.2622430293456846",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874811A383174f",
      "endTime": "2020-03-08 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
Engine error error=context cancelled at randomcharstring (file:///home/zdkic/Project/apiTest/lib/utils.js:229:2174(32))
Engine Error
```

