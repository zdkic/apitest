
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583871239266.3526960319647108",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "z7alGgNxeiGgRet9lMJE6Ttgi+U=",
    "salt": "raijna74o1i4698y76ws2nelp52m8oz6",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583871239266.3526960319647108",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-10 16:13:59",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847651,
      "parentPathIncSelf": "/14679026/16847651/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 9835,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:49",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "expiry": 1583874839355,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583871239411.8906190084694435",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583871239411.8906190084694435",
  "result": {
    "balance": 505470.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583878439355,
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.no.get] -> 
{
  "id": "1583871239512.0673819353579038",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583871239512.0673819353579038",
  "result": "200310161359301257",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583871239587.4028261267097266",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583871239619.6917528748199771613",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583871239587.4028261267097266",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply] -> 
{
  "id": "1583871239667.1523517651588766",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "withdrawNo": "200310161359301257",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 449
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583871239700.8935139186260705184",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583871239667.1523517651588766",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 449,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200310161359301257",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 1
[user.withdraw.apply] -> 
{
  "id": "1583871299750.7571912738575025",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "withdrawNo": "200310161359301257",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 449
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583871299785.2901450665995878416",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583871299750.7571912738575025",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 449,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200310161359301257",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 2
[user.withdraw.apply] -> 
{
  "id": "1583871359835.2314617923258437",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "withdrawNo": "200310161359301257",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 449
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583871359868.577735177051324747",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583871359835.2314617923258437",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 449,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200310161359301257",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 3
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583871419914.6294776448431231",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583871419914.6294776448431231",
  "result": {
    "balance": 505470.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583878439355,
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 449
diff = 0
[user.withdraw.no.get] -> 
{
  "id": "1583871539997.5755335866723860",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583871539997.5755335866723860",
  "result": "200310161900050383",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583871540096.3919035330062233",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583871540136.81681371282539074",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583871540096.3919035330062233",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過低的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583871540183.6551170713600200",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "withdrawNo": "200310161900050383",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 1
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583871540218.144150391257055763",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583871540183.6551170713600200",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 1,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200310161900050383",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過高的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583871540266.8945956100823970",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "withdrawNo": "200310161900050383",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 30000000
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583871540300.328488445729064608",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583871540266.8945956100823970",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 30000000,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200310161900050383",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583871540345.0066098006684912",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583871540345.0066098006684912",
  "result": "200310161900453950",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583871540427.6751495859037594",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "123456789",
    "withdrawNo": "200310161900453950",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 449
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583871540461.2613391878360154436",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583871540427.6751495859037594",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 449,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200310161900453950",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583871545506.1342507410256997",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583871545506.1342507410256997",
  "result": "200310161905874832",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583871545589.2453729269854007",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28",
    "withdrawNo": "200310161905874832",
    "payPasword": "123456789",
    "toBankId": 1,
    "amount": 449
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2273",
    "sn": "1583871545624.7920696751099457998",
    "message": "提现密码错误",
    "reason": "sn:ae00, userId:16847651 paypwd is wrong",
    "action": "null"
  },
  "id": "1583871545589.2453729269854007",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 449,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200310161905874832",
      "payPasword": "123456789",
      "sessionId": "ae000101132310FD02CC641261F71e28",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583871545727.9935496432774704",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132310FD02CC641261F71e28"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583871545727.9935496432774704",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=5m6.256117574s
some thresholds have failed
```

