
```bash
loginId = bgqajohnload1
[auth.mobile.login] -> 
{
  "id": "1583957439303.3209152023162763",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "XQIjY/yCFfvYP+D5E1ZWxvuOIkQ=",
    "salt": "1ghd0d69eb5k2vtierdtpql5hzq1accb",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583957439303.3209152023162763",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2020-03-11 16:10:39",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847647/",
      "userId": 16847647,
      "loginCount": 27486,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:48",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 413,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "qq": "",
      "passportNumber": null,
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F15E32659A612A54F8f28",
    "expiry": 1583961039405,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
測試正常的流程
[currency.list] -> 
{
  "id": "1583957439447.9166025326890770",
  "method": "currency.list",
  "params": {
    "sn": "ae00"
  },
  "jsonrpc": "2.0"
}
[currency.list] <- 
{
  "id": "1583957439447.9166025326890770",
  "result": [
    {
      "currencyName": "人民币",
      "rate": 1,
      "currencySymbol": null,
      "sn": "BG00",
      "currencyId": 1,
      "currencyCountry": null,
      "currencyCode": "CNY",
      "status": 1
    }
  ],
  "error": null,
  "jsonrpc": "2.0"
}
currency.sn = BG00
測試获取单个币种
[currency.get] -> 
{
  "id": "1583957439547.9346235792441215",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": 1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1583957439547.9346235792441215",
  "result": {
    "currencyName": "人民币",
    "rate": 1,
    "currencySymbol": "￥",
    "sn": "BG00",
    "currencyId": 1,
    "currencyCountry": null,
    "currencyCode": "CNY",
    "status": 1
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的币种ID
[currency.get] -> 
{
  "id": "1583957439631.1704209571447274",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": -1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1583957439631.1704209571447274",
  "result": {},
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
[auth.online.logout] -> 
{
  "id": "1583957439825.1383376779247226",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F15E32659A612A54F8f28"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583957439825.1383376779247226",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=274.299277ms
```

