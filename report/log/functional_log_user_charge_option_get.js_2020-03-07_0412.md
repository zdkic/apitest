
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583525572853.3913974663501917",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "fijkhTgj5MA3bND0fW6BHqmzd2w=",
    "salt": "lw9ff3tw2o9yhd7o4pkgitxfcnebqkfk",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583525572853.3913974663501917",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-06 16:12:52",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9732,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "expiry": 1583529172968,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573017.7108320400033709",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 1,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573017.7108320400033709",
  "result": {
    "amount": 100,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 133,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573145.7054789281863322",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 2,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573145.7054789281863322",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573236.5440707259962136",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 3,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573236.5440707259962136",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573324.0634331636943187",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 1,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573324.0634331636943187",
  "result": {
    "amount": 300,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 333,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573412.0225515951822896",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 2,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573412.0225515951822896",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573499.5926313281651050",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 3,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573499.5926313281651050",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573593.4999567118174013",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 1,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573593.4999567118174013",
  "result": {
    "amount": 165,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 198,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573737.0717397748761913",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 2,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573737.0717397748761913",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583525573835.0190486888307211",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64",
    "chargeType": 3,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583525573835.0190486888307211",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583525573994.3991392456107247",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323181B7E5EE41153EA3c64"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583525573994.3991392456107247",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=901.034933ms
```

