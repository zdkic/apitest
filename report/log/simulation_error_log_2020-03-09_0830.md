

```bash
[user.charge.list] -> 
{
  "id": "1583713856491.4539970450132522",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713856557.8786493667188129775",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713856491.4539970450132522",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713856591.5695202929888950",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713856633.144119776175998047",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713856591.5695202929888950",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713856771.3206535511137686",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231538CDE3F411E7031ee5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713856812.8033559649279456612",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713856771.3206535511137686",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231538CDE3F411E7031ee5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713856952.2599783744731792",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713856994.3195366648620271238",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713856952.2599783744731792",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713857020.7056676323121005",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857061.721569899970840608",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857020.7056676323121005",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713857093.9373522220100416",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713857167.8637754546883395552",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857093.9373522220100416",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713857134.8018673482567953",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857181.8069800548790958444",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857134.8018673482567953",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713857334.8017049143778688",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324106DBDE91911E7038666",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857400.6773167528349384576",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857334.8017049143778688",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324106DBDE91911E7038666",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583713857274.1073598099229818",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae00010113231538CDE3F411E7031ee5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583713857274.1073598099229818",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231538CDE3F411E7031ee5",
      "endTime": "2020-03-09 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 119,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.charge.list] -> 
{
  "id": "1583713857395.6610622788869222",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857461.2669016510796152992",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857395.6610622788869222",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713857437.8609973585304885",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324106DBDE91911E7038666",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857485.5489159482394386",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857437.8609973585304885",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324106DBDE91911E7038666",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713857496.6468940016449752",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857537.37176895737036800",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857496.6468940016449752",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713857516.8217612505173336",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857581.19237098390356742",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857516.8217612505173336",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713857575.2786501323047249",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713857656.2318305741594755076",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857575.2786501323047249",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847656
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 11,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583713857619.9040312418485031",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857673.8682607849945167868",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857619.9040312418485031",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713857695.4261466343181049",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857760.9077504227243965623",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857695.4261466343181049",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713857759.2081726214403013",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713857827.9196306372167581616",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857759.2081726214403013",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583713857795.2402787649799681",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713857842.5727654716800630202",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713857795.2402787649799681",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713858059.6275186594104792",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713858127.9146492765417307888",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858059.6275186594104792",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583713858368.8914594503908109",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713858440.526921192188937995",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858368.8914594503908109",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713858404.9269946804634562",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713858451.1227238595177234960",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858404.9269946804634562",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713858417.6898328087522606",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011322197084C55911E7035298",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713858489.7781938601591046128",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858417.6898328087522606",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011322197084C55911E7035298",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713858475.0797212664338751",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713858523.8646455811853106935",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858475.0797212664338751",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713858523.8918582897192664",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011322197084C55911E7035298",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713858570.108232645029134370",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858523.8918582897192664",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011322197084C55911E7035298",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713858643.1496099942824716",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011324106DBDE91911E7038666",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713858684.369894468381458449",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858643.1496099942824716",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324106DBDE91911E7038666",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713858659.0559836370920681",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713858730.794483529338667392",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858659.0559836370920681",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713858827.7422275840201823",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713858868.865080459469062209",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713858827.7422275840201823",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713859063.9440752524815630",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713859104.8790602593634990448",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859063.9440752524815630",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713859211.0280336848473269",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713859278.211757718944350740",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859211.0280336848473269",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713859363.4925999310098783",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713859411.2377906249456485024",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859363.4925999310098783",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713859443.7218846217284109",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713859508.2308943640863064649",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859443.7218846217284109",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713859496.9931580443080351",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713859544.148804210845417488",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859496.9931580443080351",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713859504.2081670151679255",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713859579.9218270088150630380",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859504.2081670151679255",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583713859543.6168226325975036",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713859585.40561273878414344",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859543.6168226325975036",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713859563.4074306767611827",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713859656.8214000019826015984",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859563.4074306767611827",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 21,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713859913.9011417664250699",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713859955.1820019468395610634",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713859913.9011417664250699",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713860091.6893425638160250",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713860138.282935342269476",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860091.6893425638160250",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713860167.3561754176753178",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713860239.8806748156354885456",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860167.3561754176753178",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713860395.1076188622403457",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231538CDE3F411E7031ee5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713860443.9193390514858491766",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860395.1076188622403457",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231538CDE3F411E7031ee5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713860516.0515040105722120",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713860558.8922738889253058969",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860516.0515040105722120",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713860528.3760522290523561",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713860611.9186001522511181824",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860528.3760522290523561",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 10,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583713860588.1911011199757387",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583713860588.1911011199757387",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "endTime": "2020-03-09 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 42,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.withdraw.list] -> 
{
  "id": "1583713860652.0723022533215880",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713860700.8250241574093438908",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860652.0723022533215880",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713860753.2643725819911969",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713860800.1307224772943086081",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860753.2643725819911969",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713860936.2793378662174066",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011322197084C55911E7035298",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713860985.459369370257850408",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713860936.2793378662174066",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011322197084C55911E7035298",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583713861030.8682498994137737",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583713861030.8682498994137737",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "endTime": "2020-03-09 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 34,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.charge.list] -> 
{
  "id": "1583713861059.1236721013455771",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713861106.6764263419516810736",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861059.1236721013455771",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713861143.4879850739376454",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713861195.2269701674828806",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861143.4879850739376454",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 12,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583713861371.3770275966487337",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583713861371.3770275966487337",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "endTime": "2020-03-09 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 31,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583713861431.4437725290852435",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011322197084C55911E7035298",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713861504.9204683206558728192",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861431.4437725290852435",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011322197084C55911E7035298",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713861534.4441622097052283",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011324106DBDE91911E7038666",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713861583.9223281842541559776",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861534.4441622097052283",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011324106DBDE91911E7038666",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847652
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583713861615.2960168707305297",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713861688.8554581924768317343",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861615.2960168707305297",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713861723.6389097898591040",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713861765.650260011784079370",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861723.6389097898591040",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713861837.5157898775653028",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713861897.936775265425162240",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861837.5157898775653028",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 15,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713861912.6913013890410177",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713861993.9113033707429560119",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713861912.6913013890410177",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713862028.0772310528235059",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713862071.2323927927334584324",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862028.0772310528235059",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713862042.1206780718070628",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713862084.144960026669238281",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862042.1206780718070628",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713862118.2899745262903406",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713862161.8052223378216238976",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862118.2899745262903406",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713862276.5402863774638349",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713862347.372719598963852680",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862276.5402863774638349",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583713862312.1437516532904787",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011322197084C55911E7035298",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583713862312.1437516532904787",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011322197084C55911E7035298",
      "endTime": "2020-03-09 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 30,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.withdraw.list] -> 
{
  "id": "1583713862381.2209439410075835",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713862429.8094722096809114314",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862381.2209439410075835",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713862395.5666397195964364",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713862466.167979009938900032",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862395.5666397195964364",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713862500.7244772372378933",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713862542.2255175692517638",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862500.7244772372378933",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583713862576.3687101070616975",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583713862576.3687101070616975",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 34,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583713862884.2006608387731949",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713862958.304207538613649937",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862884.2006608387731949",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583713862936.2351295403594776",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713863000.144968933087854855",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713862936.2351295403594776",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713863035.1403462992632155",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713863082.9221048450949316598",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713863035.1403462992632155",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713863117.5830938778837377",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713863185.6893726747305883629",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713863117.5830938778837377",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713863480.6321004833960157",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713863545.7698155857359388506",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713863480.6321004833960157",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713863643.1363301344196302",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713863684.220818351298594864",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713863643.1363301344196302",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713863703.1069193910230867",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713863752.8313628962351741951",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713863703.1069193910230867",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713863759.4546055550435009",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713863808.9223345089121598776",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713863759.4546055550435009",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713863992.2111763973672949",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864034.3513388944932732964",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713863992.2111763973672949",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713864022.9573360104342360",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864098.9043121672365670199",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864022.9573360104342360",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583713864071.6798403627618139",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864113.9162269625377865688",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864071.6798403627618139",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713864097.2705738517463064",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864145.183674247288849474",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864097.2705738517463064",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713864081.0262077748920930",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864155.6845450266905262076",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864081.0262077748920930",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713864123.6597066684017745",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011322197084C55911E7035298",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864173.2400440046141444",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864123.6597066684017745",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011322197084C55911E7035298",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583713864181.4013004619636856",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864224.8824468114339004157",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864181.4013004619636856",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713864263.7107710072328261",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F12E464285F11E7034098",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864338.9205350559427378826",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864263.7107710072328261",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 8,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713864371.6619832683250502",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864413.6253054567206597494",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864371.6619832683250502",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713864594.1890751669784632",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864642.1271144022785656372",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864594.1890751669784632",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583713864668.0587230556218696",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864716.6773325357840858800",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864668.0587230556218696",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713864683.2621130809492573",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326174C9A0A9111E7037e62",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864758.8574853591714412260",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864683.2621130809492573",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011326174C9A0A9111E7037e62",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583713864696.2269309677314331",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864745.9178322536933621632",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864696.2269309677314331",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713864750.7562049645196137",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864799.8556239814695779818",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864750.7562049645196137",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713864804.2318123699918399",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713864846.7727447957780299514",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864804.2318123699918399",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713864865.9984530445454832",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F12E464285F11E7034098",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864930.193764774249841800",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864865.9984530445454832",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713864925.1684627532150915",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713864993.1589885640844904585",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713864925.1684627532150915",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[sn.lottery.order.query] -> 
{
  "id": "1583713864986.0129567953511898",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132112B200F6E111E7032bbb",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583713864986.0129567953511898",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132112B200F6E111E7032bbb",
      "endTime": "2020-03-09 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 41,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583713865231.2271146345640851",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132713F665270011E7032178",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713865280.8916839170785935260",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865231.2271146345640851",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101132713F665270011E7032178",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713865274.1495456776609216",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011324106DBDE91911E7038666",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713865321.3467807219570509328",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865274.1495456776609216",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324106DBDE91911E7038666",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713865295.8401905172688036",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713865343.43507747005661184",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865295.8401905172688036",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583713865329.7406675752381023",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713865377.720728152078877264",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865329.7406675752381023",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713865387.3296631975440783",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324106DBDE91911E7038666",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713865437.9220905797644304384",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865387.3296631975440783",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324106DBDE91911E7038666",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713865452.4731870402367779",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713865493.6629260787057490878",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865452.4731870402367779",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713865475.2789344939659949",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324106DBDE91911E7038666",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713865523.1152921656021764488",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865475.2789344939659949",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324106DBDE91911E7038666",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713865487.3000205270242771",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713865530.1228356867110422788",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865487.3000205270242771",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713865728.3385071741143342",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011322197084C55911E7035298",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713865778.7875632258760048510",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865728.3385071741143342",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011322197084C55911E7035298",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713865838.0184799623338485",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F12E464285F11E7034098",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713865897.11455060144097572",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865838.0184799623338485",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713865873.0995126234766070",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132713F665270011E7032178",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713865916.5186031308171116540",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865873.0995126234766070",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae000101132713F665270011E7032178",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583713865876.2234268041816662",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201724BAA31E11E70359ba",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713865925.342855377649467392",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865876.2234268041816662",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae00010113201724BAA31E11E70359ba",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583713865928.8007018036801387",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325106A1890E111E70322a5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713865973.398569434945555464",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713865928.8007018036801387",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325106A1890E111E70322a5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583713866009.0078940993823893",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011328112596DB2011E70339f5",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583713866076.1315087343839084825",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713866009.0078940993823893",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-09 23:59:59",
      "sessionId": "ae0001011328112596DB2011E70339f5",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847656
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583713866070.0209309483242130",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713866141.8358673108739030396",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713866070.0209309483242130",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583713866187.7254189407954112",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F12E464285F11E7034098",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713866228.6124010385186668524",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713866187.7254189407954112",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F11E7034098",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583713866183.8075351230527007",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231538CDE3F411E7031ee5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713866230.1909606668501517321",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713866183.8075351230527007",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231538CDE3F411E7031ee5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713866457.6739424415406929",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011322197084C55911E7035298",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713867018.2418481610346741904",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713866457.6739424415406929",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011322197084C55911E7035298",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 14,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583713866499.8024864105000989",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132713F665270011E7032178",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
[user.order.query] -> 
{
  "id": "1583713866414.6326673526262324",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231538CDE3F411E7031ee5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-09 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713867021.1157427316779912320",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713866414.6326673526262324",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231538CDE3F411E7031ee5",
      "endTime": "2020-03-09 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 21,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583713867019.9061162713664583468",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583713866499.8024864105000989",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132713F665270011E7032178",
      "endTime": "2020-03-09 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 17,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list]error code = 3100
Engine error error=context cancelled at randomcharstring (file:///home/zdkic/Project/apiTest/lib/utils.js:229:2185(29))
Engine Error
```

