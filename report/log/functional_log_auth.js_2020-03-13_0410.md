
```bash
[auth.mobile.login]測試正常流程
[auth.mobile.login] -> 
{
  "id": "1584043819611.8972321243024607",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "vyyaZQd5H+eMeK2LPd3sv4GVj6w=",
    "salt": "r63pe2l8cpy9s7ava6anqjl8a8rf22ve",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1584043819611.8972321243024607",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2020-03-12 16:10:19",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847647,
      "parentPathIncSelf": "/14679026/16847647/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 27506,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:48",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 413,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F19065DB2BA12E8CB29de",
    "expiry": 1584047419729,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.mobile.login]測試錯誤帳密
[auth.mobile.login] -> 
{
  "id": "1584043819763.0300367300982618",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "nCbL77Ev0jGcf30nfNFMny6e0iY=",
    "salt": "lust5j97cfiw0c8ncp89swdceye181kp",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2218",
    "sn": "1584043819842.7654707042468167550",
    "message": "用户名或密码错误,登录失败.",
    "reason": "loginId:bgqajohnload1, password:nCbL77Ev0jGcf30nfNFMny6e0iY=, from sn:ae00",
    "action": "null"
  },
  "id": "1584043819763.0300367300982618",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "nCbL77Ev0jGcf30nfNFMny6e0iY=",
      "captchaCode": "6711",
      "loginId": "bgqajohnload1",
      "salt": "lust5j97cfiw0c8ncp89swdceye181kp",
      "reqIp": "202.11.82.1",
      "captchaKey": "3094255924",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 29,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]測試正常流程
[auth.session.validate] -> 
{
  "id": "1584043819867.8086379060026513",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "ae000101131F19065DB2BA12E8CB29de",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "id": "1584043819867.8086379060026513",
  "result": {
    "flag": "1",
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1584051019729,
    "sessionId": "ae000101131F19065DB2BA12E8CB29de",
    "userId": 16847647
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.session.validate]測試錯誤sessionId
[auth.session.validate] -> 
{
  "id": "1584043819943.1485261197930383",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "12345678",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1584043819993.4082515273918203360",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1584043819943.1485261197930383",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "0",
      "sessionId": "12345678",
      "opIp": "202.11.82.1",
      "withUser": "0"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.online.logout]測試正常流程
[auth.online.logout] -> 
{
  "id": "1584043820018.0955241075014185",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F19065DB2BA12E8CB29de"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1584043820018.0955241075014185",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
[auth.online.logout]測試錯誤sessionId
[auth.online.logout] -> 
{
  "id": "1584043820144.6290590198160266",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "12345678"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1584043820196.8898961113731282944",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1584043820144.6290590198160266",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.online.logout",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "12345678",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
Test finished i=1 t=616.129509ms
```

