

```bash
[auth.mobile.login] -> 
{
  "id": "1574728220110.8992573591885342",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload3",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "mMAZIHiNk24ig8IQvVzEIntceuQ=",
    "salt": "7zfn7tu4isjgibxsv1p868s1a1tn2v96",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574728220164.7777715980737426872",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574728220110.8992573591885342",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "mMAZIHiNk24ig8IQvVzEIntceuQ=",
      "loginId": "bgqajohnload3",
      "salt": "7zfn7tu4isjgibxsv1p868s1a1tn2v96",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.mobile.login]error code = 2303
[auth_mobile_login]error code = 2303
[auth.mobile.login] -> 
{
  "id": "1574728220200.7208288232329459",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload4",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "Fvh2wrA1QtBqLeagbeMNXI0e3g0=",
    "salt": "2j3pm1n64vh4wguqe0ghb5a8bk6cnybc",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574728220258.7993880435845038044",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574728220200.7208288232329459",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "Fvh2wrA1QtBqLeagbeMNXI0e3g0=",
      "loginId": "bgqajohnload4",
      "salt": "2j3pm1n64vh4wguqe0ghb5a8bk6cnybc",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 10,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.mobile.login]error code = 2303
[auth_mobile_login]error code = 2303
[user.order.query] -> 
{
  "id": "1574728221026.7755805270826869",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221075.1261431321497059330",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221026.7755805270826869",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.cashflow.query] -> 
{
  "id": "1574728221050.3133695852694107",
  "jsonrpc": "2.0",
  "method": "user.cashflow.query",
  "params": {
    "accountItem": null,
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "pageIndex": 1,
    "pageSize": 100
  }
}
[user.cashflow.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728221127.354650549125121",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728221050.3133695852694107",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.cashflow.query",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "exclusiveAccountItems": [
        10801,
        20801
      ],
      "accountItem": null,
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.cashflow.query]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728221103.5508843196571398",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728221174.8495388391474985928",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728221103.5508843196571398",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 700
[user.order.query] -> 
{
  "id": "1574728221339.7045680684543743",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221423.577379059992363017",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221339.7045680684543743",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 12,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728221402.3877248208401094",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221483.76350708056064512",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221402.3877248208401094",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728221463.7343204701777058",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221536.22525146074336132",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221463.7343204701777058",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728221512.3387137884026149",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221561.9205208053190082021",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221512.3387137884026149",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728221524.2452853133935661",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221599.1239370228551909648",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221524.2452853133935661",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728221565.7482309008389238",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221619.2306758922031072032",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221565.7482309008389238",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728221628.1489706497515413",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728221677.9147929450225006254",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221628.1489706497515413",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574728221705.6282175214836710",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728221779.7777111756345097454",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728221705.6282175214836710",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[sn.payment.biglist] -> 
{
  "id": "1574728221741.5218257775771260",
  "jsonrpc": "2.0",
  "method": "sn.payment.biglist",
  "params": {
    "changeNo": "191125203022300381",
    "isFromMobile": 1,
    "iframeFlag": 0
  }
}
[sn.payment.biglist] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728221789.9061091060182661105",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728221741.5218257775771260",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.biglist",
    "params": {
      "changeNo": "191125203022300381",
      "reqIp": "202.11.82.1",
      "isFromMobile": 1,
      "iframeFlag": 0,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.biglist]error code = 700
[user.order.query] -> 
{
  "id": "1574728221807.1307360060354264",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728221857.1230891759202486470",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728221807.1307360060354264",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[sn.payment.batch.limit.query] -> 
{
  "id": "1574728221819.4815626659528297",
  "jsonrpc": "2.0",
  "method": "sn.payment.batch.limit.query",
  "params": {
    "payIds": []
  }
}
[sn.payment.batch.limit.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728221868.3664317956042787332",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728221819.4815626659528297",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.batch.limit.query",
    "params": {
      "reqIp": "202.11.82.1",
      "payIds": []
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.batch.limit.query]error code = 700
[sn.payment.charge.bank.list] -> 
{
  "id": "1574728221894.5406398429444818",
  "jsonrpc": "2.0",
  "method": "sn.payment.charge.bank.list",
  "params": {}
}
[sn.payment.charge.bank.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728221943.7962143955347226582",
    "message": "缺少参数",
    "reason": "sn参数不能为空",
    "action": "null"
  },
  "id": "1574728221894.5406398429444818",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.charge.bank.list",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.charge.bank.list]error code = 700
[user.charge.list] -> 
{
  "id": "1574728221943.1764367154240958",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728222015.9200553803288148336",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728221943.1764367154240958",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728222005.0814415725653883",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728222077.8249659932387294319",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222005.0814415725653883",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728222043.6404695640769785",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728222092.8600747153779834878",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222043.6404695640769785",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728222107.3029848694417819",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728222170.8323770831425503184",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222107.3029848694417819",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728222185.4255490882362753",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728222258.3690797558353641744",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222185.4255490882362753",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728222245.7967353393526480",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728222317.9205197655034475470",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728222245.7967353393526480",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 700
[user.withdraw.list] -> 
{
  "id": "1574728222288.1698471505844920",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728222337.2310435841149504260",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222288.1698471505844920",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[thirdparty.ky.game.url] -> 
{
  "id": "1574728222306.7336748835786170",
  "jsonrpc": "2.0",
  "method": "thirdparty.ky.game.url",
  "params": {
    "lineCode": 1,
    "gameId": "830"
  }
}
[thirdparty.ky.game.url] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728222380.289616981723070480",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728222306.7336748835786170",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.ky.game.url",
    "params": {
      "lineCode": 1,
      "gameId": "830",
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[thirdparty.ky.game.url]error code = 700
[thirdparty.ky.order.query] -> 
{
  "id": "1574728222406.8182537191927300",
  "jsonrpc": "2.0",
  "method": "thirdparty.ky.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "209",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1
  }
}
[thirdparty.ky.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728222459.1152956689118937088",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728222406.8182537191927300",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.ky.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "moduleId": "209"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 1,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[thirdparty.ky.order.query]error code = 700
[sn.thirdparty.pwd.get] -> 
{
  "id": "1574728222488.3678807324336816",
  "jsonrpc": "2.0",
  "method": "sn.thirdparty.pwd.get",
  "params": {
    "terminal": 8
  }
}
[sn.thirdparty.pwd.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728222542.6213841584522114816",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728222488.3678807324336816",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.thirdparty.pwd.get",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.thirdparty.pwd.get]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728222488.5510697279416794",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728222564.9115171285825992695",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222488.5510697279416794",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1574728222673.5040923599478085",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728222752.293650419358910497",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222673.5040923599478085",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847656
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1574728222720.9736441407289391",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728222770.9078129771828723414",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222720.9736441407289391",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728222728.5429196864057910",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728222802.8826685236736621919",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728222728.5429196864057910",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1574728222848.3272655354645615",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728222919.7779115124471364480",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728222848.3272655354645615",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728222908.6530746158864022",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728222979.6916963466236132608",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728222908.6530746158864022",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 700
[user.order.query] -> 
{
  "id": "1574728223092.9469078399731378",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323171E1AB85F56B87C48c8",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728223146.159389741084655616",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728223092.9469078399731378",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323171E1AB85F56B87C48c8",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1574728223149.2142382034984975",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1574728223149.2142382034984975",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 96,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.charge.list] -> 
{
  "id": "1574728223275.5907846476665821",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728223349.3082749958370181136",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728223275.5907846476665821",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1574728223312.2341435781861140",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728223366.7052631379195985913",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728223312.2341435781861140",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728223380.5219620437047280",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728223430.6915101269692431791",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728223380.5219620437047280",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728223514.7520759624243969",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728223587.7483712352491140554",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728223514.7520759624243969",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 700
[sn.payment.biglist] -> 
{
  "id": "1574728223554.4331649774078243",
  "jsonrpc": "2.0",
  "method": "sn.payment.biglist",
  "params": {
    "changeNo": "191125203022300384",
    "isFromMobile": 1,
    "iframeFlag": 0
  }
}
[sn.payment.biglist] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728223617.304485662424367302",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728223554.4331649774078243",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.biglist",
    "params": {
      "changeNo": "191125203022300384",
      "reqIp": "202.11.82.1",
      "isFromMobile": 1,
      "iframeFlag": 0,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 12,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.biglist]error code = 700
[user.withdraw.list] -> 
{
  "id": "1574728223616.8467387148586878",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728223685.7343400528891344456",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728223616.8467387148586878",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 16,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 700
[sn.payment.batch.limit.query] -> 
{
  "id": "1574728223647.1572100200518040",
  "jsonrpc": "2.0",
  "method": "sn.payment.batch.limit.query",
  "params": {
    "payIds": []
  }
}
[sn.payment.batch.limit.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728223698.9187236106089774592",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728223647.1572100200518040",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.batch.limit.query",
    "params": {
      "reqIp": "202.11.82.1",
      "payIds": []
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.batch.limit.query]error code = 700
[sn.payment.charge.bank.list] -> 
{
  "id": "1574728223726.6912796908315060",
  "jsonrpc": "2.0",
  "method": "sn.payment.charge.bank.list",
  "params": {}
}
[sn.payment.charge.bank.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728223781.9195781798268614624",
    "message": "缺少参数",
    "reason": "sn参数不能为空",
    "action": "null"
  },
  "id": "1574728223726.6912796908315060",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.charge.bank.list",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.charge.bank.list]error code = 700
[user.charge.list] -> 
{
  "id": "1574728223754.2612573158312405",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728223826.3215625110753001992",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728223754.2612573158312405",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728223855.2113884644343648",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728223909.13789396304414464",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728223855.2113884644343648",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728223993.1011791963562590",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728224074.2335118141423616513",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728223993.1011791963562590",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728224052.0355947478862643",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728224127.512479321355731988",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728224052.0355947478862643",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 700
[user.withdraw.list] -> 
{
  "id": "1574728224103.4219285245377686",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728224152.6912593849290178424",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728224103.4219285245377686",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728224116.8516301512537666",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728224191.8734660811646958080",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728224116.8516301512537666",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 700
[user.withdraw.list] -> 
{
  "id": "1574728224156.6602082766120541",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728224205.5704562997085328",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728224156.6602082766120541",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 700
[user.charge.list] -> 
{
  "id": "1574728224234.2642462082615377",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728224305.8178149817632815102",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728224234.2642462082615377",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728224296.1895253877818826",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728224370.6330838268689252007",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728224296.1895253877818826",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728224334.5864852958474281",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728224387.2060005721512690208",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728224334.5864852958474281",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728224398.6855088755644718",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728224449.2388192033456655041",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728224398.6855088755644718",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1574728224641.6404633716193477",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728224707.9196332829136304540",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728224641.6404633716193477",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 12,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[auth.session.validate] -> 
{
  "id": "1574728224656.9561185052754153",
  "method": "auth.session.validate",
  "params": {
    "withAmount": "1",
    "withUser": "1"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728224727.5926736008726446064",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574728224656.9561185052754153",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "1",
      "withUser": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 1,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728224777.5528438329549828",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011323171E1AB85F56B87C48c8",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728224857.2347594251548754050",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728224777.5528438329549828",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011323171E1AB85F56B87C48c8",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1574728225107.4150967165731332",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728225156.18370674704385154",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728225107.4150967165731332",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1574728225186.2929108334189112",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728225236.6689252503616468459",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728225186.2929108334189112",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728225187.2719290705403404",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728225241.5620446571810963378",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728225187.2719290705403404",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728225259.8197421840433357",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728225334.13265198156677292",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728225259.8197421840433357",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 700
[user.cashflow.query] -> 
{
  "id": "1574728225319.1783738890079273",
  "jsonrpc": "2.0",
  "method": "user.cashflow.query",
  "params": {
    "accountItem": null,
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "pageIndex": 1,
    "pageSize": 100
  }
}
[user.cashflow.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728225390.9199719269648036832",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728225319.1783738890079273",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.cashflow.query",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "exclusiveAccountItems": [
        10801,
        20801
      ],
      "accountItem": null,
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.cashflow.query]error code = 700
[user.withdraw.list] -> 
{
  "id": "1574728225362.7056719633303940",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728225412.9212042495161971120",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728225362.7056719633303940",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 700
[user.order.query] -> 
{
  "id": "1574728225575.2742295201304871",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728225669.8930492642064056232",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728225575.2742295201304871",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728225618.9632406451979879",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728225833.9058709162554028463",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728225618.9632406451979879",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728225741.4792836283689903",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728225982.8784545941555180507",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728225741.4792836283689903",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 11,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1574728225907.8853567015651497",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728226063.3459892754758434912",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728225907.8853567015651497",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728225860.3509854100286866",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728226087.9149009574385024694",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728225860.3509854100286866",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728226000.3680341408963818",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728226225.15239784308163978",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226000.3680341408963818",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1574728226011.2143986059678286",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728226252.739368312667899008",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226011.2143986059678286",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728226203.4775494739460216",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728226471.378381533562815043",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728226203.4775494739460216",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 700
[user.withdraw.list] -> 
{
  "id": "1574728226205.5877596635726617",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728226473.9204961769045229485",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728226205.5877596635726617",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728226103.5210967930965356",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728226506.738942337253638706",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226103.5210967930965356",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1574728226162.2703296978348172",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728226587.1322512514556101650",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226162.2703296978348172",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1574728226398.3496002253821422",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728226705.1900528977546657800",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226398.3496002253821422",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728226404.1405789314026014",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728226866.2632073616161767685",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226404.1405789314026014",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1574728226590.4865755720770040",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728226906.9168990174414109694",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226590.4865755720770040",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574728226523.6805833460941736",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728227015.468444735376196708",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728226523.6805833460941736",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728226583.9582220070948586",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011323171E1AB85F56B87C48c8",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728227072.1279586655132469413",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226583.9582220070948586",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011323171E1AB85F56B87C48c8",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.notice.list] -> 
{
  "id": "1574728226811.8297349100199306",
  "jsonrpc": "2.0",
  "method": "user.notice.list",
  "params": {
    "popupFlag": "N"
  }
}
[user.notice.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728227214.8358680736198145504",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728226811.8297349100199306",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.notice.list",
    "params": {
      "reqIp": "202.11.82.1",
      "popupFlag": "N",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.notice.list]error code = 700
[user.order.query] -> 
{
  "id": "1574728226844.6750785931753982",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728227241.4082354610520160",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226844.6750785931753982",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574728226906.1336250593740556",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728227268.8061399077637439388",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226906.1336250593740556",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1574728227091.9211325542604048",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728227566.5313508613589415934",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728227091.9211325542604048",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728226944.0331211638091492",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F12E464285F56B87B2f7f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728227677.8789890605413957372",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728226944.0331211638091492",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae000101131F12E464285F56B87B2f7f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1574728227006.6650400903677128",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728227733.90107812575201344",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728227006.6650400903677128",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1574728227244.1960528697849069",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728227740.619456102041535042",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728227244.1960528697849069",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[auth.session.validate] -> 
{
  "id": "1574728227065.5202933927128122",
  "method": "auth.session.validate",
  "params": {
    "withAmount": "1",
    "withUser": "1"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728227774.577886836737703980",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574728227065.5202933927128122",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "1",
      "withUser": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]error code = 700
[user.charge.list] -> 
{
  "id": "1574728227128.2381444359726543",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728227825.8641871095046471680",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728227128.2381444359726543",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 700
[user.charge.list] -> 
{
  "id": "1574728227368.1054994926642931",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728227848.9041743498310038976",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728227368.1054994926642931",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1574728227457.7602040372485632",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728227939.8610547477935599871",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728227457.7602040372485632",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324164BAD7C0A56B87Cd91a",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.notice.new.query] -> 
{
  "id": "1574728227466.6843419768264808",
  "jsonrpc": "2.0",
  "method": "sn.notice.new.query",
  "params": {
    "popupFlag": "N"
  }
}
[sn.notice.new.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728227942.8982983592900427127",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728227466.6843419768264808",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.notice.new.query",
    "params": {
      "reqIp": "202.11.82.1",
      "popupFlag": "N",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.notice.new.query]error code = 700
[user.order.query] -> 
{
  "id": "1574728227585.9601416504639701",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728228034.9144558210110125440",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728227585.9601416504639701",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728227754.8034756934784584",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728228139.6767735918032781310",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728227754.8034756934784584",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.notice.list] -> 
{
  "id": "1574728227940.6450643427886989",
  "jsonrpc": "2.0",
  "method": "user.notice.list",
  "params": {
    "popupFlag": "N"
  }
}
[user.notice.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728228299.8438250034594823552",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728227940.6450643427886989",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.notice.list",
    "params": {
      "reqIp": "202.11.82.1",
      "popupFlag": "N",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.notice.list]error code = 700
[thirdparty.ky.game.url] -> 
{
  "id": "1574728227728.7034979317895318",
  "jsonrpc": "2.0",
  "method": "thirdparty.ky.game.url",
  "params": {
    "lineCode": 1,
    "gameId": "830"
  }
}
[thirdparty.ky.game.url] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728228307.9205222388617181183",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728227728.7034979317895318",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.ky.game.url",
    "params": {
      "lineCode": 1,
      "gameId": "830",
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[thirdparty.ky.game.url]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728228008.4382847589025097",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728228403.9218442651325939703",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728228008.4382847589025097",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1574728228035.8523042267618388",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728228459.2612098916455809028",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728228035.8523042267618388",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 700
[user.withdraw.list] -> 
{
  "id": "1574728228038.6108189168869620",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011320118C53288E56B87C3464",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728228466.604116787136577552",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728228038.6108189168869620",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320118C53288E56B87C3464",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.notice.history.query] -> 
{
  "id": "1574728228098.1122439902587735",
  "jsonrpc": "2.0",
  "method": "sn.notice.history.query",
  "params": {}
}
[sn.notice.history.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728228535.2458990157978815751",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728228098.1122439902587735",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.notice.history.query",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.notice.history.query]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728227969.4757160829711080",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728228555.9150188357689406958",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728227969.4757160829711080",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1574728228091.0931280611825180",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728228776.285362061773205",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728228091.0931280611825180",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[sn.payment.biglist] -> 
{
  "id": "1574728228314.7015828361612620",
  "jsonrpc": "2.0",
  "method": "sn.payment.biglist",
  "params": {
    "changeNo": "191125203022300393",
    "isFromMobile": 1,
    "iframeFlag": 0
  }
}
[sn.payment.biglist] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728228794.8628289955486482360",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728228314.7015828361612620",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.biglist",
    "params": {
      "changeNo": "191125203022300393",
      "reqIp": "202.11.82.1",
      "isFromMobile": 1,
      "iframeFlag": 0,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.biglist]error code = 700
[user.order.query] -> 
{
  "id": "1574728228332.8398318255244666",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728228818.126391648157517840",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728228332.8398318255244666",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1574728228269.5777533729369761",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728228982.9197185869268647638",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728228269.5777533729369761",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "terminal": 8
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 700
[sn.notice.new.query] -> 
{
  "id": "1574728228535.6039365706163165",
  "jsonrpc": "2.0",
  "method": "sn.notice.new.query",
  "params": {
    "popupFlag": "N"
  }
}
[sn.notice.new.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229017.9078560849960614878",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728228535.6039365706163165",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.notice.new.query",
    "params": {
      "reqIp": "202.11.82.1",
      "popupFlag": "N",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.notice.new.query]error code = 700
[thirdparty.ky.order.query] -> 
{
  "id": "1574728228546.6606640057300575",
  "jsonrpc": "2.0",
  "method": "thirdparty.ky.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": "209",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1
  }
}
[thirdparty.ky.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229023.1211754963623018501",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728228546.6606640057300575",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.ky.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "moduleId": "209"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[thirdparty.ky.order.query]error code = 700
[user.charge.list] -> 
{
  "id": "1574728228660.7781951943298640",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728229143.8070443930044317696",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728228660.7781951943298640",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1574728228713.1460433061262974",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1574728228713.1460433061262974",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325143A5B8C6156B87C9ab9",
      "endTime": "2019-11-26 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 51,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.notice.unread.touch] -> 
{
  "id": "1574728228786.9653561033529621",
  "jsonrpc": "2.0",
  "method": "user.notice.unread.touch",
  "params": {
    "terminal": 8
  }
}
[user.notice.unread.touch] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229290.18129022613471232",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728228786.9653561033529621",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.notice.unread.touch",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.notice.unread.touch]error code = 700
[user.charge.list] -> 
{
  "id": "1574728228572.1787419358944210",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326106A49649E56B87Cbf90",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728229339.3468060201504997377",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728228572.1787419358944210",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326106A49649E56B87Cbf90",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1574728228693.6659539494982483",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728229390.8487876820966834102",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728228693.6659539494982483",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1574728228633.2492318985053070",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1574728228633.2492318985053070",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "endTime": "2019-11-26 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 41,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.withdraw.list] -> 
{
  "id": "1574728229012.7343240705885986",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B87Ca516",
    "startTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574728229513.8052436126220091357",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728229012.7343240705885986",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B87Ca516",
      "endTime": "2019-11-26 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.payment.batch.limit.query] -> 
{
  "id": "1574728229030.1671598579221914",
  "jsonrpc": "2.0",
  "method": "sn.payment.batch.limit.query",
  "params": {
    "payIds": []
  }
}
[sn.payment.batch.limit.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229522.20848395673534466",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": null
  },
  "id": "1574728229030.1671598579221914",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.payment.batch.limit.query",
    "params": {
      "reqIp": "202.11.82.1",
      "payIds": []
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 1,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.payment.batch.limit.query]error code = 700
[sn.order.switch.list] -> 
{
  "id": "1574728228872.2418648709028954",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229580.937892218681968712",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728228872.2418648709028954",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[auth.session.validate] -> 
{
  "id": "1574728229156.1232918250246130",
  "method": "auth.session.validate",
  "params": {
    "withAmount": "1",
    "withUser": "1"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229605.8249515895619304647",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574728229156.1232918250246130",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "1",
      "withUser": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]error code = 700
[auth.session.validate] -> 
{
  "id": "1574728229180.2599702138931410",
  "method": "auth.session.validate",
  "params": {
    "withAmount": "1",
    "withUser": "1"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229618.8930620451688283967",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574728229180.2599702138931410",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "1",
      "withUser": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]error code = 700
[sn.order.switch.list] -> 
{
  "id": "1574728228932.9709438812825281",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229619.295023987997753539",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728228932.9709438812825281",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[report.day.order.groupby.query] -> 
{
  "id": "1574728229199.2844020173376293",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2019-11-26 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132714C6219B1656B87C3fe3",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1574728229622.9205356493725433728",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574728229199.2844020173376293",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2019-11-26 23:59:59",
      "sessionId": "ae000101132714C6219B1656B87C3fe3",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[sn.order.switch.list] -> 
{
  "id": "1574728229218.8160888824980786",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574728229624.3553621752815550547",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574728229218.8160888824980786",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
Engine error error=context cancelled at b (file:///home/zdkic/Project/apiTest/lib/utils.js:229:5807(107))
Engine Error
```

