
```bash
loginId = bgqajohnload4
[auth.mobile.login] -> 
{
  "id": "1574107885415.9758865009470095",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload4",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "akvJyqFzC2HCU2S37og1QpxJ9qU=",
    "salt": "egl1qckhqla8sk673nw4c6b937u4n037",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574107885514.9508585164440418",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574107885415.9758865009470095",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "akvJyqFzC2HCU2S37og1QpxJ9qU=",
      "loginId": "bgqajohnload4",
      "salt": "egl1qckhqla8sk673nw4c6b937u4n037",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth_mobile_login]error code = 2303
loginId = bgqajohnload4
測試正常的流程
[user.balance.get] -> 
{
  "id": "1574107885521.3636028836659892",
  "method": "user.balance.get",
  "params": {},
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574107885613.82191835825572419",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574107885521.3636028836659892",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.balance.get",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.no.get] -> 
{
  "id": "1574107885619.6382334047475043",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574107885619.6382334047475043",
  "result": "191118161125356945",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1574107885694.6579521811844410",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {}
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574107885766.6917172711785476696",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574107885694.6579521811844410",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply] -> 
{
  "id": "1574107885772.2824492523078050",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191118161125356945",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 448
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574107885845.8736893099963037692",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574107885772.2824492523078050",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 448,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191118161125356945",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 1
[user.withdraw.apply] -> 
{
  "id": "1574107945864.6245204411951754",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191118161125356945",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 448
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574107945940.634486929245096352",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574107945864.6245204411951754",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 448,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191118161125356945",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 2
[user.withdraw.apply] -> 
{
  "id": "1574108005958.7117121231051551",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191118161125356945",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 448
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574108006031.650772403378458313",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574108005958.7117121231051551",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 448,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191118161125356945",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 3
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1574108066049.2764685388461306",
  "method": "user.balance.get",
  "params": {},
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574108066122.5595704699380022752",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574108066049.2764685388461306",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.balance.get",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
amount = 448
diff = NaN
[user.withdraw.no.get] -> 
{
  "id": "1574108186131.8980271710780128",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574108186131.8980271710780128",
  "result": "191118161626058356",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1574108186233.2851903708878456",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {}
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574108186303.1491958457932070944",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574108186233.2851903708878456",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過低的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1574108186315.0215676359786868",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191118161626058356",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 1
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574108186385.8642111847352155632",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574108186315.0215676359786868",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 1,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191118161626058356",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過高的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1574108186398.4274126269128097",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191118161626058356",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 30000000
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574108186472.79109861689788425",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574108186398.4274126269128097",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 30000000,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191118161626058356",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1574108186481.4072649717735919",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574108186481.4072649717735919",
  "result": "191118161626888501",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1574108186566.5142815352315388",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "123456789",
    "withdrawNo": "191118161626888501",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 448
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574108186637.6322890736773348864",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574108186566.5142815352315388",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 448,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191118161626888501",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1574108191650.1285655090217707",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574108191650.1285655090217707",
  "result": "191118161631248339",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1574108191728.6511918782055922",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191118161631248339",
    "payPasword": "123456789",
    "toBankId": 1,
    "amount": 448
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574108191799.1461455464726282852",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574108191728.6511918782055922",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 448,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191118161631248339",
      "payPasword": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload4
[auth.online.logout] -> 
{
  "id": "1574108191891.2565739557172371",
  "method": "auth.online.logout",
  "params": {},
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574108191988.2397367130372178197",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574108191891.2565739557172371",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.online.logout",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
Test finished i=1 t=5m6.28530799s
some thresholds have failed
```

