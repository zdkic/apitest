
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583957572673.2077030600090652",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "L76MhMfsjhYGExYj5qMyCZ+wWkQ=",
    "salt": "4g1kkylcn2qc85u7ksa7nxundb2iyxao",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583957572673.2077030600090652",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-11 16:12:52",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9857,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "qq": "",
      "passportNumber": null,
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "expiry": 1583961172773,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957572812.3876376877660374",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 1,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957572812.3876376877660374",
  "result": {
    "amount": 100,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 133,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957572927.9661839574236247",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 2,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957572927.9661839574236247",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957573006.0484398517816454",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 3,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957573006.0484398517816454",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957573093.3108401022001762",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 1,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957573093.3108401022001762",
  "result": {
    "amount": 300,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 333,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957573182.0962096178402393",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 2,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957573182.0962096178402393",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957573272.8946918237741198",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 3,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957573272.8946918237741198",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957573360.9603370283505510",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 1,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957573360.9603370283505510",
  "result": {
    "amount": 165,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 198,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957573454.4979559280827451",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 2,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957573454.4979559280827451",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583957573541.5248363620903656",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4",
    "chargeType": 3,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583957573541.5248363620903656",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583957573683.5984452535348987",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323193CC80AA312A56A23d4"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583957573683.5984452535348987",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=808.287149ms
```

