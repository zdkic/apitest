
```bash
[auth.mobile.login] -> 
{
  "id": "1574722920364.8327473625317933",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload3",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "tVv6RuZBwZs9/pXJUrWi6hBMkGM=",
    "salt": "ni7drlt4regezceqtio8i8k740qkd4hu",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574722920422.631490072612439329",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574722920364.8327473625317933",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "tVv6RuZBwZs9/pXJUrWi6hBMkGM=",
      "loginId": "bgqajohnload3",
      "salt": "ni7drlt4regezceqtio8i8k740qkd4hu",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.mobile.login]error code = 2303
[auth_mobile_login]error code = 2303
[auth.mobile.login] -> 
{
  "id": "1574722920451.4104680097721104",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload4",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "NLg6OAuf9HNkBpbL3TwB6JMKXAE=",
    "salt": "7sok192vq10kscrszof2xucsadlf7m1m",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574722920509.8556766718546657248",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574722920451.4104680097721104",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "NLg6OAuf9HNkBpbL3TwB6JMKXAE=",
      "loginId": "bgqajohnload4",
      "salt": "7sok192vq10kscrszof2xucsadlf7m1m",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.mobile.login]error code = 2303
[auth_mobile_login]error code = 2303
[sn.order.switch.list] -> 
{
  "id": "1574722921205.9949921795070760",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921282.7415176784783063679",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921205.9949921795070760",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722921245.0739945937752420",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921308.635043831612261024",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921245.0739945937752420",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722921242.1806678754351130",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921322.22522448260384768",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921242.1806678754351130",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722921273.0533951094734092",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921326.730481715540017184",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921273.0533951094734092",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722921306.5639223854175088",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921360.6794665075551418284",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921306.5639223854175088",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722921346.2132862295122244",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921404.8304074754294280959",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921346.2132862295122244",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
{
  "id": "1574722921385.9142541532908893",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921438.2409516583638550816",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921385.9142541532908893",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722921390.4560098352714280",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921443.6350831305637993",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921390.4560098352714280",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722921420.1304119320474682",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921475.9174957232461166428",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921420.1304119320474682",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722921429.6279234700750584",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921487.2305987170462091776",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921429.6279234700750584",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722921459.6558198977381165",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921517.1010496858946159428",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921459.6558198977381165",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722921463.8746928057784975",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921522.288266247728005192",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722921463.8746928057784975",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722921499.6876499022706433",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921553.6041388915929560440",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921499.6876499022706433",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722921514.1033754209193660",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921566.8923317131331631700",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722921514.1033754209193660",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722921548.0842235101664688",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327122BF41B2956B458f472",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921602.8070440618339336032",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921548.0842235101664688",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327122BF41B2956B458f472",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722921566.7787374410154072",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B4580934",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921619.7997472567442978560",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921566.7787374410154072",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B4580934",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722921565.1611477598100777",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921642.1199084088630313114",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921565.1611477598100777",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722921599.8565413860605990",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921652.145347843993961744",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921599.8565413860605990",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722921602.8801475423428995",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921678.324263983554297863",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921602.8801475423428995",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722921637.2606619602051447",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921694.2405568741572479011",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921637.2606619602051447",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722921666.5838467397961741",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921721.20407676694578176",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921666.5838467397961741",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 15,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722921702.8912077558847217",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921756.9221917309906370552",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921702.8912077558847217",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
[user.order.query] -> 
{
  "id": "1574722921756.6175456394643686",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "id": "1574722921756.8574689708742301",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921810.8784971874208448344",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921756.6175456394643686",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921810.1477555445081063684",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921756.8574689708742301",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722921779.2827349284071041",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921837.5457725022942261727",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921779.2827349284071041",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722921786.4363616823574345",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921840.306248075951735554",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921786.4363616823574345",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722921815.9489200931995643",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921869.9223350802533695358",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921815.9489200931995643",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722921835.7492554150316146",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921892.9097027014104367059",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722921835.7492554150316146",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.cost.sum] -> 
{
  "id": "1574722921860.4099323033822690",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722921919.9222521546759453816",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722921860.4099323033822690",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722921854.2723755858908606",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921920.8645463622295730175",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921854.2723755858908606",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 15,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722921889.3298610457419455",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327122BF41B2956B458f472",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921954.8632274408590719488",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921889.3298610457419455",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327122BF41B2956B458f472",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 13,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722921943.6668057522701447",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B4580934",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722921996.2417307143242778376",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921943.6668057522701447",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B4580934",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722921932.0403733097767892",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922010.879902601511519232",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921932.0403733097767892",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[sn.order.switch.list] -> 
[user.order.query] -> 
{
  "id": "1574722921963.8135710872037578",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "id": "1574722921991.3931551330172827",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922045.85573014489399872",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722921963.8135710872037578",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922044.2423645753521815720",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722921991.3931551330172827",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922001.7945177494558599",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922066.8331658618004766415",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922001.7945177494558599",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922035.8908223556016766",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922088.2594143835717501252",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922035.8908223556016766",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722922069.4748046285072045",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922126.315318069170947089",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922069.4748046285072045",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722922102.6995802723332526",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922160.1665224661704655116",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922102.6995802723332526",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722922112.1089476871651565",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922165.9220556393695851492",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922112.1089476871651565",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722922139.3839091141365723",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922199.8212268800729398840",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922139.3839091141365723",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722922150.7106268457139386",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922204.6322464469728361581",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922150.7106268457139386",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722922175.4000949846066217",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922229.513410366110171144",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922175.4000949846066217",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722922201.0384329967773190",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922258.5846842597181472",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722922201.0384329967773190",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722922215.0587023890907640",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922274.9040973399953752052",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922215.0587023890907640",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 8,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722922228.1090385297584121",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922284.9024847884730371",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722922228.1090385297584121",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722922250.6775485711278881",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327122BF41B2956B458f472",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922309.87855927465741056",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922250.6775485711278881",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327122BF41B2956B458f472",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922287.0380354909718200",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B4580934",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922340.9214266839062789632",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922287.0380354909718200",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B4580934",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922325.4842892074693558",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922379.2324484885332363274",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922325.4842892074693558",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722922300.7045228767476833",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922382.9149594458582399679",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922300.7045228767476833",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[sn.order.switch.list] -> 
{
  "id": "1574722922332.2668538123390104",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922409.8916526777174048767",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922332.2668538123390104",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722922373.5672792829053354",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922426.3497612467578863765",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922373.5672792829053354",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922407.4766237955119713",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922463.7977774884705927164",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922407.4766237955119713",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722922433.8876952917848542",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922491.360289852560002070",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922433.8876952917848542",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722922466.0214939482480861",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922520.8736981622525526016",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922466.0214939482480861",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722922488.8973032803407734",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922542.144124598383348452",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922488.8973032803407734",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722922518.9387529942105476",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922572.5173963001744194908",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922518.9387529942105476",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722922516.4873410073175173",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922574.8501579140811455598",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922516.4873410073175173",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722922541.3423451341001709",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922595.9194078836927216543",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922541.3423451341001709",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722922566.8529798386002389",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922624.1431617910424322",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722922566.8529798386002389",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722922572.1396316574087412",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922633.7970095594894508030",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922572.1396316574087412",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722922600.5430440400895170",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922652.5596741071299083744",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722922600.5430440400895170",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722922626.9646496544602413",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327122BF41B2956B458f472",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922700.36284422686930",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922626.9646496544602413",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327122BF41B2956B458f472",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 23,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922645.5091550144046525",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B4580934",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922707.9187061730465726318",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922645.5091550144046525",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B4580934",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 17,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722922656.4578110259624600",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922735.1211575093627535872",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922656.4578110259624600",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[sn.order.switch.list] -> 
{
  "id": "1574722922685.4117625148795172",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922761.6876996627771277055",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922685.4117625148795172",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722922743.5736099484110722",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922809.1155879195197572170",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922743.5736099484110722",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query] -> 
[user.order.query]error code = 3100
{
  "id": "1574722922727.8662198620640583",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922809.8484745376499037942",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922727.8662198620640583",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922761.1268287230999183",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922814.8779756923491710678",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922761.1268287230999183",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722922785.0789748516032226",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922839.3463901466682984712",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922785.0789748516032226",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722922836.2439311687263427",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922891.8971042838602907512",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922836.2439311687263427",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722922838.7027967220171540",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922892.6340434933634299711",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922838.7027967220171540",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.cost] -> 
{
  "id": "1574722922864.7717117305846169",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922917.9146504590000898038",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722922864.7717117305846169",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722922866.2604383784180787",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922923.1225119861970060033",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922866.2604383784180787",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722922906.5128552442190300",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922964.6879245474724823032",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922906.5128552442190300",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722922916.9611035606046558",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922968.8790867444480606095",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722922916.9611035606046558",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722922934.5621441900645956",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722922987.8646911198514118648",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922934.5621441900645956",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722922942.8883458140383077",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722922995.739193187092676624",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722922942.8883458140383077",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722922974.8448690262976376",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327122BF41B2956B458f472",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923027.36312473172984576",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722922974.8448690262976376",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327122BF41B2956B458f472",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923013.7623915250346586",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B4580934",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923067.2364393240348868864",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923013.7623915250346586",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B4580934",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722923011.4061119777128692",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923089.6034521593130710620",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923011.4061119777128692",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722923045.5832066906363629",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923098.217309713647026241",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923045.5832066906363629",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722923048.9733557355035641",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923125.435171884754141458",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923048.9733557355035641",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722923081.4270074847220866",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923135.636759035691780",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923081.4270074847220866",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923112.5966486913907401",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923166.252204225395428114",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923112.5966486913907401",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722923150.1501056543321576",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923203.8623820990545115836",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923150.1501056543321576",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
{
  "id": "1574722923190.1122079911131976",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923243.8493190625133051868",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923190.1122079911131976",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722923192.8882242441663255",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923247.7362233102022852332",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923192.8882242441663255",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923222.8919347634088327",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923280.8635335621293832960",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923222.8919347634088327",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722923228.3302748093389163",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923282.7484399807839206263",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923228.3302748093389163",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.cost.sum] -> 
{
  "id": "1574722923267.6121450092889797",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
[user.order.query] -> 
{
  "id": "1574722923264.3194601599550668",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923319.8534231472454761984",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923264.3194601599550668",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923319.5755228636754476416",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722923267.6121450092889797",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722923297.2386014677457954",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923351.8773009873483775612",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923297.2386014677457954",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722923306.5411452469580541",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923358.8402027976899428340",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722923306.5411452469580541",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722923336.8105019805334036",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327122BF41B2956B458f472",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923389.2669803902955700257",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923336.8105019805334036",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327122BF41B2956B458f472",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923370.5270035537862892",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B4580934",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923424.6628669468240688082",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923370.5270035537862892",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B4580934",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722923373.2186395425182662",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923455.2310699174329581576",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923373.2186395425182662",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722923411.9625249501544561",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923464.8903597765170231280",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923411.9625249501544561",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722923409.0851358818318530",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923493.1226177704296187912",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923409.0851358818318530",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722923440.4701870814661052",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923496.227542835934543874",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923440.4701870814661052",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 8,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923478.6125752752256127",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923543.6011143453197399808",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923478.6125752752256127",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 21,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722923517.4972166358635685",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923588.5752481866397235184",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923517.4972166358635685",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 21,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
{
  "id": "1574722923576.4471888037408891",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923631.4506350570963717",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923576.4471888037408891",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722923579.6792184439142623",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923633.4752836911602988864",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923579.6792184439142623",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923613.2971093490993461",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923667.8871948047011413465",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923613.2971093490993461",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722923613.6683149317844492",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923671.61942190209778752",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923613.6683149317844492",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722923628.7124901432886949",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923681.6840684141721141248",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923628.7124901432886949",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722923654.3642742833230318",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923711.8534096907587270390",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722923654.3642742833230318",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722923661.7688457217567613",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923715.3571425578777854064",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923661.7688457217567613",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722923694.5971385507147908",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923750.6844204174503034840",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722923694.5971385507147908",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722923700.6296917246886389",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327122BF41B2956B458f472",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923754.869618040143167504",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923700.6296917246886389",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327122BF41B2956B458f472",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923729.6332026135215502",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328127D1841FA56B4580934",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923782.351564659691768321",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923729.6332026135215502",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328127D1841FA56B4580934",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574722923734.9187669347470702",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923810.648590403580740113",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923734.9187669347470702",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574722923769.8469059451805468",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C056B458a2a2",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923828.9205048449987756024",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923769.8469059451805468",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C056B458a2a2",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
[sn.order.switch.list] -> 
{
  "id": "1574722923772.7423796166855565",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923872.9150179604947138492",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923772.7423796166855565",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 24,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
{
  "id": "1574722923799.5767385003977545",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201724BAA31E56B4584ecf",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923873.6321919025068555264",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923799.5767385003977545",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201724BAA31E56B4584ecf",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 24,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722923835.4071367562463793",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923902.9076615807587302910",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923835.4071367562463793",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 14,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574722923906.6986810566327594",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923977.2605337919317606467",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923906.6986810566327594",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
{
  "id": "1574722923931.8090539991640797",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722923984.4541405158637568",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722923931.8090539991640797",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722923929.3170716689444925",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456B4582240",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722923986.2852821045281296",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923929.3170716689444925",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE456B4582240",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574722924003.4724279778244414",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722924056.363264384673384477",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574722924003.4724279778244414",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574722923998.1743872484517907",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132411DCB8AE5756B4586d0d",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722924057.6538939457628258262",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722923998.1743872484517907",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132411DCB8AE5756B4586d0d",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574722924008.1478922019335260",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574722924060.9139473516879787452",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574722924008.1478922019335260",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574722924006.5290540790981064",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132512A4CE252856B4580d60",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722924064.6120952627322027966",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722924006.5290540790981064",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132512A4CE252856B4580d60",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574722924020.6834120182789151",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113261905E0A8B756B458ef6e",
    "startTime": "2018-08-01",
    "endTime": "2019-11-25 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574722924091.8177929784528862184",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574722924020.6834120182789151",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113261905E0A8B756B458ef6e",
      "endTime": "2019-11-25 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 11,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
Engine error error=context cancelled at core-js/shim.min.js:7:24691(41)
Engine Error
```

