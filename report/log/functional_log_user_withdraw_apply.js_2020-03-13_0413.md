
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1584044032370.8280129251917220",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "qg6VmfFHOWyqEqOFKqQEWbpS/dM=",
    "salt": "68yx8v44ueu4t2av6tjuwpo4zau9wzaj",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1584044032370.8280129251917220",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-12 16:13:52",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847651,
      "parentPathIncSelf": "/14679026/16847651/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 9885,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:49",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "expiry": 1584047632472,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1584044032514.4344824196589386",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1584044032514.4344824196589386",
  "result": {
    "balance": 505530.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1584051232472,
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.no.get] -> 
{
  "id": "1584044032623.9404863173360331",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1584044032623.9404863173360331",
  "result": "200312161352928307",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1584044032699.2762996821394233",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1584044032753.9151260004570758528",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1584044032699.2762996821394233",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply] -> 
{
  "id": "1584044032785.7680131030027043",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "withdrawNo": "200312161352928307",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 420
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1584044032839.104746349676725258",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1584044032785.7680131030027043",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 420,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200312161352928307",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 1
[user.withdraw.apply] -> 
{
  "id": "1584044092874.1767520773114159",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "withdrawNo": "200312161352928307",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 420
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1584044092929.41694683516898400",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1584044092874.1767520773114159",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 420,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200312161352928307",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 2
[user.withdraw.apply] -> 
{
  "id": "1584044152969.4103666752391905",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "withdrawNo": "200312161352928307",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 420
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1584044153022.703769389847036449",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1584044152969.4103666752391905",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 420,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200312161352928307",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 3
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1584044213054.6252857836799553",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1584044213054.6252857836799553",
  "result": {
    "balance": 505530.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1584051232472,
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 420
diff = 0
[user.withdraw.no.get] -> 
{
  "id": "1584044333138.8129048251653004",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1584044333138.8129048251653004",
  "result": "200312161853003408",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1584044333237.4588514110288451",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1584044333287.793906782589223796",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1584044333237.4588514110288451",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過低的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1584044333319.2818946652986745",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "withdrawNo": "200312161853003408",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 1
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1584044333370.8070133863468220256",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1584044333319.2818946652986745",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 1,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200312161853003408",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過高的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1584044333414.7199332311160164",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "withdrawNo": "200312161853003408",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 30000000
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1584044333465.595741869839237914",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1584044333414.7199332311160164",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 30000000,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200312161853003408",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1584044333494.1514560088109063",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1584044333494.1514560088109063",
  "result": "200312161853251258",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1584044333584.7760234904435075",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "123456789",
    "withdrawNo": "200312161853251258",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 420
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1584044333635.45036344166204035",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1584044333584.7760234904435075",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 420,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200312161853251258",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1584044338663.4367607352826209",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1584044338663.4367607352826209",
  "result": "200312161858193672",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1584044338740.4471417107670393",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
    "withdrawNo": "200312161858193672",
    "payPasword": "123456789",
    "toBankId": 1,
    "amount": 420
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2273",
    "sn": "1584044338792.577658687420498256",
    "message": "提现密码错误",
    "reason": "sn:ae00, userId:16847651 paypwd is wrong",
    "action": "null"
  },
  "id": "1584044338740.4471417107670393",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 420,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200312161858193672",
      "payPasword": "123456789",
      "sessionId": "ae0001011323171E1AB85F12E8F64e0e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1584044338889.0457198538311859",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12E8F64e0e"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1584044338889.0457198538311859",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=5m6.305185646s
some thresholds have failed
```

