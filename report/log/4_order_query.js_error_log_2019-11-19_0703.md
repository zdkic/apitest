
```bash
[auth.mobile.login] -> 
{
  "id": "1574118234670.5699872209456409",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload3",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "8voxqPJ/rT9GXymP33ikaGr2XBw=",
    "salt": "wbt1e0pnw8smtiohoy2e0p45p1yhaa1i",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574118234743.282679719428096",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574118234670.5699872209456409",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "8voxqPJ/rT9GXymP33ikaGr2XBw=",
      "loginId": "bgqajohnload3",
      "salt": "wbt1e0pnw8smtiohoy2e0p45p1yhaa1i",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.mobile.login]error code = 2303
[auth_mobile_login]error code = 2303
[auth.mobile.login] -> 
{
  "id": "1574118234753.5566716083531306",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload4",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "qP9Vz5oh0WpSS8/9xqPhfq9kNWk=",
    "salt": "9zvb8q6457ik2liq63qfd3zb9m6rr61o",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574118234826.8358164133366839134",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574118234753.5566716083531306",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "qP9Vz5oh0WpSS8/9xqPhfq9kNWk=",
      "loginId": "bgqajohnload4",
      "salt": "9zvb8q6457ik2liq63qfd3zb9m6rr61o",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.mobile.login]error code = 2303
[auth_mobile_login]error code = 2303
[sn.order.switch.list] -> 
{
  "id": "1574118235485.5289977258684827",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235582.5761993983802717696",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235485.5289977258684827",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118235517.8092112535326593",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235586.2630243624518107160",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235517.8092112535326593",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118235521.0092223522908433",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235614.9196246397801659392",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235521.0092223522908433",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118235551.6520513273549222",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235625.9223072681576742736",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235551.6520513273549222",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118235591.6089974350481001",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235661.606930762664993",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235591.6089974350481001",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118235623.6683843962861725",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235693.6016527421028564975",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235623.6683843962861725",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118235664.0126574936539848",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235735.8598769085787388904",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235664.0126574936539848",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118235673.9899694507480216",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235744.7773072081270602752",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235673.9899694507480216",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.cost] -> 
{
  "id": "1574118235702.5869370740717880",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235776.3462859410928910377",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235702.5869370740717880",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118235706.4380800162439771",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235776.2017612701865561097",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235706.4380800162439771",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118235736.1498859340943891",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235806.9079256478939855807",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235736.1498859340943891",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118235752.0486667372681970",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235821.2610087809163345933",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118235752.0486667372681970",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.cost.sum] -> 
{
  "id": "1574118235786.6593997573210986",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235854.6484339038432706544",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118235786.6593997573210986",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118235781.0739939479822440",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235857.36032721965287424",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235781.0739939479822440",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118235806.8276215284094001",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235875.180214354444027910",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235806.8276215284094001",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118235841.0909268468476348",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328160BF4143454DBEF5d12",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235911.2310381810466621506",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235841.0909268468476348",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328160BF4143454DBEF5d12",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118235846.6001000911432570",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235943.9223368051057359608",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235846.6001000911432570",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118235883.8001672856922081",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235953.3468616242433818792",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235883.8001672856922081",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118235882.6456333549486975",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118235977.2329575970002568708",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235882.6456333549486975",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118235920.4043022283685875",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118235990.7772007846731317248",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118235920.4043022283685875",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118235951.7966426745865335",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236025.2317976133716690180",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235951.7966426745865335",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118235985.1399534153330289",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236056.9079080441023021052",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118235985.1399534153330289",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118236032.0639073053863020",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
[user.order.cost] -> 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236102.5745952108604030912",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236032.0639073053863020",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
{
  "id": "1574118236034.0920430430475539",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236103.7489405294938422186",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236034.0920430430475539",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118236066.6590200231790066",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236135.8628174485423126012",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236066.6590200231790066",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118236067.0283209825556881",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236141.2486094087174883856",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236067.0283209825556881",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118236095.6145425687484337",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236170.9205285059164897197",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236095.6145425687484337",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118236111.5579377840624865",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236179.8285919343384722424",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118236111.5579377840624865",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118236133.3465835436174579",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236203.81707222268396291",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236133.3465835436174579",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118236149.8708988213315655",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236217.9149871328860028800",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118236149.8708988213315655",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118236167.6852437887171328",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236241.1444679115557832740",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236167.6852437887171328",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118236201.9926825613492188",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328160BF4143454DBEF5d12",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236275.5760097239114381053",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236201.9926825613492188",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328160BF4143454DBEF5d12",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118236211.4008933184904085",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236303.8808709705512186590",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236211.4008933184904085",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118236245.8359270249723429",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236319.4328276194385592576",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236245.8359270249723429",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118236243.5046547960755784",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236336.9178254632524692976",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236243.5046547960755784",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118236276.2242856390829614",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236350.8989096689008506430",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236276.2242856390829614",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118236310.6317623526518093",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236379.6306341291206114300",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236310.6317623526518093",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118236343.4834781008310322",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236413.1173191281043324996",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236343.4834781008310322",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118236382.1331412941818994",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236452.11329407742446101",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236382.1331412941818994",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118236387.5059928239285235",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236457.45183640677908526",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236387.5059928239285235",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.cost] -> 
{
  "id": "1574118236421.2609252406664994",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236491.882716728337647141",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236421.2609252406664994",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118236422.8511489382428209",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236517.1045435903858442248",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236422.8511489382428209",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118236464.7588994331738394",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236538.2184467933984458754",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118236464.7588994331738394",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118236453.3714310927104915",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236538.7484204020622295004",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236453.3714310927104915",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 15,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118236497.9029648045639568",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236566.34972190273",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236497.9029648045639568",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118236499.6647858387026793",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236572.6232906282031185184",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118236499.6647858387026793",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118236550.9171862037150839",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236620.289357513613180944",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236550.9171862037150839",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118236566.3652718403068714",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328160BF4143454DBEF5d12",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236636.6863483024209606126",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236566.3652718403068714",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328160BF4143454DBEF5d12",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118236569.7759801002526612",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236661.118255793605836824",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236569.7759801002526612",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118236598.2909277790492833",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236668.9029105642846470128",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236598.2909277790492833",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118236635.2093308831260273",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236706.72059832967366657",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236635.2093308831260273",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118236606.5944917853510902",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236705.2966262770332877417",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236606.5944917853510902",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118236669.5093122080225492",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236746.8898469370193507703",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236669.5093122080225492",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118236717.2326677293843630",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236790.5976158911409421207",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236717.2326677293843630",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118236744.1323977491591311",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236814.8935136712878652908",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236744.1323977491591311",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118236754.5959535080804306",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236823.8033959390588681978",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236754.5959535080804306",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118236785.9550522271555001",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236859.8923554920160752888",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236785.9550522271555001",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118236799.6065122193607101",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236871.6883724541996220354",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236799.6065122193607101",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.cost.sum] -> 
{
  "id": "1574118236831.3275582375164211",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236898.212232211897272401",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118236831.3275582375164211",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118236825.2330671985778867",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236902.8791020973839874040",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236825.2330671985778867",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118236850.4377680539157950",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236924.1531646798769176620",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236850.4377680539157950",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118236880.6084484536809966",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118236951.369964290992915976",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118236880.6084484536809966",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118236901.5431615079355325",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236971.3479075036206597401",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236901.5431615079355325",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118236926.3808401671485773",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328160BF4143454DBEF5d12",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118236998.9196348772139908656",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236926.3808401671485773",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328160BF4143454DBEF5d12",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118236931.6198561968706718",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237023.6176572246427877359",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236931.6198561968706718",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118236963.4427729507299519",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237032.8693912520416411040",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118236963.4427729507299519",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118236967.6797096565554944",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237058.4833816879844873",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118236967.6797096565554944",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118237008.1308421840464302",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237083.2450334013387194784",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237008.1308421840464302",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118237031.1411577197890771",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237100.1315073086267198480",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237031.1411577197890771",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118237066.0166200900781408",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237149.7417426334640111060",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237066.0166200900781408",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 12,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
{
  "id": "1574118237108.5569563092433709",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237176.2900323245335644673",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237108.5569563092433709",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118237110.4902771286713486",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237180.586594719083544782",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237110.4902771286713486",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118237140.8420713030439902",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237209.8610877539184148460",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237140.8420713030439902",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118237156.5736870232363704",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237230.865889917180397600",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237156.5736870232363704",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118237179.5887773024592482",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
[user.order.cost.sum] -> 
{
  "id": "1574118237184.6096551566469315",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237249.2497272746728245248",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237179.5887773024592482",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237252.8624243473307466485",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118237184.6096551566469315",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118237219.7475542569283802",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237292.9205206993836441583",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237219.7475542569283802",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118237237.0370604698800977",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237304.9205340041382969324",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118237237.0370604698800977",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 1,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118237258.1055834819456922",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237333.8898961919137808128",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237258.1055834819456922",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118237289.9561902651324934",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328160BF4143454DBEF5d12",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237363.9139421226915002616",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237289.9561902651324934",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328160BF4143454DBEF5d12",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118237292.0903067359240882",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237386.3107785125036032018",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237292.0903067359240882",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118237320.3679326911625936",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237389.299773103515518979",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237320.3679326911625936",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118237331.0361270007207266",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237422.1477503418800816254",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237331.0361270007207266",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118237362.0884344247540774",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237432.6881147783721516542",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237362.0884344247540774",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118237394.1119299998410451",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237464.6908511845770461173",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237394.1119299998410451",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118237430.7227334239262854",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237499.44548843929879954",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237430.7227334239262854",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
{
  "id": "1574118237471.8824950188372687",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237541.612612711805173780",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237471.8824950188372687",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 1,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118237473.0239170410041764",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237542.9181115579398356984",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237473.0239170410041764",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118237508.3118040737183855",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237578.6628306886212763600",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237508.3118040737183855",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118237511.3423525172031567",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237581.1915776446675550772",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237511.3423525172031567",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118237538.4017018503988457",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237607.297319218843222352",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237538.4017018503988457",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118237549.4783054718815939",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237618.6889799997053599444",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118237549.4783054718815939",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118237574.1413165590200270",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237648.129623642258540544",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237574.1413165590200270",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118237585.4175954416943755",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237653.7709025938938134526",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118237585.4175954416943755",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118237611.8553384735815760",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237687.5750744879893313464",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237611.8553384735815760",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118237652.8993483266835792",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328160BF4143454DBEF5d12",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237721.8988901723179040544",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237652.8993483266835792",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328160BF4143454DBEF5d12",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118237653.1505817693082625",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237746.72199210412541113",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237653.1505817693082625",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118237686.3150226683656262",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237760.7493954417292934073",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237686.3150226683656262",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118237690.5360089425146030",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237783.576514639672788736",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237690.5360089425146030",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118237717.5109151465273173",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237786.297424939099111952",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237717.5109151465273173",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118237755.4626417108257478",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237825.433190019932438565",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237755.4626417108257478",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118237791.1861228727161878",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237861.2344193995594596963",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237791.1861228727161878",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118237826.7972248831591455",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237896.4190148671356602112",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237826.7972248831591455",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118237833.7229057706311118",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237903.720857433108381696",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237833.7229057706311118",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118237863.7230440762467821",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237934.6831819579260402554",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237863.7230440762467821",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118237869.8479626835461363",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237942.8357308373049016319",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118237869.8479626835461363",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118237905.7912644157191966",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118237975.8565844283320416471",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237905.7912644157191966",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118237910.7749677084041358",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118237978.2200106435586",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118237910.7749677084041358",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118237942.5812638365327810",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238012.8342889713802067966",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237942.5812638365327810",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118237950.3055062310293161",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238018.6907517973128740607",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118237950.3055062310293161",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118237976.8949886240464832",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238046.74553522076061010",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118237976.8949886240464832",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118238009.1054239243466339",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328160BF4143454DBEF5d12",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238078.414340150807773577",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238009.1054239243466339",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328160BF4143454DBEF5d12",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118238016.7160576100795055",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238106.5170088391747172319",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118238016.7160576100795055",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118238046.8496460305710471",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F16B879B3C054DBEE69aa",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238116.9156810147692902",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238046.8496460305710471",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F16B879B3C054DBEE69aa",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.order.switch.list] -> 
{
  "id": "1574118238051.2409784951024038",
  "jsonrpc": "2.0",
  "method": "sn.order.switch.list",
  "params": {
    "terminal": 8
  }
}
[sn.order.switch.list] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238144.4085890766235762690",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118238051.2409784951024038",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.order.switch.list",
    "params": {
      "reqIp": "202.11.82.1",
      "terminal": 8,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.order.switch.list]error code = 700
[user.order.query] -> 
{
  "id": "1574118238083.7261967728728818",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E4354DBEEa913",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238157.9160233590133338624",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238083.7261967728728818",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E4354DBEEa913",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118238114.1480322493133401",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238183.2891926724467032848",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118238114.1480322493133401",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.query] -> 
{
  "id": "1574118238151.7195530016754120",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238221.8902820403604340474",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118238151.7195530016754120",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 700
[user.order.cost] -> 
{
  "id": "1574118238190.1417923904300052",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238259.1171656098801910484",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118238190.1417923904300052",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118238188.5239912005612312",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113231129BB332854DBEEc1cb",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238257.1783643748457906193",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238188.5239912005612312",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113231129BB332854DBEEc1cb",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost] -> 
{
  "id": "1574118238229.7002305651110334",
  "jsonrpc": "2.0",
  "method": "user.order.cost",
  "params": {}
}
[user.order.cost] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238299.1441187137780731265",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574118238229.7002305651110334",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 1,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost]error code = 700
[user.order.query] -> 
{
  "id": "1574118238226.3512616238432836",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132412E841BA6A54DBEE2020",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238300.182397997623020044",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238226.3512616238432836",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132412E841BA6A54DBEE2020",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118238268.7454536884467441",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238336.9186745096014313424",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118238268.7454536884467441",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118238268.6395279550923530",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325183D46FA1654DBEF78c7",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238339.920214354690850816",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238268.6395279550923530",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325183D46FA1654DBEF78c7",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1574118238297.9486113792755098",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4154DBEFfb43",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238367.563521185316872",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238297.9486113792755098",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4154DBEFfb43",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.cost.sum] -> 
{
  "id": "1574118238306.7036383192933582",
  "jsonrpc": "2.0",
  "method": "user.order.cost.sum",
  "params": {}
}
[user.order.cost.sum] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574118238374.153195092826652706",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574118238306.7036383192933582",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.cost.sum",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.cost.sum]error code = 700
[user.order.query] -> 
{
  "id": "1574118238342.9837626046489130",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011327199C81874854DBEFe4d0",
    "startTime": "2018-08-01",
    "endTime": "2019-11-18 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1574118238411.6049882115307994624",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1574118238342.9837626046489130",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011327199C81874854DBEFe4d0",
      "endTime": "2019-11-18 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
Engine error error=context cancelled at core-js/shim.min.js:1:1(0)
Test finished i=0 t=3.011099392s
No data generated, because no script iterations finished, consider making the test duration longer
some thresholds have failed
```

