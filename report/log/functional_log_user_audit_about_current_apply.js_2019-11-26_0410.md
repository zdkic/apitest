
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574712655528.1465901726538611",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "Ft0iR2jJxo4z4qMB/FqF7Zq1LF0=",
    "salt": "ywnumkvjf2y53hn3bymmdnd6o84vg6jh",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574712655528.1465901726538611",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-25 16:10:55",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5602,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323181B7E5EE456AC537757",
    "expiry": 1574716255619,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.withdraw.option.get] -> 
{
  "id": "1574712655649.1622203636253960",
  "jsonrpc": "2.0",
  "method": "user.withdraw.option.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757",
    "terminal": 8
  }
}
[user.withdraw.option.get] <- 
{
  "id": "1574712655649.1622203636253960",
  "result": {
    "feeAmount": 0,
    "minAmount": 2,
    "amount": 0,
    "availableAmount": 500352.2,
    "balance": {
      "balance": 500352.2
    },
    "sn": "ae00",
    "nonFeeTimes": 0,
    "maxAmount": 2000000,
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.account.default.get] -> 
{
  "id": "1574712655760.7734546857429304",
  "jsonrpc": "2.0",
  "method": "user.withdraw.account.default.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757",
    "terminal": 8
  }
}
[user.withdraw.account.default.get] <- 
{
  "id": "1574712655760.7734546857429304",
  "result": {
    "bankAccount": "123456789012",
    "accountStatus": 1,
    "bankAccountOwner": "user",
    "bankId": 1,
    "isDefault": 1,
    "bankBranch": "1111",
    "memo": null,
    "bankName": "工商银行",
    "sn": "ae00",
    "userId": 16847651,
    "withdrawAccountId": 31285,
    "lastUpdateTime": "2018-09-06 05:46:20"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.cancel] -> 
{
  "id": "1574712655847.3865072101114889",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  }
}
[user.audit.about.current.cancel] <- 
{
  "id": "1574712655847.3865072101114889",
  "result": 1,
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.no.get] -> 
{
  "id": "1574712655933.4787467302502658",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574712655933.4787467302502658",
  "result": "191125161055971429",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574712656015.7627277439427068",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "191125161055971429",
    "amount": 424
  }
}
[user.audit.about.current.apply] <- 
{
  "id": "1574712656015.7627277439427068",
  "result": 1,
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.trace] -> 
{
  "id": "1574712656096.7475725445146027",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  }
}
[user.audit.about.current.trace] <- 
{
  "id": "1574712656096.7475725445146027",
  "result": {
    "state": 0,
    "withdrawAmount": 424,
    "applyTime": "2019-11-25 16:10:56",
    "withdrawNo": "191125161055971429",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M="
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1574712656178.9005875040263857",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574712656178.9005875040263857",
  "result": "191125161056522481",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574712656267.5563125430963939",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "123456789",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "191125161056522481",
    "amount": 478
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574712656322.78900993097466696",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574712656267.5563125430963939",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 478,
      "reqIp": "202.11.82.1",
      "withdrawNo": "191125161056522481",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1574712656347.5400915682105990",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574712656347.5400915682105990",
  "result": "191125161056148861",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574712656426.3819325043589383",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757",
    "payPasword": "123456789",
    "withdrawNo": "191125161056148861",
    "amount": 518
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2273",
    "sn": "1574712656513.288235186619156480",
    "message": "提现密码错误",
    "reason": "sn:ae00, userId:16847651 paypwd is wrong",
    "action": "null"
  },
  "id": "1574712656426.3819325043589383",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 518,
      "reqIp": "202.11.82.1",
      "withdrawNo": "191125161056148861",
      "payPasword": "123456789",
      "sessionId": "ae0001011323181B7E5EE456AC537757",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 24,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的充值流水号
[user.withdraw.no.get] -> 
{
  "id": "1574712656538.4754432872355510",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574712656538.4754432872355510",
  "result": "191125161056466632",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574712656628.6206351790909855",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "123456789",
    "amount": 262
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2902",
    "sn": "1574712656686.149053718107603201",
    "message": "当前操作重复或太频繁，请稍后操作。",
    "reason": "cache key already exists.",
    "action": "null"
  },
  "id": "1574712656628.6206351790909855",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 262,
      "reqIp": "202.11.82.1",
      "withdrawNo": "123456789",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323181B7E5EE456AC537757",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的取款金额
[user.withdraw.no.get] -> 
{
  "id": "1574712656710.5369058246993578",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574712656710.5369058246993578",
  "result": "191125161056483032",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574712656796.5758511547834684",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "191125161056483032",
    "amount": 240000
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2902",
    "sn": "1574712656854.8016397990577043048",
    "message": "当前操作重复或太频繁，请稍后操作。",
    "reason": "cache key already exists.",
    "action": "null"
  },
  "id": "1574712656796.5758511547834684",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "withdrawNo": "191125161056483032",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323181B7E5EE456AC537757",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574712656928.0591244478180402",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323181B7E5EE456AC537757"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574712656928.0591244478180402",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.230314453s
```

