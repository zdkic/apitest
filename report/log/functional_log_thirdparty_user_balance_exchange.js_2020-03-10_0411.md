
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583784712548.2164945756262218",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "05qYtLbR3TgSApyYNaPNZZK9QSU=",
    "salt": "ypioi2s68o9uts0mel0cc32qp6v7m3si",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583784712548.2164945756262218",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-09 16:11:52",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9804,
      "regIp": "202.11.82.1",
      "parentId": 14679026,
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "expiry": 1583788312645,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1583784712687.3667404717039554",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583784712687.3667404717039554",
  "result": {
    "balance": 505418.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583791912645,
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583784712802.7898423597286268",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "from": 0,
    "to": 4,
    "amount": 14
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583784712802.7898423597286268",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583784712884.2369404607882698",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583784712884.2369404607882698",
  "result": {
    "balance": 505418.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583791912645,
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 14
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1583784712964.5425573096439424",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1583784712964.5425573096439424",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583784713042.5865772558207072",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 14
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583784713107.1441167894016967200",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583784713042.5865772558207072",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 14,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583784713146.6574062539672274",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583784713146.6574062539672274",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583784713225.1523234158626967",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "from": -1,
    "to": 5,
    "amount": 14
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1583784713283.471807111190413572",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:14.0",
    "action": "null"
  },
  "id": "1583784713225.1523234158626967",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 14,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae00010113231538CDE3F4121E5E31b2",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583784713310.3399957995044302",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2",
    "from": 0,
    "to": -5,
    "amount": 14
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583784713310.3399957995044302",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583784713461.1522198762183971",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231538CDE3F4121E5E31b2"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583784713461.1522198762183971",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=703.11095ms
some thresholds have failed
```

