
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574712677220.9096193298723617",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "GE/ajJ+8XxlLESMyt8r3KJd68o4=",
    "salt": "oezh9cptj1wcyo57tdon9b0k40jmkhf6",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574712677220.9096193298723617",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-25 16:11:17",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5605,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231223A719CE56AC570bdb",
    "expiry": 1574716277349,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1574712677384.3280306536151463",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC570bdb"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574712677384.3280306536151463",
  "result": {
    "balance": 500352.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574716277349,
    "sessionId": "ae00010113231223A719CE56AC570bdb",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1574712677493.5456987238060470",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC570bdb",
    "amount": 17
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1574712677493.5456987238060470",
  "result": {
    "success": "1",
    "cashflowId": 648737519249395700,
    "balance": 500369.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1574712677575.1711875006688572",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC570bdb"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574712677575.1711875006688572",
  "result": {
    "balance": 500369.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574719877349,
    "sessionId": "ae00010113231223A719CE56AC570bdb",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 17
diff = 17
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1574712677657.5068837553364155",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 17
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574712677725.38447829194244420",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574712677657.5068837553364155",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 17,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 20,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1574712677756.5460203623487919",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC570bdb",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1574712677812.4396654015103910416",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1574712677756.5460203623487919",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231223A719CE56AC570bdb",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574712677883.7446960174932962",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC570bdb"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574712677883.7446960174932962",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=451.413229ms
```

