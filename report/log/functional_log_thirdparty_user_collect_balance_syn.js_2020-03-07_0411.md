
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583525533341.2727087356065093",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "cy2FbQ0V2JX1VkZAOTjkWojFofA=",
    "salt": "9gyy6cgo12ig20m1w3mm612igc7lc7zk",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583525533341.2727087356065093",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-06 16:12:13",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9730,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323193CC80AA31153E22a17",
    "expiry": 1583529133436,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常流程
[thirdparty.user.collect.balance.syn] -> 
{
  "id": "1583525533471.1855361381869494",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.syn",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153E22a17"
  }
}
[thirdparty.user.collect.balance.syn] <- 
{
  "id": "1583525533471.1855361381869494",
  "result": true,
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[thirdparty.user.collect.balance.info] -> 
{
  "id": "1583525533589.5617138631762078",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.info",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153E22a17"
  }
}
[thirdparty.user.collect.balance.info] <- 
{
  "id": "1583525533589.5617138631762078",
  "result": {},
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID(thirdparty_user_collect_balance_syn)
[thirdparty.user.collect.balance.syn] -> 
{
  "id": "1583525533670.0301655610483880",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.syn",
  "params": {
    "sessionId": "123456789"
  }
}
[thirdparty.user.collect.balance.syn] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525533724.8275355519200262640",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583525533670.0301655610483880",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.collect.balance.syn",
    "params": {
      "reqIp": "202.11.82.1",
      "ip": "202.11.82.1",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID(thirdparty_user_collect_balance_info)
[thirdparty.user.collect.balance.info] -> 
{
  "id": "1583525533751.2438036892654656",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.info",
  "params": {
    "sessionId": "123456789"
  }
}
[thirdparty.user.collect.balance.info] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525533801.9220413178045773552",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583525533751.2438036892654656",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.collect.balance.info",
    "params": {
      "reqIp": "202.11.82.1",
      "ip": "202.11.82.1",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583525533873.0643867807567744",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153E22a17"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583525533873.0643867807567744",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=356.074204ms
```

