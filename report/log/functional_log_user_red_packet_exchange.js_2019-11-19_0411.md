
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574107878192.8856993398934386",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "ENAIAMrg3uHCVsErVaRpp79JW/E=",
    "salt": "wuomu6gq42embh4vaqa15y7vnkirz1lc",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574107878192.8856993398934386",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-18 16:11:18",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5581,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231129BB332854D3D7ed0b",
    "expiry": 1574111478298,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1574107878310.1933676666459601",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231129BB332854D3D7ed0b"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574107878310.1933676666459601",
  "result": {
    "balance": 500324.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574115078298,
    "sessionId": "ae00010113231129BB332854D3D7ed0b",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1574107878419.4159708741453098",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231129BB332854D3D7ed0b",
    "amount": 28
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1574107878419.4159708741453098",
  "result": {
    "success": "1",
    "cashflowId": 646200808145424400,
    "balance": 500352.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1574107878504.4645096179203821",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231129BB332854D3D7ed0b"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574107878504.4645096179203821",
  "result": {
    "balance": 500352.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574115078298,
    "sessionId": "ae00010113231129BB332854D3D7ed0b",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 28
diff = 28
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1574107878586.2909288372589471",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 28
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574107878658.876513318876023458",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574107878586.2909288372589471",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 28,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1574107878666.1159683831376980",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231129BB332854D3D7ed0b",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1574107878742.9221665244994599904",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1574107878666.1159683831376980",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB332854D3D7ed0b",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574107878811.3233608047531605",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231129BB332854D3D7ed0b"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574107878811.3233608047531605",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=438.141032ms
```

