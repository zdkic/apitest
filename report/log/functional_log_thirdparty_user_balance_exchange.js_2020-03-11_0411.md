
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583871117198.2746349873998425",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "3ZDvLjA4PkfEu2IgACzXdYku7a8=",
    "salt": "rj3qjunvjvcc9fex3vc499l1447aau74",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583871117198.2746349873998425",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-10 16:11:57",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847651,
      "parentPathIncSelf": "/14679026/16847651/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 9829,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:49",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "expiry": 1583874717271,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1583871117319.8928378525505130",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583871117319.8928378525505130",
  "result": {
    "balance": 505444.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583878317271,
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583871117428.1462516236098594",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "from": 0,
    "to": 4,
    "amount": 24
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583871117428.1462516236098594",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583871117506.5500624339944416",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583871117506.5500624339944416",
  "result": {
    "balance": 505444.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583878317271,
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 24
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1583871117595.9250691701110061",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1583871117595.9250691701110061",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583871117671.5955274278911212",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 24
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583871117704.63382997117716784",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583871117671.5955274278911212",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 24,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583871117750.0430052394568006",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583871117750.0430052394568006",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583871117829.9292492508507522",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "from": -1,
    "to": 5,
    "amount": 24
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1583871117862.3332673113052676360",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:24.0",
    "action": "null"
  },
  "id": "1583871117829.9292492508507522",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 24,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae000101132310FD02CC641261DF7fa4",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583871117909.7693108965883848",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4",
    "from": 0,
    "to": -5,
    "amount": 24
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583871117909.7693108965883848",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583871118058.7584679449757866",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132310FD02CC641261DF7fa4"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583871118058.7584679449757866",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=668.048793ms
some thresholds have failed
```

