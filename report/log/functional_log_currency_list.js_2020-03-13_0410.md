
```bash
loginId = bgqajohnload1
[auth.mobile.login] -> 
{
  "id": "1584043839038.7082405137897127",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "S4PMmiV1g7+U1ghQ9D5oTfONE74=",
    "salt": "9k45ld5cpn5r1iqw3jkluv7kj8caceag",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1584043839038.7082405137897127",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2020-03-12 16:10:39",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847647,
      "parentPathIncSelf": "/14679026/16847647/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 27507,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:48",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 413,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F173906B5DB12E8CF8092",
    "expiry": 1584047439141,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
測試正常的流程
[currency.list] -> 
{
  "id": "1584043839179.5292567170548285",
  "method": "currency.list",
  "params": {
    "sn": "ae00"
  },
  "jsonrpc": "2.0"
}
[currency.list] <- 
{
  "id": "1584043839179.5292567170548285",
  "result": [
    {
      "currencyName": "人民币",
      "rate": 1,
      "currencySymbol": null,
      "sn": "BG00",
      "currencyCountry": null,
      "currencyId": 1,
      "currencyCode": "CNY",
      "status": 1
    }
  ],
  "error": null,
  "jsonrpc": "2.0"
}
currency.sn = BG00
測試获取单个币种
[currency.get] -> 
{
  "id": "1584043839278.4744908341063108",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": 1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1584043839278.4744908341063108",
  "result": {
    "currencyName": "人民币",
    "rate": 1,
    "currencySymbol": "￥",
    "sn": "BG00",
    "currencyCountry": null,
    "currencyId": 1,
    "currencyCode": "CNY",
    "status": 1
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的币种ID
[currency.get] -> 
{
  "id": "1584043839354.2004527557686883",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": -1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1584043839354.2004527557686883",
  "result": {},
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
[auth.online.logout] -> 
{
  "id": "1584043839487.9557123416778016",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F173906B5DB12E8CF8092"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1584043839487.9557123416778016",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=254.128761ms
```

