

```bash
[user.charge.list] -> 
{
  "id": "1583973054588.7462976076968511",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973054660.2922231525033152",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973054588.7462976076968511",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973054693.9206915150246685",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973054752.41957585848609",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973054693.9206915150246685",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583973054466.5878178382577429",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101131F136A8882D412B182c641",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583973054466.5878178382577429",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "endTime": "2020-03-12 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 219,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583973055065.1027295474573386",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F136A8882D412B182c641",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973055138.594475428522576172",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055065.1027295474573386",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973055185.9520966981051912",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973055257.599022937064751174",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055185.9520966981051912",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973055228.9193726226706440",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973055274.9184941320152023021",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055228.9193726226706440",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973055294.5036311051010575",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973055342.875046432321585171",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055294.5036311051010575",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583973055306.7107925824510916",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583973055306.7107925824510916",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "endTime": "2020-03-12 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 36,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.order.query] -> 
{
  "id": "1583973055479.3455213006550509",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973055526.36169560822513690",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055479.3455213006550509",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973055486.7462547790974548",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326183EA03D4112B182c0f3",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973055560.4820295520108548",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055486.7462547790974548",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae0001011326183EA03D4112B182c0f3",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973055547.7911629102847773",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973055619.2253189514529024",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055547.7911629102847773",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973055848.1628623605381258",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113221134FB934812B1824c11",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973055920.9205321179693104128",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055848.1628623605381258",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973055893.6219868103132327",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973055942.648531714993553668",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973055893.6219868103132327",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973056029.5444014460972450",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973056101.9220642234695940",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056029.5444014460972450",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973056130.8596329535775455",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973056179.3278990140868002440",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056130.8596329535775455",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973056451.0003034825756458",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113221134FB934812B1824c11",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973056556.9223295895204264948",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056451.0003034825756458",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 33,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973056571.1140322879701743",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973056642.8917120905574727599",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056571.1140322879701743",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973056672.2677402350188139",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973056720.288832189194436872",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056672.2677402350188139",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973056751.2781772834289296",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973056824.9123623507811221360",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056751.2781772834289296",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973056876.4876814952039340",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F136A8882D412B182c641",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973056950.8538807008277544950",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056876.4876814952039340",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973056979.9689560428868594",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F136A8882D412B182c641",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973057027.7493974656490665980",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973056979.9689560428868594",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973057236.9910804713202629",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973057313.29291145499509108",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973057236.9910804713202629",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973057415.1364635498853006",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973057489.8573586765351222175",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973057415.1364635498853006",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973057523.4004955347201957",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973057570.8933987173224415223",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973057523.4004955347201957",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973057539.9615842141299517",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973057615.9220554984878944704",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973057539.9615842141299517",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973057583.7576769034786534",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F136A8882D412B182c641",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973057630.63334638739146057",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973057583.7576769034786534",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973057644.5600444705893948",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973057694.8140078686622695163",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973057644.5600444705893948",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973058117.1851931617440716",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973058164.1152932499724239104",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973058117.1851931617440716",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583973058903.7991440682177685",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973058952.1192056061335060480",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973058903.7991440682177685",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583973059343.8554340509165571",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583973059343.8554340509165571",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 37,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.order.query] -> 
{
  "id": "1583973059616.3451924747577026",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973059664.49752076826199204",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973059616.3451924747577026",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583973059743.9391130106123246",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973059791.8921054619828091992",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973059743.9391130106123246",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973059765.5557495976733798",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973059854.7205759376278142373",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973059765.5557495976733798",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 48,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973059931.9620129164902696",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973059979.7493971974617562656",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973059931.9620129164902696",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583973059944.8069495895548120",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583973059944.8069495895548120",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 39,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[sn.lottery.order.query] -> 
{
  "id": "1583973060010.2087690391018428",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583973060010.2087690391018428",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 37,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.order.query] -> 
{
  "id": "1583973060167.4869894480919563",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973060213.9079176996335894416",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060167.4869894480919563",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973060487.7241439399846269",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F136A8882D412B182c641",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973060560.9217420745462773680",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060487.7241439399846269",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973060531.7597457081726474",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973060579.9187059110433781248",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060531.7597457081726474",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973060556.6677195768503195",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973060605.2625739870605230144",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060556.6677195768503195",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973060635.1755859464834153",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973060682.6800223492426759728",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060635.1755859464834153",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973060667.7795458379705989",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973060738.7329573199237676010",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060667.7795458379705989",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973060766.6814309213724321",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973060814.2895974631753976209",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060766.6814309213724321",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973060768.5432725709486244",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973060814.27109720092051541",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060768.5432725709486244",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973060788.9381869909470052",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011324191E03B79912B182022f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973060860.5858340845667398",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060788.9381869909470052",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847652
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973060953.0853600662521061",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061002.8935134648084709360",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060953.0853600662521061",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973060970.2475048902596658",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973061043.1928389910286320660",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973060970.2475048902596658",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973061032.7375516439191068",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973061108.686873778819432514",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061032.7375516439191068",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847656
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973061095.3682332603625441",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061155.9187323308771639200",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061095.3682332603625441",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583973061089.6239282105513423",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101131F136A8882D412B182c641",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583973061089.6239282105513423",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "endTime": "2020-03-12 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 46,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.charge.list] -> 
{
  "id": "1583973061270.0025214599963556",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061340.613970866362925184",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061270.0025214599963556",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973061330.7351438584438209",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061401.8573724354493609759",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061330.7351438584438209",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973061369.6849972378112599",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061416.8642912626553126624",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061369.6849972378112599",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973061430.1041812858274498",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061478.7923510492573712350",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061430.1041812858274498",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973061553.8365026418338412",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061599.604645676500977920",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061553.8365026418338412",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973061605.4803628998721213",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061653.7772928730938193886",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061605.4803628998721213",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973061580.5648531009025869",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061656.2324599064035724881",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061580.5648531009025869",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973061616.9594550506008858",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973061666.7854266417208491319",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061616.9594550506008858",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583973061685.3755753223625611",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061734.288441900078481474",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061685.3755753223625611",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973061686.1919278764941498",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061736.266310589758914817",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061686.1919278764941498",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973061692.4145626948672905",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F136A8882D412B182c641",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973061766.2382087543706485904",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061692.4145626948672905",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973061752.4935378658207639",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061803.1234549348641541392",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061752.4935378658207639",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973061833.5527834782898245",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973061882.6771495928048386016",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061833.5527834782898245",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973061933.3709063049691208",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973062006.4071281781251246352",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973061933.3709063049691208",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973062156.3957354554675792",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062203.40849365570947348",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062156.3957354554675792",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973062189.3725985883052897",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062261.6845251511947475665",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062189.3725985883052897",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973062289.2839425678040191",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062337.6645760485890194899",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062289.2839425678040191",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973062414.7760640529025489",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973062497.9205277786160020464",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062414.7760640529025489",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973062475.4480498297548253",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113221134FB934812B1824c11",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973062551.6222274289748211648",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062475.4480498297548253",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973062659.0461127255031125",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F136A8882D412B182c641",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973062741.289358502236258349",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062659.0461127255031125",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 28,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973062656.1710605211384323",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062752.7762655149467369472",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062656.1710605211384323",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 10,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973062781.8234972512632651",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062829.3117987124205322374",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062781.8234972512632651",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973062782.0720294788304823",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062830.1586955919533753232",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062782.0720294788304823",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973062782.1603199610501747",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973062832.9169204316534063104",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062782.1603199610501747",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973062782.6310664498399721",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062854.3620896325207328320",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062782.6310664498399721",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973062861.4824057401913687",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062908.7515802438637434864",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062861.4824057401913687",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973062883.5633727563375186",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973062930.9214129205921430912",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062883.5633727563375186",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973062897.7823900996861066",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F136A8882D412B182c641",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973062969.8637867387888074496",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973062897.7823900996861066",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101131F136A8882D412B182c641",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973063012.4506505864081810",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973063059.6624504754987859820",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063012.4506505864081810",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973063139.3818646737020288",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973063210.936776161429570192",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063139.3818646737020288",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973063240.9354736899448743",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973063288.6754623355883470713",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063240.9354736899448743",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973063249.3316182360788482",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011324191E03B79912B182022f",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973063298.9007225024875792",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063249.3316182360788482",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847652
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973063375.5555783721358860",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973063433.8322335039109855160",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063375.5555783721358860",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973063627.8483816226277139",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973063677.355788769716355072",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063627.8483816226277139",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[sn.lottery.order.query] -> 
{
  "id": "1583973063723.5957750956472700",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583973063723.5957750956472700",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 38,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.order.query] -> 
{
  "id": "1583973063840.8444671017264260",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973063893.7777690098152766143",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063840.8444671017264260",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 8,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973063942.1644176354460990",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011326183EA03D4112B182c0f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973063994.8790392740335583096",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063942.1644176354460990",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4112B182c0f3",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973063953.5998297244526575",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064002.8922183770121355200",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063953.5998297244526575",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973064006.4140347598401084",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064055.8780858149263556535",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064006.4140347598401084",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973063987.0285231041237684",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973064060.603582406716375049",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973063987.0285231041237684",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583973064024.9994233215993200",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011326183EA03D4112B182c0f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064071.9205320184552488608",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064024.9994233215993200",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4112B182c0f3",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973064028.9584134026845973",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011326183EA03D4112B182c0f3",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064074.2369957825741999624",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064028.9584134026845973",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011326183EA03D4112B182c0f3",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973064032.7198365633458259",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113221134FB934812B1824c11",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064081.8038778684263347870",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064032.7198365633458259",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973064101.0636538346156891",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113221134FB934812B1824c11",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973064152.3562363798029419520",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064101.0636538346156891",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113221134FB934812B1824c11",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973064105.7944919782527932",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973064155.890604472202430465",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064105.7944919782527932",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583973064166.7504260526694994",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064219.8132035990844850128",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064166.7504260526694994",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583973064299.5630972515686266",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132519EA461C7B12B182e723",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973064348.2396478089174453506",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064299.5630972515686266",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae000101132519EA461C7B12B182e723",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973064474.4242662477704611",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973064524.1152958073057853991",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064474.4242662477704611",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583973064647.7759136776269596",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011326183EA03D4112B182c0f3",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583973064696.9165388018922209216",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064647.7759136776269596",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-12 23:59:59",
      "sessionId": "ae0001011326183EA03D4112B182c0f3",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583973064657.7491420417744907",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064705.2378395749093884960",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064657.7491420417744907",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973064742.0597357986566904",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064797.2882331524522311973",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064742.0597357986566904",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973064765.6543950594096348",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064837.1878283670928820752",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064765.6543950594096348",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973064829.6810643501360186",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064877.1829895916316804098",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064829.6810643501360186",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973064867.8899808429759930",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011320105B21859612B18239e0",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064914.648604780161549380",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064867.8899808429759930",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011320105B21859612B18239e0",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973064891.7718056775941811",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064938.5548214520752617214",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064891.7718056775941811",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973064939.5534017459572625",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973064987.219587865363957435",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064939.5534017459572625",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973064967.6060198072933355",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065015.910589691601371269",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973064967.6060198072933355",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973065016.1745924069727560",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132813090F70FB12B18239e5",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065062.6770033683051034608",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973065016.1745924069727560",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132813090F70FB12B18239e5",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973065005.7343144986505846",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065080.1807773045650163456",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973065005.7343144986505846",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583973065110.0755469011278115",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011324191E03B79912B182022f",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065158.9218529718788618240",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973065110.0755469011278115",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011324191E03B79912B182022f",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583973065167.5694575807761422",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065214.9214131115814468288",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973065167.5694575807761422",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973065179.9465468623291591",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065228.8853049922188984316",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973065179.9465468623291591",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583973065187.6511750265564695",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113271757F40A9C12B1822d93",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065258.90247931666908196",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973065187.6511750265564695",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113271757F40A9C12B1822d93",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.audit.about.current.cancel] -> 
{
  "id": "1583973065212.7383813910794319",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae000101132313A7A7599B12B1824ea1"
  }
}
[user.audit.about.current.cancel] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583973065270.7600108443009728",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.cancel",
    "action": "null"
  },
  "id": "1583973065212.7383813910794319",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.cancel",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132313A7A7599B12B1824ea1",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 14,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.cancel]error code = 502
[user.withdraw.list] -> 
{
  "id": "1583973065259.8969573409790943",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132118D07D44BC12B1827973",
    "startTime": "2018-08-01",
    "endTime": "2020-03-12 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583973065306.1801442058712399908",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583973065259.8969573409790943",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132118D07D44BC12B1827973",
      "endTime": "2020-03-12 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
Engine error error=context cancelled at d (file:///home/zdkic/Project/apiTest/lib/utils.js:229:6249(9))
Engine Error
```

