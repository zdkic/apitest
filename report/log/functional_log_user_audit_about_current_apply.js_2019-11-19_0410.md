
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574107855996.4117629955291321",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "tcX1gzH2c+rR4lTYJTvtLyj/9C4=",
    "salt": "cocrbnusvg8v7dh0wnwdz9kzrt1p2fc0",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574107855996.4117629955291321",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-18 16:10:56",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5578,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132310FD02CC6454D3D33074",
    "expiry": 1574111456099,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.withdraw.option.get] -> 
{
  "id": "1574107856110.4749080232917222",
  "jsonrpc": "2.0",
  "method": "user.withdraw.option.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074",
    "terminal": 8
  }
}
[user.withdraw.option.get] <- 
{
  "id": "1574107856110.4749080232917222",
  "result": {
    "feeAmount": 0,
    "minAmount": 2,
    "amount": 0,
    "availableAmount": 500324.2,
    "balance": {
      "balance": 500324.2
    },
    "sn": "ae00",
    "nonFeeTimes": 0,
    "maxAmount": 2000000,
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.account.default.get] -> 
{
  "id": "1574107856226.3594079062931597",
  "jsonrpc": "2.0",
  "method": "user.withdraw.account.default.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074",
    "terminal": 8
  }
}
[user.withdraw.account.default.get] <- 
{
  "id": "1574107856226.3594079062931597",
  "result": {
    "bankAccount": "123456789012",
    "accountStatus": 1,
    "bankAccountOwner": "user",
    "bankId": 1,
    "isDefault": 1,
    "bankBranch": "1111",
    "memo": null,
    "bankName": "工商银行",
    "sn": "ae00",
    "userId": 16847651,
    "withdrawAccountId": 31285,
    "lastUpdateTime": "2018-09-06 05:46:20"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.cancel] -> 
{
  "id": "1574107856307.7288769248585445",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  }
}
[user.audit.about.current.cancel] <- 
{
  "id": "1574107856307.7288769248585445",
  "result": 1,
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.no.get] -> 
{
  "id": "1574107856389.0157685355612571",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574107856389.0157685355612571",
  "result": "191118161056841925",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574107856472.0994880271331807",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "191118161056841925",
    "amount": 555
  }
}
[user.audit.about.current.apply] <- 
{
  "id": "1574107856472.0994880271331807",
  "result": 1,
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.trace] -> 
{
  "id": "1574107856559.4564862146970449",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  }
}
[user.audit.about.current.trace] <- 
{
  "id": "1574107856559.4564862146970449",
  "result": {
    "state": 0,
    "withdrawAmount": 555,
    "applyTime": "2019-11-18 16:10:56",
    "withdrawNo": "191118161056841925",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M="
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1574107856644.0221402489864277",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574107856644.0221402489864277",
  "result": "191118161056624797",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574107856726.8408928073783851",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "123456789",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "191118161056624797",
    "amount": 244
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574107856798.216173890274067984",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574107856726.8408928073783851",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 244,
      "reqIp": "202.11.82.1",
      "withdrawNo": "191118161056624797",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1574107856807.4334122468270232",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574107856807.4334122468270232",
  "result": "191118161056211289",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574107856890.6317680939279795",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074",
    "payPasword": "123456789",
    "withdrawNo": "191118161056211289",
    "amount": 472
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2273",
    "sn": "1574107856965.6600935649505181438",
    "message": "提现密码错误",
    "reason": "sn:ae00, userId:16847651 paypwd is wrong",
    "action": "null"
  },
  "id": "1574107856890.6317680939279795",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 472,
      "reqIp": "202.11.82.1",
      "withdrawNo": "191118161056211289",
      "payPasword": "123456789",
      "sessionId": "ae000101132310FD02CC6454D3D33074",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的充值流水号
[user.withdraw.no.get] -> 
{
  "id": "1574107856972.4867064953340063",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574107856972.4867064953340063",
  "result": "191118161057713556",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574107857062.5673015541992317",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "123456789",
    "amount": 181
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2902",
    "sn": "1574107857142.623467073953202372",
    "message": "当前操作重复或太频繁，请稍后操作。",
    "reason": "cache key already exists.",
    "action": "null"
  },
  "id": "1574107857062.5673015541992317",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 181,
      "reqIp": "202.11.82.1",
      "withdrawNo": "123456789",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC6454D3D33074",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的取款金额
[user.withdraw.no.get] -> 
{
  "id": "1574107857149.3165699833601084",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1574107857149.3165699833601084",
  "result": "191118161057194495",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1574107857239.0432772927481711",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "191118161057194495",
    "amount": 240000
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2902",
    "sn": "1574107857314.8903501838946121664",
    "message": "当前操作重复或太频繁，请稍后操作。",
    "reason": "cache key already exists.",
    "action": "null"
  },
  "id": "1574107857239.0432772927481711",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "withdrawNo": "191118161057194495",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC6454D3D33074",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574107857398.3417008445223665",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132310FD02CC6454D3D33074"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574107857398.3417008445223665",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.211116493s
```

