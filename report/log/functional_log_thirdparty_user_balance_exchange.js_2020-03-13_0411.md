
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1584043913715.5846935318051557",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "EOkvpXmulqQMzjkurWWztf2hKA4=",
    "salt": "y756csbn1khy8dpdtrdsh2h2xgsssbla",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1584043913715.5846935318051557",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-12 16:11:53",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847651,
      "parentPathIncSelf": "/14679026/16847651/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 9879,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:49",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "expiry": 1584047513809,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1584043913846.7464817713138953",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1584043913846.7464817713138953",
  "result": {
    "balance": 505504.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1584051113809,
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1584043913953.1687961400087587",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "from": 0,
    "to": 4,
    "amount": 50
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1584043913953.1687961400087587",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1584043914031.1546335879316988",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1584043914031.1546335879316988",
  "result": {
    "balance": 505504.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1584051113809,
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 50
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1584043914108.5139237871219394",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1584043914108.5139237871219394",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1584043914183.0846074667978823",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 50
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1584043914234.9079185372934551518",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1584043914183.0846074667978823",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 50,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1584043914259.3862484407453574",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1584043914259.3862484407453574",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1584043914337.7048623887810187",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "from": -1,
    "to": 5,
    "amount": 50
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1584043914391.9213963510485073912",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:50.0",
    "action": "null"
  },
  "id": "1584043914337.7048623887810187",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 50,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae000101132313A7A7599B12E8DEf41e",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1584043914418.1641544373176544",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e",
    "from": 0,
    "to": -5,
    "amount": 50
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1584043914418.1641544373176544",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1584043914549.4582826997568525",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132313A7A7599B12E8DEf41e"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1584043914549.4582826997568525",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=649.037705ms
some thresholds have failed
```

