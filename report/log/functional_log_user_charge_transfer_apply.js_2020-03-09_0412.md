
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583698393730.5624153534919857",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "eQIAlpgYavov8n9FhFvfcnkbOIc=",
    "salt": "4uebfa46cwqckwqpfl339rofgo8g9zt7",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583698393730.5624153534919857",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-08 16:13:13",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9783,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132316F3E281C811DAEE7d7e",
    "expiry": 1583701993831,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.charge.bank.list] -> 
{
  "id": "1583698393871.2222115743638311",
  "jsonrpc": "2.0",
  "method": "user.charge.bank.list",
  "params": {
    "sn": "ae00",
    "terminal": 8
  }
}
[user.charge.bank.list] <- 
{
  "id": "1583698393871.2222115743638311",
  "result": [
    {
      "bankId": 1,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "工商银行",
      "status": "1"
    },
    {
      "bankId": 2,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "交通银行",
      "status": "1"
    },
    {
      "bankId": 3,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "农业银行",
      "status": "1"
    },
    {
      "bankId": 4,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "建设银行",
      "status": "1"
    },
    {
      "bankId": 5,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "招商银行",
      "status": "1"
    },
    {
      "bankId": 6,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "民生银行",
      "status": "1"
    },
    {
      "bankId": 7,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "上海浦东发展银行",
      "status": "1"
    },
    {
      "bankId": 8,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "北京银行",
      "status": "1"
    },
    {
      "bankId": 9,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "兴业银行",
      "status": "1"
    },
    {
      "bankId": 10,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "中信银行",
      "status": "1"
    },
    {
      "bankId": 11,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "光大银行",
      "status": "1"
    },
    {
      "bankId": 12,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "华夏银行",
      "status": "1"
    },
    {
      "bankId": 13,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广东发展银行",
      "status": "1"
    },
    {
      "bankId": 14,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "平安银行",
      "status": "1"
    },
    {
      "bankId": 15,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "中国邮政",
      "status": "1"
    },
    {
      "bankId": 16,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "中国银行",
      "status": "1"
    },
    {
      "bankId": 17,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "上海银行",
      "status": "1"
    },
    {
      "bankId": 18,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "渤海银行",
      "status": "1"
    },
    {
      "bankId": 19,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "东莞银行",
      "status": "1"
    },
    {
      "bankId": 20,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "宁波银行",
      "status": "1"
    },
    {
      "bankId": 21,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "东亚银行",
      "status": "1"
    },
    {
      "bankId": 22,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "上海市农村商业银行",
      "status": "1"
    },
    {
      "bankId": 23,
      "chargeMode": 0,
      "bankType": "1",
      "accountType": 2,
      "bankName": "支付宝",
      "status": "1"
    },
    {
      "bankId": 24,
      "chargeMode": 0,
      "bankType": "1",
      "accountType": 2,
      "bankName": "微信",
      "status": "1"
    },
    {
      "bankId": 25,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "天津银行",
      "status": "1"
    },
    {
      "bankId": 26,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "南京银行",
      "status": "1"
    },
    {
      "bankId": 27,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "快捷支付",
      "status": "1"
    },
    {
      "bankId": 28,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "银行卡支付",
      "status": "1"
    },
    {
      "bankId": 30,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "江西农村信用社",
      "status": "1"
    },
    {
      "bankId": 31,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "深圳农村商业银行",
      "status": "1"
    },
    {
      "bankId": 32,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "福建省农村信用社联合社",
      "status": "1"
    },
    {
      "bankId": 33,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广州农村商业银行",
      "status": "1"
    },
    {
      "bankId": 34,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广东省农村信用社联合社",
      "status": "1"
    },
    {
      "bankId": 35,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "杭州银行",
      "status": "1"
    },
    {
      "bankId": 36,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "温州银行",
      "status": "1"
    },
    {
      "bankId": 37,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "江苏银行",
      "status": "1"
    },
    {
      "bankId": 38,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "成都银行",
      "status": "1"
    },
    {
      "bankId": 39,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "吉林银行",
      "status": "1"
    },
    {
      "bankId": 40,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "重庆银行",
      "status": "1"
    },
    {
      "bankId": 41,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "汉口银行",
      "status": "1"
    },
    {
      "bankId": 42,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广西农村信用社",
      "status": "1"
    },
    {
      "bankId": 43,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "北京农村商业银行",
      "status": "1"
    },
    {
      "bankId": 44,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "江苏农村商业银行",
      "status": "1"
    },
    {
      "bankId": 58,
      "chargeMode": 3,
      "bankType": "1",
      "accountType": 2,
      "bankName": "财付通转账",
      "status": "1"
    },
    {
      "bankId": 59,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广东省佛山市南海农商银行",
      "status": "1"
    },
    {
      "bankId": 60,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "贵州省农村信用社",
      "status": "1"
    },
    {
      "bankId": 61,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "柳州银行",
      "status": "1"
    },
    {
      "bankId": 65,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "桂林银行",
      "status": "1"
    },
    {
      "bankId": 66,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "郑州银行",
      "status": "1"
    },
    {
      "bankId": 67,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "浙江农商银行",
      "status": "1"
    },
    {
      "bankId": 68,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "付呗支付",
      "status": "1"
    },
    {
      "bankId": 69,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "美团支付",
      "status": "1"
    },
    {
      "bankId": 70,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "河南农村信用社",
      "status": "1"
    },
    {
      "bankId": 71,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "哈尔滨银行",
      "status": "1"
    },
    {
      "bankId": 72,
      "chargeMode": 2,
      "bankType": "0",
      "accountType": 2,
      "bankName": "支付宝转帐银行卡",
      "status": "1"
    },
    {
      "bankId": 73,
      "chargeMode": 1,
      "bankType": "0",
      "accountType": 2,
      "bankName": "微信转帐银行卡",
      "status": "1"
    },
    {
      "bankId": 75,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "通联支付",
      "status": "1"
    },
    {
      "bankId": 76,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "重庆农村商业银行",
      "status": "1"
    },
    {
      "bankId": 77,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "湖北农村信用社",
      "status": "1"
    },
    {
      "bankId": 78,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "快速充值通道",
      "status": "1"
    },
    {
      "bankId": 79,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "贵阳银行",
      "status": "1"
    },
    {
      "bankId": 80,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广西国民村镇银行",
      "status": "1"
    },
    {
      "bankId": 81,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "山东省农村信用社",
      "status": "1"
    },
    {
      "bankId": 82,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "保定银行",
      "status": "1"
    },
    {
      "bankId": 83,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "河北银行",
      "status": "1"
    },
    {
      "bankId": 84,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "支付宝快捷通道",
      "status": "1"
    },
    {
      "bankId": 85,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "微信快捷通道",
      "status": "1"
    },
    {
      "bankId": 86,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "微信, 支付宝快捷通道",
      "status": "1"
    },
    {
      "bankId": 87,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 2,
      "bankName": "云闪付",
      "status": "1"
    },
    {
      "bankId": 88,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广发银行",
      "status": "1"
    },
    {
      "bankId": 89,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广东大展银行",
      "status": "1"
    },
    {
      "bankId": 90,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "晋城银行",
      "status": "1"
    },
    {
      "bankId": 91,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广西北部湾银行",
      "status": "1"
    },
    {
      "bankId": 92,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "台州银行",
      "status": "1"
    },
    {
      "bankId": 93,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "邮政银行",
      "status": "1"
    },
    {
      "bankId": 94,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "农村信用社",
      "status": "1"
    },
    {
      "bankId": 95,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "厦门银行",
      "status": "1"
    },
    {
      "bankId": 96,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "湘江银行",
      "status": "1"
    },
    {
      "bankId": 97,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "长沙银行",
      "status": "1"
    },
    {
      "bankId": 98,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "长安银行",
      "status": "1"
    },
    {
      "bankId": 99,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "天津农村商业银行",
      "status": "1"
    },
    {
      "bankId": 100,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "浙江省农村信用社",
      "status": "1"
    },
    {
      "bankId": 101,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "威海市商业银行",
      "status": "1"
    },
    {
      "bankId": 102,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "四川农村商业银行",
      "status": "1"
    },
    {
      "bankId": 103,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "江西银行",
      "status": "1"
    },
    {
      "bankId": 104,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "浙江泰隆商业银行",
      "status": "1"
    },
    {
      "bankId": 105,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "盛京银行",
      "status": "1"
    },
    {
      "bankId": 106,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "石嘴山市城市信用社",
      "status": "1"
    },
    {
      "bankId": 107,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "石嘴山银行",
      "status": "1"
    },
    {
      "bankId": 108,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "吉林省农村信用社",
      "status": "1"
    },
    {
      "bankId": 109,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "苏州银行",
      "status": "1"
    },
    {
      "bankId": 110,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "云南省农村信用社联合社",
      "status": "1"
    },
    {
      "bankId": 111,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "武汉农村商业银行",
      "status": "1"
    },
    {
      "bankId": 112,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "邯郸银行",
      "status": "1"
    },
    {
      "bankId": 113,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "甘肃银行",
      "status": "1"
    },
    {
      "bankId": 114,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "河南省农村信用社",
      "status": "1"
    },
    {
      "bankId": 115,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "重庆三峡银行",
      "status": "1"
    },
    {
      "bankId": 116,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "湖北银行",
      "status": "1"
    },
    {
      "bankId": 117,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "中原银行",
      "status": "1"
    },
    {
      "bankId": 118,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "宜宾市商业银行",
      "status": "1"
    },
    {
      "bankId": 119,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "长治银行",
      "status": "1"
    },
    {
      "bankId": 120,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "网商银行",
      "status": "1"
    },
    {
      "bankId": 121,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "包商银行",
      "status": "1"
    },
    {
      "bankId": 122,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "江苏农商银行",
      "status": "1"
    },
    {
      "bankId": 123,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "宁夏银行",
      "status": "1"
    },
    {
      "bankId": 124,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "贵州农信银行",
      "status": "1"
    },
    {
      "bankId": 125,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "昆仑银行",
      "status": "1"
    },
    {
      "bankId": 126,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "锦州银行",
      "status": "1"
    },
    {
      "bankId": 127,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "张家口银行",
      "status": "1"
    },
    {
      "bankId": 128,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "东莞农村商业银行",
      "status": "1"
    },
    {
      "bankId": 129,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "湖南省农村信用社联合社",
      "status": "1"
    },
    {
      "bankId": 130,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "安徽省农村信用社",
      "status": "1"
    },
    {
      "bankId": 131,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "内蒙古农村信用社",
      "status": "1"
    },
    {
      "bankId": 132,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "江西赣州银座村镇银行",
      "status": "1"
    },
    {
      "bankId": 133,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "徽商银行",
      "status": "1"
    },
    {
      "bankId": 134,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "江苏省农村信用社联合社",
      "status": "1"
    },
    {
      "bankId": 135,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "四川省农村信用社",
      "status": "1"
    },
    {
      "bankId": 136,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "农商银行",
      "status": "1"
    },
    {
      "bankId": 137,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广东华兴银行",
      "status": "1"
    },
    {
      "bankId": 138,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "贵州银行",
      "status": "1"
    },
    {
      "bankId": 139,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "邮政储蓄银行",
      "status": "1"
    },
    {
      "bankId": 140,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "赣州银行",
      "status": "1"
    },
    {
      "bankId": 141,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "浙江民泰商业银行",
      "status": "1"
    },
    {
      "bankId": 142,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "陕西省农村信用社联合社",
      "status": "1"
    },
    {
      "bankId": 143,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "广州银行",
      "status": "1"
    },
    {
      "bankId": 144,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "浙商银行",
      "status": "1"
    },
    {
      "bankId": 145,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 1,
      "bankName": "青岛银行",
      "status": "1"
    },
    {
      "bankId": 146,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 2,
      "bankName": "QQ钱包",
      "status": "1"
    },
    {
      "bankId": 147,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 2,
      "bankName": "京东钱包",
      "status": "1"
    },
    {
      "bankId": 148,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 2,
      "bankName": "百度钱包",
      "status": "1"
    },
    {
      "bankId": 151,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 2,
      "bankName": "余额宝",
      "status": "1"
    },
    {
      "bankId": 153,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "余额宝宝",
      "status": "1"
    },
    {
      "bankId": 154,
      "chargeMode": 0,
      "bankType": "0",
      "accountType": 0,
      "bankName": "余额宝宝宝",
      "status": "1"
    }
  ],
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.no.get] -> 
{
  "id": "1583698394039.2765627099096117",
  "jsonrpc": "2.0",
  "method": "user.charge.no.get",
  "params": {
    "terminal": 8
  }
}
[user.charge.no.get] <- 
{
  "id": "1583698394039.2765627099096117",
  "result": "200308161306016860",
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.transfer.apply] -> 
{
  "id": "1583698394114.4839529592176943",
  "jsonrpc": "2.0",
  "method": "user.charge.transfer.apply",
  "params": {
    "sessionId": "ae000101132316F3E281C811DAEE7d7e",
    "chargeNo": "200308161306016860",
    "chargeFrom": 1,
    "amount": 408,
    "fromAccountOwner": "test",
    "memo": null,
    "toAccountId": 12001,
    "fromBankId": 1,
    "fromChannel": "8",
    "chargeFromChannel": "8",
    "couponMode": 1,
    "terminal": 1
  }
}
[user.charge.transfer.apply] <- 
{
  "id": "1583698394114.4839529592176943",
  "result": 27880987,
  "error": null,
  "jsonrpc": "2.0"
}
測試存款申请单流水号已使用
[user.charge.transfer.apply] -> 
{
  "id": "1583698394238.2328374360772752",
  "jsonrpc": "2.0",
  "method": "user.charge.transfer.apply",
  "params": {
    "sessionId": "ae000101132316F3E281C811DAEE7d7e",
    "chargeNo": "200308161306016860",
    "chargeFrom": 1,
    "amount": 491,
    "fromAccountOwner": "test",
    "memo": null,
    "toAccountId": 12001,
    "fromBankId": 1,
    "fromChannel": "8",
    "chargeFromChannel": "8",
    "couponMode": 1,
    "terminal": 1
  }
}
[user.charge.transfer.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2450",
    "sn": "1583698394333.7992428637317627742",
    "message": "小于入款申请间隔时间，请稍后再试！",
    "reason": "isUserChargeIntervalAllow for sn ae00, userId 16847651, chargeType 1, interval 222",
    "action": "null"
  },
  "id": "1583698394238.2328374360772752",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.transfer.apply",
    "params": {
      "amount": 491,
      "transferTime": "2020-03-09 08:13:14",
      "memo": null,
      "toAccountId": 12001,
      "sessionId": "ae000101132316F3E281C811DAEE7d7e",
      "terminal": 1,
      "chargeNo": "200308161306016860",
      "chargeFromChannel": "8",
      "fromAccountOwner": "test",
      "fromBankId": 1,
      "reqIp": "202.11.82.1",
      "fromChannel": "8",
      "couponMode": 1,
      "opIp": "202.11.82.1",
      "chargeFrom": 1
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 41,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的支付方式来源
[user.charge.no.get] -> 
{
  "id": "1583698394362.9439578361381146",
  "jsonrpc": "2.0",
  "method": "user.charge.no.get",
  "params": {
    "terminal": 8
  }
}
[user.charge.no.get] <- 
{
  "id": "1583698394362.9439578361381146",
  "result": "200308161306016862",
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.transfer.apply] -> 
{
  "id": "1583698394443.0136014605441730",
  "jsonrpc": "2.0",
  "method": "user.charge.transfer.apply",
  "params": {
    "sessionId": "ae000101132316F3E281C811DAEE7d7e",
    "chargeNo": "200308161306016862",
    "chargeFrom": 1,
    "amount": 375,
    "fromAccountOwner": "test",
    "memo": null,
    "toAccountId": 12001,
    "fromBankId": 1,
    "fromChannel": -1,
    "chargeFromChannel": "8",
    "couponMode": 1,
    "terminal": 1
  }
}
[user.charge.transfer.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2450",
    "sn": "1583698394518.7944347457757904819",
    "message": "小于入款申请间隔时间，请稍后再试！",
    "reason": "isUserChargeIntervalAllow for sn ae00, userId 16847651, chargeType 1, interval 222",
    "action": "null"
  },
  "id": "1583698394443.0136014605441730",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.transfer.apply",
    "params": {
      "amount": 375,
      "transferTime": "2020-03-09 08:13:14",
      "memo": null,
      "toAccountId": 12001,
      "sessionId": "ae000101132316F3E281C811DAEE7d7e",
      "terminal": 1,
      "chargeNo": "200308161306016862",
      "chargeFromChannel": "8",
      "fromAccountOwner": "test",
      "fromBankId": 1,
      "reqIp": "202.11.82.1",
      "fromChannel": -1,
      "couponMode": 1,
      "opIp": "202.11.82.1",
      "chargeFrom": 1
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 33,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.charge.no.get] -> 
{
  "id": "1583698394557.2498498563134486",
  "jsonrpc": "2.0",
  "method": "user.charge.no.get",
  "params": {
    "terminal": 8
  }
}
[user.charge.no.get] <- 
{
  "id": "1583698394557.2498498563134486",
  "result": "200308161306016863",
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.transfer.apply] -> 
{
  "id": "1583698394638.6518925171801377",
  "jsonrpc": "2.0",
  "method": "user.charge.transfer.apply",
  "params": {
    "sessionId": "123456789",
    "chargeNo": "200308161306016863",
    "chargeFrom": 1,
    "amount": 377,
    "fromAccountOwner": "test",
    "memo": null,
    "toAccountId": 12001,
    "fromBankId": 1,
    "fromChannel": "8",
    "chargeFromChannel": "8",
    "couponMode": 1,
    "terminal": 1
  }
}
[user.charge.transfer.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583698394696.9169316745287761280",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583698394638.6518925171801377",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.transfer.apply",
    "params": {
      "amount": 377,
      "transferTime": "2020-03-09 08:13:14",
      "memo": null,
      "toAccountId": 12001,
      "sessionId": "123456789",
      "terminal": 1,
      "chargeNo": "200308161306016863",
      "chargeFromChannel": "8",
      "fromAccountOwner": "test",
      "fromBankId": 1,
      "reqIp": "202.11.82.1",
      "fromChannel": "8",
      "couponMode": 1,
      "opIp": "202.11.82.1",
      "chargeFrom": 1
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的接收账户ID
[user.charge.no.get] -> 
{
  "id": "1583698394739.1970756111892500",
  "jsonrpc": "2.0",
  "method": "user.charge.no.get",
  "params": {
    "terminal": 8
  }
}
[user.charge.no.get] <- 
{
  "id": "1583698394739.1970756111892500",
  "result": "200308161306016864",
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.transfer.apply] -> 
{
  "id": "1583698394824.1448870131679231",
  "jsonrpc": "2.0",
  "method": "user.charge.transfer.apply",
  "params": {
    "sessionId": "ae000101132316F3E281C811DAEE7d7e",
    "chargeNo": "200308161306016864",
    "chargeFrom": 1,
    "amount": 111,
    "fromAccountOwner": "test",
    "memo": null,
    "toAccountId": -1,
    "fromBankId": 1,
    "fromChannel": "8",
    "chargeFromChannel": "8",
    "couponMode": 1,
    "terminal": 1
  }
}
[user.charge.transfer.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2418",
    "sn": "1583698394899.2688672342323904836",
    "message": "入款账号不存在",
    "reason": "sn:ae00, uid:16847651, toAccountId:-1",
    "action": "null"
  },
  "id": "1583698394824.1448870131679231",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.transfer.apply",
    "params": {
      "amount": 111,
      "transferTime": "2020-03-09 08:13:14",
      "memo": null,
      "toAccountId": -1,
      "sessionId": "ae000101132316F3E281C811DAEE7d7e",
      "terminal": 1,
      "chargeNo": "200308161306016864",
      "chargeFromChannel": "8",
      "fromAccountOwner": "test",
      "fromBankId": 1,
      "reqIp": "202.11.82.1",
      "fromChannel": "8",
      "couponMode": 1,
      "opIp": "202.11.82.1",
      "chargeFrom": 1
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 20,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583698394972.6977841711762931",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132316F3E281C811DAEE7d7e"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583698394972.6977841711762931",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.05422608s
some thresholds have failed
```

