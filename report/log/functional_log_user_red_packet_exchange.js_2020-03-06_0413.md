
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583439214497.2789639081509154",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "GD8RWywxVCdDqOeIQD364l6IcYs=",
    "salt": "39cqrquptxmly8t6y7igxwgrthk3bhm5",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583439214497.2789639081509154",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-05 16:13:34",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9709,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132316F3E281C811107216b2",
    "expiry": 1583442814611,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583439214649.9665121984593428",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132316F3E281C811107216b2"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583439214649.9665121984593428",
  "result": {
    "balance": 505303.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583446414611,
    "sessionId": "ae000101132316F3E281C811107216b2",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1583439214768.9697076111898817",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae000101132316F3E281C811107216b2",
    "amount": 13
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1583439214768.9697076111898817",
  "result": {
    "success": "1",
    "cashflowId": 685339269460660200,
    "balance": 505316.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583439214965.3175958500578030",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132316F3E281C811107216b2"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583439214965.3175958500578030",
  "result": {
    "balance": 505316.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583446414611,
    "sessionId": "ae000101132316F3E281C811107216b2",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 13
diff = 13
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1583439215054.6386571714523284",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 13
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583439215110.8348256403222428656",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583439215054.6386571714523284",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 13,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1583439215136.7342167303545421",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae000101132316F3E281C811107216b2",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1583439215186.6914836864829929340",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1583439215136.7342167303545421",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132316F3E281C811107216b2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583439215291.5872746269205074",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132316F3E281C811107216b2"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583439215291.5872746269205074",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=563.162144ms
```

