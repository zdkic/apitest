
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583525513334.7775529064065308",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "fgzYGQ27q/gspjiRHWs3ee8i1hE=",
    "salt": "iru6w5asuxdhj6giv0njxlbc139exnz1",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583525513334.7775529064065308",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-06 16:11:53",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9729,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "expiry": 1583529113417,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1583525513461.0918445173413747",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583525513461.0918445173413747",
  "result": {
    "balance": 505316.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583532713417,
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583525513613.7066564841414780",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "from": 0,
    "to": 4,
    "amount": 31
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583525513613.7066564841414780",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583525513717.4652688538639901",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583525513717.4652688538639901",
  "result": {
    "balance": 505316.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583532713417,
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 31
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1583525513796.0658230838556362",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1583525513796.0658230838556362",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583525513880.9317048989184213",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 31
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525513936.6916514144030014152",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583525513880.9317048989184213",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 31,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583525513967.0607692046828316",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583525513967.0607692046828316",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583525514044.9047125643428760",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "from": -1,
    "to": 5,
    "amount": 31
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1583525514096.5737574929582521776",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:31.0",
    "action": "null"
  },
  "id": "1583525514044.9047125643428760",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 31,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae0001011323193CC80AA31153DE4c95",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583525514122.4040626582057798",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95",
    "from": 0,
    "to": -5,
    "amount": 31
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583525514122.4040626582057798",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583525514312.4291218029171226",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153DE4c95"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583525514312.4291218029171226",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=737.687667ms
some thresholds have failed
```

