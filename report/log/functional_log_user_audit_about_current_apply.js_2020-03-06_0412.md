
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583439154652.4750818841658010",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "vLkgL6Uv42/x+Vg4GN0ERhOFBqA=",
    "salt": "9twqxocokbfrp9h2c5n4uohz5o14ex7e",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583439154652.4750818841658010",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-05 16:12:34",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9706,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132310FD02CC641110667efb",
    "expiry": 1583442754748,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.withdraw.option.get] -> 
{
  "id": "1583439154797.7274945657208203",
  "jsonrpc": "2.0",
  "method": "user.withdraw.option.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb",
    "terminal": 8
  }
}
[user.withdraw.option.get] <- 
{
  "id": "1583439154797.7274945657208203",
  "result": {
    "feeAmount": 0,
    "minAmount": 2,
    "amount": 0,
    "availableAmount": 505303.2,
    "balance": {
      "balance": 505303.2
    },
    "sn": "ae00",
    "nonFeeTimes": 0,
    "maxAmount": 2000000,
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.account.default.get] -> 
{
  "id": "1583439154947.1799450543284386",
  "jsonrpc": "2.0",
  "method": "user.withdraw.account.default.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb",
    "terminal": 8
  }
}
[user.withdraw.account.default.get] <- 
{
  "id": "1583439154947.1799450543284386",
  "result": {
    "bankAccount": "123456789012",
    "accountStatus": 1,
    "bankAccountOwner": "user",
    "bankId": 1,
    "isDefault": 1,
    "bankBranch": "1111",
    "memo": null,
    "bankName": "工商银行",
    "sn": "ae00",
    "userId": 16847651,
    "withdrawAccountId": 31285,
    "lastUpdateTime": "2018-09-06 05:46:20"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.cancel] -> 
{
  "id": "1583439155030.6201943422979853",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  }
}
[user.audit.about.current.cancel] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439155082.7818177895833976573",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.cancel",
    "action": "null"
  },
  "id": "1583439155030.6201943422979853",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.cancel",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132310FD02CC641110667efb",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.no.get] -> 
{
  "id": "1583439155109.8679055316034591",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439155109.8679055316034591",
  "result": "200305161235854381",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583439155196.5384507010422316",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200305161235854381",
    "amount": 385
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439155251.8781627821463043712",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583439155196.5384507010422316",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 385,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200305161235854381",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641110667efb",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.trace] -> 
{
  "id": "1583439155277.7059831886374785",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  }
}
[user.audit.about.current.trace] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439155335.7984840356407279040",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.trace",
    "action": "null"
  },
  "id": "1583439155277.7059831886374785",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.trace",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132310FD02CC641110667efb",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583439155361.1887899729960282",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439155361.1887899729960282",
  "result": "200305161235268421",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583439155445.9647074196621788",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "123456789",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200305161235268421",
    "amount": 531
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583439155495.149833937039019018",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583439155445.9647074196621788",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 531,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200305161235268421",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583439155521.8571412791312155",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439155521.8571412791312155",
  "result": "200305161235062425",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583439155601.5951849302434267",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb",
    "payPasword": "123456789",
    "withdrawNo": "200305161235062425",
    "amount": 505
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439155659.2990672355867509539",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583439155601.5951849302434267",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 505,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200305161235062425",
      "payPasword": "123456789",
      "sessionId": "ae000101132310FD02CC641110667efb",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的充值流水号
[user.withdraw.no.get] -> 
{
  "id": "1583439155685.0161921370294328",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439155685.0161921370294328",
  "result": "200305161235449165",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583439155770.9038255808029975",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "123456789",
    "amount": 560
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439155827.576531405700465697",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583439155770.9038255808029975",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 560,
      "reqIp": "202.11.82.1",
      "withdrawNo": "123456789",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641110667efb",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的取款金额
[user.withdraw.no.get] -> 
{
  "id": "1583439155854.3439832451391417",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439155854.3439832451391417",
  "result": "200305161235991375",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583439155932.5341695964273460",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200305161235991375",
    "amount": 240000
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439156010.6403837116752510304",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583439155932.5341695964273460",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200305161235991375",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641110667efb",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 33,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583439156092.9521931090715750",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132310FD02CC641110667efb"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583439156092.9521931090715750",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.245307383s
some thresholds have failed
```

