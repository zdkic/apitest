
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583525632112.3169379013453784",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "9VTqZMuEIz9p4TCGNlF6M2Kh5MM=",
    "salt": "xpn83ujdz37rosaeaxeumwho3zme27d7",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583525632112.3169379013453784",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-06 16:13:52",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9735,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "expiry": 1583529232226,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583525632261.9290200877740288",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583525632261.9290200877740288",
  "result": {
    "balance": 505359.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583532832226,
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.no.get] -> 
{
  "id": "1583525632365.5734229081049781",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525632365.5734229081049781",
  "result": "200306161352562150",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583525632447.7024201405836784",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525632496.6755115490499608032",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583525632447.7024201405836784",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply] -> 
{
  "id": "1583525632526.0864481558916658",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "withdrawNo": "200306161352562150",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 450
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583525632577.868351127920574496",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583525632526.0864481558916658",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 450,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200306161352562150",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 1
[user.withdraw.apply] -> 
{
  "id": "1583525692613.4590129162585188",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "withdrawNo": "200306161352562150",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 450
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583525692671.1444749862917636102",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583525692613.4590129162585188",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 450,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200306161352562150",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 2
[user.withdraw.apply] -> 
{
  "id": "1583525752706.0389241690324203",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "withdrawNo": "200306161352562150",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 450
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583525752755.8934569914652800206",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583525752706.0389241690324203",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 450,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200306161352562150",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 3
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583525812786.1573039729817178",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583525812786.1573039729817178",
  "result": {
    "balance": 505359.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583532832226,
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 450
diff = 0
[user.withdraw.no.get] -> 
{
  "id": "1583525932880.1791640291291071",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525932880.1791640291291071",
  "result": "200306161852069240",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583525932987.4215447607525314",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525933041.648538701589381384",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583525932987.4215447607525314",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過低的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583525933082.8644575948485473",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "withdrawNo": "200306161852069240",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 1
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583525933137.8610738399398984127",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583525933082.8644575948485473",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 1,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200306161852069240",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過高的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583525933180.1442555738548533",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "withdrawNo": "200306161852069240",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 30000000
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583525933230.2346727559666861651",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583525933180.1442555738548533",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 30000000,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200306161852069240",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583525933271.1019872582424983",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525933271.1019872582424983",
  "result": "200306161853465103",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583525933363.4187733215254740",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "123456789",
    "withdrawNo": "200306161853465103",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 450
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525933419.1153209040730327298",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583525933363.4187733215254740",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 450,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200306161853465103",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583525938449.6559563355361692",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525938449.6559563355361692",
  "result": "200306161858346814",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583525938531.2194542224293310",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9",
    "withdrawNo": "200306161858346814",
    "payPasword": "123456789",
    "toBankId": 1,
    "amount": 450
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2273",
    "sn": "1583525938581.9223175602228148916",
    "message": "提现密码错误",
    "reason": "sn:ae00, userId:16847651 paypwd is wrong",
    "action": "null"
  },
  "id": "1583525938531.2194542224293310",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 450,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200306161858346814",
      "payPasword": "123456789",
      "sessionId": "ae000101132310FD02CC641153F6dee9",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583525938672.4760086815844874",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132310FD02CC641153F6dee9"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583525938672.4760086815844874",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=5m6.347441967s
some thresholds have failed
```

