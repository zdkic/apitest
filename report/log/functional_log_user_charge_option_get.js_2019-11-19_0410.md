
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574107863682.0139110794957020",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "Ve7jOHIwzl/1s6ruwSRcTvU96SU=",
    "salt": "gozix2dzuig94y5mrv0sizrzq9l4dqay",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574107863682.0139110794957020",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-18 16:11:03",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5579,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "expiry": 1574111463787,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107863799.5105858223930732",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 1,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107863799.5105858223930732",
  "result": {
    "amount": 100,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 133,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107863903.4658018614434216",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 2,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107863903.4658018614434216",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107863993.0270389016413324",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 3,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107863993.0270389016413324",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107864073.6372614399379926",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 1,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107864073.6372614399379926",
  "result": {
    "amount": 300,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 333,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107864151.6475561147539900",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 2,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107864151.6475561147539900",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107864231.8577988433170210",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 3,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107864231.8577988433170210",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107864309.6529833650712267",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 1,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107864309.6529833650712267",
  "result": {
    "amount": 165,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 198,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107864393.4355154486638210",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 2,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107864393.4355154486638210",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1574107864472.5242269958386444",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2",
    "chargeType": 3,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1574107864472.5242269958386444",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574107864620.6924223694778232",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323193CC80AA354D3D478c2"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574107864620.6924223694778232",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=753.046411ms
```

