
```bash
[auth.mobile.login]測試正常流程
[auth.mobile.login] -> 
{
  "id": "1583525419659.5065483343688767",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "CjExIAtNMy1BI1CjmvL7KWF/EA0=",
    "salt": "t21dg7fb0ninqbwug5wyzutgw8c6xhq2",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583525419659.5065483343688767",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2020-03-06 16:10:19",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847647/",
      "userId": 16847647,
      "loginCount": 27380,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:48",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 413,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F173906B5DB1153CBa6d0",
    "expiry": 1583529019808,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.mobile.login]測試錯誤帳密
[auth.mobile.login] -> 
{
  "id": "1583525419869.1264018098236807",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "7pQe1SwLNZi437IfkwJwjBLN56I=",
    "salt": "1anr7ocsy1s4ptni6epkje6hzv6ajuf4",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2218",
    "sn": "1583525419947.144550612180549824",
    "message": "用户名或密码错误,登录失败.",
    "reason": "loginId:bgqajohnload1, password:7pQe1SwLNZi437IfkwJwjBLN56I=, from sn:ae00",
    "action": "null"
  },
  "id": "1583525419869.1264018098236807",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "7pQe1SwLNZi437IfkwJwjBLN56I=",
      "captchaCode": "6711",
      "loginId": "bgqajohnload1",
      "salt": "1anr7ocsy1s4ptni6epkje6hzv6ajuf4",
      "reqIp": "202.11.82.1",
      "captchaKey": "3094255924",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 30,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]測試正常流程
[auth.session.validate] -> 
{
  "id": "1583525419974.2128067213327586",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "ae000101131F173906B5DB1153CBa6d0",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "id": "1583525419974.2128067213327586",
  "result": {
    "flag": "1",
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583532619808,
    "sessionId": "ae000101131F173906B5DB1153CBa6d0",
    "userId": 16847647
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.session.validate]測試錯誤sessionId
[auth.session.validate] -> 
{
  "id": "1583525420055.3328042346144624",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "12345678",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525420104.8502135762365317051",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1583525420055.3328042346144624",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "0",
      "sessionId": "12345678",
      "opIp": "202.11.82.1",
      "withUser": "0"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.online.logout]測試正常流程
[auth.online.logout] -> 
{
  "id": "1583525420130.1256891866379176",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F173906B5DB1153CBa6d0"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583525420130.1256891866379176",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
[auth.online.logout]測試錯誤sessionId
[auth.online.logout] -> 
{
  "id": "1583525420232.7001684039766377",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "12345678"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525420289.7966301896001910238",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1583525420232.7001684039766377",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.online.logout",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "12345678",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
Test finished i=1 t=663.781932ms
```

