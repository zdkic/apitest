
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583439235024.2741233342492363",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "Q9syHoAlxNWU7PRW+5CeAIB68Io=",
    "salt": "s4b4ivf8tkl3268054p5ahvyvb4uwlzo",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583439235024.2741233342492363",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-05 16:13:55",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9710,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231538CDE3F41110777370",
    "expiry": 1583442835140,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583439235176.6666958230396780",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583439235176.6666958230396780",
  "result": {
    "balance": 505316.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583442835140,
    "sessionId": "ae00010113231538CDE3F41110777370",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.no.get] -> 
{
  "id": "1583439235285.6400826262233344",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439235285.6400826262233344",
  "result": "200305161355073075",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583439235366.7020414028506244",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439235421.7318486942499561",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583439235366.7020414028506244",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply] -> 
{
  "id": "1583439235456.3933362406867143",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370",
    "withdrawNo": "200305161355073075",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 427
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583439235513.162992774617514497",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583439235456.3933362406867143",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 427,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200305161355073075",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 1
[user.withdraw.apply] -> 
{
  "id": "1583439295545.2364943987118701",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370",
    "withdrawNo": "200305161355073075",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 427
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583439295602.7700583565209090039",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583439295545.2364943987118701",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 427,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200305161355073075",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 2
[user.withdraw.apply] -> 
{
  "id": "1583439355635.9745755287035660",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370",
    "withdrawNo": "200305161355073075",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 427
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583439355684.333972284727312932",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583439355635.9745755287035660",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 427,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200305161355073075",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 3
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583439415715.7541658167265045",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583439415715.7541658167265045",
  "result": {
    "balance": 505316.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583446435140,
    "sessionId": "ae00010113231538CDE3F41110777370",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 427
diff = 0
[user.withdraw.no.get] -> 
{
  "id": "1583439535799.5648907415127693",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439535799.5648907415127693",
  "result": "200305161855570302",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583439535909.7778344095296817",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583439535964.6753992057304694720",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583439535909.7778344095296817",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過低的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583439536008.3893248570101920",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370",
    "withdrawNo": "200305161855570302",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 1
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583439536059.7492863449974159096",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583439536008.3893248570101920",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 1,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200305161855570302",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過高的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583439536094.7066781960879547",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370",
    "withdrawNo": "200305161855570302",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 30000000
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583439536150.9055328336070295296",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583439536094.7066781960879547",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 30000000,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200305161855570302",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583439536181.5802670484674685",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439536181.5802670484674685",
  "result": "200305161856079732",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583439536263.4300080560584913",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "123456789",
    "withdrawNo": "200305161856079732",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 427
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583439536312.9221948985068729344",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583439536263.4300080560584913",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 427,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200305161856079732",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583439541344.5273094681824587",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583439541344.5273094681824587",
  "result": "200305161901120922",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583439541843.1957581094283974",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370",
    "withdrawNo": "200305161901120922",
    "payPasword": "123456789",
    "toBankId": 1,
    "amount": 427
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2273",
    "sn": "1583439541903.2882303796558954512",
    "message": "提现密码错误",
    "reason": "sn:ae00, userId:16847651 paypwd is wrong",
    "action": "null"
  },
  "id": "1583439541843.1957581094283974",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 427,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200305161901120922",
      "payPasword": "123456789",
      "sessionId": "ae00010113231538CDE3F41110777370",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583439541987.1294501322906876",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231538CDE3F41110777370"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583439541987.1294501322906876",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=5m6.756901922s
some thresholds have failed
```

