
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583611953481.1514365432715100",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "SKJS3fE1lYCALv7BOqQDWPH9ON4=",
    "salt": "ufkt4xz5nh50yfh62pwowi8tx7l1p96s",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583611953481.1514365432715100",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-07 16:12:33",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9756,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231129BB332811976676cc",
    "expiry": 1583615553575,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.withdraw.option.get] -> 
{
  "id": "1583611953635.2073623539282799",
  "jsonrpc": "2.0",
  "method": "user.withdraw.option.get",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc",
    "terminal": 8
  }
}
[user.withdraw.option.get] <- 
{
  "id": "1583611953635.2073623539282799",
  "result": {
    "feeAmount": 0,
    "minAmount": 2,
    "amount": 0,
    "availableAmount": 505359.2,
    "balance": {
      "balance": 505359.2
    },
    "sn": "ae00",
    "nonFeeTimes": 0,
    "maxAmount": 2000000,
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.account.default.get] -> 
{
  "id": "1583611953792.9920987381252831",
  "jsonrpc": "2.0",
  "method": "user.withdraw.account.default.get",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc",
    "terminal": 8
  }
}
[user.withdraw.account.default.get] <- 
{
  "id": "1583611953792.9920987381252831",
  "result": {
    "accountStatus": 1,
    "bankAccount": "123456789012",
    "bankAccountOwner": "user",
    "bankId": 1,
    "isDefault": 1,
    "bankBranch": "1111",
    "memo": null,
    "bankName": "工商银行",
    "sn": "ae00",
    "userId": 16847651,
    "withdrawAccountId": 31285,
    "lastUpdateTime": "2018-09-06 05:46:20"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.cancel] -> 
{
  "id": "1583611953887.7114520290362538",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  }
}
[user.audit.about.current.cancel] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583611953967.2598717727917097216",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.cancel",
    "action": "null"
  },
  "id": "1583611953887.7114520290362538",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.cancel",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB332811976676cc",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 26,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.no.get] -> 
{
  "id": "1583611953995.9339588908335124",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583611953995.9339588908335124",
  "result": "200307161234570488",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583611954077.5807601612257135",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200307161234570488",
    "amount": 515
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583611954134.1745428276472447363",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583611954077.5807601612257135",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 515,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200307161234570488",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB332811976676cc",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.trace] -> 
{
  "id": "1583611954162.5193046043953542",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  }
}
[user.audit.about.current.trace] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583611954213.22128265478346",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.trace",
    "action": "null"
  },
  "id": "1583611954162.5193046043953542",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.trace",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB332811976676cc",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583611954239.8502026147621434",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583611954239.8502026147621434",
  "result": "200307161234511171",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583611954326.2729582671922737",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "123456789",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200307161234511171",
    "amount": 545
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583611954376.5188111516020899164",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583611954326.2729582671922737",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 545,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200307161234511171",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583611954403.3378646315013535",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583611954403.3378646315013535",
  "result": "200307161234452329",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583611954487.6160116374043349",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc",
    "payPasword": "123456789",
    "withdrawNo": "200307161234452329",
    "amount": 571
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583611954540.8790076374137042944",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583611954487.6160116374043349",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 571,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200307161234452329",
      "payPasword": "123456789",
      "sessionId": "ae00010113231129BB332811976676cc",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的充值流水号
[user.withdraw.no.get] -> 
{
  "id": "1583611954573.5795162058605612",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583611954573.5795162058605612",
  "result": "200307161234637286",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583611954665.6052960847530477",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "123456789",
    "amount": 447
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583611954717.2478105698317176458",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583611954665.6052960847530477",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 447,
      "reqIp": "202.11.82.1",
      "withdrawNo": "123456789",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB332811976676cc",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的取款金额
[user.withdraw.no.get] -> 
{
  "id": "1583611954747.1264080584482245",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583611954747.1264080584482245",
  "result": "200307161234454814",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583611954845.0910307467152474",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200307161234454814",
    "amount": 240000
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583611954904.8065926896597532240",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583611954845.0910307467152474",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200307161234454814",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB332811976676cc",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583611954992.5314517865783897",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231129BB332811976676cc"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583611954992.5314517865783897",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.297075763s
some thresholds have failed
```

