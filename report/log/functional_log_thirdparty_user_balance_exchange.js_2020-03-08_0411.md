
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583611913707.4525503416769331",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "numqzwDU3cpzPU3AgLc8Pmc1ILc=",
    "salt": "wdc1kvfc5ws400dj34t0pbfjru4tio87",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583611913707.4525503416769331",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-07 16:11:53",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9754,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "expiry": 1583615513812,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1583611913857.7747212560637743",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583611913857.7747212560637743",
  "result": {
    "balance": 505359.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583619113812,
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583611913978.9707107721268954",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "from": 0,
    "to": 4,
    "amount": 11
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583611913978.9707107721268954",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583611914062.7179316597743696",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583611914062.7179316597743696",
  "result": {
    "balance": 505359.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583619113812,
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 11
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1583611914149.6292854515843084",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1583611914149.6292854515843084",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583611914224.1000055948362973",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 11
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583611914273.8322327599687516150",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583611914224.1000055948362973",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 11,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583611914313.7774144646778401",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583611914313.7774144646778401",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583611914397.7826216919390358",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "from": -1,
    "to": 5,
    "amount": 11
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1583611914448.9078902494643877854",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:11.0",
    "action": "null"
  },
  "id": "1583611914397.7826216919390358",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 11,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae00010113231538CDE3F411975Ea6be",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583611914484.2313102125008453",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be",
    "from": 0,
    "to": -5,
    "amount": 11
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583611914484.2313102125008453",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583611914624.3111195245899881",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231538CDE3F411975Ea6be"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583611914624.3111195245899881",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=704.534538ms
some thresholds have failed
```

