
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574107848991.4904903119990642",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "kplBZwo7tx0lT+yBLa7/dcUsvAU=",
    "salt": "360zdevqzrdnpmsefixev8mwrlo4mquq",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574107848991.4904903119990642",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-18 16:10:49",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5577,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323181B7E5EE454D3D1ef47",
    "expiry": 1574111449099,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常流程
[thirdparty.user.collect.balance.syn] -> 
{
  "id": "1574107849109.6655529057472294",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.syn",
  "params": {
    "sessionId": "ae0001011323181B7E5EE454D3D1ef47"
  }
}
[thirdparty.user.collect.balance.syn] <- 
{
  "id": "1574107849109.6655529057472294",
  "result": true,
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[thirdparty.user.collect.balance.info] -> 
{
  "id": "1574107849214.7984486415904118",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.info",
  "params": {
    "sessionId": "ae0001011323181B7E5EE454D3D1ef47"
  }
}
[thirdparty.user.collect.balance.info] <- 
{
  "id": "1574107849214.7984486415904118",
  "result": {
    "33": "1",
    "34": "1",
    "35": "1",
    "36": "1",
    "48": "1"
  },
  "error": null,
  "jsonrpc": "2.0"
}
gameId = 33 , result = 1 (1 成功 0 执行中 -1 失败)
gameId = 34 , result = 1 (1 成功 0 执行中 -1 失败)
gameId = 35 , result = 1 (1 成功 0 执行中 -1 失败)
gameId = 36 , result = 1 (1 成功 0 执行中 -1 失败)
gameId = 48 , result = 1 (1 成功 0 执行中 -1 失败)
測試錯誤的会话ID(thirdparty_user_collect_balance_syn)
[thirdparty.user.collect.balance.syn] -> 
{
  "id": "1574107849297.5563026934527214",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.syn",
  "params": {
    "sessionId": "123456789"
  }
}
[thirdparty.user.collect.balance.syn] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574107849369.8880392303727738862",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574107849297.5563026934527214",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.collect.balance.syn",
    "params": {
      "reqIp": "202.11.82.1",
      "ip": "202.11.82.1",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID(thirdparty_user_collect_balance_info)
[thirdparty.user.collect.balance.info] -> 
{
  "id": "1574107849375.5466320573176688",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.info",
  "params": {
    "sessionId": "123456789"
  }
}
[thirdparty.user.collect.balance.info] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574107849448.8988023497074589668",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574107849375.5466320573176688",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.collect.balance.info",
    "params": {
      "reqIp": "202.11.82.1",
      "ip": "202.11.82.1",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574107849528.0004347304311379",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323181B7E5EE454D3D1ef47"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574107849528.0004347304311379",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=345.366102ms
```

