
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574107841529.6634062344231274",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "M4sAvN4xm2sxATa0azffzhfweQU=",
    "salt": "0pmomm8cq60puea1bj9cptrb5y9ctsv9",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574107841529.6634062344231274",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-18 16:10:41",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5576,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "expiry": 1574111441643,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1574107841650.9164681436324413",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574107841650.9164681436324413",
  "result": {
    "balance": 500324.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574115041643,
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574107841756.2279002981593475",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "from": 0,
    "to": 4,
    "amount": 3
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2436",
    "sn": "1574107841833.7123673507257368428",
    "message": "未知第三方平台异常",
    "reason": "sn:ae00, uid:16847651, to:4, amount:3.0",
    "action": "null"
  },
  "id": "1574107841756.2279002981593475",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 3,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
      "to": 4,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1574107841840.6029387294908421",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574107841840.6029387294908421",
  "result": {
    "balance": 500324.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574115041643,
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 3
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1574107841927.6450391644257813",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1574107841927.6450391644257813",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574107842009.9707053448197919",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 3
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574107842081.6299760147415039792",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574107842009.9707053448197919",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 3,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574107842088.1997298853783854",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2436",
    "sn": "1574107842169.9213801887243960254",
    "message": "未知第三方平台异常",
    "reason": "sn:ae00, uid:16847651, to:5, amount:240000.0",
    "action": "null"
  },
  "id": "1574107842088.1997298853783854",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574107842176.4762725617195789",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "from": -1,
    "to": 5,
    "amount": 3
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1574107842252.3188654787029385729",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:3.0",
    "action": "null"
  },
  "id": "1574107842176.4762725617195789",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 3,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574107842259.2092188413520824",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
    "from": 0,
    "to": -5,
    "amount": 3
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2436",
    "sn": "1574107842340.1645508796608151554",
    "message": "未知第三方平台异常",
    "reason": "sn:ae00, uid:16847651, to:-5, amount:3.0",
    "action": "null"
  },
  "id": "1574107842259.2092188413520824",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 3,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "ae0001011323171E1AB85F54D3D0ec94",
      "to": -5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574107842423.8345443845302252",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323171E1AB85F54D3D0ec94"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574107842423.8345443845302252",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=694.590991ms
some thresholds have failed
```

