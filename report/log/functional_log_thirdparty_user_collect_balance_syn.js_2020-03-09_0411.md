
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583698333774.5785181131097511",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "yVBnTSJMK5XsG4fTp9mxGjC90lk=",
    "salt": "xudcyuaem3c1pnxvhykopfdj3f8ng9f7",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583698333774.5785181131097511",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-08 16:12:13",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9780,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323141DDC270B11DAE23d69",
    "expiry": 1583701933871,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常流程
[thirdparty.user.collect.balance.syn] -> 
{
  "id": "1583698333920.2793509618363946",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.syn",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DAE23d69"
  }
}
[thirdparty.user.collect.balance.syn] <- 
{
  "id": "1583698333920.2793509618363946",
  "result": true,
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[thirdparty.user.collect.balance.info] -> 
{
  "id": "1583698334029.7324962455113879",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.info",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DAE23d69"
  }
}
[thirdparty.user.collect.balance.info] <- 
{
  "id": "1583698334029.7324962455113879",
  "result": {},
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID(thirdparty_user_collect_balance_syn)
[thirdparty.user.collect.balance.syn] -> 
{
  "id": "1583698334108.3342814379866697",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.syn",
  "params": {
    "sessionId": "123456789"
  }
}
[thirdparty.user.collect.balance.syn] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583698334172.144118632782365365",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583698334108.3342814379866697",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.collect.balance.syn",
    "params": {
      "reqIp": "202.11.82.1",
      "ip": "202.11.82.1",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID(thirdparty_user_collect_balance_info)
[thirdparty.user.collect.balance.info] -> 
{
  "id": "1583698334201.1602488494129260",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.collect.balance.info",
  "params": {
    "sessionId": "123456789"
  }
}
[thirdparty.user.collect.balance.info] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583698334257.8026749068211633894",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583698334201.1602488494129260",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.collect.balance.info",
    "params": {
      "reqIp": "202.11.82.1",
      "ip": "202.11.82.1",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583698334348.5834381197063976",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DAE23d69"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583698334348.5834381197063976",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=365.014247ms
```

