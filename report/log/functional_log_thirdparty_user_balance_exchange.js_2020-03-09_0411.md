
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583698313853.3817079038267019",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "S8irvyVUKPMOry0cbkOlgYxHrCk=",
    "salt": "5d485swtq1rgkcsu9orf4i2x3d79hasu",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583698313853.3817079038267019",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-08 16:11:53",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9779,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "expiry": 1583701913944,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1583698313996.7425969104798571",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583698313996.7425969104798571",
  "result": {
    "balance": 505372.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583705513944,
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583698314127.2626661392435196",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "from": 0,
    "to": 4,
    "amount": 43
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583698314127.2626661392435196",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583698314205.2412758507722429",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583698314205.2412758507722429",
  "result": {
    "balance": 505372.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583705513944,
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 43
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1583698314293.6621323607461030",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1583698314293.6621323607461030",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583698314373.1763467588687053",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 43
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583698314434.8857454358820797343",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583698314373.1763467588687053",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 43,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 14,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583698314474.6757949105215380",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583698314474.6757949105215380",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583698314551.7537428029930321",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "from": -1,
    "to": 5,
    "amount": 43
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1583698314601.657947778026311188",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:43.0",
    "action": "null"
  },
  "id": "1583698314551.7537428029930321",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 43,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae0001011323141DDC270B11DADE216b",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583698314631.7279953105749994",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b",
    "from": 0,
    "to": -5,
    "amount": 43
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583698314631.7279953105749994",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583698314767.9100770885517085",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323141DDC270B11DADE216b"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583698314767.9100770885517085",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=715.060903ms
some thresholds have failed
```

