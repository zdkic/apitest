
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583871218533.5625605235750726",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "Fwj7JaufCUNWKKo1A0F1/p1e6KA=",
    "salt": "cn13ieczj5cw61ohexqrdlkbilodrx6j",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583871218533.5625605235750726",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-10 16:13:38",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847651,
      "parentPathIncSelf": "/14679026/16847651/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 9834,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:49",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231223A719CE1261F30d3e",
    "expiry": 1583874818631,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583871218688.2375563112967478",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231223A719CE1261F30d3e"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583871218688.2375563112967478",
  "result": {
    "balance": 505444.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583878418631,
    "sessionId": "ae00010113231223A719CE1261F30d3e",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1583871218794.1889960542186983",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE1261F30d3e",
    "amount": 26
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1583871218794.1889960542186983",
  "result": {
    "success": "1",
    "cashflowId": 687151225570070500,
    "balance": 505470.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583871218934.0342774645505871",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231223A719CE1261F30d3e"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583871218934.0342774645505871",
  "result": {
    "balance": 505470.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583878418631,
    "sessionId": "ae00010113231223A719CE1261F30d3e",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 26
diff = 26
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1583871219018.6310423120282325",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 26
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583871219050.6153547657068412672",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583871219018.6310423120282325",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 26,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1583871219095.1948812093947610",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE1261F30d3e",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1583871219128.7511860122226114550",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1583871219095.1948812093947610",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231223A719CE1261F30d3e",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583871219221.4975516301694232",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231223A719CE1261F30d3e"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583871219221.4975516301694232",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=483.277735ms
```

