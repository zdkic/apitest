

```bash
[user.charge.list] -> 
{
  "id": "1583800254352.3472102490093315",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800254422.9182822030560640754",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800254352.3472102490093315",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800254454.6595773961105521",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800254502.153578684933228545",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800254454.6595773961105521",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800254655.1830435100236004",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800254733.2616769505195589649",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800254655.1830435100236004",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800254714.9571358758570715",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800254787.6911863101917151072",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800254714.9571358758570715",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583800254762.2360384820892480",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800254809.7906842359918214382",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800254762.2360384820892480",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800254941.7910611687659964",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800254986.1154120686352992567",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800254941.7910611687659964",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583800255197.4746471465046314",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583800255197.4746471465046314",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "endTime": "2020-03-10 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 91,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.charge.list] -> 
{
  "id": "1583800255378.3993689341449712",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800255448.9220908524949339632",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800255378.3993689341449712",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800255476.2441514530349167",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800255523.2816983319184074",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800255476.2441514530349167",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800255782.8867410553371355",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800255832.9483599344713729",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800255782.8867410553371355",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800255901.5347990472217062",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800255948.3462434685783655498",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800255901.5347990472217062",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800256100.5619738517642040",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256171.9186986993504745344",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256100.5619738517642040",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800256158.0898877331819997",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256229.8916951060715077559",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256158.0898877331819997",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800256200.7045004640792360",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256247.8928315744290847744",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256200.7045004640792360",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 1,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800256222.9437427668354876",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256295.8826870493625695920",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256222.9437427668354876",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800256258.1198872990450073",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256306.6822951889269669888",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256258.1198872990450073",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800256324.0564445326235328",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256372.7710048178153717744",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256324.0564445326235328",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800256459.4731887038182856",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256516.9216588038265092000",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256459.4731887038182856",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800256634.9072574897510980",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800256681.7980096754124766710",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256634.9072574897510980",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800256985.6522905967540807",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257038.55318642337925633",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800256985.6522905967540807",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800257049.1012188269830683",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257096.7767996517188631541",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257049.1012188269830683",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800257134.6475371557565784",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800257208.6836463680297614848",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257134.6475371557565784",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583800257245.1391127621069073",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257318.7708719306916608703",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257245.1391127621069073",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800257346.5788987830763019",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257396.1030207222271115906",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257346.5788987830763019",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800257365.9900099784841797",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800257437.1520809608695792884",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257365.9900099784841797",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847647
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583800257587.8791524792269008",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257632.7484836669823106688",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257587.8791524792269008",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800257606.3281316968346261",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800257679.6341064944523394556",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257606.3281316968346261",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583800257668.9299087795091914",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257738.151443743396855873",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257668.9299087795091914",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800257766.9974803347980705",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257813.6908379988617510400",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257766.9974803347980705",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800257846.9081349543773704",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800257920.6699068144979132407",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257846.9081349543773704",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583800257908.4561155224911086",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800257979.865256352392560713",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800257908.4561155224911086",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800258009.0236625183152468",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258056.1518144632903434816",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258009.0236625183152468",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800258089.7476856497632903",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132111EC943E93122A820884",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800258163.8141909989670174652",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258089.7476856497632903",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132111EC943E93122A820884",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583800258127.7792811477641431",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258176.6479547073661239264",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258127.7792811477641431",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800258148.2054350755190446",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258220.2377901702780240202",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258148.2054350755190446",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800258263.7350688174282917",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258310.317664303503327746",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258263.7350688174282917",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800258316.5946315282798560",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258363.8762698586231930752",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258316.5946315282798560",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800258370.8738523208080867",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258416.1153627666093048002",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258370.8738523208080867",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800258387.7439782888111720",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800258462.9222512199967112158",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258387.7439782888111720",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583800258608.6107457768592711",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258655.184798561844611457",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258608.6107457768592711",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800258731.6942618877022228",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258778.909729256113700886",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258731.6942618877022228",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800258930.8712800715756021",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800258999.290500737809007968",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258930.8712800715756021",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800258992.5767742746560396",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800259064.111923833782420072",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800258992.5767742746560396",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583800259028.8390581897450561",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800259074.8643917093985040383",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800259028.8390581897450561",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583800259050.6089903620599865",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583800259050.6089903620599865",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 36,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583800259293.7388403183931579",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132111EC943E93122A820884",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800259365.9209683030900210682",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800259293.7388403183931579",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132111EC943E93122A820884",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583800259353.6080689366000283",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800259427.1225032700115550616",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800259353.6080689366000283",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[sn.lottery.order.query] -> 
{
  "id": "1583800259602.2637052433390146",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583800259602.2637052433390146",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 39,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[report.day.order.groupby.query] -> 
{
  "id": "1583800259978.7170258473826987",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800260029.6339887859368640474",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800259978.7170258473826987",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583800260020.3587474910286418",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800260070.9078155636342439860",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260020.3587474910286418",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583800260200.8090522384379718",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800260277.155374536915698194",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260200.8090522384379718",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 8,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583800260318.9924699424446680",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260390.8930072908674153192",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260318.9924699424446680",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800260378.3379357606691020",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260426.2341873069443861010",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260378.3379357606691020",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800260419.9002924579839368",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260466.8934574312600630783",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260419.9002924579839368",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800260438.9481957344308641",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260511.9038158203103198974",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260438.9481957344308641",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800260482.4014069126286813",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260530.576509416971961546",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260482.4014069126286813",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800260500.8941973163607434",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260571.1459173563537686816",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260500.8941973163607434",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800260541.9191894944312446",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260589.9214312058892694900",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260541.9191894944312446",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800260560.6833277704146282",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800260633.2331880958840079362",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260560.6833277704146282",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583800260600.3303084678353946",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260648.1351647769341001728",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260600.3303084678353946",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583800260679.0931443441440824",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583800260679.0931443441440824",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 47,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.order.query] -> 
{
  "id": "1583800260794.5921121951522839",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260840.4962930915663658962",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260794.5921121951522839",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583800260739.6814062210262200",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583800260739.6814062210262200",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 36,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.order.query] -> 
{
  "id": "1583800260920.1219652239694344",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800260969.8790939883938511072",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800260920.1219652239694344",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800261040.6715145939719476",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201552691E43122A82070a",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800261141.2468258538886021788",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800261040.6715145939719476",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae00010113201552691E43122A82070a",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 45,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583800261321.2486935084029189",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800261369.9218710106242727871",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800261321.2486935084029189",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800261388.6347470710469226",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800261940.9075727407152886139",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800261388.6347470710469226",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 20,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800261877.5208345656263159",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800261960.47290273880949263",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800261877.5208345656263159",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847653
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 32,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583800261825.8687988289797948",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800261954.9182804180822046550",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800261825.8687988289797948",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847651
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 24,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583800261462.6460387863591173",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800261987.8601804918828825420",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800261462.6460387863591173",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847655
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 62,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
[user.order.query] -> 
{
  "id": "1583800262005.0914089880548293",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "id": "1583800262002.1362105714766189",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262061.8616482521434030079",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262002.1362105714766189",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 18,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262082.9124000332729794484",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262005.0914089880548293",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 18,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800262005.2254589477622996",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262082.2923483240522581856",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262005.2254589477622996",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 19,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800262000.2823179853227744",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262062.9196192096325646582",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262000.2823179853227744",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 31,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800262001.9430353877026756",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262062.9214215222346710528",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262001.9430353877026756",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 33,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800262008.9387931449587773",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800262101.486393159983628288",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262008.9387931449587773",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847654
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 24,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583800262294.6270128362289993",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262342.1158060847997928466",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262294.6270128362289993",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800262446.6907406813074534",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262492.6339765344091813786",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262446.6907406813074534",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800262488.8050126505743235",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262559.54360408930584608",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262488.8050126505743235",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800262525.8888714520356375",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262571.8645325714763004800",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262525.8888714520356375",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[sn.lottery.order.query] -> 
{
  "id": "1583800262494.9770183745527121",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583800262494.9770183745527121",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 45,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.withdraw.list] -> 
{
  "id": "1583800262588.5610572259921835",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262636.9177209298726420224",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262588.5610572259921835",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800262611.3327105099699356",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262685.5615941859508942808",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262611.3327105099699356",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 13,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800262724.2436626398626971",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800262771.9222667179973720832",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262724.2436626398626971",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800262846.5458599280861923",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201552691E43122A82070a",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800262920.8570208528492117952",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262846.5458599280861923",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae00010113201552691E43122A82070a",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[report.day.order.groupby.query] -> 
{
  "id": "1583800262908.0842475871969392",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132111EC943E93122A820884",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800262981.7420170763486347132",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800262908.0842475871969392",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132111EC943E93122A820884",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583800263148.6164311402212955",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263223.9190153570077227904",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263148.6164311402212955",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 11,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800263238.6078043265021699",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263286.4221359390752768392",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263238.6078043265021699",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800263261.7931106451575212",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263310.653098707772768768",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263261.7931106451575212",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800263321.3320660838258973",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263368.8646695701808217600",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263321.3320660838258973",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800263352.3083076703667019",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800263402.72098003886948865",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263352.3083076703667019",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847650
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.charge.list] -> 
{
  "id": "1583800263330.4257445955381283",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263403.8627487215229779712",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263330.4257445955381283",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800263357.2437705719484798",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101131F116F4D90DF122A8206aa",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263418.289675237778915408",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263357.2437705719484798",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101131F116F4D90DF122A8206aa",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 16,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800263446.9871092445463681",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263493.1901126011826751552",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263446.9871092445463681",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800263459.9013928454959767",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263508.8610594930158139391",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263459.9013928454959767",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800263511.1461142101605790",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132111EC943E93122A820884",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800263586.1342931957377401248",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263511.1461142101605790",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132111EC943E93122A820884",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583800263538.2856829310255910",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263586.7493844335171925738",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263538.2856829310255910",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800263554.6867570672903285",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263602.1297256894280303932",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263554.6867570672903285",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800263630.7855982084746989",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
[user.order.query] -> 
{
  "id": "1583800263669.0572110367785859",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263717.9077603434053640",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263630.7855982084746989",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263718.1254258028748800028",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263669.0572110367785859",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800263746.1971167850535621",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011323181B7E5EE4122A827546",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263793.9209152599976819712",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263746.1971167850535621",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011323181B7E5EE4122A827546",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800263872.5007537474703443",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263962.9222767302690207448",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263872.5007537474703443",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800263951.1208117854672077",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800263999.9200915058928172783",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800263951.1208117854672077",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800264011.3948505579977722",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264059.77700837637423209",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264011.3948505579977722",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800264010.4447055512313644",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae000101132715B5B0EC51122A8203d6",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264059.5467194016606846400",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264010.4447055512313644",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132715B5B0EC51122A8203d6",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800264052.8761051893117185",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113201552691E43122A82070a",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800264124.7451063528488435656",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264052.8761051893117185",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae00010113201552691E43122A82070a",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847648
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[sn.lottery.order.query] -> 
{
  "id": "1583800264165.1769610033703707",
  "jsonrpc": "2.0",
  "method": "sn.lottery.order.query",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": 106,
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "beginTime": "2018-08-01 00:00:00",
    "pageNo": 1
  }
}
[sn.lottery.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "1",
    "sn": "null",
    "message": "最大允许查询时间不允许超过12个月",
    "reason": "最大允许查询时间不允许超过12个月",
    "action": "null"
  },
  "id": "1583800264165.1769610033703707",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "sn.lottery.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageNo": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "beginTime": "2018-08-01 00:00:00",
      "moduleId": 106
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 80,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[sn.lottery.order.query]error code = 1
[user.charge.list] -> 
{
  "id": "1583800264276.5962861306431987",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264327.9182751627702940632",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264276.5962861306431987",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800264370.8857483779331390",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae0001011328146F01E3F0122A825fad",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264418.22556517569921420",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264370.8857483779331390",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011328146F01E3F0122A825fad",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800264503.5594244476959620",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae0001011325161DBCC8C3122A828f5c",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264549.45600118778905860",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264503.5594244476959620",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae0001011325161DBCC8C3122A828f5c",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.order.query] -> 
{
  "id": "1583800264516.7914169375018066",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132611D8F1831B122A827efc",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264566.6931889331912725",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264516.7914169375018066",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132611D8F1831B122A827efc",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800264655.5128962831996543",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264728.3459935510955443745",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264655.5128962831996543",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[user.withdraw.list] -> 
{
  "id": "1583800264758.9884833855789149",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113201552691E43122A82070a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264805.8050008411898623936",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264758.9884833855789149",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113201552691E43122A82070a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[user.order.query] -> 
{
  "id": "1583800264766.0501292290160944",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264822.7996118873149259488",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264766.0501292290160944",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
[user.charge.list] -> 
{
  "id": "1583800264779.6208574401021051",
  "jsonrpc": "2.0",
  "method": "user.charge.list",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.charge.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264855.370421086668980240",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264779.6208574401021051",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.charge.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.charge.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800264845.3178962978195160",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae00010113241473A8BF39122A826ab9",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800264895.432358252015075886",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264845.3178962978195160",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae00010113241473A8BF39122A826ab9",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847652
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.withdraw.list] -> 
{
  "id": "1583800264885.9910061405977684",
  "jsonrpc": "2.0",
  "method": "user.withdraw.list",
  "params": {
    "sessionId": "ae00010113221241AA34DD122A82bd0a",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "popupFlag": "N",
    "pageIndex": 1,
    "pageSize": 100,
    "scope": 0
  }
}
[user.withdraw.list] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800264932.6735047451532574708",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800264885.9910061405977684",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.list",
    "params": {
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "popupFlag": "N",
      "scope": 0,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae00010113221241AA34DD122A82bd0a",
      "endTime": "2020-03-10 23:59:59",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.list]error code = 3100
[report.day.order.groupby.query] -> 
{
  "id": "1583800265080.4446547864255238",
  "jsonrpc": "2.0",
  "method": "report.day.order.groupby.query",
  "params": {
    "beginTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleIds": [],
    "sessionId": "ae000101132111EC943E93122A820884",
    "terminal": 8
  }
}
[report.day.order.groupby.query] <- 
{
  "result": "0",
  "error": {
    "code": "4003",
    "sn": "1583800265130.8330462884322525054",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800265080.4446547864255238",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "report.day.order.groupby.query",
    "params": {
      "moduleIds": [],
      "reqIp": "202.11.82.1",
      "beginTime": "2018-08-01",
      "endTime": "2020-03-10 23:59:59",
      "sessionId": "ae000101132111EC943E93122A820884",
      "terminal": 8,
      "sn": "ae00",
      "userId": 16847649
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[report.day.order.groupby.query]error code = 4003
[user.order.query] -> 
{
  "id": "1583800265145.3232096758825787",
  "jsonrpc": "2.0",
  "method": "user.order.query",
  "params": {
    "sessionId": "ae000101132111EC943E93122A820884",
    "startTime": "2018-08-01",
    "endTime": "2020-03-10 23:59:59",
    "moduleId": "2",
    "pageIndex": 1,
    "pageSize": 100,
    "withItemDesc": 1,
    "terminal": 8
  }
}
[user.order.query] <- 
{
  "result": "0",
  "error": {
    "code": "3100",
    "sn": "1583800265200.8034096210962792408",
    "message": "时间查询区间限制为120天，如有疑问，请联系客服。",
    "reason": "null",
    "action": "null"
  },
  "id": "1583800265145.3232096758825787",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.order.query",
    "params": {
      "withItemDesc": 1,
      "reqIp": "202.11.82.1",
      "pageIndex": 1,
      "pageSize": 100,
      "startTime": "2018-08-01",
      "sessionId": "ae000101132111EC943E93122A820884",
      "endTime": "2020-03-10 23:59:59",
      "terminal": 8,
      "moduleId": "2",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 14,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.order.query]error code = 3100
Engine error error=context cancelled at randomcharstring (file:///home/zdkic/Project/apiTest/lib/utils.js:229:2215(39))
Engine Error
```

