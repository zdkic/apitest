
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583784831428.4860949051386953",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "7D+Mz8GfxzXv/pxaYeZjtWcH2So=",
    "salt": "rxolo2kk1iydil8bg70yly0bp6yp2dib",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583784831428.4860949051386953",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-09 16:13:51",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9810,
      "regIp": "202.11.82.1",
      "parentId": 14679026,
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231129BB3328121E766654",
    "expiry": 1583788431531,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583784831566.3274134643976065",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583784831566.3274134643976065",
  "result": {
    "balance": 505444.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583792031531,
    "sessionId": "ae00010113231129BB3328121E766654",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.no.get] -> 
{
  "id": "1583784831678.5907728469527798",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583784831678.5907728469527798",
  "result": "200309161351656346",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583784831753.3911547910847417",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583784831804.4107512125517742337",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583784831753.3911547910847417",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply] -> 
{
  "id": "1583784831841.0406303700070952",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654",
    "withdrawNo": "200309161351656346",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 446
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583784831893.1730508199770980384",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583784831841.0406303700070952",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 446,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200309161351656346",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 1
[user.withdraw.apply] -> 
{
  "id": "1583784891935.1441734208413543",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654",
    "withdrawNo": "200309161351656346",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 446
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583784891994.6052783456180352948",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583784891935.1441734208413543",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 446,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200309161351656346",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 2
[user.withdraw.apply] -> 
{
  "id": "1583784952033.9295152168565988",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654",
    "withdrawNo": "200309161351656346",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 446
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583784952090.8835499410622970880",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583784952033.9295152168565988",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 446,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200309161351656346",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 3
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583785012125.1493599776067799",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583785012125.1493599776067799",
  "result": {
    "balance": 505444.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583792031531,
    "sessionId": "ae00010113231129BB3328121E766654",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 446
diff = 0
[user.withdraw.no.get] -> 
{
  "id": "1583785132207.2643908752118956",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583785132207.2643908752118956",
  "result": "200309161852169440",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1583785132327.8144712367280422",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  }
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583785132377.8497726961019256828",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.get",
    "action": "null"
  },
  "id": "1583785132327.8144712367280422",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過低的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583785132429.2388930488840284",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654",
    "withdrawNo": "200309161852169440",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 1
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583785132487.3832598484614119696",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583785132429.2388930488840284",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 1,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200309161852169440",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過高的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1583785132531.4435745268021144",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654",
    "withdrawNo": "200309161852169440",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 30000000
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "0000",
    "sn": "1583785132591.8208931822197142269",
    "message": "对不起，北京时间（02:00:00 ~ 09:00:00）期间暂停出款！如有疑问，请联系客服",
    "reason": "checkWithdrawIsLimit sn is ae00, flag is 1, range is 02:00:00,09:00:00",
    "action": "null"
  },
  "id": "1583785132531.4435745268021144",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 30000000,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200309161852169440",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583785132622.2759110914837295",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583785132622.2759110914837295",
  "result": "200309161852930461",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583785132724.4167027912165728",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "123456789",
    "withdrawNo": "200309161852930461",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "toBankId": 1,
    "amount": 446
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583785132773.7479893892692574207",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583785132724.4167027912165728",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 446,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200309161852930461",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583785137801.9942935248888470",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583785137801.9942935248888470",
  "result": "200309161857762888",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1583785137886.8554416802599626",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654",
    "withdrawNo": "200309161857762888",
    "payPasword": "123456789",
    "toBankId": 1,
    "amount": 446
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2273",
    "sn": "1583785137936.3459264586158572712",
    "message": "提现密码错误",
    "reason": "sn:ae00, userId:16847651 paypwd is wrong",
    "action": "null"
  },
  "id": "1583785137886.8554416802599626",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 446,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "200309161857762888",
      "payPasword": "123456789",
      "sessionId": "ae00010113231129BB3328121E766654",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583785138033.9304985636943863",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231129BB3328121E766654"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583785138033.9304985636943863",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=5m6.401181195s
some thresholds have failed
```

