
```bash
[auth.mobile.login]測試正常流程
[auth.mobile.login] -> 
{
  "id": "1574107808122.7510238830808167",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "2MGJA3H5mYOqY5gocpQbj7N6qKE=",
    "salt": "mbocqw7m2gnegrcge5bfwozuexdjodt4",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574107808122.7510238830808167",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2019-11-18 16:10:08",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847647/",
      "userId": 16847647,
      "loginCount": 25624,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:48",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2030,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F116F4D90DF54D3C9bdb8",
    "expiry": 1574111408388,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.mobile.login]測試錯誤帳密
[auth.mobile.login] -> 
{
  "id": "1574107808410.0038853115344562",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "imGQQ0XoOa6GVdMlfmqYLByc6TM=",
    "salt": "s1pxzhmxvwgsen8rmyvnx49rjr6jxw32",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2218",
    "sn": "1574107808491.9203083811193535212",
    "message": "用户名或密码错误,登录失败.",
    "reason": "loginId:bgqajohnload1, password:imGQQ0XoOa6GVdMlfmqYLByc6TM=, from sn:ae00",
    "action": "null"
  },
  "id": "1574107808410.0038853115344562",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "imGQQ0XoOa6GVdMlfmqYLByc6TM=",
      "loginId": "bgqajohnload1",
      "salt": "s1pxzhmxvwgsen8rmyvnx49rjr6jxw32",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 9,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]測試正常流程
[auth.session.validate] -> 
{
  "id": "1574107808497.4048925978822892",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "ae000101131F116F4D90DF54D3C9bdb8",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "id": "1574107808497.4048925978822892",
  "result": {
    "flag": "1",
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574111408388,
    "sessionId": "ae000101131F116F4D90DF54D3C9bdb8",
    "userId": 16847647
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.session.validate]測試錯誤sessionId
[auth.session.validate] -> 
{
  "id": "1574107808576.3218851003429948",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "12345678",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574107808649.4509305583828993",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1574107808576.3218851003429948",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "0",
      "sessionId": "12345678",
      "opIp": "202.11.82.1",
      "withUser": "0"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.online.logout]測試正常流程
[auth.online.logout] -> 
{
  "id": "1574107808657.1551391613353219",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F116F4D90DF54D3C9bdb8"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574107808657.1551391613353219",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
[auth.online.logout]測試錯誤sessionId
[auth.online.logout] -> 
{
  "id": "1574107808736.7548732013495652",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "12345678"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574107808809.2309220904399355904",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1574107808736.7548732013495652",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.online.logout",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "12345678",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
Test finished i=1 t=695.200352ms
```

