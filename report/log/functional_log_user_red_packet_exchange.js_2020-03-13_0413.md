
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1584044012289.5615892912113002",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "1QFJPOKaU2h30GbyACW8PKJP8aw=",
    "salt": "7hezr77yaiu5kmgupoa3fx62fpjay1hu",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1584044012289.5615892912113002",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-12 16:13:32",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847651,
      "parentPathIncSelf": "/14679026/16847651/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 9884,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:49",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132310FD02CC6412E8F277fb",
    "expiry": 1584047612387,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1584044012424.2549176197164350",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6412E8F277fb"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1584044012424.2549176197164350",
  "result": {
    "balance": 505504.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1584051212387,
    "sessionId": "ae000101132310FD02CC6412E8F277fb",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1584044012524.7768296949702059",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae000101132310FD02CC6412E8F277fb",
    "amount": 26
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1584044012524.7768296949702059",
  "result": {
    "success": "1",
    "cashflowId": 687875975082676200,
    "balance": 505530.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1584044012697.1697321433145482",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132310FD02CC6412E8F277fb"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1584044012697.1697321433145482",
  "result": {
    "balance": 505530.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1584051212387,
    "sessionId": "ae000101132310FD02CC6412E8F277fb",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 26
diff = 26
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1584044012787.0213552738538882",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 26
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1584044012838.229756166212174604",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1584044012787.0213552738538882",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 26,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1584044012864.9106246860985822",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae000101132310FD02CC6412E8F277fb",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1584044012916.1213441373514433732",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1584044012864.9106246860985822",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132310FD02CC6412E8F277fb",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1584044013003.1670514423684948",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132310FD02CC6412E8F277fb"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1584044013003.1670514423684948",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=519.215812ms
```

