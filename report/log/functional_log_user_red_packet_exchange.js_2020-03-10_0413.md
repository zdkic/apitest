
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583784811481.8223986564952679",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "iPokTuB6p37VG76ZdCp0Ts/Za1g=",
    "salt": "lramm45uv8ntcxkg7f5mkjpmhqkf9iw8",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583784811481.8223986564952679",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-09 16:13:31",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9809,
      "regIp": "202.11.82.1",
      "parentId": 14679026,
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323171E1AB85F121E726e0a",
    "expiry": 1583788411572,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583784811612.2491012978519453",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F121E726e0a"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583784811612.2491012978519453",
  "result": {
    "balance": 505418.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583792011572,
    "sessionId": "ae0001011323171E1AB85F121E726e0a",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1583784811712.7280756700949262",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F121E726e0a",
    "amount": 26
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1583784811712.7280756700949262",
  "result": {
    "success": "1",
    "cashflowId": 686788808088490000,
    "balance": 505444.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583784811879.0834861688137105",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F121E726e0a"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583784811879.0834861688137105",
  "result": {
    "balance": 505444.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583792011572,
    "sessionId": "ae0001011323171E1AB85F121E726e0a",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 26
diff = 26
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1583784811988.4120770299955857",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 26
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583784812040.54151120020307970",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583784811988.4120770299955857",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 26,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1583784812068.8960292475409607",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F121E726e0a",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1583784812120.8897983940095900660",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1583784812068.8960292475409607",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011323171E1AB85F121E726e0a",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583784812215.3358690680385980",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323171E1AB85F121E726e0a"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583784812215.3358690680385980",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=536.350572ms
```

