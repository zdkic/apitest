
```bash
loginId = bgqajohnload4
[auth.mobile.login] -> 
{
  "id": "1574712684285.7134095766557733",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload4",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "OqfcAkSRknFwQ8wjwVM4CQeq2QE=",
    "salt": "dts3enbjiycr1rf7h3um8ybihdqnhd20",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2303",
    "sn": "1574712684414.7996686496163151328",
    "message": "请输入验证码",
    "reason": null,
    "action": null
  },
  "id": "1574712684285.7134095766557733",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "OqfcAkSRknFwQ8wjwVM4CQeq2QE=",
      "loginId": "bgqajohnload4",
      "salt": "dts3enbjiycr1rf7h3um8ybihdqnhd20",
      "reqIp": "202.11.82.1",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 37,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth_mobile_login]error code = 2303
loginId = bgqajohnload4
測試正常的流程
[user.balance.get] -> 
{
  "id": "1574712684440.0958203342363105",
  "method": "user.balance.get",
  "params": {},
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712684521.3984019220807633032",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574712684440.0958203342363105",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.balance.get",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.no.get] -> 
{
  "id": "1574712684547.8446873435168149",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574712684547.8446873435168149",
  "result": "191125161124325156",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1574712684627.4282138064964864",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {}
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712684683.1409922539039768608",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712684627.4282138064964864",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.apply] -> 
{
  "id": "1574712684714.3744791736303616",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191125161124325156",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 442
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712684775.9147901232167695163",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712684714.3744791736303616",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 442,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191125161124325156",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 1
[user.withdraw.apply] -> 
{
  "id": "1574712744805.0697211738663801",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191125161124325156",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 442
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712744867.2378047115072442010",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712744805.0697211738663801",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 442,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191125161124325156",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 2
[user.withdraw.apply] -> 
{
  "id": "1574712804895.6635499924706165",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191125161124325156",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 442
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712804952.2479469372864806918",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712804895.6635499924706165",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 442,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191125161124325156",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
retry_count = 3
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1574712864981.6226350834032442",
  "method": "user.balance.get",
  "params": {},
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712865035.4410151880744518912",
    "message": "缺少参数",
    "reason": "fixed-error-sessionId is empty",
    "action": null
  },
  "id": "1574712864981.6226350834032442",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.balance.get",
    "params": {
      "reqIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 0,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
amount = 442
diff = NaN
[user.withdraw.no.get] -> 
{
  "id": "1574712986281.3449046339935762",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574712986281.3449046339935762",
  "result": "191125161626847007",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.get] -> 
{
  "id": "1574712986380.0049195634235352",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.get",
  "params": {}
}
[user.audit.about.current.get] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712986433.9146544598024174796",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712986380.0049195634235352",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.get",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過低的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1574712986459.6462569171462928",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191125161626847007",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 1
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712986522.1447912007885654084",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712986459.6462569171462928",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 1,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191125161626847007",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 23,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試過高的申请出款金额
[user.withdraw.apply] -> 
{
  "id": "1574712986564.3371157432895382",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191125161626847007",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 30000000
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712986618.8356288232954003136",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712986564.3371157432895382",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 30000000,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191125161626847007",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1574712986643.4565005624418390",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574712986643.4565005624418390",
  "result": "191125161626372532",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1574712986725.3889496867931363",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "sessionId": "123456789",
    "withdrawNo": "191125161626372532",
    "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
    "toBankId": 1,
    "amount": 442
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574712986783.72057598601658424",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574712986725.3889496867931363",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 442,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191125161626372532",
      "payPasword": "t92XxCcZrHxj6k3DXhAhoxDgbTs=",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1574712991809.2309402533980814",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {}
}
[user.withdraw.no.get] <- 
{
  "id": "1574712991809.2309402533980814",
  "result": "191125161631281092",
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.apply] -> 
{
  "id": "1574712991886.9423020536951241",
  "jsonrpc": "2.0",
  "method": "user.withdraw.apply",
  "params": {
    "withdrawNo": "191125161631281092",
    "payPasword": "123456789",
    "toBankId": 1,
    "amount": 442
  }
}
[user.withdraw.apply] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712992656.5332191086711406528",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712991886.9423020536951241",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.withdraw.apply",
    "params": {
      "amount": 442,
      "reqIp": "202.11.82.1",
      "toBankId": 1,
      "withdrawNo": "191125161631281092",
      "payPasword": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 15,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload4
[auth.online.logout] -> 
{
  "id": "1574712992749.9014122624967819",
  "method": "auth.online.logout",
  "params": {},
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "result": "0",
  "error": {
    "code": "700",
    "sn": "1574712992824.6917271625948577700",
    "message": "缺少参数",
    "reason": "sessionId参数不能为空",
    "action": "null"
  },
  "id": "1574712992749.9014122624967819",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.online.logout",
    "params": {
      "reqIp": "202.11.82.1",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
Test finished i=1 t=5m8.251084678s
some thresholds have failed
```

