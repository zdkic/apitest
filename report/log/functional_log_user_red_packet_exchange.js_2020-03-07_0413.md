
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583525612121.8564939552037405",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "G4lUmkz9GkMxJ/aV1onHuL1CEk0=",
    "salt": "5lwlb3t4nppxfttqm2n6j1bospsse6jc",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583525612121.8564939552037405",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-06 16:13:32",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9734,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323193CC80AA31153F227ca",
    "expiry": 1583529212226,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583525612265.7609009170106150",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153F227ca"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583525612265.7609009170106150",
  "result": {
    "balance": 505316.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583532812226,
    "sessionId": "ae0001011323193CC80AA31153F227ca",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1583525612374.7713470518469732",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153F227ca",
    "amount": 43
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1583525612374.7713470518469732",
  "result": {
    "success": "1",
    "cashflowId": 685701647276707800,
    "balance": 505359.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583525612537.7012097025526998",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153F227ca"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583525612537.7012097025526998",
  "result": {
    "balance": 505359.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583532812226,
    "sessionId": "ae0001011323193CC80AA31153F227ca",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 43
diff = 43
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1583525612631.5652861228023220",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 43
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525612686.9078887064702795455",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583525612631.5652861228023220",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 43,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1583525612713.0194388427757397",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153F227ca",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1583525612771.9078130094156332886",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1583525612713.0194388427757397",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011323193CC80AA31153F227ca",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 13,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583525612865.9557245972203996",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323193CC80AA31153F227ca"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583525612865.9557245972203996",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=540.423533ms
```

