
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583957612216.7849753881715734",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "LqcXa/MZTMnMXt826EjxrRBhxV0=",
    "salt": "juflm6zldbs1dawg2g6cgec9a0p4u0zv",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583957612216.7849753881715734",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-11 16:13:32",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9859,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "qq": "",
      "passportNumber": null,
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231129BB332812A57235bc",
    "expiry": 1583961212311,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583957612356.4227932883088575",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231129BB332812A57235bc"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583957612356.4227932883088575",
  "result": {
    "balance": 505470.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583964812311,
    "sessionId": "ae00010113231129BB332812A57235bc",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1583957612470.4828547819142531",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231129BB332812A57235bc",
    "amount": 34
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1583957612470.4828547819142531",
  "result": {
    "success": "1",
    "cashflowId": 687513586986389500,
    "balance": 505504.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583957612649.2929928118986729",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231129BB332812A57235bc"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583957612649.2929928118986729",
  "result": {
    "balance": 505504.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583964812311,
    "sessionId": "ae00010113231129BB332812A57235bc",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 34
diff = 34
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1583957612745.9613626353107234",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 34
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583957612805.8068188761389249128",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583957612745.9613626353107234",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 34,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1583957612831.0388455713676289",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231129BB332812A57235bc",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1583957612881.7980378462390499307",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1583957612831.0388455713676289",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB332812A57235bc",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583957612957.5585922867446213",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231129BB332812A57235bc"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583957612957.5585922867446213",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=550.514932ms
```

