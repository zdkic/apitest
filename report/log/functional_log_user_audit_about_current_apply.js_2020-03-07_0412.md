
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583525552873.2154901822719187",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "k0fNQUZBlgJur08rcOSAd/+YQyk=",
    "salt": "wyk3qnb6oo30eg2x307tpufkn21ugb37",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583525552873.2154901822719187",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-06 16:12:32",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9731,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231129BB33281153E6cc5b",
    "expiry": 1583529152979,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.withdraw.option.get] -> 
{
  "id": "1583525553023.7368401389450391",
  "jsonrpc": "2.0",
  "method": "user.withdraw.option.get",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b",
    "terminal": 8
  }
}
[user.withdraw.option.get] <- 
{
  "id": "1583525553023.7368401389450391",
  "result": {
    "feeAmount": 0,
    "minAmount": 2,
    "amount": 0,
    "availableAmount": 505316.2,
    "balance": {
      "balance": 505316.2
    },
    "sn": "ae00",
    "nonFeeTimes": 0,
    "maxAmount": 2000000,
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.account.default.get] -> 
{
  "id": "1583525553160.5480128117138210",
  "jsonrpc": "2.0",
  "method": "user.withdraw.account.default.get",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b",
    "terminal": 8
  }
}
[user.withdraw.account.default.get] <- 
{
  "id": "1583525553160.5480128117138210",
  "result": {
    "accountStatus": 1,
    "bankAccount": "123456789012",
    "bankAccountOwner": "user",
    "bankId": 1,
    "isDefault": 1,
    "bankBranch": "1111",
    "memo": null,
    "bankName": "工商银行",
    "sn": "ae00",
    "userId": 16847651,
    "withdrawAccountId": 31285,
    "lastUpdateTime": "2018-09-06 05:46:20"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.cancel] -> 
{
  "id": "1583525553250.8320477711373026",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  }
}
[user.audit.about.current.cancel] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525553307.7200691728879190015",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.cancel",
    "action": "null"
  },
  "id": "1583525553250.8320477711373026",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.cancel",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB33281153E6cc5b",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.no.get] -> 
{
  "id": "1583525553334.0531182479610932",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525553334.0531182479610932",
  "result": "200306161233138515",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583525553431.8279281143826791",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200306161233138515",
    "amount": 499
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525553488.6017925096723708512",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583525553431.8279281143826791",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 499,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200306161233138515",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB33281153E6cc5b",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.trace] -> 
{
  "id": "1583525553515.6128981629260965",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  }
}
[user.audit.about.current.trace] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525553569.7399228299376768503",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.trace",
    "action": "null"
  },
  "id": "1583525553515.6128981629260965",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.trace",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231129BB33281153E6cc5b",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583525553598.8631193803545334",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525553598.8631193803545334",
  "result": "200306161233708054",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583525553683.2958557306643829",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "123456789",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200306161233708054",
    "amount": 521
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583525553738.2451085256950024832",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583525553683.2958557306643829",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 521,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200306161233708054",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583525553766.0012147102442170",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525553766.0012147102442170",
  "result": "200306161233326003",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583525553848.4898879492430022",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b",
    "payPasword": "123456789",
    "withdrawNo": "200306161233326003",
    "amount": 312
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525553905.351426183633567904",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583525553848.4898879492430022",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 312,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200306161233326003",
      "payPasword": "123456789",
      "sessionId": "ae00010113231129BB33281153E6cc5b",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的充值流水号
[user.withdraw.no.get] -> 
{
  "id": "1583525553932.2815512055452416",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525553932.2815512055452416",
  "result": "200306161233777253",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583525554037.6289555062661240",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "123456789",
    "amount": 218
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525554094.9042796011850516",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583525554037.6289555062661240",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 218,
      "reqIp": "202.11.82.1",
      "withdrawNo": "123456789",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB33281153E6cc5b",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的取款金额
[user.withdraw.no.get] -> 
{
  "id": "1583525554122.9123041437258308",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583525554122.9123041437258308",
  "result": "200306161234095633",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583525554232.6352056626745401",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200306161234095633",
    "amount": 240000
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583525554285.8844923671429430358",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583525554232.6352056626745401",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200306161234095633",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae00010113231129BB33281153E6cc5b",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583525554382.3708503140342074",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231129BB33281153E6cc5b"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583525554382.3708503140342074",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.291204863s
some thresholds have failed
```

