
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583871178379.6023650008413557",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "4AJes1a57pFE6FU0u9Ciye9kRDw=",
    "salt": "5arox21tp7jzq8ccs2h0hpq6gue2rfxy",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583871178379.6023650008413557",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-10 16:12:58",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "regCode": "badzNl",
      "isOnline": null,
      "userId": 16847651,
      "parentPathIncSelf": "/14679026/16847651/",
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "loginCount": 9832,
      "unreadNotice": 0,
      "regTime": "2018-09-06 05:41:49",
      "regFrom": 8,
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "expiry": 1583874778460,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871178530.7684666472710699",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 1,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871178530.7684666472710699",
  "result": {
    "amount": 100,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 133,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871178648.1777131400597959",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 2,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871178648.1777131400597959",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871178736.8561315062796101",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 3,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871178736.8561315062796101",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871178818.2712486377920254",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 1,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871178818.2712486377920254",
  "result": {
    "amount": 300,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 333,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871178905.3530643512707975",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 2,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871178905.3530643512707975",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871178992.0229764054567236",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 3,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871178992.0229764054567236",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871179073.7507282993317804",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 1,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871179073.7507282993317804",
  "result": {
    "amount": 165,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 198,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871179171.9893464672861437",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 2,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871179171.9893464672861437",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583871179249.3059955273503293",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d",
    "chargeType": 3,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583871179249.3059955273503293",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583871179419.6445159823572274",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323171E1AB85F1261EB4c4d"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583871179419.6445159823572274",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=807.314497ms
```

