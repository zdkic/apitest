
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583957552910.2422418049146105",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "f/cFbwkyAR+xl3Ggc74KV6Kbjv8=",
    "salt": "k6hemgrrg8sps365pa7x0fla9h1wkitt",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583957552910.2422418049146105",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-11 16:12:33",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9856,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "qq": "",
      "passportNumber": null,
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132313A7A7599B12A56655bf",
    "expiry": 1583961153028,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.withdraw.option.get] -> 
{
  "id": "1583957553069.0721658708889182",
  "jsonrpc": "2.0",
  "method": "user.withdraw.option.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf",
    "terminal": 8
  }
}
[user.withdraw.option.get] <- 
{
  "id": "1583957553069.0721658708889182",
  "result": {
    "feeAmount": 0,
    "minAmount": 2,
    "amount": 0,
    "availableAmount": 505470.2,
    "balance": {
      "balance": 505470.2
    },
    "sn": "ae00",
    "nonFeeTimes": 0,
    "maxAmount": 2000000,
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.account.default.get] -> 
{
  "id": "1583957553209.5770595471577941",
  "jsonrpc": "2.0",
  "method": "user.withdraw.account.default.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf",
    "terminal": 8
  }
}
[user.withdraw.account.default.get] <- 
{
  "id": "1583957553209.5770595471577941",
  "result": {
    "bankAccount": "123456789012",
    "accountStatus": 1,
    "bankAccountOwner": "user",
    "bankId": 1,
    "isDefault": 1,
    "bankBranch": "1111",
    "memo": null,
    "bankName": "工商银行",
    "sn": "ae00",
    "userId": 16847651,
    "withdrawAccountId": 31285,
    "lastUpdateTime": "2018-09-06 05:46:20"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.cancel] -> 
{
  "id": "1583957553296.0829284693566987",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  }
}
[user.audit.about.current.cancel] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583957553348.2455878552160109506",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.cancel",
    "action": "null"
  },
  "id": "1583957553296.0829284693566987",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.cancel",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132313A7A7599B12A56655bf",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.no.get] -> 
{
  "id": "1583957553373.5375621779769452",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583957553373.5375621779769452",
  "result": "200311161233702068",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583957553462.9669321225409815",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200311161233702068",
    "amount": 293
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583957553515.6340434457614352206",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583957553462.9669321225409815",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 293,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200311161233702068",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132313A7A7599B12A56655bf",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.trace] -> 
{
  "id": "1583957553547.9590259133253608",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  }
}
[user.audit.about.current.trace] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583957553600.297485549638863492",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.trace",
    "action": "null"
  },
  "id": "1583957553547.9590259133253608",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.trace",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae000101132313A7A7599B12A56655bf",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583957553626.6242297420696600",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583957553626.6242297420696600",
  "result": "200311161233749778",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583957553707.9423440891391061",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "123456789",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200311161233749778",
    "amount": 330
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583957553757.2905121961210283008",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583957553707.9423440891391061",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 330,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200311161233749778",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583957553784.9866107447198280",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583957553784.9866107447198280",
  "result": "200311161233853144",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583957553868.3965493495347793",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf",
    "payPasword": "123456789",
    "withdrawNo": "200311161233853144",
    "amount": 278
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583957553947.9183957619906166565",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583957553868.3965493495347793",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 278,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200311161233853144",
      "payPasword": "123456789",
      "sessionId": "ae000101132313A7A7599B12A56655bf",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 40,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的充值流水号
[user.withdraw.no.get] -> 
{
  "id": "1583957553984.1620539777885375",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583957553984.1620539777885375",
  "result": "200311161234162300",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583957554065.5661439143558102",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "123456789",
    "amount": 170
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583957554118.7492150365292313470",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583957554065.5661439143558102",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 170,
      "reqIp": "202.11.82.1",
      "withdrawNo": "123456789",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132313A7A7599B12A56655bf",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的取款金额
[user.withdraw.no.get] -> 
{
  "id": "1583957554144.6507981207412690",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583957554144.6507981207412690",
  "result": "200311161234268389",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583957554224.4856508205699313",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200311161234268389",
    "amount": 240000
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583957554276.36398302461166096",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583957554224.4856508205699313",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200311161234268389",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae000101132313A7A7599B12A56655bf",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583957554348.3420610870758049",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132313A7A7599B12A56655bf"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583957554348.3420610870758049",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.233301272s
some thresholds have failed
```

