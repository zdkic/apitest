
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583698373709.9779101174027566",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "Geo23tLGe51r2gx5mRHvl/LWco0=",
    "salt": "upiaoz4zzg6f1z8z9mq75t6yvra7w1wx",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583698373709.9779101174027566",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-08 16:12:53",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9782,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "expiry": 1583701973819,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698373866.0629863510296642",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 1,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698373866.0629863510296642",
  "result": {
    "amount": 100,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 133,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698373995.9458248320752381",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 2,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698373995.9458248320752381",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698374079.2677148617853072",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 3,
    "amount": 100
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698374079.2677148617853072",
  "result": {
    "amount": 100,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 100,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 200
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698374175.8156397779397075",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 1,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698374175.8156397779397075",
  "result": {
    "amount": 300,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 333,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698374262.8409181939674824",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 2,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698374262.8409181939674824",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698374348.5019922572739410",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 3,
    "amount": 300
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698374348.5019922572739410",
  "result": {
    "amount": 300,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 300,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 600
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698374433.9442209955568108",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 1,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698374433.9442209955568108",
  "result": {
    "amount": 165,
    "chargeCoupon": 13,
    "couponAuditBetAmount": 198,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698374511.4553118721745491",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 2,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698374511.4553118721745491",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.charge.option.get] -> 
{
  "id": "1583698374607.6394015579973472",
  "jsonrpc": "2.0",
  "method": "user.charge.option.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8",
    "chargeType": 3,
    "amount": 165
  }
}
[user.charge.option.get] <- 
{
  "id": "1583698374607.6394015579973472",
  "result": {
    "amount": 165,
    "chargeCoupon": 0,
    "couponAuditBetAmount": 165,
    "isAudit": 1,
    "hasCommision": 1,
    "auditBetAmount": 330
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583698374760.7550509976259060",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323171E1AB85F11DAEAb0b8"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583698374760.7550509976259060",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=825.152659ms
```

