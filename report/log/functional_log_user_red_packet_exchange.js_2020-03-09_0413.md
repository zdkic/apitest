
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583698413271.6468628738466246",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "WYrwG1oi7u2DtNYF1TZVvJMdrc4=",
    "salt": "0wk4fxvuca3e29atj3dx3rs7lwm0fvus",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583698413271.6468628738466246",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-08 16:13:33",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9784,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231538CDE3F411DAF2de95",
    "expiry": 1583702013360,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.balance.get] -> 
{
  "id": "1583698413413.1748907456944045",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F411DAF2de95"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583698413413.1748907456944045",
  "result": {
    "balance": 505372.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583705613360,
    "sessionId": "ae00010113231538CDE3F411DAF2de95",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.red.packet.exchange] -> 
{
  "id": "1583698413553.7451139769516667",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F411DAF2de95",
    "amount": 46
  }
}
[user.red.packet.exchange] <- 
{
  "id": "1583698413553.7451139769516667",
  "result": {
    "success": "1",
    "cashflowId": 686426427952992300,
    "balance": 505418.2
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試提款後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583698413674.5056638242730597",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231538CDE3F411DAF2de95"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583698413674.5056638242730597",
  "result": {
    "balance": 505418.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583705613360,
    "sessionId": "ae00010113231538CDE3F411DAF2de95",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 46
diff = 46
測試錯誤的会话ID
[user.red.packet.exchange] -> 
{
  "id": "1583698413759.0545978375615838",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "123456789",
    "amount": 46
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583698413809.5185881647926476800",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583698413759.0545978375615838",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": 46,
      "reqIp": "202.11.82.1",
      "sessionId": "123456789",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的资金存款申请金额
[user.red.packet.exchange] -> 
{
  "id": "1583698413837.7392245283401143",
  "jsonrpc": "2.0",
  "method": "user.red.packet.exchange",
  "params": {
    "sessionId": "ae00010113231538CDE3F411DAF2de95",
    "amount": -100
  }
}
[user.red.packet.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2417",
    "sn": "1583698413893.163506759280118297",
    "message": "存入金额需要为正数",
    "reason": "sn:ae00, uid:16847651, amount:-100.0",
    "action": "null"
  },
  "id": "1583698413837.7392245283401143",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.red.packet.exchange",
    "params": {
      "amount": -100,
      "reqIp": "202.11.82.1",
      "sessionId": "ae00010113231538CDE3F411DAF2de95",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583698413983.5054337948955208",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231538CDE3F411DAF2de95"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583698413983.5054337948955208",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=508.403435ms
```

