
```bash
loginId = bgqajohnload1
[auth.mobile.login] -> 
{
  "id": "1574107815361.2156226963074918",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "YTmYFmhJSAwsK+WTH1QQAn3PV2I=",
    "salt": "6pd3p3k47bmcenf0w3r15efmev67t1m6",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574107815361.2156226963074918",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2019-11-18 16:10:15",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847647/",
      "userId": 16847647,
      "loginCount": 25625,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:48",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2030,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F19065DB2BA54D3CB6cc8",
    "expiry": 1574111415463,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
測試正常的流程
[currency.list] -> 
{
  "id": "1574107815473.9633831925267333",
  "method": "currency.list",
  "params": {
    "sn": "ae00"
  },
  "jsonrpc": "2.0"
}
[currency.list] <- 
{
  "id": "1574107815473.9633831925267333",
  "result": [
    {
      "currencyName": "人民币",
      "rate": 1,
      "currencySymbol": null,
      "sn": "BG00",
      "currencyId": 1,
      "currencyCountry": null,
      "currencyCode": "CNY",
      "status": 1
    }
  ],
  "error": null,
  "jsonrpc": "2.0"
}
currency.sn = BG00
測試获取单个币种
[currency.get] -> 
{
  "id": "1574107815583.1351146085485170",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": 1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1574107815583.1351146085485170",
  "result": {
    "currencyName": "人民币",
    "rate": 1,
    "currencySymbol": "￥",
    "sn": "BG00",
    "currencyId": 1,
    "currencyCountry": null,
    "currencyCode": "CNY",
    "status": 1
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的币种ID
[currency.get] -> 
{
  "id": "1574107815669.4506226603745739",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": -1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1574107815669.4506226603745739",
  "result": {},
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
[auth.online.logout] -> 
{
  "id": "1574107815828.0045518804489158",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F19065DB2BA54D3CB6cc8"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574107815828.0045518804489158",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=275.036825ms
```

