
```bash
loginId = bgqajohnload1
[auth.mobile.login] -> 
{
  "id": "1574712614921.5563431459681536",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "KpIcrXY3x6t5F8t9Q+eJaMRFiqA=",
    "salt": "h4mfecef4zdk5l941ss1x01oajpuikvn",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574712614921.5563431459681536",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2019-11-25 16:10:15",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847647/",
      "userId": 16847647,
      "loginCount": 25646,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:48",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2030,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F104623AFAC56AC4Baba0",
    "expiry": 1574716215010,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
測試正常的流程
[currency.list] -> 
{
  "id": "1574712615040.0026209862592109",
  "method": "currency.list",
  "params": {
    "sn": "ae00"
  },
  "jsonrpc": "2.0"
}
[currency.list] <- 
{
  "id": "1574712615040.0026209862592109",
  "result": [
    {
      "currencyName": "人民币",
      "rate": 1,
      "currencySymbol": null,
      "sn": "BG00",
      "currencyId": 1,
      "currencyCountry": null,
      "currencyCode": "CNY",
      "status": 1
    }
  ],
  "error": null,
  "jsonrpc": "2.0"
}
currency.sn = BG00
測試获取单个币种
[currency.get] -> 
{
  "id": "1574712615149.0200624153136879",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": 1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1574712615149.0200624153136879",
  "result": {
    "currencyName": "人民币",
    "rate": 1,
    "currencySymbol": "￥",
    "sn": "BG00",
    "currencyId": 1,
    "currencyCountry": null,
    "currencyCode": "CNY",
    "status": 1
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的币种ID
[currency.get] -> 
{
  "id": "1574712615226.8171844085310359",
  "method": "currency.get",
  "params": {
    "sn": "ae00",
    "currencyId": -1
  },
  "jsonrpc": "2.0"
}
[currency.get] <- 
{
  "id": "1574712615226.8171844085310359",
  "result": {},
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload1
[auth.online.logout] -> 
{
  "id": "1574712615338.7026643149251480",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F104623AFAC56AC4Baba0"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574712615338.7026643149251480",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=267.117356ms
```

