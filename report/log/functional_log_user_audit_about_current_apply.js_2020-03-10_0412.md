
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583784752006.9449359726442598",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "ct1zfE0CDKXV44lpBBJOuq9Jwuc=",
    "salt": "l69766fjvtpvlyg1es1pzt3k8gd7tqfj",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583784752006.9449359726442598",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-09 16:12:32",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9806,
      "regIp": "202.11.82.1",
      "parentId": 14679026,
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323193CC80AA3121E667d1e",
    "expiry": 1583788352100,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
測試正常的流程
[user.withdraw.option.get] -> 
{
  "id": "1583784752134.4080861349793164",
  "jsonrpc": "2.0",
  "method": "user.withdraw.option.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e",
    "terminal": 8
  }
}
[user.withdraw.option.get] <- 
{
  "id": "1583784752134.4080861349793164",
  "result": {
    "feeAmount": 0,
    "minAmount": 2,
    "amount": 0,
    "availableAmount": 505418.2,
    "balance": {
      "balance": 505418.2
    },
    "sn": "ae00",
    "nonFeeTimes": 0,
    "maxAmount": 2000000,
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.withdraw.account.default.get] -> 
{
  "id": "1583784752260.2922077306927348",
  "jsonrpc": "2.0",
  "method": "user.withdraw.account.default.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e",
    "terminal": 8
  }
}
[user.withdraw.account.default.get] <- 
{
  "id": "1583784752260.2922077306927348",
  "result": {
    "bankAccount": "123456789012",
    "accountStatus": 1,
    "bankAccountOwner": "user",
    "isDefault": 1,
    "bankId": 1,
    "bankBranch": "1111",
    "memo": null,
    "bankName": "工商银行",
    "sn": "ae00",
    "userId": 16847651,
    "withdrawAccountId": 31285,
    "lastUpdateTime": "2018-09-06 05:46:20"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.cancel] -> 
{
  "id": "1583784752358.0864450345921507",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.cancel",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  }
}
[user.audit.about.current.cancel] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583784752409.4875237778915328",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.cancel",
    "action": "null"
  },
  "id": "1583784752358.0864450345921507",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.cancel",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011323193CC80AA3121E667d1e",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.withdraw.no.get] -> 
{
  "id": "1583784752436.4006864134929099",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583784752436.4006864134929099",
  "result": "200309161232949904",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583784752533.7373561179359491",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200309161232949904",
    "amount": 448
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583784752585.220676562199511072",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583784752533.7373561179359491",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 448,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200309161232949904",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323193CC80AA3121E667d1e",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[user.audit.about.current.trace] -> 
{
  "id": "1583784752612.5057485920885254",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.trace",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  }
}
[user.audit.about.current.trace] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583784752663.6299928934520698816",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.trace",
    "action": "null"
  },
  "id": "1583784752612.5057485920885254",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.trace",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "ae0001011323193CC80AA3121E667d1e",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的会话ID
[user.withdraw.no.get] -> 
{
  "id": "1583784752690.5053942415416346",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583784752690.5053942415416346",
  "result": "200309161232375953",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583784752800.5357512438652757",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "123456789",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200309161232375953",
    "amount": 131
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583784752850.8056939710678760428",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583784752800.5357512438652757",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 131,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200309161232375953",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "123456789"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的取款密码
[user.withdraw.no.get] -> 
{
  "id": "1583784752879.8866649820381713",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583784752879.8866649820381713",
  "result": "200309161232935316",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583784752964.0328119995816127",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e",
    "payPasword": "123456789",
    "withdrawNo": "200309161232935316",
    "amount": 305
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583784753019.8385671547098281852",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583784752964.0328119995816127",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 305,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200309161232935316",
      "payPasword": "123456789",
      "sessionId": "ae0001011323193CC80AA3121E667d1e",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的充值流水号
[user.withdraw.no.get] -> 
{
  "id": "1583784753048.3430444478173494",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583784753048.3430444478173494",
  "result": "200309161233671770",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583784753135.6562242330153791",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "123456789",
    "amount": 296
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583784753186.6230588638158109657",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583784753135.6562242330153791",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 296,
      "reqIp": "202.11.82.1",
      "withdrawNo": "123456789",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323193CC80AA3121E667d1e",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的取款金额
[user.withdraw.no.get] -> 
{
  "id": "1583784753214.7844924523284574",
  "jsonrpc": "2.0",
  "method": "user.withdraw.no.get",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  }
}
[user.withdraw.no.get] <- 
{
  "id": "1583784753214.7844924523284574",
  "result": "200309161233772149",
  "error": null,
  "jsonrpc": "2.0"
}
[user.audit.about.current.apply] -> 
{
  "id": "1583784753307.0786999335673804",
  "jsonrpc": "2.0",
  "method": "user.audit.about.current.apply",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e",
    "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
    "withdrawNo": "200309161233772149",
    "amount": 240000
  }
}
[user.audit.about.current.apply] <- 
{
  "result": "0",
  "error": {
    "code": "502",
    "sn": "1583784753366.216746731244093624",
    "message": "Method not found! ",
    "reason": "MNF:user.audit.about.current.apply",
    "action": "null"
  },
  "id": "1583784753307.0786999335673804",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "user.audit.about.current.apply",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "withdrawNo": "200309161233772149",
      "payPasword": "H88gooEjJRJ8TXyxSix3fQ9ZY9M=",
      "sessionId": "ae0001011323193CC80AA3121E667d1e",
      "sn": "ae00"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 4,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583784753488.3602572398670044",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323193CC80AA3121E667d1e"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583784753488.3602572398670044",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=1.260396391s
some thresholds have failed
```

