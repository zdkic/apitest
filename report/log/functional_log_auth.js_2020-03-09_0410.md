
```bash
[auth.mobile.login]測試正常流程
[auth.mobile.login] -> 
{
  "id": "1583698220234.5814206661657515",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "9EPImTQHxnCIcHB/0MTt6qlR7r8=",
    "salt": "ny22kri9y3214w5yo4ujlydmf3j372i7",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583698220234.5814206661657515",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2020-03-08 16:10:20",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847647/",
      "userId": 16847647,
      "loginCount": 27422,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:48",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 413,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F19065DB2BA11DACCae95",
    "expiry": 1583701820352,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.mobile.login]測試錯誤帳密
[auth.mobile.login] -> 
{
  "id": "1583698220398.0621863835747004",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "bM0dGvQiYSfJ/l7exVNv/sb2Pck=",
    "salt": "k54gqzpqu20xck34qf7z5c8q807s4xo4",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2218",
    "sn": "1583698220459.1152928101713119273",
    "message": "用户名或密码错误,登录失败.",
    "reason": "loginId:bgqajohnload1, password:bM0dGvQiYSfJ/l7exVNv/sb2Pck=, from sn:ae00",
    "action": "null"
  },
  "id": "1583698220398.0621863835747004",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "bM0dGvQiYSfJ/l7exVNv/sb2Pck=",
      "captchaCode": "6711",
      "loginId": "bgqajohnload1",
      "salt": "k54gqzpqu20xck34qf7z5c8q807s4xo4",
      "reqIp": "202.11.82.1",
      "captchaKey": "3094255924",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]測試正常流程
[auth.session.validate] -> 
{
  "id": "1583698220500.9733514341472463",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "ae000101131F19065DB2BA11DACCae95",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "id": "1583698220500.9733514341472463",
  "result": {
    "flag": "1",
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583705420352,
    "sessionId": "ae000101131F19065DB2BA11DACCae95",
    "userId": 16847647
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.session.validate]測試錯誤sessionId
[auth.session.validate] -> 
{
  "id": "1583698220596.0089774420290948",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "12345678",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583698220647.5183254491519974976",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1583698220596.0089774420290948",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "0",
      "sessionId": "12345678",
      "opIp": "202.11.82.1",
      "withUser": "0"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.online.logout]測試正常流程
[auth.online.logout] -> 
{
  "id": "1583698220674.0337234848104083",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F19065DB2BA11DACCae95"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583698220674.0337234848104083",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
[auth.online.logout]測試錯誤sessionId
[auth.online.logout] -> 
{
  "id": "1583698220812.0695884553115067",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "12345678"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583698220862.8059189184291405744",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1583698220812.0695884553115067",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.online.logout",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "12345678",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
Test finished i=1 t=662.54327ms
```

