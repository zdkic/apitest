
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583957513278.1765340722281695",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "iTOMVk1fmrJ+bHvj6/Md50hvyjc=",
    "salt": "8l24puagep6weim9cw3w5vr2nntr3cuv",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583957513278.1765340722281695",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-11 16:11:53",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9854,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "qq": "",
      "passportNumber": null,
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "expiry": 1583961113366,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1583957513404.1300340682231891",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583957513404.1300340682231891",
  "result": {
    "balance": 505470.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583964713366,
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583957513506.0332574371218258",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "from": 0,
    "to": 4,
    "amount": 10
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583957513506.0332574371218258",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583957513593.8677835580019620",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583957513593.8677835580019620",
  "result": {
    "balance": 505470.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583964713366,
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 10
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1583957513675.1746177025248829",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1583957513675.1746177025248829",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583957513759.0483263678413138",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 10
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583957513811.8923596771244883948",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583957513759.0483263678413138",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 10,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583957513841.3311271161213974",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583957513841.3311271161213974",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583957513931.6385956841504453",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "from": -1,
    "to": 5,
    "amount": 10
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1583957513986.437834876569798208",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:10.0",
    "action": "null"
  },
  "id": "1583957513931.6385956841504453",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 10,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 6,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583957514018.7134925216577607",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3",
    "from": 0,
    "to": -5,
    "amount": 10
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583957514018.7134925216577607",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583957514187.7208709927099016",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae0001011323171E1AB85F12A55Ea3f3"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583957514187.7208709927099016",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=696.510117ms
some thresholds have failed
```

