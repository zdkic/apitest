
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1583439114284.2974354181138442",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "o+mapCQAfj3nscH1FlX98ivK900=",
    "salt": "ve2ny78jrhlahyutwfj1e1oi87kfdcoc",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583439114284.2974354181138442",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2020-03-05 16:11:54",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 9704,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 2495,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "expiry": 1583442714401,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1583439114446.6090753488229776",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583439114446.6090753488229776",
  "result": {
    "balance": 505303.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583446314401,
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583439114565.3950321437199029",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "from": 0,
    "to": 4,
    "amount": 29
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583439114565.3950321437199029",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1583439114649.8081670530423084",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1583439114649.8081670530423084",
  "result": {
    "balance": 505303.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583446314401,
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 29
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1583439114731.0813172485884096",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1583439114731.0813172485884096",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583439114806.6488954527908609",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 29
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583439114861.8935069907653820392",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1583439114806.6488954527908609",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 29,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583439114887.0267715187958452",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583439114887.0267715187958452",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583439114970.3724399260648173",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "from": -1,
    "to": 5,
    "amount": 29
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1583439115028.36064603158237068",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:29.0",
    "action": "null"
  },
  "id": "1583439114970.3724399260648173",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 29,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae000101132316F3E281C811105Eda78",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1583439115056.0028337088644583",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78",
    "from": 0,
    "to": -5,
    "amount": 29
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "id": "1583439115056.0028337088644583",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1583439115197.9058100091508479",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101132316F3E281C811105Eda78"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583439115197.9058100091508479",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=687.758065ms
some thresholds have failed
```

