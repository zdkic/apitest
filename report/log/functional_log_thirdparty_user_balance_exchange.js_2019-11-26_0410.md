
```bash
loginId = bgqajohnload5
[auth.mobile.login] -> 
{
  "id": "1574712640904.2521336833321162",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload5",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "JV94Smm1TrODAOkWXO/xMD7irF0=",
    "salt": "492j9ftvn8e5zsh10cqgbv3w4jqu2ptl",
    "domain": "ae.bg1207.com"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1574712640904.2521336833321162",
  "result": {
    "loginId": "bgqajohnload5",
    "auth": {
      "loginId": "bgqajohnload5",
      "loginLastUpdateTime": "2019-11-25 16:10:40",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-13 02:43:00",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847651/",
      "userId": 16847651,
      "loginCount": 5600,
      "parentId": 14679026,
      "regIp": "202.11.82.1",
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:49",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-13 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 0,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847651,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:49",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "expiry": 1574716240996,
    "userId": 16847651,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
loginId = bgqajohnload5
[user.balance.get] -> 
{
  "id": "1574712641020.5679874909396669",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574712641020.5679874909396669",
  "result": {
    "balance": 500352.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574719840996,
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試正常的流程
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574712641128.3777535237808103",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "from": 0,
    "to": 4,
    "amount": 42
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2436",
    "sn": "1574712641188.8546133617126916096",
    "message": "未知第三方平台异常",
    "reason": "sn:ae00, uid:16847651, to:4, amount:42.0",
    "action": "null"
  },
  "id": "1574712641128.3777535237808103",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 42,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "ae00010113231223A719CE56AC50f094",
      "to": 4,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 8,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試額度轉換後的帳戶餘額
[user.balance.get] -> 
{
  "id": "1574712641210.1074969620798619",
  "method": "user.balance.get",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094"
  },
  "jsonrpc": "2.0"
}
[user.balance.get] <- 
{
  "id": "1574712641210.1074969620798619",
  "result": {
    "balance": 500352.2,
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1574719840996,
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "userId": 16847651
  },
  "error": null,
  "jsonrpc": "2.0"
}
amount = 42
diff = 0
測試刷新额度接口
[thirdparty.user.balance.refresh] -> 
{
  "id": "1574712641289.4300599019879845",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.refresh",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "thirdpartyId": "4",
    "terminal": 1
  }
}
[thirdparty.user.balance.refresh] <- 
{
  "id": "1574712641289.4300599019879845",
  "result": {
    "flag": "0",
    "balance": 0
  },
  "error": null,
  "jsonrpc": "2.0"
}
測試錯誤的会话ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574712641374.1220408446159658",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "123456789",
    "from": 0,
    "to": 5,
    "amount": 42
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1574712641431.9052217624157995000",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[123456789] with error length",
    "action": "null"
  },
  "id": "1574712641374.1220408446159658",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 42,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "123456789",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試超過的額度轉換
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574712641455.0920164816360909",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "from": 0,
    "to": 5,
    "amount": 240000
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2436",
    "sn": "1574712641526.227440614891603584",
    "message": "未知第三方平台异常",
    "reason": "sn:ae00, uid:16847651, to:5, amount:240000.0",
    "action": "null"
  },
  "id": "1574712641455.0920164816360909",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 240000,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "ae00010113231223A719CE56AC50f094",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 13,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的来源平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574712641551.4212480476504080",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "from": -1,
    "to": 5,
    "amount": 42
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2435",
    "sn": "1574712641609.6809438236096331600",
    "message": "额度转换出现异常",
    "reason": "sn:ae00, uid:16847651, from:-1, to:5, amount:42.0",
    "action": "null"
  },
  "id": "1574712641551.4212480476504080",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 42,
      "reqIp": "202.11.82.1",
      "from": -1,
      "sessionId": "ae00010113231223A719CE56AC50f094",
      "to": 5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 5,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
測試錯誤的目标平台ID
[thirdparty.user.balance.exchange] -> 
{
  "id": "1574712641634.5525944702732977",
  "jsonrpc": "2.0",
  "method": "thirdparty.user.balance.exchange",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094",
    "from": 0,
    "to": -5,
    "amount": 42
  }
}
[thirdparty.user.balance.exchange] <- 
{
  "result": "0",
  "error": {
    "code": "2436",
    "sn": "1574712641694.8356392720389748734",
    "message": "未知第三方平台异常",
    "reason": "sn:ae00, uid:16847651, to:-5, amount:42.0",
    "action": "null"
  },
  "id": "1574712641634.5525944702732977",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "amount": 42,
      "reqIp": "202.11.82.1",
      "from": 0,
      "sessionId": "ae00010113231223A719CE56AC50f094",
      "to": -5,
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 7,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
loginId = bgqajohnload5
[auth.online.logout] -> 
{
  "id": "1574712641779.6514899387600310",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae00010113231223A719CE56AC50f094"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1574712641779.6514899387600310",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
Test finished i=1 t=697.819467ms
some thresholds have failed
```

