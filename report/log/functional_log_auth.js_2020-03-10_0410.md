
```bash
[auth.mobile.login]測試正常流程
[auth.mobile.login] -> 
{
  "id": "1583784619407.4370660246593358",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "uN/eWj38aZSMHMvgE/8WIV5x6zw=",
    "salt": "34vlwxhji5yplpju7yiuv2ddofyhkqge",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "id": "1583784619407.4370660246593358",
  "result": {
    "loginId": "bgqajohnload1",
    "auth": {
      "loginId": "bgqajohnload1",
      "loginLastUpdateTime": "2020-03-09 16:10:19",
      "level": null,
      "parentPath": "/14679026",
      "passwordLastUpdateTime": "2018-11-14 02:08:34",
      "isOnline": null,
      "regCode": "badzNl",
      "parentPathIncSelf": "/14679026/16847647/",
      "userId": 16847647,
      "loginCount": 27443,
      "regIp": "202.11.82.1",
      "parentId": 14679026,
      "unreadNotice": 0,
      "regFrom": 8,
      "regTime": "2018-09-06 05:41:48",
      "password": null,
      "balance": null,
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "realSn": null,
      "regType": "n",
      "regChannel": "ae.bg1207.com",
      "status": 1
    },
    "profile": {
      "birthday": "2018-11-14 00:00:00",
      "certType": null,
      "userStatus": 1,
      "gender": null,
      "awardPoint": 413,
      "memo": null,
      "remark": null,
      "idNumber": null,
      "userImage": null,
      "balance": null,
      "nickname": null,
      "tel": null,
      "currency": "1",
      "sn": "ae00",
      "wechatName": null,
      "regType": "n",
      "email": "7/yEMprqVoBn6BNb67KIRsQ==",
      "passportNumber": null,
      "qq": "",
      "alipay": null,
      "address": null,
      "mobile": "",
      "wechat": "",
      "alipayName": null,
      "userId": 16847647,
      "recommendUserId": 12185640,
      "certNumber": null,
      "regTime": "2018-09-06 05:41:48",
      "name": "7L9kJ1+V9qTjZLIaebf3fZg==",
      "payPassword": null,
      "loginMobile": ""
    },
    "sessionType": 1,
    "sn": "ae00",
    "sessionId": "ae000101131F12E464285F121E4Bc4f4",
    "expiry": 1583788219531,
    "userId": 16847647,
    "regType": "n"
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.mobile.login]測試錯誤帳密
[auth.mobile.login] -> 
{
  "id": "1583784619578.4723857571556592",
  "method": "auth.mobile.login",
  "params": {
    "sn": "ae00",
    "loginId": "bgqajohnload1",
    "withAuth": "1",
    "withProfile": "1",
    "saltedPassword": "VCIq7+4BQWmJ7pgbt7sgPzfNNmg=",
    "salt": "tndofmq9frlp1yvuk25ygsyiqp83zvqe",
    "domain": "ae.bg1207.com",
    "captchaKey": "3094255924",
    "captchaCode": "6711"
  },
  "jsonrpc": "2.0"
}
[auth.mobile.login] <- 
{
  "result": "0",
  "error": {
    "code": "2218",
    "sn": "1583784619652.9119694446897577728",
    "message": "用户名或密码错误,登录失败.",
    "reason": "loginId:bgqajohnload1, password:VCIq7+4BQWmJ7pgbt7sgPzfNNmg=, from sn:ae00",
    "action": "null"
  },
  "id": "1583784619578.4723857571556592",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.mobile.login",
    "params": {
      "withProfile": "1",
      "saltedPassword": "VCIq7+4BQWmJ7pgbt7sgPzfNNmg=",
      "captchaCode": "6711",
      "loginId": "bgqajohnload1",
      "salt": "tndofmq9frlp1yvuk25ygsyiqp83zvqe",
      "reqIp": "202.11.82.1",
      "captchaKey": "3094255924",
      "domain": "ae.bg1207.com",
      "loginIp": "202.11.82.1",
      "sn": "ae00",
      "withAuth": "1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 22,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.session.validate]測試正常流程
[auth.session.validate] -> 
{
  "id": "1583784619680.1276582656158543",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "ae000101131F12E464285F121E4Bc4f4",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "id": "1583784619680.1276582656158543",
  "result": {
    "flag": "1",
    "sessionType": 1,
    "sn": "ae00",
    "expiry": 1583791819531,
    "sessionId": "ae000101131F12E464285F121E4Bc4f4",
    "userId": 16847647
  },
  "error": null,
  "jsonrpc": "2.0"
}
[auth.session.validate]測試錯誤sessionId
[auth.session.validate] -> 
{
  "id": "1583784619757.3261429259789887",
  "method": "auth.session.validate",
  "params": {
    "sessionId": "12345678",
    "withAmount": "0",
    "withUser": "0"
  },
  "jsonrpc": "2.0"
}
[auth.session.validate] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583784619812.2362139184096889920",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1583784619757.3261429259789887",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.session.validate",
    "params": {
      "reqIp": "202.11.82.1",
      "withAmount": "0",
      "sessionId": "12345678",
      "opIp": "202.11.82.1",
      "withUser": "0"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 2,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
[auth.online.logout]測試正常流程
[auth.online.logout] -> 
{
  "id": "1583784619839.8628516892566687",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "ae000101131F12E464285F121E4Bc4f4"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "id": "1583784619839.8628516892566687",
  "result": "1",
  "error": null,
  "jsonrpc": "2.0"
}
[auth.online.logout]測試錯誤sessionId
[auth.online.logout] -> 
{
  "id": "1583784619937.9039280412393724",
  "method": "auth.online.logout",
  "params": {
    "sessionId": "12345678"
  },
  "jsonrpc": "2.0"
}
[auth.online.logout] <- 
{
  "result": "0",
  "error": {
    "code": "2202",
    "sn": "1583784619992.8933839764809694200",
    "message": "登录会话已失效，请重新登录.",
    "reason": "sessionId[12345678] with error length",
    "action": "null"
  },
  "id": "1583784619937.9039280412393724",
  "jsonrpc": "2.0",
  "request": {
    "cxt": "/cloud",
    "method": "auth.online.logout",
    "params": {
      "reqIp": "202.11.82.1",
      "sessionId": "12345678",
      "opIp": "202.11.82.1"
    },
    "uri": null,
    "redirectUri": null,
    "elapsed": 3,
    "from": "202.11.82.1",
    "server": "10.153.1.101",
    "result": null
  }
}
Test finished i=1 t=620.236066ms
```

