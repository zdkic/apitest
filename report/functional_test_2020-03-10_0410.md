
### Environment ###

Host | sn 
--- | --- 
http://ae.bg1207.com | ae00 
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | --- 
10.37.3.28 | Ubuntu 18.04.3 LTS | 2020/03/10 04:10:01
### Testing Result ###
- 通過冒煙測試
- 測試結果

     <img src="../report/images/img_hEh6c1Pc_2020-03-10_0410.png"  width="400" height="300">
- **auth.js**
  - `鉴权`
  - 接口功能

     ```bash
    ✓ [auth.session.validate]測試沒有error
    ✓ [auth.session.validate]測試userId
    ✓ [auth.session.validate]測試錯誤sessionId應該有error
    ✓ [auth.online.logout]測試錯誤sessionId應該有error
    ✓ status is 200
    ✓ [auth.mobile.login]測試沒有error
    ✓ [auth.mobile.login]測試有sessionId
    ✓ [auth.online.logout]測試沒有error
    ✓ [auth.online.logout]測試result
    ✓ [auth.mobile.login]測試有loginId
    ✓ [[auth.mobile.login]]測試錯誤密碼應該有error
    ✓ [auth.session.validate]測試flag為1

     ```
  - Functional log [here](../report/log/functional_log_auth.js_2020-03-10_0410.md)
- **currency_list.js**
  - `币种`
  - 接口功能

     ```bash
    ✓ 檢察币种与RMB的汇率
    ✓ 檢查币种ID是否為空
    ✓ 查询币种列表
    ✓ 查询获取单个币种
    ✓ 檢察币种ID
    ✓ 檢察币种代码
    ✓ 檢察币种记录状态
    ✓ 檢查币种ID
    ✓ 符号
    ✓ 币种与RMB的汇率
    ✓ 檢察币种名称
    ✓ 币种记录状态
    ✓ 檢察币种符号
    ✓ status is 200
    ✓ 檢查厅主ID
    ✓ 檢查币种代码
    ✓ 币种名称

     ```
  - Functional log [here](../report/log/functional_log_currency_list.js_2020-03-10_0410.md)
- **layer_deposit_to_account_qrcode_get.js**
  - `指定分层用于接受入款的账号列表`
  - 接口功能

     ```bash
    ✓ 檢查QR code的長度
    ✓ 檢查QR code
    ✓ 測試錯誤的会话ID:回傳值有error物件
    ✓ 測試錯誤的会话ID
    ✓ 測試錯誤的帐号ID
    ✓ status is 200
    ✓ 指定分层用于接受入款的账号列表

     ```
  - Functional log [here](../report/log/functional_log_layer_deposit_to_account_qrcode_get.js_2020-03-10_0410.md)
- **qrcode.js**
  - `二维码`
  - 接口功能

     ```bash
    ✓ status is 200
    ✓ Content-Disposition
    ✓ Content-Type
    ✓ Response body length > 0

     ```
  - Functional log [here](../report/log/functional_log_qrcode.js_2020-03-10_0410.md)
- **sn_settings_get.js**
  - `获取厅主支付类型与出入款开关信息`
  - 接口功能

     ```bash
    ✓ status is 200
    ✓ 檢查响应参数

     ```
  - Functional log [here](../report/log/functional_log_sn_settings_get.js_2020-03-10_0411.md)
- **thirdparty_user_balance_exchange.js**
  - `额度转换`
  - 接口功能

     ```bash
    ✗ 測試額度轉換後的帳戶餘額
     ↳  0% — ✓ 0 / ✗ 1
    ✗ 檢查刷新额度接口响应
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的会话ID
    ✓ 測試錯誤的来源平台ID
    ✗ 測試錯誤的目标平台ID:回傳值有error物件
     ↳  0% — ✓ 0 / ✗ 1
    ✓ status is 200
    ✓ 檢查响应参数結果
    ✓ 測試錯誤的会话ID:回傳值有error物件
    ✗ 測試超過的額度轉換:回傳值有error物件
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的来源平台ID:回傳值有error物件
    ✓ 測試正常流程

     ```
  - Functional log [here](../report/log/functional_log_thirdparty_user_balance_exchange.js_2020-03-10_0411.md)
- **thirdparty_user_collect_balance_syn.js**
  - `一键回收第三方平台余额异步`
  - 接口功能

     ```bash
    ✓ 測試正常流程
    ✓ 測試一鍵回收
    ✓ 測試錯誤的会话ID:回傳值有error物件
    ✓ 測試錯誤的会话ID
    ✓ status is 200

     ```
  - Functional log [here](../report/log/functional_log_thirdparty_user_collect_balance_syn.js_2020-03-10_0411.md)
- **user_audit_about_current_apply.js**
  - `稽核预申请`
  - 接口功能

     ```bash
    ✗ 即时稽核状态查询
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的会话ID:回傳值有error物件
    ✓ 測試超過的取款金额:回傳值有error物件
    ✗ 稽核预申请
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的会话ID
    ✗ 稽核预申请(測試超過的取款金额)
     ↳  0% — ✓ 0 / ✗ 1
    ✗ 稽核申请取消
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 获取充值流水号
    ✓ status is 200
    ✓ 用户出款金额可选项(有效范围)查询
    ✓ 查询用户默认出款银行记录
    ✓ 測試錯誤的取款密码:回傳值有error物件
    ✗ 測試錯誤的取款密码
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的充值流水号:回傳值有error物件

     ```
  - Functional log [here](../report/log/functional_log_user_audit_about_current_apply.js_2020-03-10_0412.md)
- **user_charge_option_get.js**
  - `获取厅主支付类型与出入款开关信息`
  - 接口功能

     ```bash
    ✓ status is 200
    ✓ 檢查响应参数
    ✓ 檢查取款金额
    ✓ 檢查存款优惠金额
    ✓ 檢查综合稽核打码量(优惠稽核打码量)
    ✓ 檢查是否进行常态性稽核

     ```
  - Functional log [here](../report/log/functional_log_user_charge_option_get.js_2020-03-10_0412.md)
- **user_charge_transfer_apply.js**
  - `我要充值`
  - 接口功能

     ```bash
    ✓ 用户充值时支持的银行列表
    ✗ 測試錯誤的支付方式来源
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的会话ID:回傳值有error物件
    ✓ 測試錯誤的会话ID
    ✓ 測試錯誤的接收账户ID
    ✓ 測試錯誤的接收账户ID:回傳值有error物件
    ✓ status is 200
    ✓ 获取充值流水号
    ✓ 用户账户转账充值申请
    ✓ 測試存款申请单流水号已使用:回傳值有error物件
    ✗ 測試存款申请单流水号已使用
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的支付方式来源:回傳值有error物件

     ```
  - Functional log [here](../report/log/functional_log_user_charge_transfer_apply.js_2020-03-10_0412.md)
- **user_red_packet_exchange.js**
  - `BG红包充值兑换`
  - 接口功能

     ```bash
    ✓ 測試錯誤的会话ID
    ✓ 測試錯誤的资金存款申请金额:回傳值有error物件
    ✓ 測試錯誤的资金存款申请金额
    ✓ status is 200
    ✓ BG红包充值兑换(error==null)
    ✓ BG红包充值兑换(success==1)
    ✓ 測試提款後的帳戶餘額
    ✓ 測試錯誤的会话ID:回傳值有error物件

     ```
  - Functional log [here](../report/log/functional_log_user_red_packet_exchange.js_2020-03-10_0413.md)
- **user_withdraw_apply.js**
  - `我要提款`
  - 接口功能

     ```bash
    ✓ 測試過低的申请出款金额:回傳值有error物件
    ✗ 測試過低的申请出款金额
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試過高的申请出款金额:回傳值有error物件
    ✓ 測試錯誤的会话ID:回傳值有error物件
    ✓ 測試錯誤的会话ID
    ✓ 測試錯誤的取款密码:回傳值有error物件
    ✗ 測試提款後的帳戶餘額
     ↳  0% — ✓ 0 / ✗ 1
    ✗ 即时稽核查询
     ↳  0% — ✓ 0 / ✗ 1
    ✗ 測試過高的申请出款金额
     ↳  0% — ✓ 0 / ✗ 1
    ✓ 測試錯誤的取款密码
    ✓ status is 200
    ✗ 用户取款申请
     ↳  0% — ✓ 0 / ✗ 1

     ```
  - Functional log [here](../report/log/functional_log_user_withdraw_apply.js_2020-03-10_0413.md)
