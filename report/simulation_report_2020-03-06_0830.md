
### Environment ###

Host | sn 
--- | --- 
http://ae.bg1207.com | ae00 
### Testing Agent ###
 Ip | OS | Testing Date
--- | --- | --- 
10.37.3.28 | Ubuntu 18.04.3 LTS | 2020/03/06 08:30:02
### Parameter ###

Config | Users | MAX_VU 
--- | --- | --- 
load | 10 | 500
### Grafana ###

URL | Username | Password
--- | --- | ---
http://10.37.3.28:3000 | admin | admin123
### Testing Result ###
- Connection: http://ae.bg1207.com
    - Connected
- Testing Result

     <img src="../report/images/img_kDhgurVn_2020-03-06_0830.png"  width="400" height="300">
- simulation.js
  - 混合情境
  - k6 result

     ```bash
     ```
  - Virtual Users

     ![](../report/images/img_H0bt2pmC_2020-03-06_0831.png)
  - Errors Per Second

     ![](../report/images/img_PjKaOpLV_2020-03-06_0831.png)

  - Error log [here](../report/log/simulation_error_log_2020-03-06_0830.md)
  - Requests per Second

     ![](../report/images/img_HrVShhPt_2020-03-06_0831.png)
  - http_req_duration

     ![](../report/images/img_PyW27h4T_2020-03-06_0831.png)
  - grafana url
     - <http://10.37.3.28:3000/d/BSPN9Jhmk/k6-load-testing-results?orgId=1&from=1583454603000&to=1583454676000>
