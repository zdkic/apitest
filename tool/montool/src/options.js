'use strict'

function init() {
    const ds = new dataStore()
    document.getElementById('host').value = ds.getOption('host')
    document.getElementById('sn').value = ds.getOption('sn')
    document.getElementById('concurrency').value = ds.getOption('concurrency')
    document.getElementById('duration').value = ds.getOption('duration')
    document.getElementById('userid').value = ds.getOption('userid')
    document.getElementById('password').value = ds.getOption('password')
    document.getElementById('optionsForm').addEventListener('submit', (evt) => {
        evt.preventDefault()
        ds.setOption('host', document.getElementById("host").value)
        ds.setOption('sn', document.getElementById("sn").value)
        ds.setOption('concurrency', document.getElementById("concurrency").value)
        ds.setOption('duration', document.getElementById("duration").value)
        ds.setOption('userid', document.getElementById("userid").value)
        ds.setOption('password', document.getElementById("password").value)
        window.opener = self
        window.close()
    })
}

init()