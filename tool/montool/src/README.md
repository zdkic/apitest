# ServMon
BG service monitor

## Installation
```bash
npm install
```

## Run
```bash
npm start
```

## Build & Packaging
```bash
npm run dist
```