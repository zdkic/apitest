const {
    ipcMain
} = require('electron')

const {
    app,
    BrowserWindow,
    Tray,
    Menu
} = require('electron')

const path = require('path')
const url = require('url')
const ChildProcess = require('child_process')

let win = null

if (handleSquirrelEvent()) {
    return
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (win === null) {
        createWindow()
    }
})

function createWindow() {
    win = new BrowserWindow({
        width: 500,
        height: 400,
        maximizable: false
    })

    win.setMenu(null)

    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))

    win.on('closed', () => {
        win = null
    })

    win.on('restore', () => {
        console.log('restore')
    })

    if(process.platform === "win32") {
        // Create Tray
        createTray()
    }
}

function preventMultipleInstance() {
    const gotTheLock = app.requestSingleInstanceLock()

    if (!gotTheLock) {
        app.quit()
    } else {
        app.on('second-instance', (event, commandLine, workingDirectory) => {
            // Someone tried to run a second instance, we should focus our window.
            if (win) {
                if (win.isMinimized()) win.restore()
                win.focus()
            }
        })

        // Create win, load the rest of the app, etc...
        app.on('ready', () => {})
    }
}

if(process.platform === "win32") {
    preventMultipleInstance()
}

ipcMain.on('start_check', (event, arg) => {
    win.send('start_check', arg)
})

ipcMain.on('stop_check', () => {
    win.send('stop_check')
})

ipcMain.on('clear_error', () => {
    win.send('clear_error')
})

ipcMain.on('open_devtools', () => {
    //Open DevTools
    win.webContents.openDevTools({
        mode: 'detach'
    })
})

function createTray() {
    let appIcon = null
    const iconPath = path.join(__dirname, 'clock.ico')

    const contextMenu = Menu.buildFromTemplate([{
            label: 'ServMon',
            click() {
                win.show()
            }
        },
        {
            label: 'Quit',
            click() {
                win.removeAllListeners('close')
                win.close()
            }
        }
    ]);

    appIcon = new Tray(iconPath)
    appIcon.setToolTip('BG Service Monitor')
    appIcon.setContextMenu(contextMenu)

}

function handleSquirrelEvent() {
    if (process.argv.length === 1) {
        return false;
    }

    const appFolder = path.resolve(process.execPath, '..');
    const rootAtomFolder = path.resolve(appFolder, '..');
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
    const exeName = path.basename(process.execPath);

    const spawn = function (command, args) {
        let spawnedProcess;

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {
                detached: true
            });
        } catch (error) {}

        return spawnedProcess;
    };

    const spawnUpdate = function (args) {
        return spawn(updateDotExe, args);
    };

    const squirrelEvent = process.argv[1];
    switch (squirrelEvent) {
        case '--squirrel-install':
        case '--squirrel-updated':
            spawnUpdate(['--createShortcut', exeName]);
            setTimeout(app.quit, 1000);
            return true;

        case '--squirrel-uninstall':

            spawnUpdate(['--removeShortcut', exeName]);
            setTimeout(app.quit, 1000);
            return true;

        case '--squirrel-obsolete':
            app.quit();
            return true;
    }
}