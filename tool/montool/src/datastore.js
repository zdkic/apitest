'use strict'

const store = require('electron-store')

class dataStore extends store {
  constructor(settings) {
    super(settings)

    this.host = this.get('host') || "http://ae.bg1207.com"
    this.sn = this.get('sn') || "ae00"
    this.concurrency = this.get('concurrency') || 1
    this.duration = this.get('duration') || 60
    this.userid = this.get('userid') || "bgqajohn3"
    this.password = this.get('password') || "123456"
  }

  setOption(name, value) {
    this[name] = value
    this.set(name, value)
  }

  getOption(name) {
    return this[name] || ''
  }
}

module.exports = dataStore