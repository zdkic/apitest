const request = require('request')
const fs = require('fs')
const util = require('util')


let log_file = null

old_console_log = console.log
console.log = function() {
  if(!log_file) {
    log_file = fs.createWriteStream(app.getPath('userData') + '/debug.log', {flags : 'a'})
  }
  log_file.write(util.format.apply(null, arguments) + '\n')
  old_console_log(util.format.apply(null, arguments) + '\n')
}

let id_generator = () => {
  let id = Date.now()
  id += '.'
  let possible = "0123456789"
  for (let idx = 0; idx < 16; idx++) {
    id += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return id
}

let random_string_generator = () => {
  let str = ""
  const charset = "0123456789abcdefghijklmnopqrstuvwxyz"
  for (let a = 0; 32 > a; a++)
    str += charset.charAt(Math.floor(36 * Math.random()))
  return str
}

function SHA1() {
  function b(a, b) {
    a[b >> 5] |= 128 << 24 - b % 32
    a[(b + 64 >> 9 << 4) + 15] = b
    b = Array(80)
    for (var c = 1732584193, e = -271733879, g = -1732584194, k = 271733878, p = -1009589776, t = 0; t < a.length; t += 16) {
      for (var r = c, W = e, N = g, Ca = k, na = p, A = 0; 80 > A; A++) {
        if (16 > A) b[A] = a[t + A]
        else {
          var G = b[A - 3] ^ b[A - 8] ^ b[A - 14] ^ b[A - 16]
          b[A] = G << 1 | G >>> 31
        }
        var G = c << 5 | c >>> 27;
        var Q = 20 > A ? e & g | ~e & k : 40 > A ? e ^ g ^ k : 60 > A ? e & g | e & k | g & k : e ^ g ^ k
        G = d(d(G, Q), d(d(p, b[A]), 20 > A ? 1518500249 : 40 > A ? 1859775393 : 60 > A ? -1894007588 : -899497514))
        p = k
        k = g
        g = e << 30 | e >>> 2
        e = c
        c = G
      }
      c = d(c, r)
      e = d(e, W)
      g = d(g, N)
      k = d(k, Ca)
      p = d(p, na)
    }
    return [c, e, g, k, p]
  }

  function d(a, b) {
    var c = (a & 65535) + (b & 65535)
    return (a >> 16) + (b >> 16) + (c >> 16) << 16 | c & 65535
  }

  function c(a) {
    for (var b = [], c = (1 << r) - 1, d = 0; d < a.length * r; d += r) b[d >> 5] |= (a.charCodeAt(d / r) & c) << 24 - d % 32
    return b
  }
  this.hash = function (d) {
    d = b(c(d), d.length * r)
    for (var e = a ? "0123456789ABCDEF" : "0123456789abcdef", m = "", h = 0; h < 4 * d.length; h++) m += e.charAt(d[h >> 2] >> 8 * (3 - h % 4) + 4 & 15) + e.charAt(d[h >> 2] >> 8 * (3 - h % 4) & 15)
    return m
  };
  this.hashToBase64 = function (a) {
    a = b(c(a), a.length * r)
    for (var d =
        "", m = 0; m < 4 * a.length; m += 3)
      for (var h = (a[m >> 2] >> 8 * (3 - m % 4) & 255) << 16 | (a[m + 1 >> 2] >> 8 * (3 - (m + 1) % 4) & 255) << 8 | a[m + 2 >> 2] >> 8 * (3 - (m + 2) % 4) & 255, l = 0; 4 > l; l++) d = 8 * m + 6 * l > 32 * a.length ? d + e : d + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(h >> 6 * (3 - l) & 63)
    return d + "="
  };
  var a = 0,
    e = "",
    r = 8
}

function param(name, type, length, nacessary, desc) {
  this.name = name
  this.type = type
  this.length = length
  this.nacessary = nacessary
  this.desc = desc
}

function printRequest(json, desc)
{
  console.log("[%s]接口描述:%s(%s)", json.method, desc.method, desc.url)
  console.log("[%s]请求参数:", json.method)
  if (desc && desc.params) {
    let d = {}
    for (const [k, v] of Object.entries(desc.params)) {
      d[k] = new param(k, desc.params[k][0],desc.params[k][1],desc.params[k][2],desc.params[k][3])
    }
    console.table(d)
  }
}

function printResonse(json, desc) {
  console.log("[%s]响应参数:", json.method)
  if (desc && desc.response) {
    let d = {}
    for (const [k, v] of Object.entries(desc.response)) {
      d[k] = new param(k, desc.response[k][0],desc.response[k][1],desc.response[k][2],desc.response[k][3])
    }
    console.table(d)
  }
}

function sendRequest(json, args = {}, callback, desc) {
  printRequest(json, desc)
  json.id = id_generator()
  for (const [k, v] of Object.entries(args)) {
    if (json.params.hasOwnProperty(k))
      json.params[k] = v
  }
  let body = JSON.stringify(json)
  let payload = JSON.parse(body)
  console.log('[' + json.method + '] -> ')
  console.log(JSON.stringify(payload, null, 2))
  let opt = {
    url: env["host"] + "/cloud/api/" + json.method,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: encodeURIComponent(JSON.stringify(json))
  }
  try {
    request.post(opt, (error, response, body) => {
      printResonse(json, desc)
      console.log('[' + json.method + '] <- ')
      try {
        let json = JSON.parse(body)
        if (json.error) {
          console.log(JSON.stringify(json, null, 2))
        } else {
          console.log(JSON.stringify(json, null, 2))
        }
      } catch (e) {
        console.log(JSON.stringify(json, null, 2))
      }
      callback({
        error,
        response,
        body
      })
    })
  } catch (e) {
    console.error(e.message)
    throw e;
  }
}

function auth_mobile_login(args, callback) {
  let json = {
    "id": "{{id}}",
    "method": "auth.mobile.login",
    "params": {
      "sn": "{{sn}}",
      "loginId": "{{loginId}}",
      "withAuth": "1",
      "withProfile": "1",
      "saltedPassword": "{{saltedPassword}}",
      "salt": "{{salt}}",
      "domain": "{{domain}}"
    },
    "jsonrpc": "2.0"
  }
  let desc = {
    "url":"https://www.dau6.com/wiki/doku.php?id=bg:api:cloud:auth.mobile.login",
    "method": "手机客户端登录",
    "params": {
      "sn": ["String", "4", "是", "厅主ID"],
      "loginId": ["String", "128", "是", "登录ID(手机号,邮箱等)"],
      "saltedPassword": ["String", "128", "是", "加盐密码:base64(sha1(base64(sha1($password)) + $salt))"],
      "salt": ["String", "64", "是", "密码盐份"],
      "clientVersion": ["String", "64", "是", "客户端版本号，例如(ios-1.0.1，android-1.0.1)"],
      "loginFrom": ["int", "1", "否", "登陆终端来源 同充值常量定义 1; pc-web, 3; Android app，4; iOS app,7; system backend，8; html 5 (mobile);9 weixin"],
      "loginChannel": ["String", "255", "否", "登陆网址"]
    },
    "response" : {
      "sessionId": ["String", null, "是", "登录成功后,会话ID"],
      "sessionType": ["String", null, "是", "会话类型: w,周有效;d,天有效;t,临时有效2小时"],
      "userId": ["String", null, "是", "用户ID"],
      "expiry": ["String", null, "是", "过期时间"],
      "auth": ["String", null, "否", "鉴权数据(主要用于debug或者后台服务)"],
      "profile": ["String", null, "否", "用户基本信息(主要用于debug或者后台服务)"]
    }
  }
  json.params.salt = random_string_generator()
  let sha1 = new SHA1()
  let r1 = sha1.hashToBase64(args.password)
  let r2 = r1 + json.params.salt
  json.params.saltedPassword = sha1.hashToBase64(r2)
  return sendRequest(json, args, callback, desc)
}

function auth_mobile_trial_login(args, callback) {
  let json = {
    "id": "{{id}}",
    "method": "auth.mobile.trial.login",
    "params": {
      "sn": "{{sn}}"
    },
    "jsonrpc": "2.0"
  }
  let desc = {
    "url":"https://www.dau6.com/wiki/doku.php?id=bg:api:cloud:auth.mobile.trial.login",
    "method": "试玩登录",
    "params": {
      "sn": ["String", "4", "是", "厅主ID"],
      "agentCode": ["String", "10", "否", "当前页面推广码"],
      "clientVersion": ["String", "64", "是", "客户端版本号，例如(ios-1.0.1，android-1.0.1)"]
    },
    "response" : {
      "sessionId": ["String", null, "是", "登录成功后,会话ID"],
      "sessionType": ["String", null, "是", "会话类型: w,周有效;d,天有效;t,临时有效2小时"],
      "sn": ["String", null, "是", "试用厅ID"],
      "loginId": ["String", null, "是", "用户登录ID"],
      "userId": ["String", null, "是", "用户ID"],
      "expiry": ["String", null, "是", "过期时间"],
      "auth": ["String", null, "否", "鉴权数据(主要用于debug或者后台服务)"],
      "profile": ["String", null, "否", "用户基本信息(主要用于debug或者后台服务)"],
      "trial": ["String", null, "否", "试玩用户. 1, 试玩账户; 0,非试玩;(默认)"]
    }
  }
  return sendRequest(json, args, callback, desc)
}

function auth_online_logout(args, callback) {
  let json = {
    "id": "{{id}}",
    "method": "auth.online.logout",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  }
  let desc = {
    "url":"https://www.dau6.com/wiki/doku.php?id=bg:api:platform:auth.online.logout",
    "method": "获取用户登出",
    "params": {
      "sessionId": ["String", "32", "是", "用户会话ID"]
    },
    "response" : {
      "result": ["String", "1", "是", "1,成功;0,失败;"]
    }
  }
  return sendRequest(json, args, callback, desc)
}

function sn_info(args, callback) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.info",
    "params": {
      "domain": "{{domain}}"
    }
  }
  let desc = {
    "url":"https://www.dau6.com/wiki/doku.php?id=bg:api:platform:sn.info",
    "method": "获取厅主（简化）信息",
    "params": {
      "sn": ["String", "4", "否", "厅主ID（与domain而二选一）"],
      "domain": ["String", "4", "否", "厅主ID"]
    },
    "response" : {
      "sn": ["String", "4", "是", "厅主ID"],
      "name": ["String", "128", "是", "厅主名称"],
      "status": ["int", "1", "是", "状态. 1,正常;2,停用;3,删除;4,维护中"],
      "memo": ["String", "128", "是", "备注"],
      "snType": ["int", "1", "是", "厅主类型。1,现金；2，API；3,陪玩;4,彩票;5,旧版彩票;"],
      "websiteTitle": ["String", "128", "是", "前端站点页面Title"],
      "firstPageFlag": ["int", "1", "否", "第一次进入前端站点的页面。1，首页；2，注册页；3，簡易模式；"],
      "soundHintFlag": ["string", "1", "否", "声音提示标记。a，嘀嘀声。b，真人播报；"],
      "ipRegisterLimit": ["int", "32", "否", "同一个IP注册会员的限制数。0，没有限制；"],
      "bankAccountBoundLimit": ["int", "32", "否", "同一个银行帐号绑定会员的限制数。0，没有限制；"],
      "onlineCustomerServiceUrl": ["string", "255", "否", "在线客服链接"],
      "isBuildin": ["int", "1", "否", "是否内置。1，内置;0,否；"],
      "isIpReject": ["int", "1", "否", "是否IP限制。1，是;0,否(默认)；"],
      "modules": ["list", null, "是", "維護模塊ID"],
      "maintain": ["object", null, "是", "維護信息"],
      "currencyCode": ["string", null, "是", "本位币"],
      "level": ["int", null, "是", "等级"],
      "isZone": ["int", null, "是", "是否受限"],
      "zone": ["string", null, "是", "受限地区"],
      "webDomain": ["string", null, "是", "域名站点信息 （没有sn，传domain时候产生）"]
    }
  }
  return sendRequest(json, args, callback, desc)
}

function auth_session_validate(args, callback) {
  let json = {
    "id": "{{id}}",
    "method": "auth.session.validate",
    "params": {
      "sessionId": "{{sessionId}}",
      "withAmount": "0",
      "withUser": "0"
    },
    "jsonrpc": "2.0"
  }
  let desc = {
    "url":"https://www.dau6.com/wiki/doku.php?id=bg:api:platform:auth.session.validate",
    "method": "验证会话有效性",
    "params": {
      "sessionId": ["String", "128", "是", "会话ID"],
      "withAmount": ["String", "1", "是", "0,不取余额(默认值); 1,同时返回余额"],
      "withUser": ["String", "1", "是", "1,同时返回loginId"]
    },
    "response" : {
      "flag": ["String", "1", "是", "1,成功;0,失败;"],
      "sessionType": ["int", "1", "是", "会话类型"],
      "userId": ["long", "32", "是", "用户ID"],
      "expiry": ["long", "32", "是", "过期时间"],
      "sn": ["String", "4", "是", "厅主ID"],
      "availableAmount": ["float", "32", "否", "可下注金额"]
    }
  }
  return sendRequest(json, args, callback, desc)
}