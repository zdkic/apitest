const electron = require('electron')
const {
  ipcRenderer
} = require('electron')
const { app, dialog } = require('electron').remote
const remote = require('electron').remote
const BrowserWindow = electron.remote.BrowserWindow
const url = require('url')
const notifier = require('node-notifier')
const path = require('path')

let env = {
  "host": "http://ae.bg1207.com",
  "sn": "ae00",
  "concurrency": 1,
  "duration": 60,
  "userid": "bgqajohn3",
  "password": "123456"
};

let timer = null
let optionsWin = null
let is_stopped = true
let error_count = 0

/**
 * System Notification
 * @param {string} msg
 */
function notice(msg) {
  /** Show Form */
  const window = remote.getCurrentWindow()
  window.restore()
  window.show()

  /** https://github.com/mikaelbr/node-notifier */
  notifier.notify({
    title: '检测服务状态',
    message: msg,
    icon: path.join(__dirname, 'clock.ico'),
    sound: true,
  })
}

function refreshUI() {
  const ds = new dataStore()
  document.getElementById('host').innerHTML = ds.getOption('host')
  document.getElementById('sn').innerHTML = ds.getOption('sn')
  document.getElementById('concurrency').innerHTML = ds.getOption('concurrency')
  document.getElementById('duration').innerHTML = ds.getOption('duration')
}

function createOptions() {
  if (!optionsWin) {
    optionsWin =
      new BrowserWindow({
        width: 360,
        height: 240,
        maximizable: false,
        parent: remote.getCurrentWindow(),
        modal: true
      })

    optionsWin.setMenu(null)

    optionsWin.loadURL(url.format({
      pathname: path.join(__dirname, 'options.html'),
      protocol: 'file:',
      slashes: true
    }))

    optionsWin.on('closed', () => {
      refreshUI()
      optionsWin = null
    })

    optionsWin.show()
  }
}

function createSaveDialog() {
  const options = {
    defaultPath: app.getPath("desktop") + '/debug.log'
  }
  dialog.showSaveDialog(null, options, (path) => {
    if(path) {
      const data = fs.readFileSync(app.getPath('userData') + '/debug.log')
    const log_file = fs.createWriteStream(path, {flags : 'w'})
    log_file.write(data)
    }
  })
}

function createDebugDialog() {
  ipcRenderer.send('open_devtools')
}

ipcRenderer.on('start_check', (event, arg) => {
  name = arg[0]
  error = arg[1]
  message = arg[2]
  if (error) {
    error_count++
    notice(message)
  }
  if(error) {
    document.getElementById(name).innerHTML = '<label style="color:red;">' + 'NG' + '</label>'
  } else {
    document.getElementById(name).innerHTML = '<label style="color:green;">' + 'OK' + '</label>'
  }
  if (error_count == 0) {
    document.getElementById('status').innerHTML = '<label style="color:green;">' + '正常' + '</label>'
  } else {
    document.getElementById('status').innerHTML = '<label style="color:red;">' + '异常' + '</label>'
  }
})

ipcRenderer.on('stop_check', () => {
  notice('停止检测')
  document.getElementById('start_time').innerHTML = "--"
  document.getElementById('status').innerHTML = "--"
  var labels = document.getElementById("api_panel").getElementsByTagName('label')
  for (var idx = 0; idx < labels.length; idx++) {
    labels[idx].innerHTML = "--"
  }
  is_stopped = true
})

ipcRenderer.on('clear_error', () => {
  error_count = 0
})

function get_current_time() {
  function _leading_zeros(num) {
    return (num < 10 ? '0' : '') + num;
  }
  var d = new Date()

  return _leading_zeros(d.getFullYear()) + '/' +
    _leading_zeros(d.getMonth() + 1) + '/' +
    _leading_zeros(d.getDate()) + ' ' +
    _leading_zeros(d.getHours()) + ':' +
    _leading_zeros(d.getMinutes()) + ':' +
    _leading_zeros(d.getSeconds())
}

function init() {
  const starBtn = document.getElementById('startBtn')
  const stopBtn = document.getElementById('stopBtn')

  starBtn.disabled = false
  stopBtn.disabled = true

  refreshUI()

  starBtn.addEventListener("click", function () {
    if (!is_stopped)
      return
    is_stopped = false
    notice('开始检测')
    document.getElementById('start_time').innerHTML = '<label>' + get_current_time() + '</label>'
    starBtn.disabled = true
    stopBtn.disabled = false
    start_check()
  })

  stopBtn.addEventListener("click", function () {
    if (is_stopped)
      return
    stop_check()
    starBtn.disabled = false
    stopBtn.disabled = true
  })

  const optionsBtn = document.getElementById('optionsBtn')
  optionsBtn.addEventListener("click", function () {
    createOptions()
  })

  const saveBtn = document.getElementById('saveBtn')
  saveBtn.addEventListener("click", function () {
    createSaveDialog()
  });

  const debugBtn = document.getElementById('debugBtn')
  debugBtn.addEventListener("click", function () {
    createDebugDialog()
  })
}

let api_validator = (api_name, data, data_checker = null, api = null, args = {}, callback = null) => {
  try {
    if (!data.body) {
      ipcRenderer.send('start_check', [-1, "No response available!"])
      return
    }
    let json = JSON.parse(data.body)
    if (!json.error) {
      if (data_checker) {
        let ret = data_checker(json)
        if (ret) {
          ipcRenderer.send('start_check', [api_name, true, ret])
        } else {
          ipcRenderer.send('start_check', [api_name, json.error, data.body])
        }
      } else {
        ipcRenderer.send('start_check', [api_name, json.error, data.body])
      }
      if (api) {
        for (const [k, v] of Object.entries(args)) {
          if (json.result.hasOwnProperty(k))
            args[k] = json.result[k]
        }
        api(args, callback)
      }
    } else {
      ipcRenderer.send('start_check', [api_name, json.error, data.body])
    }
  } catch (e) {
    console.error(e.message)
    ipcRenderer.send('start_check', [api_name, true, e.message])
  }
}

function validate_mobile_login_impl(trial) {
  let logout_callback = (data) => {
    api_validator("auth_online_logout", data)
  }
  let session_validate_callback = (data) => {
    api_validator("auth_session_validate", 
      data,
      (json) => {
        if (!json.result.hasOwnProperty("sessionId") || !json.result["sessionId"].length)
          return "No sessionId field"
      },
      auth_online_logout, {
        "sessionId": null
      }, logout_callback)
  }
  let login_callback = (data) => {
    api_validator(trial?"auth_mobile_trial_login":"auth_mobile_login",
      data,
      null,
      auth_session_validate, {
        "sessionId": null
      }, session_validate_callback)
  }
  if (trial) {
    auth_mobile_trial_login({
      "sn": env["sn"]
    }, login_callback);
  } else {
    let domain = env["host"].replace(/^(?:https?:\/\/)?/i, "").split('/')[0]
    auth_mobile_login({
      "sn": env["sn"],
      "loginId": env["userid"],
      "password": env["password"],
      "domain": domain
    }, login_callback);
  }
}

function validate_sn_info() {
  let sn_info_callback = (data) => {
    api_validator("sn_info",
      data,
      (json) => {
        if (!json.result.hasOwnProperty("sn") || !json.result["sn"].length)
          return "No sn field"
      })
  }
  let domain = env["host"].replace(/^(?:https?:\/\/)?/i, "").split('/')[0]
  sn_info({
    "domain": domain
  }, sn_info_callback)
}

function validate_mobile_login() {
  validate_mobile_login_impl(false)
}

function validate_mobile_trial_login() {
  validate_mobile_login_impl(true)
}

function get_date_string() {
  let dt = new Date()
  let year = dt.getFullYear()
  let mon = dt.getMonth() + 1
  let day = dt.getDate()
  let hour = dt.getHours()
  let minutes = dt.getMinutes()
  let seconds = dt.getSeconds()
  let ts = `----- ${year}/${mon}/${day} ${hour}:${minutes}:${seconds} -----`
  return ts
}

function check_service(count) {
  // run once
  validate_mobile_login()
  // run concurrently
  for (let idx = 0; idx < count; idx++) {
    console.log(get_date_string())
    validate_sn_info()
    validate_mobile_trial_login()
  }
}

function start_check() {
  const ds = new dataStore()
  env["host"] = ds.getOption('host')
  env["sn"] = ds.getOption('sn')
  env["concurrency"] = ds.getOption('concurrency')
  env["duration"] = ds.getOption('duration')
  env["userid"] = ds.getOption('userid')
  env["password"] = ds.getOption('password')
  check_service(env["concurrency"])
  ipcRenderer.send('clear_error')
  timer = setInterval(() => {
    check_service(env["concurrency"])
    ipcRenderer.send('clear_error')
  }, env["duration"] * 1000)
}

function stop_check() {
  clearInterval(timer)
  ipcRenderer.send('stop_check')
}

init()