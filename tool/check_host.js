var json = {
    "id": "34297233603495",
    "jsonrpc": "2.0",
    "method": "auth.mobile.trial.login",
    "params": {
        "sn": "{{sn}}"
    }
};

var args = process.argv;
if(args.length < 3) {
    console.error("Usage: node check_host.js <sn>\nEx: node check_host.js ae00");
    process.exit(-1);
}
json.params.sn = args[2];

var body = JSON.stringify(json);
console.log(encodeURIComponent(body));