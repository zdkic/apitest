#!/bin/bash
count=0

function purge_file {
  path_pattern=$1
  for filename in `ls -v ${path_pattern}`; do
    create_date=`echo ${filename}|awk -F'_' '{print $(NF-1)}'`
    diff=$((`date -d '8 days ago' '+%s'`-`date -d "${create_date}" '+%s'`))
    if [ ${diff} -gt 0 ]; then
      count=$((${count}+1))
      rm -f ${filename}
    fi
  done
}

#移除舊的report
purge_file "../report/*_????-??-??_????.md" 

#移除舊的log
purge_file "../report/log/*_????-??-??_????.md"

#移除舊的images
purge_file "../report/images/*_????-??-??_????.png"

if [ ${count} -gt 0 ]; then
  git commit -a -m 'purge test reports' && git push
fi