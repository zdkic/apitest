import sys
from argparse import ArgumentParser
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import json
from collections import OrderedDict
import random

def main(argv):
   output = ''
   percent = True
   labels = []
   sizes = []

   parser = ArgumentParser()
   parser.add_argument("-j", help="json data")
   parser.add_argument("-o", help="output filepath", default="pie.png")
   parser.add_argument("-p", help="percent")

   args = parser.parse_args()
   json_data = args.j
   output = args.o
   if args.p == 'false':
      percent = False

   data = json.loads(json_data, object_pairs_hook=OrderedDict)

   for k,v in data.items():
      labels.append(k)
      sizes.append(v if v else u'0')

   colors = ['lightgreen', 'red', 'lightskyblue', 'orange', 'lightpink', 'seagreen', 'yellow']
   random_colors_len = len(labels) - len(colors)
   if random_colors_len > 0:
      for idx in range(random_colors_len):
         color = "#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
         colors.append(color)

   if percent:
      patches, texts, _ = plt.pie(sizes, colors=colors, autopct='%1.1f%%', startangle=90)
   else:
      patches, texts = plt.pie(sizes, colors=colors, startangle=90)

   plt.legend(patches, labels, loc="best")
   plt.axis('equal')
   plt.tight_layout()
   plt.savefig(output)

if __name__ == "__main__":
   main(sys.argv[1:])