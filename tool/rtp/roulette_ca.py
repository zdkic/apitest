all_count = 37
direct_count = 1
separate_count = 2
street_count = 3
threenum_count = 3
triangle_count = 4
fournum_count = 4
line_count = 6
column_count = 12
dozen_count = 12
red_count = 18
even_count = 18
big_count = 18

direct_pos = float(direct_count)/all_count
separate_pos = float(separate_count)/all_count
street_pos = float(street_count)/all_count
threenum_pos = float(threenum_count)/all_count
triangle_pos = float(triangle_count)/all_count
fournum_pos = float(fournum_count)/all_count
line_pos = float(line_count)/all_count
column_pos = float(column_count)/all_count
dozen_pos = float(dozen_count)/all_count
red_pos = float(red_count)/all_count
even_pos = float(even_count)/all_count
big_pos = float(big_count)/all_count

rtp_direct = float(direct_count)/all_count * 35 -  (1 - float(direct_count)/all_count)
rtp_separate = float(separate_count)/all_count * 17 -  (1 - float(separate_count)/all_count)
rtp_street = float(street_count)/all_count * 11 -  (1 - float(street_count)/all_count)
rtp_threenum = float(threenum_count)/all_count * 11 -  (1 - float(threenum_count)/all_count)
rtp_triangle = float(triangle_count)/all_count * 8 -  (1 - float(triangle_count)/all_count)
rtp_fournum = float(fournum_count)/all_count * 8 -  (1 - float(fournum_count)/all_count)
rtp_line = float(line_count)/all_count * 5 -  (1 - float(line_count)/all_count)
rtp_column = float(column_count)/all_count * 2 -  (1 - float(column_count)/all_count)
rtp_dozen = float(dozen_count)/all_count * 2 -  (1 - float(dozen_count)/all_count)
rtp_red = float(red_count)/all_count * 1 -  (1 - float(red_count)/all_count)
rtp_even = float(even_count)/all_count * 1 -  (1 - float(even_count)/all_count)
rtp_big = float(big_count)/all_count * 1 -  (1 - float(big_count)/all_count)

print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Direct', round(direct_pos, 6), 35, round(rtp_direct, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Separate', round(separate_pos, 6), 17, round(rtp_separate, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Street', round(street_pos, 6), 11, round(rtp_street, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('ThreeNum', round(threenum_pos, 6), 11, round(rtp_threenum, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Triangle', round(triangle_pos, 6), 8, round(rtp_triangle, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('FourNum', round(fournum_pos, 6), 8, round(rtp_fournum, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Line', round(line_pos, 6), 5, round(rtp_line, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Column', round(column_pos, 6), 2, round(rtp_column, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Dozen', round(dozen_pos, 6), 2, round(rtp_dozen, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Red / Black', round(red_pos, 6), 1, round(rtp_red, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Even / Odd', round(even_pos, 6), 1, round(rtp_even, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}\n".format('Big / small', round(big_pos, 6), 1, round(rtp_big, 6)))