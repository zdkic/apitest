import time

face = {}
all_count = 0
dwin = 0
twin = 0
tie = 0
dodd = 0
deven = 0
todd = 0
teven = 0
dred = 0
dblack = 0
tred = 0
tblack = 0

for i in range(1,52+1):
    face[i] = 8

def computePoints(a,b):
    def is_odd(x):
        return (x % 2) == 1
    def is_red(x):
        return (x > 13 and x <= 26) or (x > 39 and x <= 52)
    dragon_point = (a % 13) or 13
    dragon_odd = is_odd(dragon_point)
    dragon_red = is_red(a)
    tiger_point = (b % 13) or 13
    tiger_odd = is_odd(tiger_point)
    tiger_red = is_red(b)
    return dragon_point, dragon_odd, dragon_red, tiger_point, tiger_odd, tiger_red

tStart = time.time()

for a in range(1,52+1):
    p1 = face[a]
    face[a] = face[a] - 1
    for b in range(1,52+1):
        p2 = face[b]
        face[b] = face[b] - 1
        dragon_point, dragon_odd, dragon_red, tiger_point, tiger_odd, tiger_red = computePoints(a, b)
        partial_count = p1 * p2
        all_count += partial_count
        if dragon_point > tiger_point:
            dwin += partial_count
        elif dragon_point < tiger_point:
            twin += partial_count
        else:
            tie += partial_count
        if dragon_odd:
            dodd += partial_count
        else:
            deven += partial_count
        if tiger_odd:
            todd += partial_count
        else:
            teven += partial_count
        if dragon_red:
            dred += partial_count
        else:
            dblack += partial_count
        if tiger_red:
            tred += partial_count
        else:
            tblack += partial_count
        face[b] = face[b] + 1
    face[a] = face[a] + 1

tEnd = time.time()

dragon_pos=float(dwin)/all_count
tiger_pos=float(twin)/all_count
tie_pos=float(tie)/all_count
dodd_pos=float(dodd)/all_count
deven_pos=float(deven)/all_count
todd_pos=float(todd)/all_count
teven_pos=float(teven)/all_count
dred_pos=float(dred)/all_count
dblack_pos=float(dblack)/all_count
tred_pos=float(tred)/all_count
tblack_pos=float(tblack)/all_count

rtp_dragon = float(dwin)/all_count * 1 - float(tie)/all_count * 0.5 - float(twin)/all_count
rtp_tiger = float(twin)/all_count * 1 - float(tie)/all_count * 0.5 - float(dwin)/all_count
rtp_tie = float(tie)/all_count * 8 - (1 - float(tie)/all_count)
rtp_dragon_odd = float(dodd)/all_count * 0.75 - (1 - float(dodd)/all_count)
rtp_dragon_even = float(deven)/all_count * 1.05 - (1 - float(deven)/all_count)
rtp_tiger_odd = float(todd)/all_count * 0.75 - (1 - float(todd)/all_count)
rtp_tiger_even = float(teven)/all_count * 1.05 - (1 - float(teven)/all_count)
rtp_dragon_red = float(dred)/all_count * 0.9 - (1 - float(dred)/all_count)
rtp_dragon_black = float(dblack)/all_count * 0.9 - (1 - float(dblack)/all_count)
rtp_tiger_red = float(tred)/all_count * 0.9 - (1 - float(tred)/all_count)
rtp_tiger_black = float(tblack)/all_count * 0.9 - (1 - float(tblack)/all_count)

print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Dragon', round(dragon_pos, 6), 1, round(rtp_dragon, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tiger', round(tiger_pos, 6), 1, round(rtp_tiger, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tie', round(tie_pos, 6), 8, round(rtp_tie, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Dragon Odd', round(dodd_pos, 6), 0.75, round(rtp_dragon_odd, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Dragon Even', round(deven_pos, 6), 1.05, round(rtp_dragon_even, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tiger Odd', round(todd_pos, 6), 0.75, round(rtp_tiger_odd, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tiger Even', round(teven_pos, 6), 1.05, round(rtp_tiger_even, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Dragon Red', round(dred_pos, 6), 0.9, round(rtp_dragon_red, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Dragon Black', round(dblack_pos, 6), 0.9, round(rtp_dragon_black, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tiger Red', round(tred_pos, 6), 0.9, round(rtp_tiger_red, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}\n".format('Tiger Black', round(tblack_pos, 6), 0.9, round(rtp_tiger_black, 6)))

print "It cost %f seconds" % (tEnd - tStart)


