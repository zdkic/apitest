#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define CARD_NUM (13*4)

typedef enum {
    DIAMOND = 1,
    CLUB,
    HEART,
    SPADE
} SUIT;

typedef struct {
    int value;
    SUIT suit;
} card;

typedef struct {
    int all_count;
    int red_count;
    int black_count;
    int three_of_a_kind_count;
    int straight_flush_count;
    int flush_count;
    int straight_count;
    int pair_9_to_A_count;
} bet_stat;

enum {HIGHCARD=1, PAIR, STRAIGHT, FLUSH, STRAIGHT_FLUSH, THREE_OF_A_KIND};

card red_data[3];
card black_data[3];
int red_score = 0;
int black_score = 0;
bet_stat stat = {0};

typedef void (*COMBINATION_CALLBACK)(card *);

void exec_combination(card arr[],
                      card data[],
                      int start,
                      int end,
                      int index,
                      int r,
                      COMBINATION_CALLBACK callback);

int is_three_of_a_kind(card *c) {
    if(c[0].value == c[1].value && c[1].value == c[2].value)
        return 1;
    return 0;
}

int is_flush(card *c) {
    if(c[0].suit == c[1].suit && c[1].suit == c[2].suit)
        return 1;
    return 0;
}

int is_A23_straight(card *c) {
    return c[0].value == 2 && c[1].value == 3 && c[2].value == 14;
}

int is_straight(card *c) {
    if((c[0].value == c[1].value - 1 && c[1].value == c[2].value - 1) || is_A23_straight(c))
        return 1;
    return 0;
}

int is_straight_flush(card *c) {
    return is_flush(c) && is_straight(c);
}

int is_pair(card *c, int *pair_value, int *rest_value) {
    if(c[0].value == c[1].value) {
        if(pair_value) *pair_value = c[0].value;
        if(rest_value) *rest_value = c[2].value;
        return 1;
    }
    if(c[1].value == c[2].value) {
        if(pair_value) *pair_value = c[1].value;
        if(rest_value) *rest_value = c[0].value;
        return 1;
    }
    if(c[2].value == c[0].value) {
        if(pair_value) *pair_value = c[2].value;
        if(rest_value) *rest_value = c[1].value;
        return 1;
    }
    return 0;
}

int compute_score(card *c) {
    int card_type = HIGHCARD;
    int card_suit = c[2].suit * 24 + c[1].suit * 5 + c[0].suit;
    int card_value = c[2].value * 10000 + c[1].value * 100 + c[0].value;
    int pair_value = 0; 
    int rest_value = 0;
    int score = 0;

    if(is_three_of_a_kind(c)) {
        card_type = THREE_OF_A_KIND;
    } else if(is_straight_flush(c)) {
        card_type = STRAIGHT_FLUSH;
        if(is_A23_straight(c)) {
            card_value = 3 * 10000 + 2 * 100 + 1;
        }
    } else if(is_flush(c)) {
        card_type = FLUSH;
    } else if(is_straight(c)) {
        card_type = STRAIGHT;
        if(is_A23_straight(c)) {
            card_value = 3 * 10000 + 2 * 100 + 1;
        }
    } else if(is_pair(c, &pair_value, &rest_value)) {
        card_type = PAIR;
        card_value = pair_value * 10000 + pair_value * 100 + rest_value;
    }
    score = card_type * 100000000 + card_value * 100 + card_suit;
    return score;
}

void judge_card() {
    int is_red_win = 0;
    int red_card_type = HIGHCARD;
    int black_card_type = HIGHCARD;
    int winner_card_type = HIGHCARD;

    red_card_type = red_score / 100000000;
    black_card_type = black_score / 100000000;

    // judge red/black
    if(red_score > black_score) {
        winner_card_type = red_card_type;
        is_red_win = 1;
        stat.red_count += 1;
    } else {
        winner_card_type = black_card_type;
        is_red_win = 0;
        stat.black_count += 1;
    }

    // judge card type
    if(winner_card_type == THREE_OF_A_KIND) {
        stat.three_of_a_kind_count += 1;
    } else if(winner_card_type == STRAIGHT_FLUSH) {
        stat.straight_flush_count += 1;
    } else if(winner_card_type == FLUSH) {
        stat.flush_count += 1;
    } else if(winner_card_type == STRAIGHT) {
        stat.straight_count += 1;
    } else if(winner_card_type == PAIR) {
        int pair_value = 0;
        int rest_value = 0;
        if(is_red_win)
            is_pair(red_data, &pair_value, &rest_value);
        else
            is_pair(black_data, &pair_value, &rest_value);
        if(pair_value >= 9)
            stat.pair_9_to_A_count += 1;
    }
}

void black_callback(card *arr) {
    stat.all_count += 1;
    black_score = compute_score(black_data);
    judge_card();
}

void red_callback(card *arr) {
    card rest_arr[CARD_NUM-3];
    int offset = 0;

    red_score = compute_score(red_data);
    for(int i = 0; i < CARD_NUM; i++) {
        int found = 0;
        for(int j = 0; j < 3; j++) {
            if(arr[i].value == red_data[j].value && arr[i].suit == red_data[j].suit) {
                found = 1;
                break;
            }
        }
        if(!found) {
            rest_arr[offset++] = arr[i];
        }
    }
    int n = sizeof(rest_arr)/sizeof(rest_arr[0]);
    exec_combination(rest_arr, black_data, 0, n-1, 0, 3, black_callback);
}

void iterate_combination(card arr[], int n, int r) { 
    exec_combination(arr, red_data, 0, n-1, 0, r, red_callback);
}

void exec_combination(card arr[],                      // Input Array
                      card data[],                     // Temporary array to store current combination 
                      int start,                       // Staring indexes in arr[] 
                      int end,                         // Ending indexes in arr[] 
                      int index,                       // Current index in data[] 
                      int r,                           // Size of a combination to be printed
                      COMBINATION_CALLBACK callback) { // Callback function
    // Current combination is ready to be printed, print it 
    if (index == r) { 
        (*callback)(arr);
        return; 
    }
    // replace index with all possible elements. The condition 
    // "end-i+1 >= r-index" makes sure that including one element 
    // at index will make a combination with remaining elements 
    // at remaining positions 
    for (int i = start; i <= end && end-i+1 >= r-index; i++) {
        data[index] = arr[i];
        exec_combination(arr, data, i+1, end, index+1, r, callback);
    }
}

int main() {
    time_t start, end;
    card arr[CARD_NUM] = {0};
    int idx = 0;
    int r = 3; 
    int n = sizeof(arr)/sizeof(arr[0]);
    for (int i = 2; i <= 14; i++) {
        for(int j = 0; j < 4; j++) {
            arr[idx].value = i;
            arr[idx].suit = DIAMOND + j;
            idx++;
        }
    }
    start = time(NULL);
    iterate_combination(arr, n, r);
    end = time(NULL);

    printf("All count: %d\n",stat.all_count);
    printf("Red count: %d\n",stat.red_count);
    printf("Black count: %d\n",stat.black_count);
    printf("Three of a kind count: %d\n",stat.three_of_a_kind_count);
    printf("Straight Flush count: %d\n",stat.straight_flush_count);
    printf("Flush count: %d\n",stat.flush_count);
    printf("Straight count: %d\n",stat.straight_count);
    printf("Pair 9 to A count: %d\n",stat.pair_9_to_A_count);

    #define PAY_RED (0.95)
    #define PAY_BLACK (0.95)
    #define PAY_THREE_OF_A_KIND (25)
    #define PAY_STRAIGHT_FLUSH (12)
    #define PAY_FLUSH (3)
    #define PAY_STRAIGHT (3)
    #define PAY_PAIR_9_TO_A (2)

    float red_pos = ((float)stat.red_count)/stat.all_count;
    float black_pos = ((float)stat.black_count)/stat.all_count;
    float three_of_a_kind_pos = ((float)stat.three_of_a_kind_count)/stat.all_count;
    float straight_flush_pos = ((float)stat.straight_flush_count)/stat.all_count;
    float flush_pos = ((float)stat.flush_count)/stat.all_count;
    float straight_pos = ((float)stat.straight_count)/stat.all_count;
    float pair_9_to_A_pos = ((float)stat.pair_9_to_A_count)/stat.all_count;
    float rtp_red = red_pos * PAY_RED - black_pos * 1;
    float rtp_black = black_pos * PAY_BLACK - red_pos * 1;

    float rtp_three_of_a_kind = three_of_a_kind_pos * (PAY_THREE_OF_A_KIND-1) - ( 1 - three_of_a_kind_pos) * 1;
    float rtp_straight_flush = straight_flush_pos * (PAY_STRAIGHT_FLUSH-1) - (1 - straight_flush_pos) * 1;
    float rtp_flush = flush_pos * (PAY_FLUSH-1) - (1 - flush_pos) * 1;
    float rtp_straight = straight_pos * (PAY_STRAIGHT-1) - (1 - straight_pos) * 1;
    float rtp_pair_9_to_A = pair_9_to_A_pos * (PAY_PAIR_9_TO_A-1) - (1 - pair_9_to_A_pos) * 1;

    int lucky_strike_count = stat.three_of_a_kind_count +
                             stat.straight_flush_count +
                             stat.flush_count +
                             stat.straight_count +
                             stat.pair_9_to_A_count;             
    printf("Lucky Strike count: %d\n", lucky_strike_count);

    float lucky_strike_pos = ((float)lucky_strike_count)/stat.all_count;

    float rtp_lucky_strike = three_of_a_kind_pos * (PAY_THREE_OF_A_KIND-1) +
                             straight_flush_pos * (PAY_STRAIGHT_FLUSH-1) +
                             flush_pos * (PAY_FLUSH-1) +
                             straight_pos * (PAY_STRAIGHT-1) +
                             pair_9_to_A_pos * (PAY_PAIR_9_TO_A-1) -
                            (1 - three_of_a_kind_pos - straight_flush_pos - flush_pos - straight_pos - pair_9_to_A_pos) * 1;

    printf("\n- Red/Black\n\n");
    printf("%15s|%15s|%15s|%15s\n", "Event", "Possibility", "Pays", "RTP");
    printf("%15s|%15s|%15s|%15s\n", "---------------", "---------------", "---------------", "---------------");
    printf("%15s|%15.6f|%15.2f|%15.6f\n", "Red", red_pos, PAY_RED, rtp_red);
    printf("%15s|%15.6f|%15.2f|%15.6f\n", "Black", black_pos, PAY_BLACK, rtp_black);
    printf("%15s|%15.6f|%15s|%15.6f\n", "Lucky Strike", lucky_strike_pos, " - ", rtp_lucky_strike);

    printf("\n- Lucky Strike\n\n");
    printf("%15s|%15s|%15s|%15s\n", "Event", "Possibility", "Pays", "RTP");
    printf("%15s|%15s|%15s|%15s\n", "---------------", "---------------", "---------------", "---------------");
    printf("%15s|%15.6f|%15d|%15.6f\n", "Three of a kind", three_of_a_kind_pos, PAY_THREE_OF_A_KIND-1, rtp_three_of_a_kind);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Straight flush", straight_flush_pos, PAY_STRAIGHT_FLUSH-1, rtp_straight_flush);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Flush", flush_pos, PAY_FLUSH-1, rtp_flush);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Straight", straight_pos, PAY_STRAIGHT-1, rtp_straight);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Pair 9 to A", pair_9_to_A_pos, PAY_PAIR_9_TO_A-1, rtp_pair_9_to_A);
    printf("\n");

    printf("It cost %.1f second\n", difftime(end, start));

    return(0);
} 