#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define CARD_NUM (13*4)

char suit[4] = {'S','D','C','H'};

typedef struct {
    int value;
    char suit;
} card;

typedef struct {
    int all_count;
    int dragon_count;
    int phoenix_count;
    int tie_count;
    int three_of_a_kind_count;
    int straight_flush_count;
    int flush_count;
    int straight_count;
    int pair_8_plus_count;
} bet_stat;

enum {X_235=1, HIGHCARD, PAIR, STRAIGHT, FLUSH, STRAIGHT_FLUSH, THREE_OF_A_KIND};

card dragon_data[3];
card phoenix_data[3];
int dragon_score = 0;
int phoenix_score = 0;
bet_stat stat = {0};

typedef void (*COMBINATION_CALLBACK)(card *);

void exec_combination(card arr[],
                      card data[],
                      int start,
                      int end,
                      int index,
                      int r,
                      COMBINATION_CALLBACK callback);

int is_three_of_a_kind(card *c) {
    if(c[0].value == c[1].value && c[1].value == c[2].value)
        return 1;
    return 0;
}

int is_flush(card *c) {
    if(c[0].suit == c[1].suit && c[1].suit == c[2].suit)
        return 1;
    return 0;
}

int is_A23_straight(card *c) {
    return c[0].value == 2 && c[1].value == 3 && c[2].value == 14;
}

int is_straight(card *c) {
    if((c[0].value == c[1].value - 1 && c[1].value == c[2].value - 1) || is_A23_straight(c))
        return 1;
    return 0;
}

int is_straight_flush(card *c) {
    return is_flush(c) && is_straight(c);
}

int is_pair(card *c, int *pair_value, int *rest_value) {
    if(c[0].value == c[1].value) {
        if(pair_value) *pair_value = c[0].value;
        if(rest_value) *rest_value = c[2].value;
        return 1;
    }
    if(c[1].value == c[2].value) {
        if(pair_value) *pair_value = c[1].value;
        if(rest_value) *rest_value = c[0].value;
        return 1;
    }
    if(c[2].value == c[0].value) {
        if(pair_value) *pair_value = c[2].value;
        if(rest_value) *rest_value = c[1].value;
        return 1;
    }
    return 0;
}

int is_235(card *c) {
    if(c[0].value == 2 && c[1].value == 3 && c[2].value == 5 && !is_flush(c))
        return 1;
    return 0;
}

int compute_score(card *c) {
    int card_type = HIGHCARD;
    int card_value = c[2].value * 10000 + c[1].value * 100 + c[0].value;
    int pair_value = 0;
    int rest_value = 0;
    int score = 0;

    if(is_three_of_a_kind(c)) {
        card_type = THREE_OF_A_KIND;
    } else if(is_straight_flush(c)) {
        card_type = STRAIGHT_FLUSH;
        if(is_A23_straight(c)) {
            card_value = 3 * 10000 + 2 * 100 + 1;
        }
    } else if(is_flush(c)) {
        card_type = FLUSH;
    } else if(is_straight(c)) {
        card_type = STRAIGHT;
        if(is_A23_straight(c)) {
            card_value = 3 * 10000 + 2 * 100 + 1;
        }
    } else if(is_pair(c, &pair_value, &rest_value)) {
        card_type = PAIR;
        card_value = pair_value * 10000 + pair_value * 100 + rest_value;
    } else if(is_235(c)) {
        card_type = X_235;
    }
    score = card_type * 1000000 + card_value;
    return score;
}

void judge_card() {
    int is_dragon_win = 0;
    int dragon_card_type = HIGHCARD;
    int phoenix_card_type = HIGHCARD;
    int winner_card_type = HIGHCARD;

    dragon_card_type = dragon_score / 1000000;
    phoenix_card_type = phoenix_score / 1000000;

    // judge dragon/phoenix
    if(dragon_card_type == THREE_OF_A_KIND && phoenix_card_type == X_235) { // Unsuited 2-3-5
        winner_card_type = X_235;
        is_dragon_win = 0;
        stat.phoenix_count += 1;
    } else if(phoenix_card_type == THREE_OF_A_KIND && dragon_card_type == X_235) { // Unsuited 2-3-5
        winner_card_type = X_235;
        is_dragon_win = 1;
        stat.dragon_count += 1;
    } else if(dragon_score > phoenix_score) {
        winner_card_type = dragon_card_type;
        is_dragon_win = 1;
        stat.dragon_count += 1;
    } else {
        winner_card_type = phoenix_card_type;
        is_dragon_win = 0;
        stat.phoenix_count += 1;
    }
    // judge side bet
    if(winner_card_type == THREE_OF_A_KIND) {
        stat.three_of_a_kind_count += 1;
        stat.pair_8_plus_count += 1;
    } 
    else if(winner_card_type == STRAIGHT_FLUSH) {
        stat.straight_flush_count += 1;
        stat.pair_8_plus_count += 1;
    }
    else if(winner_card_type == FLUSH) {
        stat.flush_count += 1;
        stat.pair_8_plus_count += 1;
    } 
    else if(winner_card_type == STRAIGHT) {
        stat.straight_count += 1;
        stat.pair_8_plus_count += 1;
    }
    else if(winner_card_type == PAIR) {
        int pair_value = 0;
        int rest_value = 0;
        if(is_dragon_win)
            is_pair(dragon_data, &pair_value, &rest_value);
        else
            is_pair(phoenix_data, &pair_value, &rest_value);
        if(pair_value > 8)
            stat.pair_8_plus_count += 1;
    }
}

void phoenix_callback(card *arr) {
    stat.all_count += 1;
    if(dragon_data[0].value == phoenix_data[0].value &&
       dragon_data[1].value == phoenix_data[1].value &&
       dragon_data[2].value== phoenix_data[2].value) {
        if(is_flush(dragon_data) == is_flush(phoenix_data)) {
            stat.tie_count += 1;
            return;
        }
    }
    phoenix_score = compute_score(phoenix_data);
    judge_card();
}

void dragon_callback(card *arr) {
    card rest_arr[CARD_NUM-3];
    int offset = 0;

    dragon_score = compute_score(dragon_data);
    for(int i = 0; i < CARD_NUM; i++) {
        int found = 0;
        for(int j = 0; j < 3; j++) {
            if(arr[i].value == dragon_data[j].value && arr[i].suit == dragon_data[j].suit) {
                found = 1;
                break;
            }
        }
        if(!found) {
            rest_arr[offset++] = arr[i];
        }
    }
    int n = sizeof(rest_arr)/sizeof(rest_arr[0]);
    exec_combination(rest_arr, phoenix_data, 0, n-1, 0, 3, phoenix_callback);
}
  
void iterate_combination(card arr[], int n, int r) 
{ 
    exec_combination(arr, dragon_data, 0, n-1, 0, r, dragon_callback);
}

void exec_combination(card arr[],                      // Input Array
                      card data[],                     // Temporary array to store current combination 
                      int start,                       // Staring indexes in arr[] 
                      int end,                         // Ending indexes in arr[] 
                      int index,                       // Current index in data[] 
                      int r,                           // Size of a combination to be printed
                      COMBINATION_CALLBACK callback)   // Callback function
{
    // Current combination is ready to be printed, print it 
    if (index == r) { 
        (*callback)(arr);
        return; 
    }
    // replace index with all possible elements. The condition 
    // "end-i+1 >= r-index" makes sure that including one element 
    // at index will make a combination with remaining elements 
    // at remaining positions 
    for (int i = start; i <= end && end-i+1 >= r-index; i++) {
        data[index] = arr[i];
        exec_combination(arr, data, i+1, end, index+1, r, callback);
    }
}

int main() 
{
    time_t start, end;
    card arr[CARD_NUM] = {0};
    int idx = 0;
    int r = 3; 
    int n = sizeof(arr)/sizeof(arr[0]);
    for (int i = 2; i <= 14; i++) {
        for(int j = 0; j < 4; j++) {
            arr[idx].value = i;
            arr[idx].suit = suit[j];
            idx++;
        }
    }
    start = time(NULL);
    iterate_combination(arr, n, r);
    end = time(NULL);

    printf("All count: %d\n",stat.all_count);
    printf("Dragon count: %d\n",stat.dragon_count);
    printf("Phoenix count: %d\n",stat.phoenix_count);
    printf("Tie count: %d\n",stat.tie_count);
    printf("Three of a kind count: %d\n",stat.three_of_a_kind_count);
    printf("Straight Flush count: %d\n",stat.straight_flush_count);
    printf("Flush count: %d\n",stat.flush_count);
    printf("Straight count: %d\n",stat.straight_count);
    printf("Pair 8 Plus count: %d\n",stat.pair_8_plus_count);

    float dragon_pos = ((float)stat.dragon_count)/stat.all_count;
    float phoenix_pos = ((float)stat.phoenix_count)/stat.all_count;
    float tie_pos = ((float)stat.tie_count)/stat.all_count;
    float three_of_a_kind_pos = ((float)stat.three_of_a_kind_count)/stat.all_count;
    float straight_flush_pos = ((float)stat.straight_flush_count)/stat.all_count;
    float flush_pos = ((float)stat.flush_count)/stat.all_count;
    float straight_pos = ((float)stat.straight_count)/stat.all_count;
    float pair_8_plus_pos = ((float)stat.pair_8_plus_count)/stat.all_count;
    float rtp_dragon = dragon_pos * 0.95 - phoenix_pos * 1;
    float rtp_phoenix = phoenix_pos * 0.95 - dragon_pos * 1;
    float rtp_three_of_a_kind = three_of_a_kind_pos * 120 - ( 1 - three_of_a_kind_pos) * 1;
    float rtp_straight_flush = straight_flush_pos * 100 - (1 - straight_flush_pos) * 1;
    float rtp_flush = flush_pos * 8 - (1 - flush_pos) * 1;
    float rtp_straight = straight_pos * 7 - (1 - straight_pos) * 1;
    float rtp_pair_8_plus = pair_8_plus_pos * 2 - (1 - pair_8_plus_pos) * 1;

    printf("%15s|%15s|%15s|%15s\n", "Event", "Possibility", "Pays", "RTP");
    printf("%15s|%15s|%15s|%15s\n", "---------------", "---------------", "---------------", "---------------");
    printf("%15s|%15.6f|%15.2f|%15.6f\n", "Dragon", dragon_pos, 0.95, rtp_dragon);
    printf("%15s|%15.6f|%15.2f|%15.6f\n", "Phoenix", phoenix_pos, 0.95, rtp_phoenix);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Three of a kind", three_of_a_kind_pos, 120, rtp_three_of_a_kind);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Straight flush", straight_flush_pos, 100, rtp_straight_flush);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Flush", flush_pos, 8, rtp_flush);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Straight", straight_pos, 7, rtp_straight);
    printf("%15s|%15.6f|%15d|%15.6f\n", "Pair 8 Plus", pair_8_plus_pos, 2, rtp_pair_8_plus);
    printf("\n");

    printf("It cost %.1f second\n", difftime(end, start));
} 