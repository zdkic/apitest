import time

face = {}
all_count = 0
pwin = 0
bwin = 0
twin = 0
side_bet_plus_2 = 0
side_bet_plus_3 = 0
side_bet_minus_2 = 0
side_bet_minus_3 = 0
point6 = 0
point_list = range(-11, 0) + range(1,11+1)

for i in range(-11, -1+1):
    face[i]=8

for i in range(1,11+1):
    face[i]=32

def computePoints(a,b,c,d,e,f):
    def add(first, second):
        if((first + second) > 0):
            return((first + second) % 10)
        return(first + second)
    player = add(a, c)
    banker = add(b, d)
    player_card = 2
    banker_card = 2
    if(player >= 0 and banker >= 0): # positive(+)
        if(player == banker): # tie
            pass
        elif(player == 8 or player == 9 or banker == 8 or banker == 9):
            pass
        elif(player >= 0 and player <= 5):
            player = add(player, e)
            player_card = player_card + 1
            if(banker >= 0 and banker <= 5 and abs(player - banker) <= 11):
                banker = add(banker, f)
                banker_card = banker_card + 1
        elif(player == 6 or player == 7):
            if(banker >= 0 and banker <= 5 and abs(player - banker) <= 11):
                banker = add(banker, f)
                banker_card = banker_card + 1
    else: # negative(-)
        if(player == banker):
            pass
        elif(player < 0 and banker < 0):
            if(player - banker < 0  and abs(player - banker) <= 11):
                banker = add(banker, f)
                banker_card = banker_card + 1
            elif(banker - player < 0 and abs(player - banker) <= 11):
                player = add(player, e)
                player_card = player_card + 1
                if((player < 0 and player - banker < 0 and abs(player - banker) <= 11)):
                    banker = add(banker, f)
                    banker_card = banker_card + 1
        elif(player < 0 and banker >= 0 and abs(player - banker) <= 11):
            banker = add(banker, f)
            banker_card = banker_card + 1
        elif(player >= 0 and banker < 0 and abs(player - banker) <= 11):
            player = add(player, e)
            player_card = player_card + 1
        elif(player >= 0 and banker < 0 and abs(player - banker) <= 11):
            player = add(player, e)
            player_card = player_card + 1
            if((player < 0 and player - banker < 0 and abs(player - banker) <= 11)):
                banker = add(banker, f)
                banker_card = banker_card + 1
    return player, banker, player_card, banker_card

tStart = time.time()

for a in point_list:
    p1 = face[a]
    face[a] = face[a] - 1
    for c in point_list:
        if c < a:
            continue
        p2 = face[c]
        face[c] = face[c] - 1
        for b in point_list:
            b1 = face[b]
            face[b] = face[b] - 1
            for d in point_list:
                if d < b:
                    continue
                b2 = face[d]
                face[d] = face[d] - 1
                for e in point_list:
                    h5 = face[e]
                    face[e] = face[e] - 1
                    for f in point_list:
                        h6 = face[f]
                        player, banker, player_card, banker_card = computePoints(a, b, c, d, e, f)
                        factor = 1
                        if a != c:
                            factor *= 2
                        if b != d:
                            factor *= 2
                        partial_count = factor * p1 * p2 * b1 * b2 * h5 * h6
                        all_count = all_count + partial_count
                        if player == banker :
                            twin = twin + partial_count
                        elif player >= 0 and banker >= 0:
                            if player > banker:
                                pwin = pwin + partial_count
                                if player == 6:
                                    point6 += partial_count
                                if player_card == 2:
                                    side_bet_plus_2 += partial_count
                                else:
                                    side_bet_plus_3 += partial_count
                            else:
                                bwin = bwin + partial_count
                                if banker_card == 2:
                                    side_bet_plus_2 += partial_count
                                else:
                                    side_bet_plus_3 += partial_count
                        elif player < 0 and banker < 0:
                            if player < banker:
                                pwin = pwin + partial_count
                                if player_card == 2:
                                    side_bet_minus_2 += partial_count
                                else:
                                    side_bet_minus_3 += partial_count
                            else:
                                bwin = bwin + partial_count
                                if banker_card == 2:
                                    side_bet_minus_2 += partial_count
                                else:
                                    side_bet_minus_3 += partial_count
                        else:
                            if player < 0:
                                pwin = pwin + partial_count
                                if player_card == 2:
                                    side_bet_minus_2 += partial_count
                                else:
                                    side_bet_minus_3 += partial_count
                            else:
                                bwin = bwin + partial_count
                                if banker_card == 2:
                                    if banker >= 0:
                                        side_bet_plus_2 += partial_count
                                    else:
                                        side_bet_minus_2 += partial_count
                                else:
                                    if banker >= 0:
                                        side_bet_plus_3 += partial_count
                                    else:
                                        side_bet_minus_3 += partial_count
                    face[e] = face[e] + 1  
                face[d] = face[d] + 1
            face[b] = face[b] + 1
        face[c] = face[c] + 1
    face[a] = face[a] + 1

tEnd = time.time()

ppos=float(pwin)/all_count
bpos=float(bwin)/all_count
tpos=float(twin)/all_count
side_bet_plus_2_pos=float(side_bet_plus_2)/all_count
side_bet_plus_3_pos=float(side_bet_plus_3)/all_count
side_bet_minus_2_pos=float(side_bet_minus_2)/all_count
side_bet_minus_3_pos=float(side_bet_minus_3)/all_count

rtp_player=float(pwin-point6)/all_count * 1 + float(point6)/all_count * 0.5 + float(bwin)/all_count* -1
rtp_banker = float(bwin)/all_count * 0.95 + float(pwin)/all_count * -1
rtp_tie =  float(twin)/all_count * 8 + float(pwin + bwin)/all_count * -1
rtp_side_bet_plus_2 = float(side_bet_plus_2)/all_count * 1.5 + (1 - float(side_bet_plus_2)/all_count) * -1
rtp_side_bet_plus_3 = float(side_bet_plus_3)/all_count * 3 + (1 - float(side_bet_plus_3)/all_count) * -1
rtp_side_bet_minus_2 = float(side_bet_minus_2)/all_count * 1.5 + (1 - float(side_bet_minus_2)/all_count) * -1
rtp_side_bet_minus_3 = float(side_bet_minus_3)/all_count * 5 + (1 - float(side_bet_minus_3)/all_count) * -1

print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:>15}|{:^15}|{:^15}".format('Player', round(ppos, 6), 1, round(rtp_player, 6)))
print("{:^15}|{:>15}|{:^15}|{:^15}".format('Banker', round(bpos, 6), 0.95, round(rtp_banker, 6)))
print("{:^15}|{:>15}|{:^15}|{:^15}".format('Tie', round(tpos, 6), 8, round(rtp_tie, 6)))
print("{:^15}|{:>15}|{:^15}|{:^15}".format('Side bet +2', round(side_bet_plus_2_pos, 6), 1.5, round(rtp_side_bet_plus_2, 6)))
print("{:^15}|{:>15}|{:^15}|{:^15}".format('Side bet +3', round(side_bet_plus_3_pos, 6), 3, round(rtp_side_bet_plus_3, 6)))
print("{:^15}|{:>15}|{:^15}|{:^15}".format('Side bet -2', round(side_bet_minus_2_pos, 6), 1.5, round(rtp_side_bet_minus_2, 6)))
print("{:^15}|{:>15}|{:^15}|{:^15}\n".format('Side bet -3', round(side_bet_minus_3_pos, 6), 5, round(rtp_side_bet_minus_3, 6)))

print "It cost %f seconds" % (tEnd - tStart)