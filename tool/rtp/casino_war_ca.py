import time
from numba import jit

@jit
def computePoints(a,b):
    def get_point(point):
        p = point % 13
        if p == 0:
            p = 13
        if p == 1:
            p = 14
        return p
    dealer_point = get_point(a)
    player_point = get_point(b)
    return dealer_point, player_point

tStart = time.time()

@jit
def calc():
    face = {}
    all_count = 0
    dwin = 0
    pwin = 0
    tie = 0
    tie_dwin = 0
    tie_pwin = 0
    tie_tie = 0
    for i in range(1,52+1):
        face[i] = 6
    for a in range(1,52+1):
        p1 = face[a]
        face[a] = face[a] - 1
        for b in range(1,52+1):
            p2 = face[b]
            face[b] = face[b] - 1
            for c in range(1,52+1):
                p3 = face[c]
                face[c] = face[c] - 1
                for d in range(1,52+1):
                    p4 = face[d]
                    face[d] = face[d] - 1
                    dealer_point, player_point = computePoints(a, b)
                    partial_count = p1 * p2 * p3 * p4
                    all_count += partial_count
                    if dealer_point > player_point:
                        dwin += partial_count
                    elif dealer_point < player_point:
                        pwin += partial_count
                    else:
                        tie += partial_count
                        dp, pp = computePoints(c, d)
                        if dp > pp:
                            tie_dwin += partial_count
                        elif dp < pp:
                            tie_pwin += partial_count
                        else:
                            tie_tie += partial_count
                    face[d] = face[d] + 1
                face[c] = face[c] + 1
            face[b] = face[b] + 1
        face[a] = face[a] + 1
    return all_count,dwin,pwin,tie,tie_dwin,tie_pwin,tie_tie

all_count, dwin, pwin, tie, tie_dwin, tie_pwin, tie_tie = calc()

tEnd = time.time()

pwin_pos=float(pwin)/all_count
tie_pos=float(tie)/all_count
tie_dwin_pos=float(tie_dwin)/all_count
tie_pwin_pos=float(tie_pwin)/all_count
tie_tie_pos=float(tie_tie)/all_count
player_pos = float(pwin + tie_pwin)/all_count

rtp_player_win = float(pwin + tie_pwin)/all_count * 1 + float(tie_tie)/all_count * 2 - float(dwin)/all_count * 1 - float(tie_dwin)/all_count * 2 
rtp_tie = float(tie - tie_dwin)/all_count * 10 - (1 - float(tie - tie_dwin)/all_count)

print("[Casino War]")
print("{:^15}|{:^15}".format('Event', 'Possibility'))
print("{:^15}|{:^15}".format('---------------', '---------------'))
print("{:^15}|{:^15}".format('Player', round(pwin_pos, 6)))
print("{:^15}|{:^15}".format('Tie', round(tie_pos, 6)))
print("{:^15}|{:^15}".format('Win after tie', round(tie_pwin_pos, 6)))
print("{:^15}|{:^15}".format('Lose after tie', round(tie_dwin_pos, 6)))
print("{:^15}|{:^15}".format('Tie after tie', round(tie_tie_pos, 6)))

print("")
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Player Win', round(player_pos, 6), 1, round(rtp_player_win, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tie', round(tie_pos, 6), 10, round(rtp_tie, 6)))

print("It cost {0} seconds".format(tEnd - tStart))


