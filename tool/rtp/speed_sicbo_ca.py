import time

all_count = 0
big_count = 0
small_count = 0
odd_count = 0
even_count = 0
point_dict = {}

for i in range(1,6+1):
    point_dict[i] = 0

def computePoints(point):
    big = point >= 4 and point <= 6
    small = point >= 1 and point <= 3
    odd = True if (point % 2) == 1 else False
    even = True if (point % 2) == 0 else False
    return point, big, small, odd, even

tStart = time.time()

for a in range(1,6+1):
    point, big, small, odd, even = computePoints(a)
    all_count += 1
    if big:
        big_count += 1
    if small:
        small_count += 1
    if odd:
        odd_count += 1
    if even:
        even_count += 1
    point_dict[point] += 1
tEnd = time.time()

big_pos=float(big_count)/all_count
small_pos=float(small_count)/all_count
odd_pos=float(odd_count)/all_count
even_pos=float(even_count)/all_count
point_1_pos = float(point_dict[1])/all_count
point_2_pos = float(point_dict[2])/all_count
point_3_pos = float(point_dict[3])/all_count
point_4_pos = float(point_dict[4])/all_count
point_5_pos = float(point_dict[5])/all_count
point_6_pos = float(point_dict[6])/all_count

rtp_big = float(big_count)/all_count * 0.95 - (1 - float(big_count)/all_count)
rtp_small = float(small_count)/all_count * 0.95 - (1 - float(small_count)/all_count)
rtp_odd = float(odd_count)/all_count * 0.95 - (1 - float(odd_count)/all_count)
rtp_even = float(even_count)/all_count * 0.95 - (1 - float(even_count)/all_count)
rtp_point_1  = float(point_dict[1])/all_count * 4.75 - (1 - float(point_dict[1])/all_count)
rtp_point_2  = float(point_dict[2])/all_count * 4.75 - (1 - float(point_dict[2])/all_count)
rtp_point_3  = float(point_dict[3])/all_count * 4.75 - (1 - float(point_dict[3])/all_count)
rtp_point_4  = float(point_dict[4])/all_count * 4.75 - (1 - float(point_dict[4])/all_count)
rtp_point_5  = float(point_dict[5])/all_count * 4.75 - (1 - float(point_dict[5])/all_count)
rtp_point_6  = float(point_dict[6])/all_count * 4.75 - (1 - float(point_dict[6])/all_count)

print("[Speed Sicbo]")
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Big', round(big_pos, 6), 0.95, round(rtp_big, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Small', round(small_pos, 6), 0.95, round(rtp_small, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Odd', round(odd_pos, 6), 0.95, round(rtp_odd, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Even', round(even_pos, 6), 0.95, round(rtp_even, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Point 1', round(point_1_pos, 6), 4.75, round(rtp_point_1, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Point 2', round(point_2_pos, 6), 4.75, round(rtp_point_2, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Point 3', round(point_3_pos, 6), 4.75, round(rtp_point_3, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Point 4', round(point_4_pos, 6), 4.75, round(rtp_point_4, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Point 5', round(point_5_pos, 6), 4.75, round(rtp_point_5, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Point 6', round(point_6_pos, 6), 4.75, round(rtp_point_6, 6)))

print "It cost %f seconds" % (tEnd - tStart)