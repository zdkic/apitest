import time

face = {}
count = 0
all_count = 0
pwin = 0
bwin = 0
bwin6 = 0
twin = 0
ppair = 0
bpair = 0
big = 0
small = 0

for i in range(1,13+1):
    face[i] = 32

def computePoints(a,b,c,d,e,f):
    a = a if a < 10 else 0
    b = b if b < 10 else 0
    c = c if c < 10 else 0
    d = d if d < 10 else 0
    e = e if e < 10 else 0
    f = f if f < 10 else 0
    player = (a + c) % 10
    banker = (b + d) % 10
    card = 4
    if player > 7 or banker > 7:
        pass
    elif player > 5:
        if banker < 6:
            banker = (banker + f) % 10
            card = card + 1
    else:
        player = (player + e) % 10
        card = card + 1
        if e == 2 or e == 3:
            if banker < 5:
                banker = (banker + f) % 10
                card = card + 1
        elif e == 4 or e == 5:
            if banker < 6:
                banker = (banker + f) % 10
                card = card + 1
        elif e == 6 or e == 7:
            if banker < 7:
                banker = (banker + f) % 10
                card = card + 1
        else:
            if e == 8:
                if banker < 3:
                    banker = (banker + f) % 10
                    card = card + 1
            elif banker < 4:
                banker = (banker + f) % 10
                card = card + 1
    return player, banker, card


tStart = time.time()

for a in range(1,13+1):
    p1 = face[a]
    face[a] = face[a] - 1
    for c in range(1,13+1):
        if c < a:
            continue
        p2 = face[c]
        face[c] = face[c] - 1
        for b in range(1,13+1):
            b1 = face[b]
            face[b] = face[b] - 1
            for d in range(1,13+1):
                if d < b:
                    continue
                b2 = face[d]
                face[d] = face[d] - 1
                for e in range(1,13+1):
                    h5 = face[e]
                    face[e] = face[e] - 1
                    for f in range(1,13+1):
                        count = count + 1
                        h6 = face[f]
                        player, banker, card = computePoints(a, b, c, d, e, f)
                        factor = 1
                        if a != c:
                            factor *= 2
                        if b != d:
                            factor *= 2
                        partial_count = factor * p1 * p2 * b1 * b2 * h5 * h6
                        if a == c:
                            ppair = ppair + partial_count
                        if b == d:
                            bpair = bpair + partial_count
                        if card > 4:
                            big = big + partial_count
                        else:
                            small = small + partial_count
                        all_count = all_count + partial_count
                        if player > banker:
                            pwin = pwin + partial_count
                        elif player < banker:
                            bwin = bwin + partial_count
                            if banker == 6:
                                bwin6 = bwin6 + partial_count
                        else:
                            twin = twin + partial_count     
                    face[e] = face[e] + 1  
                face[d] = face[d] + 1
            face[b] = face[b] + 1
        face[c] = face[c] + 1
    face[a] = face[a] + 1

tEnd = time.time()

ppos=float(pwin)/all_count
bpos=float(bwin)/all_count
tpos=float(twin)/all_count
ppairpos=float(ppair)/all_count
bpairpos=float(bpair)/all_count
bigpos=float(big)/all_count
smallpos=float(small)/all_count
perfectpairpos=float(bpair+bpair)/4/all_count
anypairpos=float(bpair+bpair)/all_count

rtp_player = float(pwin)/all_count * 1 + float(bwin)/all_count * -1
rtp_banker = float(bwin)/all_count * 0.95 + float(pwin)/all_count * -1
rtp_tie =  float(twin)/all_count * 8 + float(pwin + bwin)/all_count * -1
rtp_player_pair = float(ppair)/all_count * 11 + (1 - float(ppair)/all_count) * -1
rtp_bank_pair = float(bpair)/all_count * 11 + (1 - float(bpair)/all_count)*-1
rtp_big = float(big)/all_count * 0.5+(1-float(big)/all_count) * -1
rtp_small = float(small)/all_count * 1.5 + (1 - float(small)/all_count)*-1
rtp_perfect_pair = float(bpair+bpair)/4 /all_count * 20 + (1 - float(bpair+bpair)/all_count) * -1
rtp_any_pair = float(bpair+bpair)/all_count * 5 + (1 - float(bpair+bpair)/all_count) * -1

print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Player', round(ppos, 6), 1, round(rtp_player, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Banker', round(bpos, 6), 0.95, round(rtp_banker, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tie', round(tpos, 6), 8, round(rtp_tie, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Player pair', round(ppairpos, 6), 11, round(rtp_player_pair, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Banker pair', round(bpairpos, 6), 11, round(rtp_bank_pair, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Big', round(bigpos, 6), 0.5, round(rtp_big, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Small', round(smallpos, 6), 1.5, round(rtp_small, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Perfect pair', round(perfectpairpos, 6), 20, round(rtp_perfect_pair, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}\n".format('Any pair', round(anypairpos, 6), 5, round(rtp_any_pair, 6)))

rtp_player = float(pwin)/all_count * 1 + float(bwin)/all_count * -1
rtp_banker = float(bwin-bwin6)/all_count * 1 + float(bwin6)/all_count * 0.5 + float(pwin)/all_count * -1
rtp_tie =  float(twin)/all_count * 8 + float(pwin + bwin)/all_count * -1
rtp_player_pair = float(ppair)/all_count * 11 + (1 - float(ppair)/all_count) * -1
rtp_bank_pair = float(bpair)/all_count * 11 + (1 - float(bpair)/all_count)*-1
rtp_big = float(big)/all_count * 0.5 + (1-float(big)/all_count) * -1
rtp_small = float(small)/all_count * 1.5 + (1 - float(small)/all_count)*-1
rtp_perfect_pair = float(bpair+bpair)/4 /all_count * 20 + (1 - float(bpair+bpair)/all_count) * -1
rtp_any_pair = float(bpair+bpair)/all_count * 5 + (1 - float(bpair+bpair)/all_count) * -1

print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'No Commission RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Player', round(ppos, 6), 1, round(rtp_player, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Banker', round(bpos, 6), 1, round(rtp_banker, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Tie', round(tpos, 6), 8, round(rtp_tie, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Player pair', round(ppairpos, 6), 11, round(rtp_player_pair, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Banker pair', round(bpairpos, 6), 11, round(rtp_bank_pair, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Big', round(bigpos, 6), 0.5, round(rtp_big, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Small', round(smallpos, 6), 1.5, round(rtp_small, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Perfect pair', round(perfectpairpos, 6), 20, round(rtp_perfect_pair, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}\n".format('Any pair', round(anypairpos, 6), 5, round(rtp_any_pair, 6)))

print "It cost %f seconds" % (tEnd - tStart)


