### 視訊遊戲機率和玩家回報率(rtp) ###
1. [傳統百家樂(Baccarat)](#baccarat)
2. [多彩百家樂(Multicolor Baccarat)](#baccarat_color)
3. [龍虎(Dragon Tiger)](#dragon_tiger)
4. [骰寶(SicBo)](#sicbo)
5. [輪盤(Roulette)](#roulette)
6. [牛牛(Bull Bull)](#bullbull)
7. [炸金花(Win Three Cards)](#w3c)
8. [賭場戰爭(Casino War)](#casino_war)
9. [極速骰寶(Speed SicBo)](#speed_sicbo)
10. [紅黑大戰(Red/Black)](#red_black)

- 傳統百家樂(Baccarat) <a name=baccarat></a>

     Event     |  Possibility  |     Pays      |      RTP
---------------|---------------|---------------|---------------
    Player     |   0.446247    |       1       |   -0.012351
    Banker     |   0.458597    |     0.95      |   -0.010579
      Tie      |   0.095156    |       8       |   -0.143596
  Player pair  |   0.074699    |      11       |   -0.103614
  Banker pair  |   0.074699    |      11       |   -0.103614
      Big      |   0.621132    |      0.5      |   -0.068303
     Small     |   0.378868    |      1.5      |   -0.052829
 Perfect pair  |   0.037349    |      20       |   -0.103614
   Any pair    |   0.149398    |       5       |   -0.103614

  - 免佣百家樂玩家回報率

     Event     |  Possibility  |     Pays      |No Commission RTP
---------------|---------------|---------------|---------------
    Player     |   0.446247    |       1       |   -0.012351
    Banker     |   0.458597    |       1       |   -0.014581
      Tie      |   0.095156    |       8       |   -0.143596
  Player pair  |   0.074699    |      11       |   -0.103614
  Banker pair  |   0.074699    |      11       |   -0.103614
      Big      |   0.621132    |      0.5      |   -0.068303
     Small     |   0.378868    |      1.5      |   -0.052829
 Perfect pair  |   0.037349    |      20       |   -0.103614
   Any pair    |   0.149398    |       5       |   -0.103614


- 多彩百家樂(Multicolor Baccarat) <a name=baccarat_color></a>

     Event     |  Possibility  |     Pays      |      RTP
---------------|---------------|---------------|---------------
    Player     |       0.448073|       1       |   -0.016443
    Banker     |       0.448491|     0.95      |   -0.022007
      Tie      |       0.103436|       8       |   -0.069073
  Side bet +2  |       0.301921|      1.5      |   -0.245197
  Side bet +3  |       0.174718|       3       |   -0.301129
  Side bet -2  |       0.311481|      1.5      |   -0.221298
  Side bet -3  |       0.108444|       5       |   -0.349338


- 龍虎(Dragon Tiger) <a name=dragon_tiger></a>

     Event     |  Possibility  |     Pays      |      RTP
---------------|---------------|---------------|---------------
    Dragon     |   0.462651    |       1       |   -0.037349
     Tiger     |   0.462651    |       1       |   -0.037349
      Tie      |   0.074699    |       8       |   -0.327711
  Dragon Odd   |   0.538462    |     0.75      |   -0.057692
  Dragon Even  |   0.461538    |     1.05      |   -0.053846
   Tiger Odd   |   0.538462    |     0.75      |   -0.057692
  Tiger Even   |   0.461538    |     1.05      |   -0.053846
  Dragon Red   |      0.5      |      0.9      |     -0.05
 Dragon Black  |      0.5      |      0.9      |     -0.05
   Tiger Red   |      0.5      |      0.9      |     -0.05
  Tiger Black  |      0.5      |      0.9      |     -0.05


- 骰寶(SicBo) <a name=sicbo></a>

     Event     |  Possibility  |     Pays      |      RTP
---------------|---------------|---------------|---------------
      Big      |   0.486111    |       1       |   -0.027778
     Small     |   0.486111    |       1       |   -0.027778
    Triples    |    0.00463    |      150      |   -0.300926
  Any Triples  |   0.027778    |      24       |   -0.305556
   One Dice    |   0.347222    |       1       |   -0.078704
  Double Dice  |   0.069444    |       2       |   -0.078704
  Three Dice   |    0.00463    |       3       |   -0.078704
     Pairs     |   0.074074    |       8       |   -0.333333
   Dominoes    |   0.138889    |       5       |   -0.166667
3 Sum (4 or 17)|   0.013889    |      50       |   -0.291667
3 Sum (5 or 16)|   0.027778    |      18       |   -0.472222
3 Sum (6 or 15)|   0.046296    |      14       |   -0.305556
3 Sum (7 or 14)|   0.069444    |      12       |   -0.097222
3 Sum (8 or 13)|   0.097222    |       8       |    -0.125
   3 Sum (9)   |   0.115741    |       6       |   -0.189815
  3 Sum (10)   |     0.125     |       6       |    -0.125
  3 Sum (11)   |     0.125     |       6       |    -0.125
  3 Sum (12)   |   0.115741    |       6       |   -0.189815
      Odd      |   0.486111    |       1       |   -0.027778
     Even      |   0.486111    |       1       |   -0.027778

- 輪盤(Roulette) <a name=roulette></a>

     Event     |  Possibility  |     Pays      |      RTP
---------------|---------------|---------------|---------------
    Direct     |   0.027027    |      35       |   -0.027027
   Separate    |   0.054054    |      17       |   -0.027027
    Street     |   0.081081    |      11       |   -0.027027
   ThreeNum    |   0.081081    |      11       |   -0.027027
   Triangle    |   0.108108    |       8       |   -0.027027
    FourNum    |   0.108108    |       8       |   -0.027027
     Line      |   0.162162    |       5       |   -0.027027
    Column     |   0.324324    |       2       |   -0.027027
     Dozen     |   0.324324    |       2       |   -0.027027
  Red / Black  |   0.486486    |       1       |   -0.027027
  Even / Odd   |   0.486486    |       1       |   -0.027027
  Big / small  |   0.486486    |       1       |   -0.027027

- 牛牛(Bull Bull) <a name=bullbull></a>
  - 機率

    Event    |  Possibility
-------------|---------------
      5P     |     0.000304
  Bull Bull  |     0.070817
  Bull 7 - 9 |     0.162706
  Bull 1 - 6 |     0.209765
    Normal   |     0.056409

  - 玩家回報率(平倍賠率)

    Event    | Equal Bet RTP
-------------|---------------
   Player    |     -0.025


  - 玩家回報率(翻倍賠率)

    Event    | Double Bet RTP
-------------|---------------
   Player    |     -0.040278


- 炸金花(Win Three Cards) <a name=w3c></a>

          Event|    Possibility|           Pays|            RTP
---------------|---------------|---------------|---------------
         Dragon|       0.499447|           0.95|      -0.024972
        Phoenix|       0.499447|           0.95|      -0.024972
Three of a kind|       0.004687|            120|      -0.432863
 Straight flush|       0.004328|            100|      -0.562843
          Flush|       0.096203|              8|      -0.134171
       Straight|       0.060514|              7|      -0.515888
    Pair 8 Plus|       0.302840|              2|      -0.091481

    
    
- 賭場戰爭(Casino War) <a name=casino_war></a>

     Event     |  Possibility
---------------|---------------
    Player     |   0.463023
      Tie      |   0.073955
 Win after tie |   0.034242
Lose after tie |   0.034242
 Tie after tie |   0.005471

     Event     |  Possibility  |     Pays      |      RTP
---------------|---------------|---------------|---------------
  Player Win   |   0.497265    |       1       |   -0.023301
      Tie      |   0.073955    |      10       |   -0.563158
      
     
- 極速骰寶(Speed SicBo) <a name=speed_sicbo></a>

     Event     |  Possibility  |     Pays      |      RTP
---------------|---------------|---------------|---------------
      Big      |      0.5      |     0.95      |    -0.025
     Small     |      0.5      |     0.95      |    -0.025
      Odd      |      0.5      |     0.95      |    -0.025
     Even      |      0.5      |     0.95      |    -0.025
    Point 1    |   0.166667    |     4.75      |   -0.041667
    Point 2    |   0.166667    |     4.75      |   -0.041667
    Point 3    |   0.166667    |     4.75      |   -0.041667
    Point 4    |   0.166667    |     4.75      |   -0.041667
    Point 5    |   0.166667    |     4.75      |   -0.041667
    Point 6    |   0.166667    |     4.75      |   -0.041667
    

- 紅黑大戰(Red/Black) <a name=red_black></a>
  - 紅/黑

          Event|    Possibility|           Pays|            RTP
---------------|---------------|---------------|---------------
            Red|       0.500000|           0.95|      -0.025000
          Black|       0.500000|           0.95|      -0.025000
   Lucky Strike|       0.302918|             - |      -0.086012

  - 幸運一擊

          Event|    Possibility|           Pays|            RTP
---------------|---------------|---------------|---------------
Three of a kind|       0.004700|             24|      -0.882506
 Straight flush|       0.004329|             11|      -0.948056
          Flush|       0.096211|              2|      -0.711366
       Straight|       0.060559|              2|      -0.818323
    Pair 9 to A|       0.137120|              1|      -0.725760

