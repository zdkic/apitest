import time

all_count = 0
big_count = 0
small_count = 0
any_triple_count = 0
one_dice_count = 3*5*5*1
double_dice_count = 3*5*1*1
three_dice_count = 1*1*1*1

'''
4,4(pair)
=>
1,4,4   3
2,4,4   3
3,4,4   3
4,4,4   1
5,4,4   3
6,4,4   3
3*5+1 = 16
'''
pairs_count = 3*5+1

'''
1,2(domino)
=>
1,1,2   3
1,2,2   3
1,2,3   6
1,2,4   6
1,2,5   6
1,2,6   6
3+3+6+6+6+6 = 30
'''
dominoes_count = 3*2+6*4
sum_dict = {}
sum_odd = 0
sum_even = 0

for i in range(3,18+1):
    sum_dict[i]=0

def is_triple(a,b,c):
    return a == b == c

def computePoints(a,b,c):
    points = a + b + c
    any_triple = False
    if(a == b and b == c):
        any_triple = True
    big = points >= 11 and points <= 17 and not is_triple(a,b,c)
    small = points >= 4 and points <= 10 and not is_triple(a,b,c)
    return points, big, small, any_triple

tStart = time.time()

for a in range(1,6+1):
    for b in range(1,6+1):
        for c in range(1,6+1):
            points, big, small, any_triple = computePoints(a, b, c)
            all_count += 1
            if big:
                big_count += 1
            if small:
                small_count += 1
            if any_triple:
                any_triple_count += 1
            sum_dict[points] += 1
            if points >=4 and points <= 17 and not is_triple(a,b,c):
                if (points % 2) == 1:
                    sum_odd += 1
                else:
                    sum_even += 1
tEnd = time.time()

big_pos=float(big_count)/all_count
small_pos=float(small_count)/all_count
triple_pos=float(any_triple_count)/6/all_count
any_triple_pos=float(any_triple_count)/all_count
one_dice_pos = float(one_dice_count)/all_count
double_dice_pos = float(double_dice_count)/all_count
three_dice_pos = float(three_dice_count)/all_count
pairs_pos = float(pairs_count)/all_count
dominoes_pos = float(dominoes_count)/all_count
sum_4_pos = float(sum_dict[4])/all_count
sum_5_pos = float(sum_dict[5])/all_count
sum_6_pos = float(sum_dict[6])/all_count
sum_7_pos = float(sum_dict[7])/all_count
sum_8_pos = float(sum_dict[8])/all_count
sum_9_pos = float(sum_dict[9])/all_count
sum_10_pos = float(sum_dict[10])/all_count
sum_11_pos = float(sum_dict[11])/all_count
sum_12_pos = float(sum_dict[12])/all_count
sum_odd_pos = float(sum_odd)/all_count
sum_even_pos = float(sum_even)/all_count

rtp_big = float(big_count)/all_count * 1 - (1 - float(big_count)/all_count)
rtp_small = float(small_count)/all_count * 1 - (1 - float(small_count)/all_count)
rtp_triple = float(any_triple_count)/6/all_count * 150 - (1 - float(any_triple_count)/6/all_count)
rtp_any_triple = float(any_triple_count)/all_count * 24 - (1 - float(any_triple_count)/all_count)
rtp_one_dice = float(one_dice_count+double_dice_count*2+three_dice_count*3)/all_count-(1-(one_dice_count+double_dice_count+three_dice_count)/216.0)
rtp_double_dice = float(one_dice_count+double_dice_count*2+three_dice_count*3)/all_count-(1-(one_dice_count+double_dice_count+three_dice_count)/216.0)
rtp_three_dice = float(one_dice_count+double_dice_count*2+three_dice_count*3)/all_count-(1-(one_dice_count+double_dice_count+three_dice_count)/216.0)
rtp_pairs = float(pairs_count)/all_count * 8 - (1 - float(pairs_count)/all_count)
rtp_dominoes = float(dominoes_count)/all_count * 5 - (1 - float(dominoes_count)/all_count)
rtp_sum_4  = float(sum_dict[4])/all_count * 50 - (1 - float(sum_dict[4])/all_count)
rtp_sum_5  = float(sum_dict[5])/all_count * 18 - (1 - float(sum_dict[5])/all_count)
rtp_sum_6  = float(sum_dict[6])/all_count * 14 - (1 - float(sum_dict[6])/all_count)
rtp_sum_7  = float(sum_dict[7])/all_count * 12 - (1 - float(sum_dict[7])/all_count)
rtp_sum_8  = float(sum_dict[8])/all_count * 8 - (1 - float(sum_dict[8])/all_count)
rtp_sum_9  = float(sum_dict[9])/all_count * 6 - (1 - float(sum_dict[9])/all_count)
rtp_sum_10  = float(sum_dict[10])/all_count * 6 - (1 - float(sum_dict[10])/all_count)
rtp_sum_11  = float(sum_dict[11])/all_count * 6 - (1 - float(sum_dict[11])/all_count)
rtp_sum_12  = float(sum_dict[12])/all_count * 6 - (1 - float(sum_dict[12])/all_count)
rtp_sum_odd  = float(sum_odd)/all_count * 1 - (1 - float(sum_odd)/all_count)
rtp_sum_even  = float(sum_even)/all_count * 1 - (1 - float(sum_even)/all_count)

print("{:^15}|{:^15}|{:^15}|{:^15}".format('Event', 'Possibility', 'Pays', 'RTP'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('---------------', '---------------', '---------------', '---------------'))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Big', round(big_pos, 6), 1, round(rtp_big, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Small', round(small_pos, 6), 1, round(rtp_small, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Triples', round(triple_pos, 6), 150, round(rtp_triple, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Any Triples', round(any_triple_pos, 6), 24, round(rtp_any_triple, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('One Dice', round(one_dice_pos, 6), 1, round(rtp_one_dice, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Double Dice', round(double_dice_pos, 6), 2, round(rtp_double_dice, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Three Dice', round(three_dice_pos, 6), 3, round(rtp_three_dice, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Pairs', round(pairs_pos, 6), 8, round(rtp_pairs, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Dominoes', round(dominoes_pos, 6), 5, round(rtp_dominoes, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (4 or 17)', round(sum_4_pos, 6), 50, round(rtp_sum_4, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (5 or 16)', round(sum_5_pos, 6), 18, round(rtp_sum_5, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (6 or 15)', round(sum_6_pos, 6), 14, round(rtp_sum_6, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (7 or 14)', round(sum_7_pos, 6), 12, round(rtp_sum_7, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (8 or 13)', round(sum_8_pos, 6), 8, round(rtp_sum_8, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (9)', round(sum_9_pos, 6), 6, round(rtp_sum_9, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (10)', round(sum_10_pos, 6), 6, round(rtp_sum_10, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (11)', round(sum_11_pos, 6), 6, round(rtp_sum_11, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('3 Sum (12)', round(sum_12_pos, 6), 6, round(rtp_sum_12, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}".format('Odd', round(sum_odd_pos, 6), 1, round(rtp_sum_odd, 6)))
print("{:^15}|{:^15}|{:^15}|{:^15}\n".format('Even', round(sum_even_pos, 6), 1, round(rtp_sum_even, 6)))

print "It cost %f seconds" % (tEnd - tStart)