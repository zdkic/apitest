#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

typedef enum {NORMAL=1, BULL1TO6, BULL7TO9, BULLBULL, X_5P}hands_type;

typedef struct {
    long long all_count;
    long long x_5p_count;
    long long bullbull_count;
    long long bull_7_to_9_count;
    long long bull_1_to_6_count;
    long long normal_count;
} bet_stat;

bet_stat stat = {0};

int get_value(int card) {
    return (card < 10)?card:10;
}

int is_5p(int *hands) {
    int count = 0;
    for(int i = 0; i < 5; i++) {
        int v = hands[i];
        if(v == 11 || v == 12 || v == 13)
            count = count + 1;
    }
    return (count == 5);
}

int get_card_score(int card) {
    int score = 0;
    score = card;
    return (score);
}

hands_type get_hands_type(int *hands) {
    hands_type type = NORMAL;

    if(is_5p(hands)) {
        type = X_5P;
    } else {
        for(int i = 0; i < 5; i++) {
            for(int j = i + 1; j < 5; j++) {
                for(int k = j + 1; k < 5; k++) {
                    int sum = 0;
                    sum = get_value(hands[i])+ get_value(hands[j]) + get_value(hands[k]);
                    if(sum % 10 == 0) {
                        int rest = 0;
                        for(int x = 0; x < 5; x++) {
                            if(x != i && x != j && x != k)
                                rest += get_value(hands[x]);
                        }
                        rest = rest % 10;
                        if(rest == 0) {
                            type = BULLBULL;
                        } else if(rest >= 7 && rest <= 9 ) {
                            type = BULL7TO9;
                        } else if(rest >= 1 && rest <= 6 ) {
                            type = BULL1TO6;
                        }
                        break;
                    }
                }
            }
        }
    }

    return type;
}

int compute_score(int *hands) {
    int score = 0;
    hands_type type = NORMAL;

    for(int idx = 0; idx < 5; idx++){
        score = score + get_card_score(hands[idx]);
    }

    type = get_hands_type(hands);

    score = score + type * 1000000;

    return score;
}

void judge_card(int banker_score, int player_score, int partial_count) {
    hands_type win_card_type = NORMAL;

    if(player_score > banker_score) {
        win_card_type = (hands_type)floor(player_score / 1000000);
        if(win_card_type == X_5P)
            stat.x_5p_count = stat.x_5p_count + partial_count;
        else if(win_card_type == BULLBULL)
            stat.bullbull_count = stat.bullbull_count + partial_count;
        else if(win_card_type == BULL7TO9)
            stat.bull_7_to_9_count = stat.bull_7_to_9_count + partial_count;
        else if(win_card_type == BULL1TO6)
            stat.bull_1_to_6_count = stat.bull_1_to_6_count + partial_count;
        else
            stat.normal_count = stat.normal_count + partial_count;
    }
    if(player_score == banker_score) {
        win_card_type = (hands_type)floor(player_score / 1000000);
        if(win_card_type == X_5P)
            stat.x_5p_count = stat.x_5p_count + partial_count/2;
        else if(win_card_type == BULLBULL)
            stat.bullbull_count = stat.bullbull_count + partial_count/2;
        else if(win_card_type == BULL7TO9)
            stat.bull_7_to_9_count = stat.bull_7_to_9_count + partial_count/2;
        else if(win_card_type == BULL1TO6)
            stat.bull_1_to_6_count = stat.bull_1_to_6_count + partial_count/2;
        else
            stat.normal_count = stat.normal_count + partial_count/2;
    }
    stat.all_count = stat.all_count + partial_count;
}

void iterate_combination(int *cards) {
    int banker_hands[5];
    int player_hands[5];
    int p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;
    int banker_score = 0;
    int player_score = 0;
    int partial_count = 0;
    for(int a = 0; a < 13; a++) {
        p1 = cards[a];
        cards[a] = cards[a] - 1;
        for(int b = a; b < 13; b++) {
            p2 = cards[b];
            cards[b] = cards[b] - 1;
            for(int c = b; c < 13; c++) {
                p3 = cards[c];
                cards[c] = cards[c] - 1;
                for(int d = c; d < 13; d++) {
                    p4 = cards[d];
                    cards[d] = cards[d] - 1;
                    for(int e = d; e < 13; e++) {
                        p5 = cards[e];
                        cards[e] = cards[e] - 1;
                        banker_hands[0] = a + 1;
                        banker_hands[1] = b + 1;
                        banker_hands[2] = c + 1;
                        banker_hands[3] = d + 1;
                        banker_hands[4] = e + 1;
                        banker_score = compute_score(banker_hands);
                        for(int f = 0; f < 13; f++) {
                            p6 = cards[f];
                            cards[f] = cards[f] - 1;
                            for(int g = f; g < 13; g++) {
                                p7 = cards[g];
                                cards[g] = cards[g] - 1;
                                for(int h = g; h < 13; h++) {
                                    p8 = cards[h];
                                    cards[h] = cards[h] - 1;
                                    for(int i = h; i < 13; i++) {
                                        p9 = cards[i];
                                        cards[i] = cards[i] - 1;
                                        for(int j = i; j < 13; j++) {
                                            p10 = cards[j];
                                            cards[j] = cards[j] - 1;
                                            player_hands[0] = f + 1;
                                            player_hands[1] = g + 1;
                                            player_hands[2] = h + 1;
                                            player_hands[3] = i + 1;
                                            player_hands[4] = j + 1;
                                            player_score = compute_score(player_hands);
                                            int factor = 1;
                                            if(a != b)
                                                factor *= 2;
                                            if(b != c)
                                                factor *= 2;
                                            if(c != d)
                                                factor *= 2;
                                            if(d != e)
                                                factor *= 2;
                                            if(g != f)
                                                factor *= 2;
                                            if(h != g)
                                                factor *= 2;
                                            if(i != h)
                                                factor *= 2;
                                            if(j != i)
                                                factor *= 2;
                                            partial_count = factor * p1 * p2 * p3 * p4 * p5 * p6 * p7 * p8 * p9 * p10;
                                            judge_card(banker_score, player_score, partial_count);
                                            cards[j] = cards[j] + 1;
                                        }
                                        cards[i] = cards[i] + 1;
                                    }
                                    cards[h] = cards[h] + 1;
                                }
                                cards[g] = cards[g] + 1;
                            }
                            cards[f] = cards[f] + 1;
                        }
                        cards[e] = cards[e] + 1;
                    }
                    cards[d] = cards[d] + 1;
                }
                cards[c] = cards[c] + 1;
            }
            cards[b] = cards[b] + 1;
        }
        cards[a] = cards[a] + 1;
    }
}

int main() 
{
    time_t start, end;
    int cards[13];
    for(int idx=0; idx < 13; idx++) {
        cards[idx] = 4;
    }
    start = time(NULL);
    printf("%s\n",ctime(&start));
    iterate_combination(cards);
    end = time(NULL);

    printf("All count: %lli\n",stat.all_count);
    printf("5P count: %lli\n",stat.x_5p_count);
    printf("BullBull count: %lli\n",stat.bullbull_count);
    printf("Bull 7 to 9 count: %lli\n",stat.bull_7_to_9_count);
    printf("Bull 1 to 6 count: %lli\n",stat.bull_1_to_6_count);
    printf("Normal count: %lli\n",stat.normal_count);

    float x_5p_pos = ((float)stat.x_5p_count)/stat.all_count;
    float x_bullbull_pos = ((float)stat.bullbull_count)/stat.all_count;
    float x_bull_7_to_9_pos = ((float)stat.bull_7_to_9_count)/stat.all_count;
    float x_bull_1_to_6_pos = ((float)stat.bull_1_to_6_count)/stat.all_count;
    float x_normal_pos = ((float)stat.normal_count)/stat.all_count;

    printf("%15s|%15s\n", "Event", "Possibility");
    printf("%15s|%15s\n", "---------------", "---------------");
    printf("%15s|%15.6f\n", "5P", x_5p_pos);
    printf("%15s|%15.6f\n", "BullBull", x_bullbull_pos);
    printf("%15s|%15.6f\n", "Bull 7 to 9", x_bull_7_to_9_pos);
    printf("%15s|%15.6f\n", "Bull 1 to 6", x_bull_1_to_6_pos);
    printf("%15s|%15.6f\n", "Normal", x_normal_pos);

    float rtp_equal_bet = 0.5 * 0.95 - 0.5 * 1;
    printf("%15s|%15s\n", "Event", "Equal Bet RTP");
    printf("%15s|%15s\n", "---------------", "---------------");
    printf("%15s|%15.6f\n", "Player", rtp_equal_bet);

    float rtp_double_bet = 4.75 * x_5p_pos + 
                           2.85 * x_bullbull_pos + 
                           1.9 * x_bull_7_to_9_pos + 
                           0.95 * x_bull_1_to_6_pos + 
                           0.95 * x_normal_pos - 
                           5 * x_5p_pos - 
                           3 * x_bullbull_pos - 
                           2 * x_bull_7_to_9_pos -
                           1 * x_bull_1_to_6_pos - 
                           1 * x_normal_pos;

    printf("%15s|%15s\n", "Event", "Double Bet RTP");
    printf("%15s|%15s\n", "---------------", "---------------");
    printf("%15s|%15.6f\n", "Player", rtp_double_bet);

    printf("It cost %.1f second\n", difftime(end, start));
} 