#!/bin/bash

BASE_DIR=".."
TREND_DIR="${BASE_DIR}/report/trend"
DATA_DIR="${TREND_DIR}/data"

function parse_time {
    str=$1
    sec=0
    case ${str} in
        *µs)
        us_str=${str::-2}
        sec=`echo "scale=4;${us_str} / 1000000"|bc`
        ;;
        *ms)
        ms_str=${str::-2}
        sec=`echo "scale=2;${ms_str} / 1000"|bc`
        ;;
        *m*s)
        str=${str::-1}
        m_str=`echo ${str}|awk -F'm' '{print $(NF-1)}'`
        sec_str=`echo ${str}|awk -F'm' '{print $(NF)}'`
        sec=`echo "scale=2;${m_str} * 60 + ${sec_str}"|bc`
        ;;
        *s)
        sec=${str::-1}
        ;;
    esac
    echo "${sec}" 
}

function parse_percent {
    str=$1
    str=${str::-1}
    percent=`echo "scale=2;${str}/100"|bc`
    echo "${percent}"
}

function parse_request_rate {
    request_count=$1
    request_rate=`echo "scale=2;${request_count}/180"|bc`
    echo "${request_rate}"
}

function purge_data_by_date {
    datafile=$1
    create_date=$2
    TMPFILE="/tmp/${datafile}.dat"
    grep -v ${create_date} ${DATA_DIR}/${datafile} > ${TMPFILE}
    mv ${TMPFILE} ${DATA_DIR}/${datafile}
}

function purge_data {
    datafile=$1
    create_date=$2
    keep_days=$3
    diff=$((`date -d "${keep_days}" '+%s'`-`date -d "${create_date}" '+%s'`))
    if [ ${diff} -gt 0 ]; then
        purge_data_by_date ${datafile} ${create_date}
    fi
}

function parse_report {
    load_report=$1
    create_date=`echo ${load_report}|awk -F'_' '{print $(NF-1)}'`
    for script in `ls -v ${BASE_DIR}/load/*_*.js`; do
        script_name=$(basename -- "${script}")
        datafile="${script_name%.*}.dat"
        touch ${DATA_DIR}/${datafile}
        http_req_duration=`cat ${load_report}|grep 'k6 result' -B 2 -A 22|grep ${script_name} -A 22|grep http_req_duration|awk '{print $2}'|awk -F'=' '{print $(NF)}'`
        http_req_duration=`parse_time ${http_req_duration}`
        errors=`cat ${load_report}|grep 'k6 result' -B 2 -A 22|grep ${script_name} -A 22|grep errors|awk '{print $3}'`
        if [ -z ${errors} ];then
            errors=0
        fi
        checks=`cat ${load_report}|grep 'k6 result' -B 2 -A 22|grep ${script_name} -A 22|grep checks|awk '{print $2}'|awk -F'=' '{print $(NF)}'`
        if [ -z ${checks} ];then
            checks="0%"
        fi
        checks=`parse_percent ${checks}`
        vus_max=`cat ${load_report}|grep 'k6 result' -B 2 -A 22|grep ${script_name} -A 22|grep vus_max|awk '{print $2}'|awk -F'=' '{print $(NF)}'`
        if [ -z ${vus_max} ];then
            vus_max=0
        fi
        request_count=`cat ${load_report}|grep 'k6 result' -B 2 -A 22|grep ${script_name} -A 22|grep http_reqs|awk '{print $2}'`
        if [ -z ${request_count} ];then
            request_rate=0
        else
            request_rate=`parse_request_rate ${request_count}`
        fi
        if [ -f ${DATA_DIR}/${datafile} ]; then
            purge_data_by_date ${datafile} ${create_date}
        fi
        echo "${create_date} ${http_req_duration} ${errors} ${checks} ${vus_max} ${request_rate}" >> ${DATA_DIR}/${datafile}
    done
}

mkdir -p ${DATA_DIR}

# compose trend data
for filename in `ls -v ${BASE_DIR}/report/load_report_*.md`; do
    parse_report ${filename}
done

# purge old trend data
for filename in `ls -v ${BASE_DIR}/report/trend/data/*.dat`; do
    while read line; do
        create_date=`echo ${line}|awk '{print $1}'`
        datafile=$(basename -- "${filename}")
        purge_data ${datafile} ${create_date} '28 days ago'
    done < ${filename}
done
