#!/bin/bash

. ./env.sh

CONFIG='once'
USERS=1

REPORT="../report/functional_test_`date '+%Y-%m-%d_%H%M'`.md"
PIE_CHART=$(mktemp ../report/images/img_XXXXXXXX_`date '+%Y-%m-%d_%H%M'`.png)
TEST_COUNT=0
FAIL_COUNT=0

function write_report {
  line=$1
  space=$2
  result=''
  for i in $(seq 1 ${space});
  do
    result+=' '
  done
  result+='- '
  result+=${line}
  echo "${result}"|tee -a ${REPORT} 
}

function run_test_script {
  desc=`head -n 1 $1 | awk '{print substr($0,3)}'`
  script_name=$(basename -- "$1")
  write_report "**${script_name}**" 0
  write_report "${desc}" 2
  TMP_JSON="/tmp/api_${script_name}.json"
  TMP_OUTPUT="/tmp/api_${script_name}.out"
  TMP_ERROR="/tmp/api_${script_name}.err"
  FUNCTIONAL_LOG="../report/log/functional_log_${script_name}_`date '+%Y-%m-%d_%H%M'`.md"

  # run k6
  rm -f ${TMP_JSON} ${TMP_OUTPUT} ${TMP_ERROR}
  k6 run -o json=${TMP_JSON} -e DEBUG=1 -e FUNCTIONAL_TEST=1 $1 2>${TMP_ERROR}|tee ${TMP_OUTPUT}

  # result
  write_report '接口功能' 2
  echo '' >> ${REPORT}
  start_pos=`grep -n 'starting' ${TMP_OUTPUT}|head -n1|cut -d: -f 1`
  start_pos=$(($start_pos+1))
  end_pos=`grep -n checks ${TMP_OUTPUT}|head -n1|cut -d: -f 1`
  end_pos=$(($end_pos-1))
  echo '     ```bash' >> ${REPORT}
  sed -n ${start_pos},${end_pos}p ${TMP_OUTPUT} >> ${REPORT}
  echo '     ```' >> ${REPORT}

  # Functional log
  write_report "Functional log [here](${FUNCTIONAL_LOG})" 2
  # write functional log
  echo '' >> ${FUNCTIONAL_LOG}
  echo '```bash' >> ${FUNCTIONAL_LOG}
  python ./parse_errlog.py -f ${script_name} >> ${FUNCTIONAL_LOG}
  echo '```' >> ${FUNCTIONAL_LOG}
  echo '' >> ${FUNCTIONAL_LOG}

  # Update test/error counter
  TEST_COUNT=$((${TEST_COUNT}+1))
  cat ${TMP_OUTPUT}|grep errors > /dev/null
  if [ $? -eq 0 ]; then
    FAIL_COUNT=$((${FAIL_COUNT}+1))
  fi
}

echo '' > ${REPORT}

echo '### Environment ###' |tee -a ${REPORT}
echo '' |tee -a ${REPORT}
echo "Host | sn " |tee -a ${REPORT}
echo "--- | --- " |tee -a ${REPORT}
echo "${HOST} | ${SN} " |tee -a ${REPORT}

echo '### Testing Agent ###' |tee -a ${REPORT}
echo " Ip | OS | Testing Date" |tee -a ${REPORT}
echo "--- | --- | --- " |tee -a ${REPORT}
echo "${AGENT} | `lsb_release -sd` | `date +"%Y/%m/%d %H:%M:%S"`" |tee -a ${REPORT}

echo '### Testing Result ###' |tee -a ${REPORT}
smoke_result=`nosetests -v smoke.py 2>&1`
echo ${smoke_result}|grep FAILED
if [ $? == 0 ]; then
  echo "- 未能通過冒煙測試!" |tee -a ${REPORT}
  exit -1
else
  echo "- 通過冒煙測試" |tee -a ${REPORT}
fi

echo "- 測試結果" |tee -a ${REPORT}
echo '' >> ${REPORT}
echo "     <img src=\"${PIE_CHART}\"  width=\"400\" height=\"300\">" |tee -a ${REPORT}

for filename in `ls -v ../functional/*.js`; do
  run_test_script ${filename}
done

PASS_COUNT=$((${TEST_COUNT}-${FAIL_COUNT}))

JSON_STRING=$( /snap/bin/jq -n \
            --arg pass "${PASS_COUNT}" \
            --arg fail "${FAIL_COUNT}" \
            '{pass: $pass, fail: $fail}' )

python pie_report.py -j "${JSON_STRING}" -o ${PIE_CHART}

echo 'done!'