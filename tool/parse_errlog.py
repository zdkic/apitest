import sys, getopt
import os
import re

def main(argv):
    function = ''
    log_file = ''
    try:
        opts, args = getopt.getopt(argv,"hf:",["function="])
    except getopt.GetoptError:
        print 'parse_errlog.py -f <function>'
        sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
         print 'parse_errlog.py -f <function>'
         sys.exit()
      elif opt in ("-f", "--function"):
         function = arg
    log_file = "/tmp/api_" + function + ".err"
    if(not os.path.isfile(log_file)):
        log_file="/tmp/load_" + function + ".err"
    with open(log_file) as f:
        content = f.readlines()
        for line in content:
            line = re.sub(r"time=.*msg=", "", line)
            if 'Running i=' in line:
                continue
            cmd = "echo " + line
            os.system(cmd)

if __name__ == "__main__":
   main(sys.argv[1:])
    