#!/bin/bash

CONFIG_FILE="../lib/config.js"

function getSiteValue {
    config=$1
    key=$2
    cat ${config} |grep ${key}|head -n 1|awk -F ": " '{print $2}'|sed 's/,//g'|sed 's/[^"]*"\([^"]*\).*/\1/'
}

function get_grafana_cookies() {
  user=$1
  password=$2
  tmp_file=$(mktemp /tmp/grafana.XXXXXXXX)
  curl -D ${tmp_file} -d "{\"User\":\"${user}\",\"email\":\"\",\"Password\":\"${password}\"}" -H 'Content-type: application/json; charset=utf-8' http://localhost:3000/login -o /dev/null 2>/dev/null
  GRAFANA_USER=`cat ${tmp_file}|grep grafana_user=|awk -F";" '{print $1}'|awk -F"=" '{print $2}'`
  GRAFANA_REMEMBER=`cat ${tmp_file}|grep grafana_remember=|awk -F";" '{print $1}'|awk -F"=" '{print $2}'`
  GRAFANA_SESS=`cat ${tmp_file} |grep grafana_sess=|awk -F";" '{print $1}'|awk -F"=" '{print $2}'`
  rm -f ${tmp_file}
}

function get_grafana_image() {
  url=$1
  image_file=$2
  curl ${url} -H "Cookie: grafana_user=${GRAFANA_USER}; grafana_remember=${GRAFANA_REMEMBER}; grafana_sess=${GRAFANA_SESS}" --output ${image_file}
}

HOST=$( getSiteValue ${CONFIG_FILE} "{{host}}" )
SN=$( getSiteValue ${CONFIG_FILE} "{{sn}}" )
LOGINID="$( getSiteValue ${CONFIG_FILE} "{{user_prefix}}" )1"
PASSWORD=$( getSiteValue ${CONFIG_FILE} "{{password}}" )
PAYPWD=$( getSiteValue ${CONFIG_FILE} "{{paypwd}}" )

AGENT=$(hostname -I | awk '{print $1}')
GRAFANA_ADMIN="admin"
GRAFANA_PASSWORD="admin123"

GRAFANA_USER=""
GRAFANA_REMEMBER=""
GRAFANA_SESS=""

get_grafana_cookies ${GRAFANA_ADMIN} ${GRAFANA_PASSWORD}

echo "HOST=${HOST}"
echo "SN=${SN}"
echo "LOGINID=${LOGINID}"
echo "PASSWORD=${PASSWORD}"
echo "PAYPWD=${PAYPWD}"
echo "AGENT=${AGENT}"
echo "GRAFANA_ADMIN=${GRAFANA_ADMIN}"
echo "GRAFANA_PASSWORD=${GRAFANA_PASSWORD}"
echo "GRAFANA_USER=${GRAFANA_USER}"
echo "GRAFANA_REMEMBER=${GRAFANA_REMEMBER}"
echo "GRAFANA_SESS=${GRAFANA_SESS}"

