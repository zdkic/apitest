var express = require('express');
var router = express.Router();

var apis = require('../public/js/api');

const GREEN = 'color:green;';
const RED = 'color:red;';

function probe(env, res) {
  let get_status_text = (name, response) => {
    let status = response.error == null?'正常':'異常';
    if(name == "sn_ip_locate_check") {
      status = (response.error == null && response.result.isBlock == 0)?'正常':'異常';
    }
    if(name == "sn_maintain") {
      status = (response.error == null && response.result.flag != 1)?'正常':'維護中';
    }
    return status;
  }
  var d = {};
  d["host"] = env["host"];
  d["userid"] = env["userid"];
  try {
    api_status = apis.get_api_status(env);
    items = []
    for (const [k, v] of Object.entries(api_status)) {
      let status = get_status_text(k, api_status[k].response);
      items.push({
        "name": k.replace(/_/g,"."),
        "desc": api_status[k].desc,
        "status": status,
        "color": api_status[k].response.error == null?GREEN:RED
      });
    }
    d["items"] = items;
  } catch(e) {
    console.error(e.message);
  }
  return d;
}

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.render('index');
});

router.post('/status', function(req, res, next) {
  var env = {};
  env.host = req.body.host;
  env.userid = req.body.name;
  env.password = req.body.password;
  env.captchaKey = req.body.captchaKey;
  env.captchaCode = req.body.captchaCode;
  var d = probe(env, res);
  return res.render('status', d);
});

module.exports = router;
