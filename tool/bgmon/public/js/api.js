const request = require('sync-request')

let id_generator = () => {
  let id = Date.now()
  id += '.'
  let possible = "0123456789"
  for (let idx = 0; idx < 16; idx++) {
    id += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return id
}

let random_string_generator = () => {
  let str = ""
  const charset = "0123456789abcdefghijklmnopqrstuvwxyz"
  for (let a = 0; 32 > a; a++)
    str += charset.charAt(Math.floor(36 * Math.random()))
  return str
}

function SHA1() {
  function b(a, b) {
    a[b >> 5] |= 128 << 24 - b % 32
    a[(b + 64 >> 9 << 4) + 15] = b
    b = Array(80)
    for (var c = 1732584193, e = -271733879, g = -1732584194, k = 271733878, p = -1009589776, t = 0; t < a.length; t += 16) {
      for (var r = c, W = e, N = g, Ca = k, na = p, A = 0; 80 > A; A++) {
        if (16 > A) b[A] = a[t + A]
        else {
          var G = b[A - 3] ^ b[A - 8] ^ b[A - 14] ^ b[A - 16]
          b[A] = G << 1 | G >>> 31
        }
        var G = c << 5 | c >>> 27;
        var Q = 20 > A ? e & g | ~e & k : 40 > A ? e ^ g ^ k : 60 > A ? e & g | e & k | g & k : e ^ g ^ k
        G = d(d(G, Q), d(d(p, b[A]), 20 > A ? 1518500249 : 40 > A ? 1859775393 : 60 > A ? -1894007588 : -899497514))
        p = k
        k = g
        g = e << 30 | e >>> 2
        e = c
        c = G
      }
      c = d(c, r)
      e = d(e, W)
      g = d(g, N)
      k = d(k, Ca)
      p = d(p, na)
    }
    return [c, e, g, k, p]
  }

  function d(a, b) {
    var c = (a & 65535) + (b & 65535)
    return (a >> 16) + (b >> 16) + (c >> 16) << 16 | c & 65535
  }

  function c(a) {
    for (var b = [], c = (1 << r) - 1, d = 0; d < a.length * r; d += r) b[d >> 5] |= (a.charCodeAt(d / r) & c) << 24 - d % 32
    return b
  }
  this.hash = function (d) {
    d = b(c(d), d.length * r)
    for (var e = a ? "0123456789ABCDEF" : "0123456789abcdef", m = "", h = 0; h < 4 * d.length; h++) m += e.charAt(d[h >> 2] >> 8 * (3 - h % 4) + 4 & 15) + e.charAt(d[h >> 2] >> 8 * (3 - h % 4) & 15)
    return m
  };
  this.hashToBase64 = function (a) {
    a = b(c(a), a.length * r)
    for (var d =
        "", m = 0; m < 4 * a.length; m += 3)
      for (var h = (a[m >> 2] >> 8 * (3 - m % 4) & 255) << 16 | (a[m + 1 >> 2] >> 8 * (3 - (m + 1) % 4) & 255) << 8 | a[m + 2 >> 2] >> 8 * (3 - (m + 2) % 4) & 255, l = 0; 4 > l; l++) d = 8 * m + 6 * l > 32 * a.length ? d + e : d + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(h >> 6 * (3 - l) & 63)
    return d + "="
  };
  var a = 0,
    e = "",
    r = 8
}

function sendRequest(env, json, args = {}) {
  let body = null
  json.id = id_generator()
  for (const [k, v] of Object.entries(args)) {
    if (json.params.hasOwnProperty(k))
      json.params[k] = v
  }
  body = JSON.stringify(json)
  let payload = JSON.parse(body)
  console.log('[' + json.method + '] -> ')
  console.log(JSON.stringify(payload, null, 2))
  let apiUrl = env["host"] + "/zb-cloud/api/"
  let mainApiUrl = env["host"] + "/mt-cloud/api/"
  let url = env["host"] + "/cloud/api/";
  if(json.method == 'sn.ip.locate.check')
    url = apiUrl
  if(json.method == 'sn.maintain')
    url = mainApiUrl
  try {
      let res = request('POST', url + json.method, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: encodeURIComponent(JSON.stringify(json))
      })
      body = JSON.parse(res.body)
      console.log('[' + json.method + '] <- ')
      console.log(JSON.stringify(body, null, 2))
  } catch (e) {
    console.error(e.toString())
    throw e;
  }
  return body
}

function sn_ip_locate_check(env, args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.ip.locate.check",
    "params": {
      "sn": "{{sn}}"
    }
  }
  return sendRequest(env, json, args)
}

function sn_maintain(env, args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.maintain",
    "params": {
      "moduleId": "{{moduleId}}"
    }
  }
  return sendRequest(env, json, args)
}

function host_info(env, args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "host.info",
    "params": {
      "domain": "{{domain}}"
    }
  }
  return sendRequest(env, json, args)
}

function sn_info(env, args) {
  let json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.info",
    "params": {
      "domain": "{{domain}}"
    }
  }
  return sendRequest(env, json, args)
}

function auth_mobile_login(env, args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.mobile.login",
    "params": {
      "sn": "{{sn}}",
      "loginId": "{{loginId}}",
      "withAuth": "1",
      "withProfile": "1",
      "saltedPassword": "{{saltedPassword}}",
      "salt": "{{salt}}",
      "domain": "{{domain}}",
      "captchaKey":"{{captchaKey}}",
      "captchaCode":"{{captchaCode}}"
    },
    "jsonrpc": "2.0"
  }
  json.params.salt = random_string_generator()
  let sha1 = new SHA1()
  let r1 = sha1.hashToBase64(args.password)
  let r2 = r1 + json.params.salt
  json.params.saltedPassword = sha1.hashToBase64(r2)
  return sendRequest(env, json, args)
}

function auth_mobile_trial_login(env, args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.mobile.trial.login",
    "params": {
      "sn": "{{sn}}"
    },
    "jsonrpc": "2.0"
  }
  return sendRequest(env, json, args)
}

function auth_online_logout(env, args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.online.logout",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  }
  return sendRequest(env, json, args)
}

function auth_session_validate(env, args) {
  let json = {
    "id": "{{id}}",
    "method": "auth.session.validate",
    "params": {
      "sessionId": "{{sessionId}}",
      "withAmount": "0",
      "withUser": "0"
    },
    "jsonrpc": "2.0"
  }
  return sendRequest(env, json, args)
}

function user_balance_get(env, args) {
  let json = {
    "id": "{{id}}",
    "method": "user.balance.get",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  };
  return sendRequest(env, json, args);
}

function get_api_status(env) {
  let api_status = {}
  let domain = env["host"].replace(/^(?:https?:\/\/)?/i, "").split('/')[0]
  let sn = null
  let sessionId = null

  api_status["host_info"] = {
    "desc":"獲取域名信息",
    "response": host_info(env, {
      "domain": domain
    })
  }
  sn = api_status["host_info"].response.result.sn

  api_status["sn_ip_locate_check"] = {
    "desc": "檢查所在地IP",
    "response":  sn_ip_locate_check(env, {
      "sn": sn
    })
  }
  api_status["sn_maintain"] = {
    "desc": "獲取維護信息",
    "response":  sn_maintain(env, {
      "moduleId": "101"
    })
  }
  
  api_status["sn_info"] = {
    "desc":"獲取廳主（簡化）信息",
    "response": sn_info(env, {
      "domain": domain
    })
  }
  
  api_status["auth_mobile_trial_login"] = {
    "desc":"試玩登錄",
    "response": auth_mobile_trial_login(env, {
      "sn": sn
    })
  }
  
  api_status["auth_mobile_login"] = {
    "desc":"手機客戶端登錄",
    "response": auth_mobile_login(env, {
      "sn": sn,
      "loginId": env["userid"],
      "password": env["password"],
      "domain": domain,
      "captchaKey":env["captchaKey"],
      "captchaCode":env["captchaCode"]
    })
  }

  sessionId = api_status["auth_mobile_login"].response.result.sessionId
  
  api_status["auth_session_validate"] = {
    "desc":"驗證會話有效性",
    "response": auth_session_validate(env, {
      "sessionId": sessionId
    })
  }

  api_status["user_balance_get"] = {
    "desc":"用戶餘額查詢",
    "response": user_balance_get(env, {
      "sessionId": sessionId
    })
  }
  
  api_status["auth_online_logout"] = {
    "desc":"用戶登出",
    "response": auth_online_logout(env, {
      "sessionId": sessionId
    })
  }
  
  return api_status
}

module.exports = { get_api_status }