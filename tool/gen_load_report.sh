#!/bin/bash

. ./env.sh

CONFIG='once'
USERS=1
MAX_VU=1

REPORT="../report/load_report_`date '+%Y-%m-%d_%H%M'`.md"

INFLUXDB="http://${AGENT}:8086/myk6db"
GRAFANA_URL="http://${AGENT}:3000/d/BSPN9Jhmk/k6-load-testing-results"
GRAFANA_RENDER_URL="http://${AGENT}:3000/render/d-solo/BSPN9Jhmk/k6-load-testing-results"

function write_report {
  line=$1
  space=$2
  result=''
  for i in $(seq 1 ${space});
  do
    result+=' '
  done
  result+='- '
  result+=${line}
  echo "${result}"|tee -a ${REPORT} 
}

function run_test_script {
  desc=`head -n 1 $1 | awk '{print substr($0,3)}'`
  script_name=$(basename -- "$1")
  write_report "${script_name} <a name="${script_name}"></a>" 0
  write_report "${desc}" 2
  TMP_JSON="/tmp/load_${script_name}.json"
  TMP_OUTPUT="/tmp/load_${script_name}.out"
  TMP_ERROR="/tmp/load_${script_name}.err"
  ERROR_LOG="../report/log/${script_name}_error_log_`date '+%Y-%m-%d_%H%M'`.md"
 
  # run k6
  rm -f ${TMP_JSON} ${TMP_OUTPUT} ${TMP_ERROR}
  start_time=`date +%s`'000'
  k6 run -o influxdb=${INFLUXDB} -o json=${TMP_JSON} -e CONFIG=${CONFIG} -e USERS=${USERS} -e MAX_VU=${MAX_VU} $1 2>${TMP_ERROR}|tee ${TMP_OUTPUT}
  end_time=$((`date +%s`+10))
  end_time+='000'

  # result
  write_report 'k6 result' 2
  echo '' >> ${REPORT}
  start_pos=`grep -n checks ${TMP_OUTPUT}|cut -d: -f 1`
  end_pos=`wc -l ${TMP_OUTPUT}|awk '{print $1}'`
  echo '     ```bash' >> ${REPORT}
  sed -n ${start_pos},${end_pos}p ${TMP_OUTPUT} >> ${REPORT}
  echo '     ```' >> ${REPORT}

  # Virtual Users
  write_report "Virtual Users" 2
  virutal_users_url="${GRAFANA_RENDER_URL}?orgId=1&from=${start_time}&to=${end_time}&var-Measurement=All&var-URL=All&panelId=1&width=400&height=200&tz=Asia%2FTaipei"
  image_file=$(mktemp ../report/images/img_XXXXXXXX_`date '+%Y-%m-%d_%H%M'`.png)
  get_grafana_image ${virutal_users_url} ${image_file}
  echo '' >> ${REPORT}
  echo "     ![](${image_file})" >> ${REPORT}

  # Errors Per Second
  cat ${TMP_ERROR}|grep -i error
  has_error=$?
  if [ ${has_error} -eq 0 ]; then
    write_report 'Errors Per Second' 2
    errors_per_second_url="${GRAFANA_RENDER_URL}?orgId=1&from=${start_time}&to=${end_time}&var-Measurement=All&var-URL=All&panelId=7&width=400&height=200&tz=Asia%2FTaipei"
    image_file=$(mktemp ../report/images/img_XXXXXXXX_`date '+%Y-%m-%d_%H%M'`.png)
    get_grafana_image ${errors_per_second_url} ${image_file}
    echo '' >> ${REPORT}
    echo "     ![](${image_file})" >> ${REPORT}
    echo '' >> ${REPORT}
    write_report "Error log [here](${ERROR_LOG})" 2
    # write error log
    echo '' >> ${ERROR_LOG}
    echo '```bash' >> ${ERROR_LOG}
    python ./parse_errlog.py -f ${script_name} >> ${ERROR_LOG}
    echo '```' >> ${ERROR_LOG}
    echo '' >> ${ERROR_LOG}
  fi

  # Requests per Second
  write_report "Requests per Second" 2
  requests_per_second_url="${GRAFANA_RENDER_URL}?orgId=1&from=${start_time}&to=${end_time}&var-Measurement=All&var-URL=All&panelId=17&width=400&height=200&tz=Asia%2FTaipei"
  image_file=$(mktemp ../report/images/img_XXXXXXXX_`date '+%Y-%m-%d_%H%M'`.png)
  get_grafana_image ${requests_per_second_url} ${image_file}
  echo '' >> ${REPORT}
  echo "     ![](${image_file})" >> ${REPORT}

  # http_req_duration
  write_report "http_req_duration" 2
  http_req_duration_url="${GRAFANA_RENDER_URL}?orgId=1&from=${start_time}&to=${end_time}&var-Measurement=All&var-URL=All&panelId=5&width=400&height=200&tz=Asia%2FTaipei"
  image_file=$(mktemp ../report/images/img_XXXXXXXX_`date '+%Y-%m-%d_%H%M'`.png)
  get_grafana_image ${http_req_duration_url} ${image_file}
  echo '' >> ${REPORT}
  echo "     ![](${image_file})" >> ${REPORT}

  # grafana url
  write_report 'grafana url(admin/admin123)' 2
  write_report "<${GRAFANA_URL}?orgId=1&from=${start_time}&to=${end_time}>" 5

  sleep 2
}

if [ $# == 1 ]; then
  CONFIG=$1
fi
if [ $# == 2 ]; then
  CONFIG=$1
  USERS=$2
fi
if [ $# == 3 ]; then
  CONFIG=$1
  USERS=$2
  MAX_VU=$3
fi

echo '' > ${REPORT}

echo '### Environment ###' |tee -a ${REPORT}
echo '' |tee -a ${REPORT}
echo "Host | sn " |tee -a ${REPORT}
echo "--- | --- " |tee -a ${REPORT}
echo "${HOST} | ${SN} " |tee -a ${REPORT}

echo '### Testing Agent ###' |tee -a ${REPORT}
echo " Ip | OS | Testing Date" |tee -a ${REPORT}
echo "--- | --- | --- " |tee -a ${REPORT}
echo "${AGENT} | `lsb_release -sd` | `date +"%Y/%m/%d %H:%M:%S"`" |tee -a ${REPORT}

echo '### Parameter ###' >> ${REPORT}
echo '' |tee -a ${REPORT}
echo "Config | Users | MAX_VU " |tee -a ${REPORT}
echo "--- | --- | --- " |tee -a ${REPORT}
echo "${CONFIG} | ${USERS} | ${MAX_VU}" |tee -a ${REPORT}

echo '### Grafana ###' |tee -a ${REPORT}
echo '' |tee -a ${REPORT}
echo "URL | Username | Password" |tee -a ${REPORT}
echo "--- | --- | ---" |tee -a ${REPORT}
echo "http://${AGENT}:3000 | ${GRAFANA_ADMIN} | ${GRAFANA_PASSWORD}" |tee -a ${REPORT}

echo "### Network ###" |tee -a ${REPORT}
data=`node ./check_host.js ${SN} ${LOGINID} ${PASSWORD}`
response=`curl -s "${HOST}/cloud/api/auth.mobile.login" -H 'Content-Type: application/x-www-form-urlencoded' --data "json=$data"`
if [ $? != 0 ]; then
  echo "- Not Connected!(${HOST})" |tee -a ${REPORT}
  exit -1
fi
error=`echo $response|jq '.error'`
if [ ${error} != null ]; then
  echo "- Not Connected!(${HOST})" |tee -a ${REPORT}
  exit -1
fi
echo "- Connected(${HOST})" |tee -a ${REPORT}

echo '' >> ${REPORT}

echo '### Testing Result ###' |tee -a ${REPORT}
idx=1
for filename in `ls -v ../load/*_*.js`; do
  script_desc=`head -n 1 ${filename} | awk '{print substr($0,3)}'`
  script_name=$(basename -- "${filename}")
  echo "${idx}. [${script_desc}](#${script_name})" |tee -a ${REPORT}
  idx=$(($idx+1))
done

echo '' >> ${REPORT}

for filename in `ls -v ../load/*_*.js`; do
  run_test_script ${filename}
done

echo 'done!'