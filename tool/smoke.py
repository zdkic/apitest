# -*- coding: utf-8 -*-
__author__ = 'john.cheng'
from nose.tools import assert_equal
from nose.tools import with_setup
from urllib import parse

import unittest
import js2py
import json
import requests
import time
import random

host = 'http://ae.bg1207.com'
domain = 'ae.bg1207.com'
headers = {
  "Content-Type": "application/x-www-form-urlencoded"
}
loginId = "bgqajohnload6"
password = "123456"
payPasword = "1234"
login_data = {}

js_code = """
function SHA1() {
  function b(a, b) {
    a[b >> 5] |= 128 << 24 - b % 32;
    a[(b + 64 >> 9 << 4) + 15] = b;
    b = Array(80);
    for (var c = 1732584193, e = -271733879, g = -1732584194, k = 271733878, p = -1009589776, t = 0; t < a.length; t += 16) {
      for (var r = c, W = e, N = g, Ca = k, na = p, A = 0; 80 > A; A++) {
        if (16 > A) b[A] = a[t + A];
        else {
          var G = b[A - 3] ^ b[A - 8] ^ b[A - 14] ^ b[A - 16];
          b[A] = G << 1 | G >>> 31
        }
        var G = c << 5 | c >>> 27;
        var Q = 20 > A ? e & g | ~e & k : 40 > A ? e ^ g ^ k : 60 > A ? e & g | e & k | g & k : e ^ g ^ k;
        G = d(d(G, Q), d(d(p, b[A]), 20 > A ? 1518500249 : 40 > A ? 1859775393 : 60 > A ? -1894007588 : -899497514));
        p = k;
        k = g;
        g = e << 30 | e >>> 2;
        e = c;
        c = G
      }
      c = d(c, r);
      e = d(e, W);
      g = d(g, N);
      k = d(k, Ca);
      p = d(p, na)
    }
    return [c, e, g, k, p]
  }

  function d(a, b) {
    var c = (a & 65535) + (b & 65535);
    return (a >> 16) + (b >> 16) + (c >> 16) << 16 | c & 65535
  }

  function c(a) {
    for (var b = [], c = (1 << r) - 1, d = 0; d < a.length * r; d += r) b[d >> 5] |= (a.charCodeAt(d / r) & c) << 24 - d % 32;
    return b
  }
  this.hash = function (d) {
    d = b(c(d), d.length * r);
    for (var e = a ? "0123456789ABCDEF" : "0123456789abcdef", m = "", h = 0; h < 4 * d.length; h++) m += e.charAt(d[h >> 2] >> 8 * (3 - h % 4) + 4 & 15) + e.charAt(d[h >> 2] >> 8 * (3 - h % 4) & 15);
    return m
  };
  this.hashToBase64 = function (a) {
    a = b(c(a), a.length * r);
    for (var d =
      "", m = 0; m < 4 * a.length; m += 3)
      for (var h = (a[m >> 2] >> 8 * (3 - m % 4) & 255) << 16 | (a[m + 1 >> 2] >> 8 * (3 - (m + 1) % 4) & 255) << 8 | a[m + 2 >> 2] >> 8 * (3 - (m + 2) % 4) & 255, l = 0; 4 > l; l++) d = 8 * m + 6 * l > 32 * a.length ? d + e : d + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(h >> 6 * (3 - l) & 63);
    return d + "="
  };
  var a = 0,
    e = "",
    r = 8
}

function randomcharstring() {
  let str = "";
  const charset = "0123456789abcdefghijklmnopqrstuvwxyz";
  for (let a = 0; 32 > a; a++)
    str += charset.charAt(Math.floor(36 * Math.random()));
  return str;
}

function get_salted_password(password) {
  let sha1 = new SHA1();
  let salt = randomcharstring();
  let r1 = sha1.hashToBase64(password);
  let r2 = r1 + salt;
  let saltedPassword = sha1.hashToBase64(r2);
  return {
    "saltedPassword": saltedPassword,
    "salt": salt
  };
}

function get_paypwd_hash(userId, paypwd) {
  let sha1 = new SHA1();
  let r1 = sha1.hashToBase64(paypwd)
  let r2 = userId + r1;
  let hash = sha1.hashToBase64(r2);
  return hash;
}
"""
js = js2py.EvalJs({})
js.execute(js_code)

def send_request(api_json, args=[]):
  api_json["id"] = str(time.time())
  for k, v in args.items():
    if k in api_json["params"]:
      api_json["params"][k] = v
  print(api_json)
  json_str = json.dumps(api_json, separators=(',', ':'))
  payload = "json=" + parse.quote(json_str)
  print("=>")
  resp = requests.post(host + '/cloud/api/' + api_json["method"], data = payload, headers=headers)
  print(resp.json())
  print("\n")
  return resp.json()

def auth_mobile_login(args):
  api_json = {
    "id": "{{id}}",
    "method": "auth.mobile.login",
    "params": {
      "sn": "{{sn}}",
      "loginId": "{{loginId}}",
      "withAuth": "1",
      "withProfile": "1",
      "saltedPassword": "{{saltedPassword}}",
      "salt": "{{salt}}",
      "domain": "{{domain}}"
    },
    "jsonrpc": "2.0"
  }
  salted_password_ret = js.get_salted_password(args["password"])
  api_json["params"]["saltedPassword"] = salted_password_ret["saltedPassword"]
  api_json["params"]["salt"] = salted_password_ret["salt"]
  api_json["params"]["domain"] = domain
  return send_request(api_json, args)

def auth_mobile_trial_login(args={}):
  api_json = {
      "id": "{{id}}",
      "jsonrpc": "2.0",
      "method": "auth.mobile.trial.login",
      "params": {
          "sn": "{{sn}}"
      }
  }
  return send_request(api_json, args)

def auth_online_logout(args={}):
  api_json = {
    "id": "{{id}}",
    "method": "auth.online.logout",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  }
  return send_request(api_json, args)

def user_balance_get(args={}):
  api_json = {
    "id": "{{id}}",
    "method": "user.balance.get",
    "params": {
      "sessionId": "{{sessionId}}"
    },
    "jsonrpc": "2.0"
  }
  return send_request(api_json, args)

def sn_info(args={}):
  api_json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "sn.info",
    "params": {
      "domain": "{{domain}}"
    }
  }
  api_json["params"]["domain"] = domain
  return send_request(api_json, args)

def auth_session_validate(args={}):
  api_json = {
    "id": "{{id}}",
    "method": "auth.session.validate",
    "params": {
      "sessionId": "{{sessionId}}",
      "withAmount": "0",
      "withUser": "0"
    },
    "jsonrpc": "2.0"
  }
  return send_request(api_json, args)

def thirdparty_user_balance_exchange(args):
  api_json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "thirdparty.user.balance.exchange",
    "params": {
      "sessionId": "{{sessionId}}",
      "from": -1,
      "to": -1,
      "amount": -1
    }
  }
  return send_request(api_json, args)

def user_withdraw_no_get(args):
  api_json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.withdraw.no.get",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  }
  return send_request(api_json, args);

def user_audit_about_current_get(args):
  api_json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.audit.about.current.get",
    "params": {
      "sessionId": "{{sessionId}}"
    }
  }
  return send_request(api_json, args)

def user_withdraw_apply(args):
  api_json = {
    "id": "{{id}}",
    "jsonrpc": "2.0",
    "method": "user.withdraw.apply",
    "params": {
      "sessionId": "{{sessionId}}",
      "withdrawNo": "{{withdrawNo}}",
      "payPasword": "{{payPasword}}",
      "toBankId": 1,
      "amount": "{{amount}}",
      "statsCacheKey": "{{statsCacheKey}}"
    }
  }
  return send_request(api_json, args)

def setUp():
  ret = auth_mobile_login({"sn":"ae00", "loginId":loginId, "password": password})
  assert ret["error"] == None
  assert ret["result"] != None
  assert ret["result"]["sessionId"] != None and len(ret["result"]["sessionId"]) > 0
  login_data[loginId] = ret

def teardown():
  sessionId = login_data[loginId]["result"]["sessionId"]
  auth_online_logout({"sessionId": sessionId})

def test_sn_info():
  ret = sn_info()
  assert ret["error"] == None
  assert ret["result"] != None

def test_auth_session_validate():
  sessionId = login_data[loginId]["result"]["sessionId"]
  ret = auth_session_validate({"sessionId": sessionId})
  assert ret["error"] == None
  assert ret["result"] != None

def test_user_balance_get():
  sessionId = login_data[loginId]["result"]["sessionId"]
  ret = user_balance_get({"sessionId": sessionId})
  assert ret["error"] == None
  assert ret["result"] != None
  assert ret["result"]["balance"] != None and ret["result"]["balance"] >= 0

# def test_thirdparty_user_balance_exchange():
#   amount = 100
#   sessionId = login_data[loginId]["result"]["sessionId"]
#   old_balance = user_balance_get({"sessionId": sessionId})["result"]["balance"]
#   ret = thirdparty_user_balance_exchange({
#     "sessionId": sessionId,
#     "from": 0,
#     "to": 4,
#     "amount": amount,
#     "terminal":1
#   })
#   assert ret["error"] == None
#   assert ret["result"] != None
#   new_balance = user_balance_get({"sessionId": sessionId})["result"]["balance"]
#   assert new_balance == old_balance - amount

# def test_user_withdraw_apply():
#   amount = 100
#   sessionId = login_data[loginId]["result"]["sessionId"]
#   old_balance = user_balance_get({"sessionId": sessionId})["result"]["balance"]
#   print("old_balance=%d"%old_balance)
#   withdraw_no_ret = user_withdraw_no_get({
#     "sessionId": sessionId
#   })
#   audit_about_current_ret = user_audit_about_current_get({
#     "sessionId": sessionId
#   })
#   withdraw_apply_ret = user_withdraw_apply({
#     "sessionId": sessionId,
#     "payPasword": js.get_paypwd_hash(login_data[loginId]["result"]["userId"], payPasword),
#     "withdrawNo": withdraw_no_ret["result"],
#     "amount": amount,
#     "statsCacheKey": audit_about_current_ret["result"]["statsCacheKey"]
#   })
#   if(withdraw_apply_ret["error"] and withdraw_apply_ret["error"]["code"] == "2438"):
#     print('距离你上次申请提现的时间太短，请稍后再申请')
#   else:
#     new_balance = user_balance_get({"sessionId": sessionId})["result"]["balance"]
#     print("new_balance=%d"%new_balance)
#     assert new_balance == old_balance - amount


if __name__ == '__main__':
  pass




