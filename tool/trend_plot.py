import sys
import os
from argparse import ArgumentParser
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def main(argv):
   dates = []
   durations = []
   errors = []
   checks = []
   vus_max = []
   request_rates = []

   parser = ArgumentParser()
   parser.add_argument("-f", help="filepath")
   parser.add_argument("-o", help="output folder")

   args = parser.parse_args()
   filepath = args.f
   output_dir = args.o

   if not os.path.exists(output_dir):
      os.makedirs(output_dir, 0755)

   with open(filepath) as f:
       for line in f:
          tokens = line.split()
          date_str = tokens[0]
          date_tokens = date_str.split("-")
          dates.append(date_tokens[1] + '-' + date_tokens[2])
          durations.append(float(tokens[1]))
          errors.append(int(tokens[2]))
          checks.append(float(tokens[3]))
          vus_max.append(int(tokens[4]))
          request_rates.append(float(tokens[5]))

   basename=os.path.basename(filepath)
   filename, ext = os.path.splitext(basename)

   plt.figure(figsize=(12.8, 4.8))
   axes = plt.subplot(511)
   axes.set_xticklabels([])
   axes.yaxis.label.set_color('blue')
   plt.ylabel('http_req_durations', fontsize='small')
   plt.tick_params(axis='y', colors='blue')
   plt.plot(dates, durations, color='blue')

   axes = plt.subplot(512)
   axes.set_xticklabels([])
   axes.yaxis.label.set_color('red')
   plt.ylabel('errors')
   plt.tick_params(axis='y', colors='red')
   plt.plot(dates, errors, color='red')

   axes = plt.subplot(513)
   axes.set_xticklabels([])
   axes.yaxis.label.set_color('orange')
   plt.ylabel('checks')
   plt.tick_params(axis='y', colors='orange')
   plt.plot(dates, checks, color='orange')

   axes = plt.subplot(514)
   axes.set_xticklabels([])
   axes.yaxis.label.set_color('green')
   plt.ylabel('vus_max')
   plt.tick_params(axis='y', colors='green')
   plt.plot(dates, vus_max, color='green')

   axes = plt.subplot(515)
   axes.set_xticklabels(dates, rotation=40)
   axes.yaxis.label.set_color('purple')
   plt.ylabel('request_rate')
   plt.tick_params(axis='y', colors='purple')
   plt.plot(dates, request_rates, color='purple')


   output_image = "{}/{}.png".format(output_dir, filename)
   plt.savefig(output_image)

if __name__ == "__main__":
   main(sys.argv[1:])