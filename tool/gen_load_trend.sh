#!/bin/bash

. ./env.sh

REPORT="../report/load_trend.md"
TREND_DIR="../report/trend"
DATA_DIR="${TREND_DIR}/data"
IMAGE_DIR="${TREND_DIR}/images"
NETWORK_ERROR_FILE="network.err"
NETWORK_ERROR_PATH="${DATA_DIR}/${NETWORK_ERROR_FILE}"

function write_report {
  line=$1
  space=$2
  result=''
  for i in $(seq 1 ${space});
  do
    result+=' '
  done
  result+='- '
  result+=${line}
  echo "${result}"|tee -a ${REPORT} 
}

function update_trend_data {
  script_name=$(basename -- "$1")
  script_desc=`head -n 1 $1 | awk '{print substr($0,3)}'`
  trend_image="${IMAGE_DIR}/${script_name%.*}.png"
  write_report "${script_desc} (${script_name})" 0
  echo '' >> ${REPORT}
  echo "     ![](${trend_image})" >> ${REPORT}
}

function purge_data_by_date {
    datafile=$1
    create_date=$2
    TMPFILE="/tmp/${datafile}.dat"
    grep -v ${create_date} ${DATA_DIR}/${datafile} > ${TMPFILE}
    mv ${TMPFILE} ${DATA_DIR}/${datafile}
}

function purge_network_error {
  while read line; do
      create_date=`echo ${line}|awk '{print $1}'`
      datafile=$(basename -- "${NETWORK_ERROR_PATH}")
      diff=$((`date -d "28 days ago" '+%s'`-`date -d "${create_date}" '+%s'`))
      if [ ${diff} -lt 0 ]; then
        purge_data_by_date ${datafile} ${create_date}
      fi
  done < ${NETWORK_ERROR_PATH}
}

function update_network_error {
  load_report=$1
  create_date=`echo ${load_report}|awk -F'_' '{print $(NF-1)}'`
  network_error=`cat ${load_report} |grep '### Network ###' -A 1|grep 'Not Connected!'`
  if [ ! -z "${network_error}" ];then
      if [ -f ${NETWORK_ERROR_PATH} ]; then
        purge_data_by_date ${NETWORK_ERROR_FILE} ${create_date}
      fi
      echo "${create_date}" >> "${DATA_DIR}/${NETWORK_ERROR_FILE}"
  fi
  purge_network_error
}

echo '' > ${REPORT}

echo '### Environment ###' |tee -a ${REPORT}
echo '' |tee -a ${REPORT}
echo "Host | sn" |tee -a ${REPORT}
echo "--- | ---" |tee -a ${REPORT}
echo "${HOST} | ${SN}" |tee -a ${REPORT}

echo '### Testing Agent ###' |tee -a ${REPORT}
echo " Ip | OS | Testing Date" |tee -a ${REPORT}
echo "--- | --- | ---" |tee -a ${REPORT}
echo "${AGENT} | `lsb_release -sd` | `date +"%Y/%m/%d %H:%M:%S"`" |tee -a ${REPORT}

echo '### Testing Result ###' |tee -a ${REPORT}

# update network error
for report in `ls -v ../report/load_report_*.md`; do
    update_network_error ${report}
done

cat ${NETWORK_ERROR_PATH} | while read LINE; do
    write_report "<span style="color:red">${LINE} 無法連線${HOST}</span>" 0
done

# generate trend data
bash gen_trend_data.sh

# generate trend images
idx=1
for filename in `ls -v ../load/*_*.js`; do
  script_name=$(basename -- "${filename}")
  echo ${script_name}
  datafile="${script_name%.*}.dat"
  python trend_plot.py -f ${DATA_DIR}/${datafile} -o ${IMAGE_DIR}
  idx=$(($idx+1))
done

# compose load trend report
for filename in `ls -v ../load/*_*.js`; do
  update_trend_data ${filename}
done

echo 'done!'