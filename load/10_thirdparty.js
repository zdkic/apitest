//第三方平台接口
import {
    sleep
  } from 'k6';
  
  import {
    DEBUG,
    vars,
    check_game_url
  } from '../lib/utils.js';
  import * as api from '../lib/api.js';
  
  if (!__ENV.hasOwnProperty('USERS')) {
    __ENV.USERS = 1;
  }
  
  if (!__ENV.hasOwnProperty('CONFIG')) {
    __ENV.CONFIG = 'once';
  }
  
  if (0 > ['once', 'baseline', 'load'].indexOf(__ENV.CONFIG)) {
    __ENV.CONFIG = 'once';
  }
  
  let opts = {
    setupTimeout: "60s",
    teardownTimeout: "60s",
    thresholds: {
      "errors": [ "count<1",
                  { threshold: "count<=100", abortOnFail: true }
                ]
    }
  };
  
  if (__ENV.CONFIG == 'once') {
    // no ramping configurations
  } else if (__ENV.CONFIG == 'baseline') {
    opts.stages = [{
        iterations: 3
      }
    ];
  } else if (__ENV.CONFIG == 'load') {
    opts.stages = [{
        iterations: 5
      }
    ];
  }
  
  export let options = opts;
  
  export function setup() {
    let data = {};
    for (let idx = 1; idx <= __ENV.USERS; idx++) {
      let loginId = vars["{{user_prefix}}"] + idx;
      let ret = api.auth_mobile_login({
        "loginId": loginId,
        "password": vars["{{password}}"]
      });
      if (ret.error) {
        console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
      }
      data[loginId] = ret.result;
    }
    return data;
  }
  
  export default function (data) {
    let user = (`${__VU}` - 1) % __ENV.USERS + 1;
    let loginId = vars["{{user_prefix}}"] + user;
    if (DEBUG) console.log('loginId = ' + loginId);
    do {
      let game_url_ret = api.thirdparty_ky_game_url({
        "sessionId": data[loginId].sessionId
      });
      if(!game_url_ret.error) {
        check_game_url(game_url_ret.result);
      }
      api.thirdparty_ky_order_query({
        "sessionId": data[loginId].sessionId
      });
      api.sn_thirdparty_pwd_get({
        "sessionId": data[loginId].sessionId
      });
      api.thirdparty_list({
        "sessionId": data[loginId].sessionId
      });
      sleep(Math.floor(Math.random() * 5) + 4);
    } while (false);
  }
  
  export function teardown(data) {
    for (let idx = 1; idx <= __ENV.USERS; idx++) {
      let loginId = vars["{{user_prefix}}"] + idx;
      api.auth_online_logout({
        "sessionId": data[loginId].sessionId
      });
    }
  }