//一键回收第三方平台余额异步
import {
  sleep
} from 'k6';

import {
  DEBUG,
  vars
} from '../lib/utils.js';
import * as api from '../lib/api.js';

if (!__ENV.hasOwnProperty('USERS')) {
  __ENV.USERS = 1;
}

if (!__ENV.hasOwnProperty('CONFIG')) {
  __ENV.CONFIG = 'once';
}

if (0 > ['once'].indexOf(__ENV.CONFIG)) {
  __ENV.CONFIG = 'once';
}

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS * 1;
}

export let options = opts;

export function setup() {
  let data = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
    api.user_balance_get({
      "sessionId": data[loginId].sessionId
    });
    api.thirdparty_user_balance_list({
      "sessionId": data[loginId].sessionId
    });
    api.thirdparty_user_collect_balance_syn({
      "sessionId": data[loginId].sessionId
    });
  }
  return data;
}

export default function (data) {
  let count = 0;
  top:
    for (;;) {
      for (let idx = 1; idx <= __ENV.USERS; idx++) {
        let loginId = vars["{{user_prefix}}"] + idx;
        if (DEBUG) console.log('loginId = ' + loginId);
        let processing = 0;
        let collect_balance_info_ret = api.thirdparty_user_collect_balance_info({
          "sessionId": data[loginId].sessionId
        });
        if (!collect_balance_info_ret.error) {
          for (const [k, v] of Object.entries(collect_balance_info_ret.result)) {
            if (v == 0) {
              if (DEBUG) console.log('gameId = ' + k + ' , result = ' + v + ' (1 成功 0 执行中 -1 失败)');
              processing++;
            }
          }
        }
        if (0 == processing)
          count++;
        if (count >= __ENV.USERS)
          break top;
      }
      sleep(5);
    }
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    api.user_balance_get({
      "sessionId": data[loginId].sessionId
    });
    api.thirdparty_user_balance_list({
      "sessionId": data[loginId].sessionId
    });
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}