//我要提款
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  get_paypwd_hash,
  errorCount
} from '../lib/utils.js';
import * as api from '../lib/api.js';

if (!__ENV.hasOwnProperty('USERS')) {
  __ENV.USERS = 1;
}

if (!__ENV.hasOwnProperty('CONFIG')) {
  __ENV.CONFIG = 'once';
}

if (0 > ['once'].indexOf(__ENV.CONFIG)) {
  __ENV.CONFIG = 'once';
}

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS * 1;
}

export let options = opts;

const amount = Math.floor(Math.random() * 50) + 100;

export function setup() {
  let data = {};
  data.balances = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
  }
  return data;
}

export default function (data) {
  let user = (`${__VU}` - 1) % __ENV.USERS + 1;
  let loginId = vars["{{user_prefix}}"] + user;
  if (DEBUG) console.log('loginId = ' + loginId);
  do {
    api.user_withdraw_account_list({
      "sessionId": data[loginId].sessionId
    });
    api.user_withdraw_list({
      "sessionId": data[loginId].sessionId
    });
    api.layer_deposit_to_account_list({
      "sessionId": data[loginId].sessionId
    });
    api.user_center_deposit_get({
      "sessionId": data[loginId].sessionId
    });
    api.user_withdraw_option_get({
      "sessionId": data[loginId].sessionId
    });
    api.user_withdraw_account_default_get({
      "sessionId": data[loginId].sessionId
    });
    api.user_audit_about_current_cancel({
      "sessionId": data[loginId].sessionId
    });
    let withdraw_no_ret = api.user_withdraw_no_get({
      "sessionId": data[loginId].sessionId
    });
    if (withdraw_no_ret.error)
      break;
    api.user_audit_about_current_apply({
      "sessionId": data[loginId].sessionId,
      "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
      "withdrawNo": withdraw_no_ret.result
    });
    api.user_audit_about_current_trace({
      "sessionId": data[loginId].sessionId
    });
    let audit_about_current_ret = api.user_audit_about_current_get({
      "sessionId": data[loginId].sessionId
    });
    if (audit_about_current_ret.error)
      break;
    let balance_ret = api.user_balance_get({
      "sessionId": data[loginId].sessionId
    });
    let old_balance = balance_ret.result.balance;
    api.user_withdraw_apply({
      "sessionId": data[loginId].sessionId,
      "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
      "withdrawNo": withdraw_no_ret.result,
      "amount": amount,
      "statsCacheKey": audit_about_current_ret.result.statsCacheKey
    });
    balance_ret = api.user_balance_get({
      "sessionId": data[loginId].sessionId
    });
    let diff = Math.floor(old_balance - balance_ret.result.balance);
    check(diff, {
      "check balance difference": (diff) => diff === amount
    }) || errorCount.add(1);
    api.user_center_withdraw_get({
      "sessionId": data[loginId].sessionId
    });
    sleep(Math.floor(Math.random() * 5) + 4);
  } while (false);
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}