//额度转换
import {
  check,
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  errorCount
} from '../lib/utils.js';
import * as api from '../lib/api.js';

if (!__ENV.hasOwnProperty('USERS')) {
  __ENV.USERS = 1;
}

if (!__ENV.hasOwnProperty('CONFIG')) {
  __ENV.CONFIG = 'once';
}

if (0 > ['once'].indexOf(__ENV.CONFIG)) {
  __ENV.CONFIG = 'once';
}

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS;
}

export let options = opts;

const amount = Math.floor(Math.random() * 50) + 1;

export function setup() {
  let data = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
    api.thirdparty_user_balance_list({
      "sessionId": data[loginId].sessionId
    });
  }
  return data;
}

export default function (data) {
  let user = (`${__VU}` - 1) % __ENV.USERS + 1;
  let loginId = vars["{{user_prefix}}"] + user;
  if (DEBUG) console.log('loginId = ' + loginId);
  let balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let original_balance = balance_ret.result.balance;
  api.thirdparty_user_balance_exchange({
    "sessionId": data[loginId].sessionId,
    "from": 0,
    "to": 5,
    "amount": amount
  });
  balance_ret = api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  let diff = original_balance - balance_ret.result.balance;
  check(diff, {
    "check balance difference": (diff) => diff === amount
  }) || errorCount.add(1);
  sleep(Math.floor(Math.random() * 5) + 4);
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    if (DEBUG) console.log('loginId = ' + loginId);
    api.thirdparty_user_balance_list({
      "sessionId": data[loginId].sessionId
    });
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}