//彩票注单
import {
  sleep
} from 'k6';

import {
  DEBUG,
  vars
} from '../lib/utils.js';
import * as api from '../lib/api.js';

if (!__ENV.hasOwnProperty('USERS')) {
  __ENV.USERS = 1;
}

if (!__ENV.hasOwnProperty('CONFIG')) {
  __ENV.CONFIG = 'once';
}

if (0 > ['once', 'baseline', 'load'].indexOf(__ENV.CONFIG)) {
  __ENV.CONFIG = 'once';
}

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  // no ramping configurations
} else if (__ENV.CONFIG == 'baseline') {
  if (!__ENV.hasOwnProperty('DURATION')) {
    __ENV.DURATION = 180;
  }
  if (!__ENV.hasOwnProperty('MAX_VU')) {
    __ENV.MAX_VU = 10;
  }
  opts.stages = [
    {
      duration: Math.round(__ENV.DURATION / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION * 8 / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION / 10) + "s",
      target: 0
    },
  ];
} else if (__ENV.CONFIG == 'load') {
  if (!__ENV.hasOwnProperty('DURATION')) {
    __ENV.DURATION = 180;
  }
  if (!__ENV.hasOwnProperty('MAX_VU')) {
    __ENV.MAX_VU = 50;
  }
  opts.stages = [
    {
      duration: Math.round(__ENV.DURATION / 20) + "s",
      target: __ENV.MAX_VU / 2
    },
    {
      duration: Math.round(__ENV.DURATION * 2 / 10) + "s",
      target: __ENV.MAX_VU * 8 / 10
    },
    {
      duration: Math.round(__ENV.DURATION * 2 / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION * 5 / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION / 20) + "s",
      target: 0
    },
  ];
}

export let options = opts;

export function setup() {
  let data = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
  }
  return data;
}

export default function (data) {
  let user = (`${__VU}` - 1) % __ENV.USERS + 1;
  let loginId = vars["{{user_prefix}}"] + user;
  if (DEBUG) console.log('loginId = ' + loginId);
  do {
    api.sn_lottery_order_query({
      "sessionId": data[loginId].sessionId
    });
    api.lottery_issue_current_all();
    api.lottery_sn_list();
    //api.lottery_data_current();
    sleep(Math.floor(Math.random() * 5) + 4);
  } while (false);
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}