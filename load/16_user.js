//用户服务接口
import {
  sleep
} from 'k6';

import {
  DEBUG,
  vars,
  get_paypwd_hash,
  get_salted_password,
  get_hash_password
} from '../lib/utils.js';
import * as api from '../lib/api.js';

const LOGINID_PREFIX = "bgqauser_";

if (!__ENV.hasOwnProperty('USERS')) {
  __ENV.USERS = 1;
}

if (!__ENV.hasOwnProperty('CONFIG')) {
  __ENV.CONFIG = 'once';
}

if (0 > ['once'].indexOf(__ENV.CONFIG)) {
  __ENV.CONFIG = 'once';
}

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  opts.vus = __ENV.USERS;
  opts.iterations = __ENV.USERS * 1;
}

export let options = opts;

export function setup() {
  let data = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = LOGINID_PREFIX + idx;
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
  }
  return data;
}

export default function (data) {
  let user = (`${__VU}` - 1) % __ENV.USERS + 1;
  let loginId = LOGINID_PREFIX + user;
  if (DEBUG) console.log('loginId = ' + loginId);
  api.user_reg_recommend_check();
  api.user_name_check_duplicate();
  api.user_mobile_check_duplicate();
  api.report_user_validamount({
    "sessionId": data[loginId].sessionId
  });
  //update profile
  api.user_profile_get({
    "sessionId": data[loginId].sessionId
  });
  let now_date = new Date().toISOString().slice(0, 10);
  api.user_profile_update({
    "sessionId": data[loginId].sessionId
  });
  api.user_profile_get({
    "sessionId": data[loginId].sessionId
  });
  sleep(2);
  //update password
  let salted_password_ret = get_salted_password("123456");
  let new_password = get_hash_password("654321");
  api.user_password_update({
    "sessionId": data[loginId].sessionId,
    "saltedPassword": salted_password_ret["saltedPassword"],
    "salt": salted_password_ret["salt"],
    "newPassword": new_password
  });
  salted_password_ret = get_salted_password("654321");
  new_password = get_hash_password("123456");
  api.user_password_update({
    "sessionId": data[loginId].sessionId,
    "saltedPassword": salted_password_ret["saltedPassword"],
    "salt": salted_password_ret["salt"],
    "newPassword": new_password
  });
  sleep(2);
  // update pay password
  let userId = data[loginId].userId;
  api.user_paypwd_update({
    "sessionId": data[loginId].sessionId,
    "oldPayPwd": get_paypwd_hash(userId, "1234"),
    "newPayPwd": get_paypwd_hash(userId, "4321")
  });
  api.user_paypwd_update({
    "sessionId": data[loginId].sessionId,
    "oldPayPwd": get_paypwd_hash(userId, "4321"),
    "newPayPwd": get_paypwd_hash(userId, "1234")
  });
  sleep(Math.floor(Math.random() * 5) + 4);
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = LOGINID_PREFIX + idx;
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}