//混合情境
import {
  check,
  sleep
} from 'k6';
import {
  Counter
} from 'k6/metrics';

import {
  DEBUG,
  vars,
  data_generator,
  get_hash_password,
  get_paypwd_hash,
  get_salted_password
} from '../lib/utils.js';
import * as api from '../lib/api.js';

if (!__ENV.hasOwnProperty('USERS')) {
  __ENV.USERS = 1;
}

if (!__ENV.hasOwnProperty('CONFIG')) {
  __ENV.CONFIG = 'once';
}

if (0 > ['once', 'baseline', 'load'].indexOf(__ENV.CONFIG)) {
  __ENV.CONFIG = 'once';
}

let opts = {
  setupTimeout: "60s",
  teardownTimeout: "60s",
  thresholds: {
    "errors": [ "count<1",
                { threshold: "count<=100", abortOnFail: true }
              ]
  }
};

if (__ENV.CONFIG == 'once') {
  // no ramping configurations
} else if (__ENV.CONFIG == 'baseline') {
  if (!__ENV.hasOwnProperty('DURATION')) {
    __ENV.DURATION = 180;
  }
  if (!__ENV.hasOwnProperty('MAX_VU')) {
    __ENV.MAX_VU = 10;
  }
  opts.stages = [
    {
      duration: Math.round(__ENV.DURATION / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION * 8 / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION / 10) + "s",
      target: 0
    },
  ];
} else if (__ENV.CONFIG == 'load') {
  if (!__ENV.hasOwnProperty('DURATION')) {
    __ENV.DURATION = 180;
  }
  if (!__ENV.hasOwnProperty('MAX_VU')) {
    __ENV.MAX_VU = 50;
  }
  opts.stages = [
    {
      duration: Math.round(__ENV.DURATION / 20) + "s",
      target: __ENV.MAX_VU / 2
    },
    {
      duration: Math.round(__ENV.DURATION * 2 / 10) + "s",
      target: __ENV.MAX_VU * 8 / 10
    },
    {
      duration: Math.round(__ENV.DURATION * 2 / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION * 5 / 10) + "s",
      target: __ENV.MAX_VU
    },
    {
      duration: Math.round(__ENV.DURATION / 20) + "s",
      target: 0
    },
  ];
}

export let options = opts;

const amount = data_generator["amount"](100, 1000);

function amount_transfer(data, loginId) {
  api.thirdparty_user_balance_exchange({
    "sessionId": data[loginId].sessionId,
    "from": 0,
    "to": 5,
    "amount": amount
  });
  api.user_balance_get({
    "sessionId": data[loginId].sessionId
  });
  api.thirdparty_user_balance_list({
    "sessionId": data[loginId].sessionId
  });
}

function collect_balance_sync(data, loginId) {
  api.thirdparty_user_collect_balance_syn({
    "sessionId": data[loginId].sessionId
  });
  api.thirdparty_user_collect_balance_info({
    "sessionId": data[loginId].sessionId
  });
}

function transaction_record(data, loginId) {
  api.user_cashflow_query({
    "sessionId": data[loginId].sessionId
  });
}

function order_query(data, loginId) {
  api.sn_order_switch_list({
    "sessionId": data[loginId].sessionId
  });
  api.user_order_query({
    "sessionId": data[loginId].sessionId
  });
}

function charge_withdraw_record(data, loginId) {
  api.user_charge_list({
    "sessionId": data[loginId].sessionId
  });
  api.user_withdraw_list({
    "sessionId": data[loginId].sessionId
  });
}

function report_query(data, loginId) {
  api.report_day_order_groupby_query({
    "sessionId": data[loginId].sessionId
  });
}

function charge(data, loginId) {
  api.user_charge_bank_list({
    "sessionId": data[loginId].sessionId
  });
  api.sn_payment_biglist({
    "sessionId": data[loginId].sessionId
  });
  api.sn_payment_batch_limit_query({
    "sessionId": data[loginId].sessionId
  });
  api.layer_deposit_to_account_noqrcode_list({
    "sessionId": data[loginId].sessionId
  });
  let accountIds = [10002, 17006, 12004, 12212, 10047, 9001, 15023, 12208, 10046, 10002];
  for (let idx = 0; idx < accountIds.length; idx++) {
    api.layer_deposit_to_account_qrcode_get({
      "sessionId": data[loginId].sessionId,
      "toAccountId": accountIds[idx]
    });
  }
  api.user_charge_option_get({
    "sessionId": data[loginId].sessionId
  });
  let charge_no_ret = api.user_charge_no_get({
    "sessionId": data[loginId].sessionId
  });
  api.user_charge_transfer_apply({
    "sessionId": data[loginId].sessionId,
    "chargeNo": charge_no_ret.result
  });
}

function withdraw(data, loginId) {
  api.layer_deposit_to_account_list({
    "sessionId": data[loginId].sessionId
  });
  api.user_withdraw_option_get({
    "sessionId": data[loginId].sessionId
  });
  api.user_withdraw_account_default_get({
    "sessionId": data[loginId].sessionId
  });
  api.user_audit_about_current_cancel({
    "sessionId": data[loginId].sessionId
  });
  let withdraw_no_ret = api.user_withdraw_no_get({
    "sessionId": data[loginId].sessionId
  });
  api.user_audit_about_current_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result
  });
  api.user_audit_about_current_trace({
    "sessionId": data[loginId].sessionId
  });
  let audit_about_current_ret = api.user_audit_about_current_get({
    "sessionId": data[loginId].sessionId
  });
  api.user_withdraw_apply({
    "sessionId": data[loginId].sessionId,
    "payPasword": get_paypwd_hash(data[loginId].userId, vars["{{paypwd}}"]),
    "withdrawNo": withdraw_no_ret.result,
    "amount": amount,
    "statsCacheKey": audit_about_current_ret.result.statsCacheKey
  });
}

function auth(data, loginId) {
  api.auth_session_validate({
    "sessionId": data[loginId].sessionId,
    "withAmount": "1",
    "withUser": "1"
  });
}

function user(data, loginId) {
  let start = vars["{{user_prefix}}"].length;
  let end = loginId.length;
  let idx = loginId.slice(start, end);
  let ret = api.auth_mobile_login({
    "loginId": "bgqauser_" + idx,
    "password": vars["{{password}}"]
  });
  if (ret.error) {
    console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    return;
  }
  //update profile
  api.user_profile_get({
    "sessionId": ret.result.sessionId
  });
  api.user_profile_update({
    "sessionId": ret.result.sessionId
  });
  api.user_profile_get({
    "sessionId": ret.result.sessionId
  });
  sleep(2);
  //update password
  let salted_password_ret = get_salted_password("123456");
  let new_password = get_hash_password("654321");
  api.user_password_update({
    "sessionId": ret.result.sessionId,
    "saltedPassword": salted_password_ret["saltedPassword"],
    "salt": salted_password_ret["salt"],
    "newPassword": new_password
  });
  sleep(10);
  salted_password_ret = get_salted_password("654321");
  new_password = get_hash_password("123456");
  api.user_password_update({
    "sessionId": ret.result.sessionId,
    "saltedPassword": salted_password_ret["saltedPassword"],
    "salt": salted_password_ret["salt"],
    "newPassword": new_password
  });
  sleep(2);
  // update pay password
  let userId = ret.result.userId;
  api.user_paypwd_update({
    "sessionId": ret.result.sessionId,
    "oldPayPwd": get_paypwd_hash(userId, "1234"),
    "newPayPwd": get_paypwd_hash(userId, "4321")
  });
  sleep(10);
  api.user_paypwd_update({
    "sessionId": ret.result.sessionId,
    "oldPayPwd": get_paypwd_hash(userId, "4321"),
    "newPayPwd": get_paypwd_hash(userId, "1234")
  });
  sleep(2);
  api.auth_online_logout({
    "sessionId": ret.result.sessionId
  });
}

function payment(data, loginId) {
  let charge_no_ret = api.user_charge_no_get({
    "sessionId": data[loginId].sessionId
  });
  api.sn_payment_biglist({
    "sessionId": data[loginId].sessionId,
    "changeNo": charge_no_ret.result
  });
  api.sn_payment_batch_limit_query({
    "sessionId": data[loginId].sessionId
  });
  api.sn_payment_charge_bank_list({
    "sessionId": data[loginId].sessionId
  });
}

function web(data, loginId) {
  api.sn_website_proitem_query({
    "sessionId": data[loginId].sessionId
  });
  api.sn_website_slice_image_url({
    "sessionId": data[loginId].sessionId
  });
  api.sn_website_logo_image_url({
    "sessionId": data[loginId].sessionId
  });
  api.sn_website_doc_query({
    "sessionId": data[loginId].sessionId
  });
}

function lottery(data, loginId) {
  api.sn_lottery_order_query({
    "sessionId": data[loginId].sessionId
  });
}

function notice(data, loginId) {
  api.sn_notice_new_layer_query();
  api.notice_system_query();
  api.user_notice_list({
    "sessionId": data[loginId].sessionId
  });
  api.sn_notice_new_query({
    "sessionId": data[loginId].sessionId
  });
  api.sn_notice_history_query({
    "sessionId": data[loginId].sessionId
  });
  api.user_notice_unread_touch({
    "sessionId": data[loginId].sessionId
  });
}

function thirdparty(data, loginId) {
  let game_url_ret = api.thirdparty_ky_game_url({
    "sessionId": data[loginId].sessionId
  });
  api.thirdparty_ky_order_query({
    "sessionId": data[loginId].sessionId
  });
  api.sn_thirdparty_pwd_get({
    "sessionId": data[loginId].sessionId
  });
}

function host(data, loginId) {
  api.host_info();
  api.module_maintain_info();
  api.sn_info();
  api.sn_settings_get();
  api.sn_switch_item_query({
    "userId": data[loginId].userId
  });
  api.sn_user_reg_setting();
  api.sn_agent_reg_setting();
}

let scernario_combination= {
  "amount_transfer":[amount_transfer, 2, new Counter("amount_transfer")],
  "collect_balance_sync":[collect_balance_sync, 2, new Counter("collect_balance_sync")],
  "transaction_record":[transaction_record, 30, new Counter("transaction_record")],
  "order_query":[order_query, 30, new Counter("order_query")],
  "charge_withdraw_record":[charge_withdraw_record, 30, new Counter("charge_withdraw_record")],
  "report_query":[report_query, 30, new Counter("report_query")],
  "charge":[charge, 1, new Counter("charge")],
  "withdraw":[withdraw, 1, new Counter("withdraw")],
  "auth":[auth, 30, new Counter("auth")],
  "payment":[payment, 30, new Counter("payment")],
  "web":[web, 30, new Counter("web")],
  "lottery":[lottery, 5, new Counter("lottery")],
  "notice":[notice, 30, new Counter("notice")],
  "thirdparty":[thirdparty, 5, new Counter("thirdparty")],
  "host": [host, 10, new Counter("host")],
  "user":[user, 0, new Counter("user")]
};

function execute_scenario(name, data, loginId) {
  let scernario = scernario_combination[name];
  let scernario_func = scernario[0];
  let scernario_counter = scernario[2];
  scernario_func(data, loginId);
  scernario_counter.add(1);
}

let scenarios = [];

for (const [k, v] of Object.entries(scernario_combination)) {
  for(let idx = 0; idx < v[1]; idx++) {
    scenarios.push(k);
  }
}

export function setup() {
  let data = {};
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    let ret = api.auth_mobile_login({
      "loginId": loginId,
      "password": vars["{{password}}"]
    });
    if (ret.error) {
      console.error('[auth_mobile_login]' + 'error code = ' + ret.error.code);
    }
    data[loginId] = ret.result;
  }
  return data;
}

export default function (data) {
  let user = (`${__VU}` - 1) % __ENV.USERS + 1;
  let loginId = vars["{{user_prefix}}"] + user;
  if (DEBUG) console.log('loginId = ' + loginId);
  let idx = Math.floor(Math.random() * scenarios.length);
  if (DEBUG) console.log(scenarios[idx]);
  execute_scenario(scenarios[idx], data, loginId);
  sleep(Math.floor(Math.random() * 5) + 4);
}

export function teardown(data) {
  for (let idx = 1; idx <= __ENV.USERS; idx++) {
    let loginId = vars["{{user_prefix}}"] + idx;
    execute_scenario("user", data, loginId);
    api.auth_online_logout({
      "sessionId": data[loginId].sessionId
    });
  }
}